<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Http\Mock\Client;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseKernelTestCase extends KernelTestCase
{
    public function setUp()
    {
        self::bootKernel();
        parent::setUp();
    }

    /**
     * @return MockObject|ResponseInterface
     *
     * @throws \ReflectionException
     */
    protected function createMockResponse(string $body, int $statusCode = 200)
    {
        /** @var ResponseInterface|MockObject $response */
        /** @var MockObject $stream */
        $stream = $this->createMock(StreamInterface::class);
        $stream->method('getContents')->willReturn($body);

        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn($stream);
        $response->method('getStatusCode')->willReturn($statusCode);
        $response->method('getHeaders')->willReturn([]);

        return $response;
    }

    protected function getHttplugMockClient(): Client
    {
        return self::$container->get('httplug.client.mock');
    }
}
