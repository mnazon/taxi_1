<?php

namespace App\Api\EventListeners;

use App\Api\DataFixtures\PlaceFixtures;
use App\Api\Entity\Trip;
use App\Api\Entity\UserTopPlace;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\EventSubscriber\UserTopPlacesListener;
use App\DataFixtures\AppFixtures;
use App\Tests\Functional\BaseWebTestCase;

class UserTopPlacesListenerTest extends BaseWebTestCase
{
    /**
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testAddTopPlacesSuccess()
    {
        $fixture = new PlaceFixtures();
        $this->loadFixtures([$fixture]);
        /** @var Trip $trip */
        $trip = $fixture->getReference(AppFixtures::TRIP_REF);
        $userTopPlacesListener = self::$container->get(UserTopPlacesListener::class);
        $event = new TripExternalStatusChanged(
            new TripStatus(TripStatus::RUNNING),
            new TripStatus(TripStatus::EXECUTED),
            $trip
        );
        $savedUserTopPlaces = $userTopPlacesListener->onTripExternalStatusChanged($event);

        //just one UserTopPlaces returned
        $this->assertInstanceOf(UserTopPlace::class, $savedUserTopPlaces[0]);
    }
}
