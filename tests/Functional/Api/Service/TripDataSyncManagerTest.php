<?php

namespace App\Api\Service;

use App\Api\Entity\TripStatusTracking;
use App\DataFixtures\AppFixtures;
use App\Tests\Functional\BaseWebTestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;

class TripDataSyncManagerTest extends BaseWebTestCase
{
    public function testSyncDataSuccess()
    {
        $fixture = new AppFixtures();
        $this->loadFixtures([$fixture]);
        //$mockResponse = '{"id":0,"links":[],"dispatching_order_uid":"0e9214ab5f67447cbbe128a01a25d5ec","order_cost":"106","add_cost":"0","currency":" грн.","order_car_info":"В 13:15 : AA1196TB Зеленый опель комбо ‎‎((093)327-80-61)","driver_phone":"050-123-45-67","drivercar_position":{"lat":50.512383,"lng":30.536734,"time_positioned_utc":"2015-06-02T11:12:23.301","altitude":null,"accuracy":null,"bearing":0,"speed":60,"status":"gpsOk"},"required_time":"2018-11-24T16:50:46.375","close_reason":0,"cancel_reason_comment":null,"order_is_archive":true,"route_address_from":{"name":"тест1 вход м.Шевченко.","number":null},"route_address_to":{"name":"тест2 вход иподром.","number":null},"driver_execution_status":3,"create_date_time":"2018-11-24T16:40:47.48","find_car_timeout":570,"find_car_delay":0,"execution_status":"CarFound"}';
        $mockResponse = '{"id":0,"links":[],"dispatching_order_uid":"0e9214ab5f67447cbbe128a01a25d5ec","order_cost":"106","add_cost":"0","currency":" грн.","order_car_info":"В 13:15 : AA1196TB Зеленый опель комбо ‎‎((093)327-80-61)","driver_phone":"050-123-45-67","drivercar_position":{"lat":50.512383,"lng":30.536734,"time_positioned_utc":"2015-06-02T11:12:23.301","altitude":null,"accuracy":null,"bearing":0,"speed":60,"status":"gpsOk"},"required_time":"2018-11-24T16:50:46.375","close_reason":0,"cancel_reason_comment":null,"order_is_archive":true,"route_address_from":{"name":"тест1 вход м.Шевченко.","number":null},"route_address_to":{"name":"тест2 вход иподром.","number":null},"driver_execution_status":3,"create_date_time":"2018-11-24T16:40:47.48","find_car_timeout":570,"find_car_delay":0,"execution_status":"Canceled"}';
        //$mockResponse = '{"id":0,"links":[],"dispatching_order_uid":"0e9214ab5f67447cbbe128a01a25d5ec","order_cost":"106","add_cost":"0","currency":" грн.","order_car_info":"AA1100AA, Черный, Acura, ","driver_phone":"050-123-45-67","drivercar_position":{"lat":50.512383,"lng":30.536734,"time_positioned_utc":"2015-06-02T11:12:23.301","altitude":null,"accuracy":null,"bearing":0,"speed":60,"status":"gpsOk"},"required_time":"2018-11-24T16:50:46.375","close_reason":0,"cancel_reason_comment":null,"order_is_archive":true,"route_address_from":{"name":"тест1 вход м.Шевченко.","number":null},"route_address_to":{"name":"тест2 вход иподром.","number":null},"driver_execution_status":3,"create_date_time":"2018-11-24T16:40:47.48","find_car_timeout":570,"find_car_delay":0,"execution_status":"CarFound"}';
        $response = $this->createMockResponse($mockResponse);
        /* @var ResponseInterface|MockObject $response */
        $this->getHttplugMockClient()->setDefaultResponse($response);

        /** @var TripDataSyncManager $tripStatusTrackingManager */
        $tripStatusTrackingManager = self::$container->get(TripDataSyncManager::class);
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::$container->get(EntityManagerInterface::class);

        $repo = $entityManager->getRepository(TripStatusTracking::class);
        $tripStatusTracking = $repo->findByTripId($fixture->getEvosTrip()->getId());
        $result = $tripStatusTrackingManager->handle($tripStatusTracking);
        $this->assertNull($result);
    }
}
