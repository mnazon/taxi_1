<?php

namespace App\Api\Controller;

use App\Api\DataFixtures\NotificationFixtures;
use App\Api\DataFixtures\PhoneConfirmationFixture;
use App\Api\DataFixtures\PlaceFixtures;
use App\Api\DataFixtures\UserFixtures;
use App\Api\DataFixtures\UserOptionFixtures;
use App\DataFixtures\AppFixtures;
use App\Tests\Functional\Api\Controller\GeoControllerTest;
use App\Tests\Functional\Api\Controller\TripControllerTest;
use App\Tests\Functional\BaseWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends BaseWebTestCase
{
    public const PLACE_STRUCTURE = [
        'name', //todo - remove
        'locality', //todo - remove

        'mainText',
        'secondaryText',
        'coordinates',
        'coordinates.lat',
        'coordinates.lng',
    ];

    public const USER_STRUCTURE = [
        'id',
        'fullName',
        'phone',
        'email',
        'isPolicyAccepted',
    ];

    public const USER_OPTION_STRUCTURE = [
        'userOption',
        'value',
    ];

    /**
     * @var UserFixtures
     */
    private $userFixtures;

    public function setUp()
    {
        parent::setUp();
        $this->userFixtures = new UserFixtures();
        $this->loadFixtures([$this->userFixtures]);
    }

    public function testUpdateProfileSuccess()
    {
        $user = $this->userFixtures->getUser();
        $data = [
            'email' => 'somemail@gmail.com',
            'fullName' => 'someUserName',
        ];
        $url = '/api/v1/users/'.$user->getId();
        $client = $this->getAuthClient($user);
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_STRUCTURE);
    }

    public function testCannotUpdateFullNameToEmptyString()
    {
        $user = $this->userFixtures->getUser();
        $data = [
            'email' => 'somemail@gmail.com',
            'fullName' => '',
        ];
        $url = '/api/v1/users/'.$user->getId();
        $client = $this->getAuthClient($user);
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_BAD_REQUEST, $response);
    }

    public function testUpdateProfileFailedBadRequest()
    {
        $this->loadFixtures([new AppFixtures()]);
        $user = $this->getUserFromFixture();
        $data = [
            'email' => 'som',
            'fullName' => 'u',
        ];
        $url = '/api/v1/users/'.$user->getId();
        $client = $this->getAuthClient($user);
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_BAD_REQUEST, $response);
        $this->assertRightErrorStructure($response, 2);
    }

    public function testTopPlacesSuccessResponse()
    {
        $this->loadFixtures([new PlaceFixtures()]);

        $user = $this->getUserFromFixture();
        $url = sprintf('/api/v1/users/%s/top-places/2', $user->getId());
        $client = $this->getAuthClient($user);
        $client->request('GET', $url);

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(GeoControllerTest::AUTOCOMPLETE_DATA_STRUCTURE, 2)
        );
    }

    public function testChangePhoneSuccess()
    {
        $phone = '+380958747486';
        $phoneConfirmation = new PhoneConfirmationFixture(0, $phone);
        $this->loadFixtures([$phoneConfirmation], true);
        $url = '/api/v1/users/phone/change';
        $client = $this->getAuthClient($this->userFixtures->getUser());
        $data = [
            'phone' => $phone,
            'confirmationCode' => $phoneConfirmation::CONFIRMATION_CODE,
        ];
        $client->request('POST', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_STRUCTURE);

        $this->getResponseAsserter()
            ->assertResponsePropertyEquals($response, 'phone', $phone);
    }

    public function testWebSocketSign()
    {
        $user = $this->userFixtures->getUser();
        $url = sprintf('/api/v1/users/%s/ws', $user->getId());
        $client = $this->getAuthClient($this->userFixtures->getUser());
        $client->request('GET', $url);

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $structure = [
            'userId',
            'timestamp',
            'websocketToken',
        ];
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $structure);
    }

    public function testGetLastUserNotificationSuccessResponse()
    {
        $fixture = new NotificationFixtures();
        $this->loadFixtures([$fixture, new PlaceFixtures()]);

        $user = $fixture->getUser();
        $url = sprintf('/api/v1/users/%s/last-notification', $user->getId());
        $client = $this->getAuthClient($this->userFixtures->getUser());
        $client->request('POST', $url);

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $structure = [
            'id',
            'trip',
        ];
        foreach (TripControllerTest::getTripStructureFull() as $item) {
            $structure[] = 'trip.'.$item;
        }
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $structure);
    }

    public function testCancelAllNotificationsSuccessResponse()
    {
        $fixture = new NotificationFixtures();
        $this->loadFixtures([$fixture]);

        $user = $fixture->getUser();
        $url = sprintf('/api/v1/users/%s/notifications/cancel', $user->getId());
        $client = $this->getAuthClient($this->userFixtures->getUser());
        $client->request('PUT', $url);

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);
    }

    public function testUserOptionsSuccessResponse()
    {
        $this->loadFixtures([new UserOptionFixtures()]);

        $user = $this->getUserFromFixture();
        $url = sprintf('/api/v1/users/%s/options', $user->getId());
        $client = $this->getAuthClient($user);
        $client->request('GET', $url);
        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(self::USER_OPTION_STRUCTURE, 1)
        );
    }

    public function testUpdateUserOptionsSuccessResponse()
    {
        $this->loadFixtures([new UserOptionFixtures()]);

        $user = $this->getUserFromFixture();
        $url = sprintf('/api/v1/users/%s/option', $user->getId());
        $client = $this->getAuthClient($this->userFixtures->getUser());
        $data = [
            'userOption' => 'APP_RATE_VIEW',
            'value' => false,
        ];
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);
    }
}
