<?php

namespace App\Api\Controller;

use App\Api\DataFixtures\PhoneConfirmationFixture;
use App\Api\DataFixtures\UserFixtures;
use App\Api\DataFixtures\UserTokenFixtures;
use App\Tests\Functional\BaseWebTestCase;

class RegistrationControllerTest extends BaseWebTestCase
{
    public const USER_TOKEN_STRUCTURE = [
        'accessToken',
        'user',
        'user.id',
        'user.phone',
        'user.email',
        'user.isPolicyAccepted',
        'user.fullName',
    ];

    public function testRegistrationSuccessWhenNoUserExist()
    {
        $this->loadFixtures([new PhoneConfirmationFixture()]);

        $data = [
            'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
            'confirmationCode' => PhoneConfirmationFixture::CONFIRMATION_CODE,
            'platform' => 'ios',
        ];
        $headers = [
            'CONTENT_TYPE' => 'application/json',
        ];
        $this->getNonAuthClient()->request(
            'POST', '/api/v1/registerOrLogin', [], [], $headers, \GuzzleHttp\json_encode($data)
        );

        $response = $this->getNonAuthClient()->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(201, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_TOKEN_STRUCTURE);
    }

    public function testRegistrationSuccessWhenUserExist()
    {
        $fixtures = [new PhoneConfirmationFixture()];
        $fixtures[] = new UserTokenFixtures();
        $this->loadFixtures($fixtures);

        $data = [
            'phone' => UserFixtures::USER_PHONE,
            'confirmationCode' => PhoneConfirmationFixture::CONFIRMATION_CODE,
            'platform' => 'android',
        ];
        $headers = [
            'CONTENT_TYPE' => 'application/json',
        ];
        $this->getNonAuthClient()->request(
            'POST', '/api/v1/registerOrLogin', [], [], $headers, \GuzzleHttp\json_encode($data)
        );

        $response = $this->getNonAuthClient()->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_TOKEN_STRUCTURE);
    }

    public function testRegistrationFailWrongConfirmationCode()
    {
        $data = [
            [true, 0, 'wrong_phone_confirmation_code'],
            [true, 4, 'wrong_phone_confirmation_code'],
            [false, 5, 'phone_banned'],
        ];

        foreach ($data as $row) {
            $needLoadData = $row[0];
            $numberOfAttemptsMade = $row[1];
            $expectedErrorCode = $row[2];
            if ($needLoadData) {
                $this->loadFixtures([new PhoneConfirmationFixture($numberOfAttemptsMade)]);
            }

            $data = [
                'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
                'confirmationCode' => '2222',
                'platform' => 'android',
            ];
            $headers = [
                'CONTENT_TYPE' => 'application/json',
            ];
            $this->getNonAuthClient()->request(
                'POST', '/api/v1/registerOrLogin', [], [], $headers, \GuzzleHttp\json_encode($data)
            );

            $response = $this->getNonAuthClient()->getResponse();

            $asserter = $this->getResponseAsserter();
            $asserter->assertStatusEquals(400, $response);

            $asserter->assertResponsePropertiesExist(
                $response,
                self::EXPECTED_API_PROBLEM_ERROR_STRUCTURE
            );

            $asserter->assertResponsePropertyEquals(
                $response,
                'violations[0].code',
                $expectedErrorCode
            );
        }
    }
}
