<?php

namespace App\Api\Controller;

use App\Api\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends BaseWebTestCase
{
    public function setUp()
    {
        $this->markTestSkipped('For now we have different login scheme via registration');
        parent::setUp();
        $this->loadFixtures([new UserFixtures()]);
    }

    public function testLoginSuccess()
    {
        $data = [
            'phone' => UserFixtures::USER_PHONE,
            'password' => UserFixtures::USER_PASS,
        ];
        $this->sendJsonRequest($data);

        $response = $this->getNonAuthClient()->getResponse();

        $properties = ['id', 'phone', 'username'];
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $properties);

        $expectedCookies = ['MOCKSESSID', 'REMEMBERME'];
        $this->getResponseAsserter()
            ->assertCookieExists($response, $expectedCookies);
    }

    public function testAuthFailedBadCredentialsProvided()
    {
        $data = [
            'phone' => UserFixtures::USER_PHONE,
            'password' => UserFixtures::USER_PASS.'WrongPass',
        ];
        $this->sendJsonRequest($data);

        $response = $this->getNonAuthClient()->getResponse();
        $properties = ['type', 'title', 'violations'];
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $properties);

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_FORBIDDEN, $response);
    }

    public function testAuthFailedBadRequest()
    {
        $data = [
            'phone1' => UserFixtures::USER_PHONE,
            'password1' => UserFixtures::USER_PASS,
        ];
        $this->sendJsonRequest($data);

        $response = $this->getNonAuthClient()->getResponse();
        $properties = ['type', 'title', 'violations'];
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $properties);

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_BAD_REQUEST, $response);
    }

    private function sendJsonRequest(array $data): void
    {
        $this->getNonAuthClient()->request('POST', '/api/v1/login', [], [], [], \GuzzleHttp\json_encode($data));
    }
}
