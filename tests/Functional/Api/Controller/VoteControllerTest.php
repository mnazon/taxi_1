<?php

namespace App\Api\Controller\V1;

use App\Api\DataFixtures\TripVoteFixtures;
use App\DataFixtures\AppFixtures;
use App\Tests\Functional\BaseWebTestCase;

class VoteControllerTest extends BaseWebTestCase
{
    const VOTE_STRUCTURE = [
        'value',
        'createdAt',
        'userName',
        'comment',
    ];

    public function testFindSuccess()
    {
        $fixture = new TripVoteFixtures();
        $this->loadFixtures([$fixture]);

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        /** @var AppFixtures $appFixtures */
        $appFixtures = $this->getFixtureLoader()->getFixture(AppFixtures::class);
        $taxiProviderId = $appFixtures->getTaxiProviderId();
        $client->request('GET', '/api/v1/votes', [
            'taxiProviderId' => $taxiProviderId,
        ]);
        $response = $client->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(200, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(self::VOTE_STRUCTURE, 1)
        );
    }

    public function testAddSuccess()
    {
        $fixture = new AppFixtures();
        $this->loadFixtures([$fixture]);

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        $voteValue = 3;
        $commentValue = 'comment test';
        $data = [
            'tripId' => $fixture->getEvosTrip()->getId(),
            'value' => $voteValue,
            'comment' => $commentValue,
        ];
        $client->request('POST', '/api/v1/votes', [], [], [], json_encode($data));
        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(201, $response);

        $asserter = $this->getResponseAsserter();
        $asserter->assertResponsePropertiesExist($response, self::VOTE_STRUCTURE);

        $asserter->assertResponsePropertyEquals($response, 'value', $voteValue);
        $asserter->assertResponsePropertyEquals($response, 'comment', $commentValue);
        $asserter->assertResponsePropertyEquals($response, 'userName', $user->getFullName());
    }
}
