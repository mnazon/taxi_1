<?php

namespace App\Api\Controller\V1;

use App\Api\DataFixtures\UserDeviceFixtures;
use App\Api\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DeviceControllerTest extends BaseWebTestCase
{
    private const USER_DEVICE_STRUCTURE = [
        'id',
        'deviceToken',
        'firebaseRegistrationToken',
    ];

    public function testSuccessCreateDeviceData()
    {
        $this->loadFixtures([new UserFixtures()]);

        $user = $this->getUserFromFixture();
        $url = '/api/v1/devices';
        $client = $this->getAuthClient($user);
        $data = [
            'deviceToken' => 'deviceToken1',
            'firebaseRegistrationToken' => 'registrationTokenValue1',
        ];
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_CREATED, $response);
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_DEVICE_STRUCTURE);
        $this->getResponseAsserter()
            ->assertResponsePropertiesEquals($response, $data);
    }

    public function testSuccessUpdateDeviceData()
    {
        $deviceFixture = new UserDeviceFixtures();
        $this->loadFixtures([$deviceFixture]);

        $user = $this->getUserFromFixture();
        $url = '/api/v1/devices';
        $client = $this->getAuthClient($user);
        $data = [
            'deviceToken' => UserDeviceFixtures::DEVICE_TOKEN,
            'firebaseRegistrationToken' => 'registrationTokenValue2',
        ];
        $client->request('PUT', $url, [], [], [], \GuzzleHttp\json_encode($data));

        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $data['id'] = $deviceFixture->getUserDevice()->getId();
        $this->getResponseAsserter()
            ->assertResponsePropertiesEquals($response, $data);
    }

    public function testSuccessFindDeviceData()
    {
        $deviceFixture = new UserDeviceFixtures();
        $this->loadFixtures([$deviceFixture]);

        $user = $this->getUserFromFixture();
        $url = '/api/v1/devices/'.UserDeviceFixtures::DEVICE_TOKEN;
        $client = $this->getAuthClient($user);
        $client->request('GET', $url);
        $response = $client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, self::USER_DEVICE_STRUCTURE);
    }
}
