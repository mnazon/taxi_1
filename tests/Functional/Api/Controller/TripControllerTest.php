<?php

namespace App\Tests\Functional\Api\Controller;

use App\Api\Controller\V1\TripController;
use App\Api\DataFixtures\PlaceFixtures;
use App\Api\DataFixtures\ProviderRatingFixtures;
use App\Api\DataFixtures\TripCostFactorFixtures;
use App\Api\DataFixtures\TripRequestFixtures;
use App\Api\DataFixtures\UserDeviceFixtures;
use App\Api\Enum\ApiErrorCode;
use App\Api\Enum\TripStatus;
use App\DataFixtures\AppFixtures;
use App\Tests\Functional\BaseWebTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bridge\PhpUnit\ClockMock;

class TripControllerTest extends BaseWebTestCase
{
    const TRIP_STRUCTURE = [
        'id',
        'cost',
        'currency',
        'currencySign',
        'createdAt',
        'vehicle',
        'driverPhone',
        'status',

        'taxiProvider.id',
        'taxiProvider.name',
        'taxiProvider.slug',
        'taxiProvider.iconUrl',
        'taxiProvider.additionalData',
        'taxiProvider.additionalData.carStandbyTime',

        'vehicle',
    ];
    private static $tripStructureFull = self::TRIP_STRUCTURE + [
        'state.canCancel',

        'startSearchingCarAt',

        'vehicle.numberPlate',
        'vehicle.makeAndModel',
        'vehicle.color',

        'request',
        'request.carType',
        'request.reservedAt',
        'request.userFullName',
        'request.userPhone',
        'request.options',
        'request.startRouteEntranceFrom',
        'request.additionalCost',
        'request.comment',

        'request.route',
    ];
    const ESTIMATED_TRIP_ITEM = [
        'id',
        'cost',
        'currency',
        'percentage',
        'taxiProvider',
        'taxiProvider.rating',
        'taxiProvider.rating.rate',
        'taxiProvider.rating.numVotes',
        'taxiProvider.additionalData',
        'taxiProvider.additionalData.carStandbyTime',
    ];

    public function setUp()
    {
        $this->getTripStructureFull();

        parent::setUp();
    }

    public static function getTripStructureFull(): array
    {
        static $isInit = false;
        if (!$isInit) {
            foreach (GeoControllerTest::PLACE_DATA_STRUCTURE as $item) {
                $tripItemName = "request.route[0].$item";
                self::$tripStructureFull[] = $tripItemName;
            }
            $isInit = true;
        }

        return self::$tripStructureFull;
    }

    /**
     * @throws \Exception
     */
    public function testSuccessCostCalculating()
    {
        $fixture = new ProviderRatingFixtures();
        $this->loadFixtures([$fixture]);

        $data = [
            'carType' => 'standart',
            'userFullName' => 'Аркадий',
            'userPhone' => '+380958747482',
            'route' => [
                [
                    'name' => 'тест1 вход м.Шевченко.',
                    'coordinates' => ['lat' => 50.433835, 'lng' => 30.373609],
                ],
                [
                    'name' => 'тест2 вход иподром.',
                    'coordinates' => ['lat' => 50.444582, 'lng' => 30.505550],
                ],
            ],
            'reservedAt' => '2019-07-20T22:09:00+03:00',
            'additionalCost' => 5,
            'options' => ['courier', 'conditioner', 'animal', 'baggage'],
            'startRouteEntranceFrom' => 1,
            'comment' => 'test test',
        ];

        $mockResponse = '{"dispatching_order_uid":"5c8a304feddc45a0b60de94465bed976","order_cost":"35","currency":" грн.","discount_trip":false,"can_pay_bonuses":false,"can_pay_cashless":false, "find_car_delay":0, "find_car_delay":0}';
        $response = $this->createMockResponse($mockResponse);
        /* @var ResponseInterface|MockObject $response */
        $this->getHttplugMockClient()->setDefaultResponse($response);

        $content = \GuzzleHttp\json_encode($data);
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $params = [
            'sort[field]' => 'price',
            'sort[direction]' => 'asc',
        ];
        $query = http_build_query($params);
        $client->request('PUT', '/api/v1/trips/estimate?'.$query, [], [], [], $content);

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(self::ESTIMATED_TRIP_ITEM, 1)
        );
    }

    public function testUpdateAdditionalCost()
    {
        $fixture = new TripRequestFixtures();
        $this->loadFixtures([$fixture]);

        $id = $fixture->getEvosTrip()->getId();
        $additionalCostValue = 5;
        $url = '/api/v1/trips/'.$id.'/cost/additional/'.$additionalCostValue;
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('PUT', $url);
        $response = $client->getResponse();

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::TRIP_STRUCTURE
        );

        $this->getResponseAsserter()->assertResponsePropertyEquals(
            $response,
            'request.additionalCost',
            $additionalCostValue
        );
    }

    public function testUpdateAdditionalCostOnCarAlreadyFound()
    {
        $fixture = new TripRequestFixtures();
        $this->loadFixtures([$fixture]);
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $id = $fixture->getInternalTrip()->getId();

        $additionalCostValue = 5;
        $url = '/api/v1/trips/'.$id.'/cost/additional/'.$additionalCostValue;

        $client->request('PUT', $url);
        $response = $client->getResponse();

        $this->getResponseAsserter()->assertResponsePropertyEquals(
            $response,
            'violations[0].code',
            ApiErrorCode::CAR_ALREADY_FOUND
        );

        $client->request('GET', '/api/v1/trips/'.$id);
        $response = $client->getResponse();
        $this->getResponseAsserter()->assertResponsePropertyEquals(
            $response,
            'status',
            TripStatus::CAR_FOUND
        );
    }

    public function testSuccessCreateWithRegsatProvider()
    {
        $this->markTestSkipped('Test this case');
        $fixture = new AppFixtures();
        $deviceFixtures = new UserDeviceFixtures();
        $this->loadFixtures([$fixture, $deviceFixtures]);

        $externalData1 = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ADR3K0MXUAGDQVBZZ2","properties":{"name":"27Б","categories":"adr_address","postal_code":"01001","street_id":"STR3K0MXUAGD","lang":"uk","street":"Хрещатик","street_type":"вул.","settlement_id":"STL1NQ7EP","settlement":"Київ","settlement_type":"місто","copyright":"Map Data © 2019, «Visicom» SC","dist_meters":0,"settlement_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STL1NQ7EP.json","street_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STR3K0MXUAGD.json"},"bbox":[30.52218,50.44356,30.52309,50.44388],"geo_centroid":{"type":"Point", "coordinates":[30.52247,50.44376]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/ADR3K0MXUAGDQVBZZ2.json"}]}';
        $externalData2 = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ADR3K0MXUAGDQVBZZ2","properties":{"name":"27Б","categories":"adr_address","postal_code":"01001","street_id":"STR3K0MXUAGD","lang":"uk","street":"Хрещатик","street_type":"вул.","settlement_id":"STL1NQ7EP","settlement":"Київ","settlement_type":"місто","copyright":"Map Data © 2019, «Visicom» SC","dist_meters":0,"settlement_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STL1NQ7EP.json","street_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STR3K0MXUAGD.json"},"bbox":[30.52218,50.44356,30.52309,50.44388],"geo_centroid":{"type":"Point", "coordinates":[30.52247,50.44376]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/ADR3K0MXUAGDQVBZZ2.json"}]}';
        /** @var ResponseInterface|MockObject $response */
        $response = $this->createMockResponse($externalData1);
        $this->getHttplugMockClient()->addResponse($response);
        $response = $this->createMockResponse($externalData2);
        $this->getHttplugMockClient()->addResponse($response);

        $successTripCreateResponse = '{"dispatching_order_uid":"a89bf07da00dabf372d810f0ca82361f","find_car_timeout":570,"find_car_delay":0}';
        $response = $this->createMockResponse($successTripCreateResponse);
        $this->getHttplugMockClient()->addResponse($response);

        $successFindTripResponse = '{"dispatching_order_uid":"a89bf07da00dabf372d810f0ca82361f","order_cost":"125","currency":" грн.","order_car_info":"","driver_phone":"","required_time":"","close_reason":-1,"cancel_reason_comment":null,"order_is_archive":false,"driver_execution_status":0,"drivercar_position":{"lat":0,"lng":0,"time_positioned_utc":"2019-07-05T15:08:37.521","altitude":null,"accuracy":null,"bearing":0,"speed":0,"status":"gpsOk"},"route_address_from":{"name":"ТЕСТ МАЙДАН НЕЗАЛЕЖНОСТІ","number":"1, 0"},"route_address_to":{"name":"ТЕСТ ВУЛИЦЯ ВІННИЦЬКА","number":"187, 0"}}';
        $response = $this->createMockResponse($successFindTripResponse);
        $this->getHttplugMockClient()->addResponse($response);

        $data = [
            'notificationToken' => '12312313213',
            'taxiProviderId' => $fixture->getTaxiProviderId(),
            'trip' => [
                'carType' => 'standart',
                'userFullName' => 'Аркадий',
                'userPhone' => '+380958747482',
                'route' => [
                    [
                        //'id' => 1,
                        'name' => 'тест1 вход м.Шевченко.',
                        'coordinates' => ['lat' => 50.433835, 'lng' => 30.373609],
                    ],
                    ['name' => 'тест2 вход иподром.', 'coordinates' => ['lat' => 50.444582, 'lng' => 30.505550]],
                ],
                'reservedAt' => (new \DateTimeImmutable('+2 hours'))->format(\DateTime::ATOM),
                'options' => ['courier', 'conditioner', 'animal', 'baggage'],
                'startRouteEntranceFrom' => 1,
                'comment' => 'test test',
            ],
        ];
        $content = \GuzzleHttp\json_encode($data);
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('POST', '/api/v1/trips', [], [], [], $content);

        $response = $client->getResponse();
        $rawContent = $response->getContent();
        $this->assertEquals(201, $response->getStatusCode(), $rawContent);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::$tripStructureFull
        );
    }

    public function testSuccessCreateWithEvosProvider()
    {
        $this->markTestSkipped('Test this case');
        $fixture = new AppFixtures();
        $deviceFixtures = new UserDeviceFixtures();
        $this->loadFixtures([$fixture, $deviceFixtures]);

        $externalData1 = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ADR3K0MXUAGDQVBZZ2","properties":{"name":"27Б","categories":"adr_address","postal_code":"01001","street_id":"STR3K0MXUAGD","lang":"uk","street":"Хрещатик","street_type":"вул.","settlement_id":"STL1NQ7EP","settlement":"Київ","settlement_type":"місто","copyright":"Map Data © 2019, «Visicom» SC","dist_meters":0,"settlement_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STL1NQ7EP.json","street_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STR3K0MXUAGD.json"},"bbox":[30.52218,50.44356,30.52309,50.44388],"geo_centroid":{"type":"Point", "coordinates":[30.52247,50.44376]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/ADR3K0MXUAGDQVBZZ2.json"}]}';
        $externalData2 = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ADR3K0MXUAGDQVBZZ2","properties":{"name":"27Б","categories":"adr_address","postal_code":"01001","street_id":"STR3K0MXUAGD","lang":"uk","street":"Хрещатик","street_type":"вул.","settlement_id":"STL1NQ7EP","settlement":"Київ","settlement_type":"місто","copyright":"Map Data © 2019, «Visicom» SC","dist_meters":0,"settlement_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STL1NQ7EP.json","street_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STR3K0MXUAGD.json"},"bbox":[30.52218,50.44356,30.52309,50.44388],"geo_centroid":{"type":"Point", "coordinates":[30.52247,50.44376]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/ADR3K0MXUAGDQVBZZ2.json"}]}';
        /** @var ResponseInterface|MockObject $response */
        $response = $this->createMockResponse($externalData1);
        $this->getHttplugMockClient()->addResponse($response);
        $response = $this->createMockResponse($externalData2);
        $this->getHttplugMockClient()->addResponse($response);

        $mockResponse = '{"dispatching_order_uid":"5c8a304feddc45a0b60de94465bed976","order_cost":"35","currency":" грн.","discount_trip":false,"can_pay_bonuses":false,"can_pay_cashless":false, "find_car_delay":0}';
        $response = $this->createMockResponse($mockResponse);
        /* @var ResponseInterface|MockObject $response */
        $this->getHttplugMockClient()->addResponse($response);

        $data = [
            'notificationToken' => '12312313213',
            'taxiProviderId' => $fixture->getTaxiProviderId(),
            'trip' => [
                'carType' => 'standart',
                'userFullName' => 'Аркадий',
                'userPhone' => '+380958747482',
                'route' => [
                    [
                        //'id' => 1,
                        'name' => 'тест1 вход м.Шевченко.',
                        'coordinates' => ['lat' => 50.433835, 'lng' => 30.373609],
                    ],
                    ['name' => 'тест2 вход иподром.', 'coordinates' => ['lat' => 50.444582, 'lng' => 30.505550]],
                ],
                'reservedAt' => (new \DateTimeImmutable('+2 hours'))->format(\DateTime::ATOM),
                'options' => ['courier', 'conditioner', 'animal', 'baggage'],
                'startRouteEntranceFrom' => 1,
                'comment' => 'test test',
            ],
        ];
        $content = \GuzzleHttp\json_encode($data);
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('POST', '/api/v1/trips', [], [], [], $content);

        $response = $client->getResponse();
        $rawContent = $response->getContent();
        $this->assertEquals(201, $response->getStatusCode(), $rawContent);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::$tripStructureFull
        );
    }

    public function testGetCurrentTrips()
    {
        $fixture = new PlaceFixtures();
        $this->loadFixtures([$fixture]);

        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', '/api/v1/trips/current');

        $response = $client->getResponse();
        $rawContent = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode(), $rawContent);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(self::$tripStructureFull, 2)
        );
    }

    public function testSuccessCompare()
    {
        $fixture = new AppFixtures();
        $deviceFixtures = new UserDeviceFixtures();
        $this->loadFixtures([$fixture, $deviceFixtures]);

        $data = [
            'baseCost' => 12,
            'taxiProviderId' => $fixture->getTaxiProviderId(),
            'trip' => [
                'carType' => 'standart',
                'userFullName' => 'Аркадий',
                'userPhone' => '+380958747482',
                'route' => [
                    ['name' => 'тест1 вход м.Шевченко.', 'coordinates' => ['lat' => 50.433835, 'lng' => 30.373609]],
                    ['name' => 'тест2 вход иподром.', 'coordinates' => ['lat' => 50.444582, 'lng' => 30.505550]],
                ],
                'options' => ['courier', 'conditioner', 'animal', 'baggage'],
                'startRouteEntranceFrom' => 1,
                'comment' => 'test test',
            ],
        ];

        $mockResponse = '{"dispatching_order_uid":"5c8a304feddc45a0b60de94465bed976","order_cost":"35","currency":" грн.","discount_trip":false,"can_pay_bonuses":false,"can_pay_cashless":false, "find_car_delay":0}';
        $response = $this->createMockResponse($mockResponse);
        /* @var ResponseInterface|MockObject $response */
        $this->getHttplugMockClient()->addResponse($response);

        $content = \GuzzleHttp\json_encode($data);
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('PUT', '/api/v1/trips/compare', [], [], [], $content);

        $response = $client->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(200, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            [
                'isPriceChanged',
                'baseCost',
                'additionalCost',
            ]
        );
    }

    public function testGetTripDataSuccess()
    {
        $fixture = new PlaceFixtures();
        $this->loadFixtures([$fixture]);
        $trip = $fixture->getReference(AppFixtures::TRIP_REF);
        $tripId = $trip->getId();

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        $client->request('GET', "/api/v1/trips/{$tripId}");

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::$tripStructureFull
        );
    }

    public function testUserTripsSuccessResponse()
    {
        $fixture = new AppFixtures();
        $this->loadFixtures([$fixture]);

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        $client->request('GET', '/api/v1/trips?userId='.$user->getId());

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);

        $this->getResponseAsserter()
            ->assertResponsePropertiesExist(
                $response,
                $this->prepareCollectionStructure(self::TRIP_STRUCTURE, 1)
            );
    }

    public function testSuccessTripCancellation()
    {
        $fixture = new AppFixtures();
        $this->loadFixtures([$fixture]);

        $mockResponse = '{"dispatching_order_uid":"5c8a304feddc45a0b60de94465bed976","order_client_cancel_result":1}';
        $response = $this->createMockResponse($mockResponse);
        /* @var ResponseInterface|MockObject $response */
        $this->getHttplugMockClient()->addResponse($response);

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        $client->request('PUT', "/api/v1/trips/{$fixture->getEvosTrip()->getId()}/cancel");

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);
    }

    public function testSuccessTripCostHint()
    {
        $fixture = new AppFixtures();
        $tripCostFixtures = new TripCostFactorFixtures();
        $this->loadFixtures([$fixture, $tripCostFixtures]);

        ClockMock::register(TripController::class);
        $date = (\DateTimeImmutable::createFromFormat(
            'Y-m-d H:i:s',
            '2019-10-08 16:00:00'
        ))->format('U');
        ClockMock::withClockMock($date);

        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', "/api/v1/trips/{$fixture->getEvosTrip()->getId()}/costHint");

        $response = $client->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(200, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            [
                'baseCost',
                'additionalCost',
            ]
        );

        ClockMock::withClockMock(false);
    }

    public function testEmptyTripCostHint()
    {
        $fixture = new AppFixtures();
        $tripCostFixtures = new TripCostFactorFixtures();
        $this->loadFixtures([$fixture, $tripCostFixtures]);

        ClockMock::register(TripController::class);
        $date = (\DateTimeImmutable::createFromFormat(
            'Y-m-d H:i:s',
            '2019-10-08 17:00:00'
        ))->format('U');
        ClockMock::withClockMock($date);

        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', "/api/v1/trips/{$fixture->getEvosTrip()->getId()}/costHint");

        $response = $client->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(200, $response);
        $this->assertEquals('null', $response->getContent());

        ClockMock::withClockMock(false);
    }

    public function testGetTripDataExternalSyncSuccess()
    {
        $fixture = new PlaceFixtures();
        $this->loadFixtures([$fixture]);
        $trip = $fixture->getReference(AppFixtures::INTERNAL_TRIP_REF);
        $tripId = $trip->getId();

        $user = $this->getUserFromFixture();
        $client = $this->logIn($this->client, $user);
        $client->request('GET', "/api/v1/trips/{$tripId}/external-sync");

        $response = $client->getResponse();
        $this->getResponseAsserter()
            ->assertStatusEquals(200, $response);

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::$tripStructureFull
        );
    }
}
