<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Controller;

use App\Api\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ResponseStatusTest extends BaseWebTestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testResponseStatusCode(string $method, string $url, $expectedStatus)
    {
        $this->getNonAuthClient()->request($method, $url);

        $this->getResponseAsserter()
            ->assertStatusEquals($expectedStatus, $this->client->getResponse());
    }

    public function dataProvider()
    {
        yield ['PUT', '/api/v1/users/1', Response::HTTP_UNAUTHORIZED];
        yield ['GET', '/api/v1/users/1', Response::HTTP_UNAUTHORIZED];
        yield ['GET', '/api/doc', Response::HTTP_UNAUTHORIZED];
    }

    public function testApiDocResponseSuccess()
    {
        $userFixtures = new UserFixtures();
        $this->loadFixtures([$userFixtures]);
        $user = $userFixtures->getUser();

        $client = $this->logIn($this->client, $user, 'apidoc');
        $client->request('GET', '/api/doc');

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $client->getResponse());
    }
}
