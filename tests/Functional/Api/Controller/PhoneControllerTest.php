<?php

namespace App\Api\Controller;

use App\Api\DataFixtures\PhoneConfirmationFixture;
use App\Tests\Functional\BaseWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PhoneControllerTest extends BaseWebTestCase
{
    const PHONE_RESPONSE_STRUCTURE = [
        'nextAttemptInSeconds',
    ];

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures([]);
    }

    public function testSendConfirmationCodeSuccess()
    {
        $data = [
            'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
        ];
        $this->getNonAuthClient()->request('POST', '/api/v1/phones/send/code', [], [], [], json_encode($data));

        $response = $this->getNonAuthClient()->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(200, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist($response, self::PHONE_RESPONSE_STRUCTURE);
        $this->getResponseAsserter()->assertResponsePropertyDoesNotExist($response, 'code');
    }

    public function testSendConfirmationCodeTestSuccess()
    {
        $data = [
            'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
        ];
        $this->getNonAuthClient()->request('POST', '/api/v1/phones/send/code/test', [], [], [
            'CAN_USE_TEST_CONFIRMATION' => true,
        ], json_encode($data));

        $response = $this->getNonAuthClient()->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(Response::HTTP_OK, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist($response, ['code', 'nextAttemptInSeconds']);
    }

    public function testConfirmationCodeSendRateLimitCheck()
    {
        $this->loadFixtures([new PhoneConfirmationFixture()]);
        $data = [
            'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
        ];
        $this->getNonAuthClient()->request('POST', '/api/v1/phones/send/code', [], [], [], json_encode($data));

        $response = $this->getNonAuthClient()->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(400, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist($response, self::EXPECTED_API_PROBLEM_ERROR_STRUCTURE);
        $this->getResponseAsserter()->assertResponsePropertyEquals($response, 'violations[0].code', 'confirmation_code_rate_limit');
    }

    public function testPhoneBanned()
    {
        $this->loadFixtures([new PhoneConfirmationFixture(0, PhoneConfirmationFixture::PHONE_NUMBER, new \DateTimeImmutable('+1 minute'))]);
        $data = [
            'phone' => PhoneConfirmationFixture::PHONE_NUMBER,
        ];
        $this->getNonAuthClient()->request('POST', '/api/v1/phones/send/code', [], [], [], json_encode($data));

        $response = $this->getNonAuthClient()->getResponse();
        $this->getResponseAsserter()->assertStatusEquals(400, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist($response, self::EXPECTED_API_PROBLEM_ERROR_STRUCTURE);
        $this->getResponseAsserter()->assertResponsePropertyEquals($response, 'violations[0].code', 'phone_banned');
    }
}
