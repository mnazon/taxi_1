<?php

namespace App\Tests\Functional\Api\Controller;

use App\Api\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseWebTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;

class GeoControllerTest extends BaseWebTestCase
{
    const PLACE_DATA_STRUCTURE = [
        'name',
        'coordinates.lat',
        'coordinates.lng',
    ];

    const AUTOCOMPLETE_DATA_STRUCTURE = [
        'mainText',
        'secondaryText',
        'selectedText',
        'placeId',
        'isExactAddress',
    ];

    public function setUp()
    {
        parent::setUp();
        $userFixtures = new UserFixtures();
        $this->loadFixtures([$userFixtures]);
    }

    //@todo - подключить behat watch this - https://codereviewvideos.com/course/beginners-guide-back-end-json-api-front-end-2018/video/easy-database-with-docker

    /**
     * @throws \Exception
     */
    public function testObjectsSearch()
    {
        $this->markTestSkipped('Test this case');
        $externalData = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"POIGFDXM4H","properties":{"name":"Хрещатик","categories":"poi_underground_railway_station","lang":"uk","address":"Київ, Хрещатик вул.","country_code":"ua","star":0,"vitrine":"Ст. м. \"Хрещатик\"","w24hours":false,"icon":"https://static.visicom.ua/db/pois/files/2.558958863883864E-307","copyright":"Map Data (c) 2020, \"\"Visicom\"\" PJSC.","dist_meters":389,"relevance":2.6580419540405273},"bbox":[30.52291,50.44722,30.52291,50.44722],"geo_centroid":{"type":"Point", "coordinates":[30.52291,50.44722]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/POIGFDXM4H.json"},{"type":"Feature","id":"POIGFDXRSR","properties":{"name":"Театральна","categories":"poi_underground_railway_station","lang":"uk","address":"Київ, Богдана Хмельницького вул.","country_code":"ua","star":0,"vitrine":"Ст. м. \"Театральна\"","w24hours":false,"icon":"https://static.visicom.ua/db/pois/files/2.558958863883864E-307","copyright":"Map Data (c) 2020, \"\"Visicom\"\" PJSC.","dist_meters":402,"relevance":2.619797945022583},"bbox":[30.51788,50.44545,30.51788,50.44545],"geo_centroid":{"type":"Point", "coordinates":[30.51788,50.44545]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/POIGFDXRSR.json"},{"type":"Feature","id":"POIGFDXRZ1","properties":{"name":"Площа Льва Толстого","categories":"poi_underground_railway_station","lang":"uk","address":"Київ, Льва Толстого пл.","country_code":"ua","star":0,"vitrine":"Ст. м. \"Площа Льва Толстого\"","w24hours":false,"icon":"https://static.visicom.ua/db/pois/files/2.558958863883864E-307","copyright":"Map Data (c) 2020, \"\"Visicom\"\" PJSC.","dist_meters":642,"relevance":2.098801851272583},"bbox":[30.51666,50.43951,30.51666,50.43951],"geo_centroid":{"type":"Point", "coordinates":[30.51666,50.43951]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/POIGFDXRZ1.json"},{"type":"Feature","id":"POIGFDXMDO","properties":{"name":"Палац спорту","categories":"poi_underground_railway_station","lang":"uk","address":"Київ, Спортивна пл.","country_code":"ua","star":0,"vitrine":"Ст. м. \"Палац Спорту\"","w24hours":false,"icon":"https://static.visicom.ua/db/pois/files/2.558958863883864E-307","copyright":"Map Data (c) 2020, \"\"Visicom\"\" PJSC.","dist_meters":666,"relevance":2.0631556510925293},"bbox":[30.52103,50.43785,30.52103,50.43785],"geo_centroid":{"type":"Point", "coordinates":[30.52103,50.43785]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/POIGFDXMDO.json"}]}';
        /** @var ResponseInterface|MockObject $response */
        $response = $this->createMockResponse($externalData);
        $this->getHttplugMockClient()
            ->addResponse($response);

        $limit = 4;
        $params = [
            'query' => 'МЕТРО',
            'limit' => $limit,
        ];
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', '/api/v1/places?'.http_build_query($params));
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            $this->prepareCollectionStructure(self::AUTOCOMPLETE_DATA_STRUCTURE, $limit)
        );
    }

    public function testGetPlaceById()
    {
        $this->markTestSkipped('Test this case');
        $externalData = '{"result":{"address_components":[{"long_name":"99","short_name":"99","types":["street_number"]},{"long_name":"\u0432\u0443\u043b\u0438\u0446\u044f \u0411\u043e\u0440\u0449\u0430\u0433\u0456\u0432\u0441\u044c\u043a\u0430","short_name":"\u0432\u0443\u043b\u0438\u0446\u044f \u0411\u043e\u0440\u0449\u0430\u0433\u0456\u0432\u0441\u044c\u043a\u0430","types":["route"]},{"long_name":"\u0412\u043e\u0440\u0437\u0435\u043b\u044c","short_name":"\u0412\u043e\u0440\u0437\u0435\u043b\u044c","types":["locality","political"]},{"long_name":"\u0406\u0440\u043f\u0456\u043d\u0441\u044c\u043a\u0430 \u043c\u0456\u0441\u044c\u043a\u0440\u0430\u0434\u0430","short_name":"\u0406\u0440\u043f\u0456\u043d\u0441\u044c\u043a\u0430 \u043c\u0456\u0441\u044c\u043a\u0440\u0430\u0434\u0430","types":["administrative_area_level_3","political"]},{"long_name":"\u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430 \u043e\u0431\u043b\u0430\u0441\u0442\u044c","short_name":"\u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430 \u043e\u0431\u043b.","types":["administrative_area_level_1","political"]},{"long_name":"\u0423\u043a\u0440\u0430\u0457\u043d\u0430","short_name":"UA","types":["country","political"]},{"long_name":"08296","short_name":"08296","types":["postal_code"]}],"formatted_address":"\u0432\u0443\u043b\u0438\u0446\u044f \u0411\u043e\u0440\u0449\u0430\u0433\u0456\u0432\u0441\u044c\u043a\u0430, 99, \u0412\u043e\u0440\u0437\u0435\u043b\u044c, \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430 \u043e\u0431\u043b., \u0423\u043a\u0440\u0430\u0457\u043d\u0430, 08296","geometry":{"location":{"lat":50.5461647,"lng":30.10554119999999},"location_type":"RANGE_INTERPOLATED","viewport":{"northeast":{"lat":50.5475136802915,"lng":30.10689018029149},"southwest":{"lat":50.5448157197085,"lng":30.10419221970849}}},"place_id":"EkBCb3JzaGNoYWhpdnMna2EgU3QsIDk5LCBWb3J6ZWwnLCBLeWl2cydrYSBvYmxhc3QsIFVrcmFpbmUsIDA4Mjk2IhoSGAoUChIJdz33SJwwK0cRtv1YTv6eu5QQYw","types":["street_address"],"name":"someText"},"status":"OK"}';
        /** @var ResponseInterface|MockObject $response */
        $response = $this->createMockResponse($externalData);
        $this->getHttplugMockClient()
            ->addResponse($response);

        $placeId = '123';
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', '/api/v1/places/'.$placeId);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::PLACE_DATA_STRUCTURE
        );
    }

    public function testGetPlaceByCoordinates()
    {
        $this->markTestSkipped('Test this case');
        $externalData = '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ADR3K0MXUAGDQVBZZ2","properties":{"name":"27Б","categories":"adr_address","postal_code":"01001","street_id":"STR3K0MXUAGD","lang":"uk","street":"Хрещатик","street_type":"вул.","settlement_id":"STL1NQ7EP","settlement":"Київ","settlement_type":"місто","copyright":"Map Data © 2019, «Visicom» SC","dist_meters":0,"settlement_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STL1NQ7EP.json","street_url":"https://api.visicom.ua/data-api/4.0/uk/feature/STR3K0MXUAGD.json"},"bbox":[30.52218,50.44356,30.52309,50.44388],"geo_centroid":{"type":"Point", "coordinates":[30.52247,50.44376]},"url":"https://api.visicom.ua/data-api/4.0/uk/feature/ADR3K0MXUAGDQVBZZ2.json"}]}';
        /** @var ResponseInterface|MockObject $response */
        $response = $this->createMockResponse($externalData);
        $this->getHttplugMockClient()
            ->addResponse($response);

        $lat = '50.4437';
        $lng = '30.5228';
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', "/api/v1/geocoding?lat=$lat&lng=$lng");
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->getResponseAsserter()->assertResponsePropertiesExist(
            $response,
            self::PLACE_DATA_STRUCTURE
        );
    }

    public function testGetPlaceByCoordinatesInNotSupportedArea()
    {
        $this->markTestSkipped('Test this case');
        $lat = '50.1231';
        $lng = '30.1111';
        $client = $this->logIn($this->client, $this->getUserFromFixture());
        $client->request('GET', "/api/v1/geocoding?lat=$lat&lng=$lng");
        $response = $client->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(400, $response);
        $this->getResponseAsserter()->assertResponsePropertiesExist($response, self::EXPECTED_API_PROBLEM_ERROR_STRUCTURE);
        $this->getResponseAsserter()->assertResponsePropertyEquals($response, 'violations[0].code', 'location_not_supported');
    }
}
