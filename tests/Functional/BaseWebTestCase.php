<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Api\DataFixtures\UserFixtures;
use App\Api\Entity\User;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Http\Mock\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser as FrameworkClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

class BaseWebTestCase extends WebTestCase
{
    const EXPECTED_API_PROBLEM_ERROR_STRUCTURE = [
        'type',
        'title',
        'violations',
        'violations[0].propertyPath',
        'violations[0].title',
        'violations[0].code',
    ];

    /**
     * @var FrameworkClient
     */
    protected $client;

    /**
     * @var FrameworkClient
     */
    protected $authClient;

    /**
     * @var ResponseAsserter
     */
    private $responseAsserter;

    /**
     * @var Loader
     */
    private $fixtureLoader;

    public function setUp()
    {
        parent::setUp();
        $headers = [
            'CONTENT_TYPE' => 'application/json',
        ];
        $this->client = $this->createClient([], $headers);
    }

    protected function createMockResponse(string $body, int $statusCode = 200)
    {
        $response = $this->createMock(ResponseInterface::class);
        $stream = $this->createMock(StreamInterface::class);
        $stream->method('getContents')->willReturn($body);
        $response->method('getBody')->willReturn($stream);
        $response->method('getStatusCode')->willReturn($statusCode);
        $response->method('getHeaders')->willReturn([]);

        return $response;
    }

    protected function getHttplugMockClient(): Client
    {
        return self::$container->get('httplug.client.mock');
    }

    public function loadFixtures(array $fixtures, bool $append = false): void
    {
        $this->fixtureLoader = new Loader();
        $entityManager = self::$container->get('doctrine')->getManager();
        $ormExecutor = new ORMExecutor($entityManager, new ORMPurger($entityManager));

        $consoleLogger = new ConsoleLogger(new ConsoleOutput(ConsoleOutput::VERBOSITY_DEBUG));
        $ormExecutor->setLogger(function ($message) use ($consoleLogger) {
            $consoleLogger->info($message);
        });
        foreach ($fixtures as $fixture) {
            $this->fixtureLoader->addFixture($fixture);
        }
        foreach ($this->fixtureLoader->getFixtures() as $fixture) {
            if ($fixture instanceof ContainerAwareInterface) {
                $fixture->setContainer(self::$container);
            }
        }
        $ormExecutor->execute($this->fixtureLoader->getFixtures(), $append);
    }

    public function getResponseAsserter(): ResponseAsserter
    {
        if (null === $this->responseAsserter) {
            $this->responseAsserter = new ResponseAsserter();
        }

        return $this->responseAsserter;
    }

    public function logIn(FrameworkClient $client, UserInterface $user, $firewallName = 'main')
    {
        $session = $client->getContainer()->get('session');

//        $firewallName = 'secure_area';
//        // if you don't define multiple connected firewalls, the context defaults to the firewall name
//        // See https://symfony.com/doc/current/reference/configuration/security.html#firewall-context
        $firewallContext = $firewallName;

        // you may need to use a different token class depending on your application.
        // for example, when using Guard authentication you must instantiate PostAuthenticationGuardToken
        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $client;
    }

    public function getNonAuthClient(array $options = []): FrameworkClient
    {
        if (null === $this->client) {
            $this->client = $this->createClient($options);
        }

        return $this->client;
    }

    /**
     * @param string $firewallName
     */
    public function getAuthClient(UserInterface $user, $firewallName = 'main', array $options = []): FrameworkClient
    {
        if (null === $this->authClient) {
            $this->authClient = $this->createClient($options);
            $this->logIn($this->authClient, $user, $firewallName);
        }

        return $this->authClient;
    }

    public function prepareCollectionStructure(array $singleStructure, $iterateCount)
    {
        $exceptedProperties = [];
        for ($i = 0; $i < $iterateCount; ++$i) {
            foreach ($singleStructure as $item) {
                $exceptedProperties[] = "[{$i}].$item";
            }
        }

        return $exceptedProperties;
    }

    public function assertRightErrorStructure(Response $response, int $violationCount = 1)
    {
        $exceptedProperties = [
            'type',
            'title',
            'detail',
            'violations',
        ];

        for ($i = 0; $i < $violationCount; ++$i) {
            $exceptedProperties[] = "violations[{$i}].propertyPath";
            $exceptedProperties[] = "violations[{$i}].title";
        }
        $this->getResponseAsserter()
            ->assertResponsePropertiesExist($response, $exceptedProperties);
    }

    public function getFixtureLoader(): Loader
    {
        return $this->fixtureLoader;
    }

    public function getUserFromFixture(): User
    {
        /** @var UserFixtures $userFixtures */
        $userFixtures = $this->getFixtureLoader()->getFixture(UserFixtures::class);

        return $userFixtures->getUser();
    }
}
