<?php

namespace App\Tests\Functional;

use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Helper class to assert different conditions on Response.
 */
class ResponseAsserter extends Assert
{
    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * Asserts the array of property names are in the JSON response.
     *
     * @expectedException
     */
    public function assertResponsePropertiesExist(Response $response, array $expectedProperties)
    {
        foreach ($expectedProperties as $propertyPath) {
            // this will blow up if the property doesn't exist
            $this->readResponseProperty($response, $propertyPath);
        }
        self::assertTrue(true, 'Some properties does not exist');
    }

    /**
     * Asserts the array of property names are in the JSON response.
     *
     * @expectedException
     */
    public function assertResponsePropertiesEquals(Response $response, array $expectedPropertiesWithValue)
    {
        foreach ($expectedPropertiesWithValue as $propertyPath => $propertyValue) {
            $this->assertResponsePropertyEquals($response, $propertyPath, $propertyValue);
        }
    }

    /**
     * @expectedException
     */
    public function assertStatusEquals(int $expectedStatus, Response $response)
    {
        self::assertEquals($expectedStatus, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Asserts the specific propertyPath is in the JSON response.
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     * @expectedException
     */
    public function assertResponsePropertyExists(Response $response, $propertyPath)
    {
        // this will blow up if the property doesn't exist
        $this->readResponseProperty($response, $propertyPath);
    }

    /**
     * Asserts the given property path does *not* exist.
     *
     * @expectedException
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     */
    public function assertResponsePropertyDoesNotExist(Response $response, $propertyPath)
    {
        try {
            // this will blow up if the property doesn't exist
            $this->readResponseProperty($response, $propertyPath);
            $this->fail(sprintf('Property "%s" exists, but it should not', $propertyPath));
        } catch (RuntimeException $e) {
            // cool, it blew up
            // this catches all errors (but only errors) from the PropertyAccess component
        }
    }

    /**
     * Asserts the response JSON property equals the given value.
     *
     * @expectedException
     *
     * @param string $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param mixed  $expectedValue
     */
    public function assertResponsePropertyEquals(Response $response, $propertyPath, $expectedValue)
    {
        $actual = $this->readResponseProperty($response, $propertyPath);
        $this->assertEquals(
            $expectedValue,
            $actual,
            sprintf(
                'Property "%s": Expected "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actual, true)
            )
        );
    }

    /**
     * Asserts the response property is an array.
     *
     * @expectedException
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     */
    public function assertResponsePropertyIsArray(Response $response, $propertyPath)
    {
        $this->assertInternalType('array', $this->readResponseProperty($response, $propertyPath));
    }

    /**
     * Asserts the given response property (probably an array) has the expected "count".
     *
     * @expectedException
     *
     * @param string $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param int    $expectedCount
     */
    public function assertResponsePropertyCount(Response $response, $propertyPath, $expectedCount)
    {
        $this->assertCount((int) $expectedCount, $this->readResponseProperty($response, $propertyPath));
    }

    /**
     * Asserts the specific response property contains the given value.
     *
     * e.g. "Hello world!" contains "world"
     *
     * @expectedException
     *
     * @param string $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param mixed  $expectedValue
     */
    public function assertResponsePropertyContains(Response $response, $propertyPath, $expectedValue)
    {
        $actualPropertyValue = $this->readResponseProperty($response, $propertyPath);
        $this->assertContains(
            $expectedValue,
            $actualPropertyValue,
            sprintf(
                'Property "%s": Expected to contain "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actualPropertyValue, true)
            )
        );
    }

    /**
     * Reads a JSON response property and returns the value.
     *
     * This will explode if the value does not exist
     *
     * @expectedException
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     *
     * @return mixed
     */
    public function readResponseProperty(Response $response, $propertyPath)
    {
        if (null === $this->accessor) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }
        $data = json_decode((string) $response->getContent());
        if (null === $data) {
            throw new \Exception(sprintf('Cannot read property "%s" - the response is invalid (is it HTML?)', $propertyPath));
        }
        try {
            return $this->accessor->getValue($data, $propertyPath);
        } catch (AccessException $e) {
            // it could be a stdClass or an array of stdClass
            $values = \is_array($data) ? $data : get_object_vars($data);
            throw new AccessException(sprintf('Error reading property "%s" from available keys (%s) of object(%s)', $propertyPath, implode(', ', array_keys($values)), var_export($values, true)), 0, $e);
        }
    }

    /**
     * @expectedException
     */
    public function assertCookieExists(Response $response, array $expectedCookies)
    {
        $cookies = $response->headers->getCookies();

        $cookiesInResponse = [];
        foreach ($cookies as $cookie) {
            $cookiesInResponse[] = $cookie->getName();
        }

        $diff = array_diff($expectedCookies, $cookiesInResponse);
        self::assertEmpty($diff, 'Cookies that not found - '.var_export($diff, true));
    }

    /**
     * @expectedException
     */
    public function assertHeadersExists(Response $response, array $expectedHeaders)
    {
        $headers = $response->headers;

        $headerNamesInResponse = [];
        foreach ($headers as $header) {
            $headerNamesInResponse[] = $header->getName();
        }

        $diff = array_diff($expectedHeaders, $headerNamesInResponse);
        self::assertEmpty($diff, 'Headers that not found - '.var_export($diff, true));
    }
}
