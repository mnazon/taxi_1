<?php

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\EstimatedTripItem;
use App\Api\DTO\OrderCostCollection;
use App\Api\DTO\Trip\EstimateSort;
use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TaxiProviderRating;
use App\Api\Enum\EstimateSortField;
use App\Api\Enum\SortDirection;
use App\Api\Repository\TaxiProviderRatingRepository;
use App\Api\Repository\TaxiProviderRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class OrderAggregatorTest extends TestCase
{
    /**
     * @dataProvider sortDataProvider
     *
     * @param $estimatedItems
     *
     * @throws \ReflectionException
     */
    public function testEstimateSort(EstimateSort $sort, array $expectedIdOrder, array $estimatedItems)
    {
        $orderAggregator = new OrderAggregator(
            $this->createMock(TripClientFactory::class),
            $this->createMock(TaxiProviderRepository::class),
            $this->createMock(TaxiProviderRatingRepository::class),
            $this->createMock(LoggerInterface::class)
        );

        $collection = new OrderCostCollection();
        foreach ($estimatedItems as $estimatedItem) {
            $provider = new TaxiProvider();
            $rating = new TaxiProviderRating($estimatedItem['rate'], $estimatedItem['votesNum'], $provider);
            $provider->setRating($rating);
            $item = new EstimatedTripItem(
                $estimatedItem['id'],
                $estimatedItem['cost'],
                'currency',
                $provider
            );
            $item->setPercentage($estimatedItem['percentage']);
            $collection->add($item);
        }
        $collection = $orderAggregator->sort($collection, $sort);
        $resultIdOrder = [];
        foreach ($collection->getData() as $item) {
            $resultIdOrder[] = $item->getId();
        }
        $this->assertEquals($expectedIdOrder, $resultIdOrder);
    }

    public function sortDataProvider()
    {
        $smartSort = new EstimateSort(EstimateSortField::SMART, SortDirection::DESC());
        $priceAsc = new EstimateSort(EstimateSortField::PRICE, SortDirection::ASC());
        $rateDesc = new EstimateSort(EstimateSortField::RATING, SortDirection::DESC());
        $percentageDesc = new EstimateSort(EstimateSortField::PERCENT, SortDirection::DESC());

        $estimatedItem1 = [
            'cost' => 100,
            'rate' => 4.2,
            'votesNum' => 10,
            'percentage' => 80,
            'id' => 1,
        ];
        $estimatedItem2 = [
            'cost' => 101,
            'rate' => 4.1,
            'votesNum' => 10,
            'percentage' => 81,
            'id' => 2,
        ];
        $estimatedItem3 = [
            'cost' => 102,
            'rate' => 4.2,
            'votesNum' => 10,
            'percentage' => 79,
            'id' => 3,
        ];
        $estimatedItem4 = [
            'cost' => 100,
            'rate' => 4.0,
            'votesNum' => 10,
            'percentage' => 80,
            'id' => 4,
        ];

        return [
            [$percentageDesc, [2, 1, 4, 3], [$estimatedItem1, $estimatedItem2, $estimatedItem3, $estimatedItem4]],
            [$priceAsc, [1, 4, 2, 3], [$estimatedItem1, $estimatedItem2, $estimatedItem3, $estimatedItem4]],
            [$rateDesc, [3, 1, 2, 4], [$estimatedItem1, $estimatedItem2, $estimatedItem3, $estimatedItem4]],
            [$smartSort, [2, 4, 1, 3], [$estimatedItem1, $estimatedItem2, $estimatedItem3, $estimatedItem4]],
        ];
    }
}
