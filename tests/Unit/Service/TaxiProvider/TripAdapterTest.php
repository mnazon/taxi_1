<?php

namespace App\Api\Service\TaxiProvider\Evos;

use Http\Client\HttpAsyncClient;
use Http\Message\MessageFactory;
use Money\Currencies\ISOCurrencies;
use Money\Parser\DecimalMoneyParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Lock\Store\FlockStore;

class TripAdapterTest extends TestCase
{
    /**
     * @dataProvider extractVehicleDataProvider
     *
     * @throws \ReflectionException
     */
    public function testSuccessExtractVehicle($response, $makeAndModel, $color, $numberPlate)
    {
        $evosTripAdapter = new TripAdapter(
            $this->createMock(HttpAsyncClient::class),
            $this->createMock(HttpAsyncClient::class),
            $this->createMock(HttpAsyncClient::class),
            $this->createMock(MessageFactory::class),
            new DecimalMoneyParser(new ISOCurrencies()),
            new FlockStore(),
            new EventDispatcher()
        );

        $responseData = \GuzzleHttp\json_decode($response, true);
        $vehile = $evosTripAdapter->extractVehicle($responseData);
        $this->assertTrue($makeAndModel == $vehile->getMakeAndModel());
        $this->assertEquals($color, $vehile->getColor());
        $this->assertEquals($numberPlate, $vehile->getNumberPlate());
    }

    public function extractVehicleDataProvider()
    {
        $response1 = '{"order_car_info":"В 13:15 : AA1196TB Зеленый опель комбо ((093)327-80-61)","driver_phone":"050-123-45-67"}';
        $response2 = '{"order_car_info":"AA1100AA, Черный, Acura, "}';
        $response3 = '{"order_car_info":"AA1100AA, Черный, Acura crv, "}';
        $response4 = '{"order_car_info":"AA1100AA, Черный, Acura, +380666098820"}';
        $response5 = '{"order_car_info":"В 02:15 : WSC08J4 Белый Daewoo Lanos ((093)255-52-83)"}';
        $response6 = '{"order_car_info":"В 08:42 : LSW67HJ Зелёный Rover 75 ((063)780-02-04)"}';
        $response7 = '{"order_car_info":"В 23:58 : SMI88WR зеленый * Opel Corsa ((097)494-56-11)"}';
        $response8 = '{"order_car_info":"В 16:58 : AA7237KX Св.Зеленый Шевроле Авео ((093)086-18-81)"}';
        $response9 = '{"order_car_info":"AAAAAA, РЫЖАЯ, PORSCHE, +380631604224"}';

        return [
            [$response1, 'опель комбо', 'Зеленый', 'AA1196TB'],
            [$response2, 'Acura', 'Черный', 'AA1100AA'],
            [$response3, 'Acura crv', 'Черный', 'AA1100AA'],
            [$response4, 'Acura', 'Черный', 'AA1100AA'],
            [$response5, 'Daewoo Lanos', 'Белый', 'WSC08J4'],
            [$response6, 'Rover 75', 'Зелёный', 'LSW67HJ'],
            [$response7, 'Opel Corsa', 'зеленый', 'SMI88WR'],
            [$response8, 'Шевроле Авео', 'Св.Зеленый', 'AA7237KX'],
            [$response9, 'PORSCHE', 'РЫЖАЯ', 'AAAAAA'],
        ];
    }
}
