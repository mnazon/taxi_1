Run application
```
docker-compose up -d
```
Build
```
docker-compose run --rm --no-deps php-fpm sh -lc 'composer install
```
Run tests
```
docker-compose exec php-fpm ./vendor/bin/phpunit
```

Fix php-cs
```
docker-compose exec php-fpm vendor/bin/php-cs-fixer fix --config=.php_cs.dist
```

Check php-cs
```
docker-compose exec php-fpm vendor/bin/php-cs-fixer fix --dry-run --config=.php_cs.dist
```

build image
```
docker build -f docker/fpm/Dockerfile  .
```

Build migrations
```
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate
```

Doc for api available on
```
http://localhost:11201/api/doc
```