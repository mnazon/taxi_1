<?php

declare(strict_types=1);

namespace App\Api\Centrifugo;

use Centrifugo\Clients\BaseClient;
use Centrifugo\Exceptions\CentrifugoTransportException;
use Centrifugo\Request;

class HttpClient extends BaseClient
{
    /**
     * @var string
     */
    private $apiKey;
    /**
     * @var array
     */
    private $options;

    public function __construct(string $apiKey, array $options = [])
    {
        $this->apiKey = $apiKey;
        $this->options = $options;
    }

    //todo - need to implement async client

    /**
     * {@inheritdoc}
     */
    public function processRequest(Request $request)
    {
        $connection = curl_init();
        try {
            $headers = $request->getHeaders();
            $headers[] = 'Authorization: '.'apikey '.$this->apiKey;
            $options = [
                CURLOPT_URL => $request->getEndpoint(),
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => $request->getEncodedParams(),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
            ];
            $options = $options + $this->options;
            curl_setopt_array($connection, $options);

            $rawResponse = curl_exec($connection);

            if (curl_errno($connection)) {
                throw new CentrifugoTransportException('HttpClient CURL error: '.curl_error($connection));
            }

            $httpCode = curl_getinfo($connection, CURLINFO_HTTP_CODE);

            if (200 != $httpCode) {
                throw new CentrifugoTransportException('HttpClient return invalid response code: '.$httpCode);
            }
        } finally {
            curl_close($connection);
        }

        return $rawResponse;
    }
}
