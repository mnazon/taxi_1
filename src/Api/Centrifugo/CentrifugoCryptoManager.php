<?php

declare(strict_types=1);

namespace App\Api\Centrifugo;

use App\Api\DTO\WebSocketSign;

class CentrifugoCryptoManager
{
    /**
     * @var string
     */
    private $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    public function createClientSign(int $userId, int $timestamp = null): WebSocketSign
    {
        if (null === $timestamp) {
            $timestamp = time();
        }

        $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
        hash_update($ctx, (string) $userId);
        hash_update($ctx, (string) $timestamp);

        return new WebSocketSign(
            $userId,
            $timestamp,
            hash_final($ctx)
        );
    }
}
