<?php

declare(strict_types=1);

namespace App\Api\Security;

use App\Api\Controller\V1\AbstractApiController;
use App\Api\Entity\User;
use App\Api\Enum\ApiErrorCode;
use App\Api\Repository\UserRepository;
use App\Api\Repository\UserTokenRepository;
use App\Api\Service\ApiProblemBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Serializer\SerializerInterface;

class UserTokenAuthenticator extends AbstractGuardAuthenticator
{
    private const USER_AUTH_HEADER = 'X-USER-AUTH-TOKEN';
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    public function __construct(
        SerializerInterface $serializer,
        UserRepository $userRepository,
        UserTokenRepository $userTokenRepository
    ) {
        $this->serializer = $serializer;
        $this->userRepository = $userRepository;
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        return $request->headers->has(self::USER_AUTH_HEADER);
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request): array
    {
        return [
            'token' => $request->headers->get(self::USER_AUTH_HEADER),
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        $token = $this->assertValidCredentials($credentials);

        $userToken = $this->userTokenRepository->findOneByAccessToken($token);
        $user = null !== $userToken ? $userToken->getUser() : null;

        return $user;
    }

    private function assertValidCredentials($credentials): string
    {
        if (!isset($credentials['token'])) {
            throw new \InvalidArgumentException('Invalid credentials instance');
        }

        return $credentials['token'];
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $builder = new ApiProblemBuilder();
        $builder->setApiErrorCode(ApiErrorCode::AUTH_FAILED());
        $apiProblem = $builder->build();

        return $this->serializeAndBuildJsonResponse($apiProblem, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $builder = new ApiProblemBuilder();
        $builder->setApiErrorCode(ApiErrorCode::AUTH_REQUIRED());
        $apiProblem = $builder->build();

        return $this->serializeAndBuildJsonResponse($apiProblem, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }

    private function serializeAndBuildJsonResponse(object $responseModel, int $statusCode): JsonResponse
    {
        $responseData = $this->serializer->serialize(
            $responseModel,
            'json',
            ['groups' => [AbstractApiController::RESPONSE_DEFAULT_GROUP]]
        );

        return new JsonResponse($responseData, $statusCode, [], true);
    }
}
