<?php

declare(strict_types=1);

namespace App\Api\Security;

use App\Api\Controller\V1\AbstractApiController;
use App\Api\DTO\LoginRequest;
use App\Api\DTO\Translation\Message;
use App\Api\Entity\User;
use App\Api\Enum\ApiErrorCode;
use App\Api\Enum\ApiProblemType;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\HttpDTO\Response\ApiProblem;
use App\Api\Repository\UserRepository;
use App\Api\Service\ApiProblemBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserPassAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var RememberMeServicesInterface
     */
    private $rememberMeServices;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder,
        Security $security
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->security = $security;
    }

    public function setRememberMeServices(RememberMeServicesInterface $rememberMeServices)
    {
        $this->rememberMeServices = $rememberMeServices;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        return 'api_user_login' === $request->attributes->get('_route');
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        $loginRequest = $this->serializer->deserialize(
            $request->getContent(),
            LoginRequest::class,
            'json'
        );
        $violations = $this->validator->validate($loginRequest);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        return $loginRequest;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $credentials = $this->assertValidCredentials($credentials);

        return $this->userRepository->findByPhone($credentials->getPhone());
    }

    private function assertValidCredentials($credentials): LoginRequest
    {
        if (!$credentials instanceof LoginRequest) {
            throw new \InvalidArgumentException('Invalid credentials instance');
        }

        return $credentials;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $credentials = $this->assertValidCredentials($credentials);

        return $this->userPasswordEncoder->isPasswordValid(
            $user, $credentials->getPassword()
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $user = $this->security->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('User not found');
        }
        $response = $this->serializeAndBuildJsonResponse($user, Response::HTTP_OK);

        if ($this->rememberMeServices) {
            $this->rememberMeServices->loginSuccess(
                $request,
                $response,
                $token
            );
        }

        return $response;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $builder = new ApiProblemBuilder();
        $builder->setApiErrorCode(ApiErrorCode::AUTH_FAILED());
        $apiProblem = $builder->build();

        return $this->serializeAndBuildJsonResponse($apiProblem, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $apiProblem = new ApiProblem(
            ApiProblemType::AUTH_REQUIRED,
            'Auth required',
            new Message('Auth required')
        );

        return $this->serializeAndBuildJsonResponse($apiProblem, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return true;
    }

    private function serializeAndBuildJsonResponse(object $responseModel, int $statusCode): JsonResponse
    {
        $responseData = $this->serializer->serialize(
            $responseModel,
            'json',
            ['groups' => [AbstractApiController::RESPONSE_DEFAULT_GROUP]]
        );

        return new JsonResponse($responseData, $statusCode, [], true);
    }
}
