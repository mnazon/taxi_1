<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\DTO\RegistrationRequest;
use App\Api\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class UserLoggedIn extends Event
{
    /**
     * @var RegistrationRequest
     */
    private $registrationRequest;

    /**
     * @var User
     */
    private $user;

    public function __construct(RegistrationRequest $registrationRequest, User $user)
    {
        $this->registrationRequest = $registrationRequest;
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getRegistrationRequest()
    {
        return $this->registrationRequest;
    }
}
