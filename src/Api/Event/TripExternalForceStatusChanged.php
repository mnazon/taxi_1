<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use Symfony\Contracts\EventDispatcher\Event;

class TripExternalForceStatusChanged extends Event
{
    /**
     * @var Trip
     */
    private $trip;

    public function __construct(Trip $trip)
    {
        $this->trip = $trip;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }
}
