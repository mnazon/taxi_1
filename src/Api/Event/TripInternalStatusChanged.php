<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;
use Symfony\Contracts\EventDispatcher\Event;

class TripInternalStatusChanged extends Event implements TripStatusChangedInterface
{
    /**
     * @var Trip
     */
    private $trip;
    /**
     * @var TripStatus
     */
    private $toStatus;

    public function __construct(Trip $trip, TripStatus $toStatus)
    {
        $this->trip = $trip;
        $this->toStatus = $toStatus;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function getToStatus(): TripStatus
    {
        return $this->toStatus;
    }
}
