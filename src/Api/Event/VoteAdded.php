<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\TripVote;
use Symfony\Contracts\EventDispatcher\Event;

class VoteAdded extends Event
{
    /**
     * @var TripVote
     */
    private $tripVote;

    public function __construct(TripVote $tripVote)
    {
        $this->tripVote = $tripVote;
    }

    public function getTripVote(): TripVote
    {
        return $this->tripVote;
    }
}
