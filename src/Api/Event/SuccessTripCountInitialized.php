<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use App\Api\Entity\UserSuccessTripCount;
use Symfony\Contracts\EventDispatcher\Event;

class SuccessTripCountInitialized extends Event
{
    /**
     * @var UserSuccessTripCount
     */
    private $successTripCount;
    /**
     * @var Trip
     */
    private $trip;

    public function __construct(UserSuccessTripCount $successTripCount, Trip $trip)
    {
        $this->successTripCount = $successTripCount;
        $this->trip = $trip;
    }

    public function getSuccessTripCount(): UserSuccessTripCount
    {
        return $this->successTripCount;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }
}
