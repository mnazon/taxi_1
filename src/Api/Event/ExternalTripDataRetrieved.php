<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\Entity\Trip;
use Symfony\Contracts\EventDispatcher\Event;

class ExternalTripDataRetrieved extends Event
{
    /**
     * @var ExternalTripResponse
     */
    private $externalTripResponse;
    /**
     * @var Trip
     */
    private $trip;

    public function __construct(ExternalTripResponse $externalTripResponse, Trip $trip)
    {
        $this->externalTripResponse = $externalTripResponse;
        $this->trip = $trip;
    }

    public function getExternalTripResponse(): ExternalTripResponse
    {
        return $this->externalTripResponse;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }
}
