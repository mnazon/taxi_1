<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;
use Symfony\Contracts\EventDispatcher\Event;

class TripCreated extends Event implements TripStatusChangedInterface
{
    /**
     * @var Trip
     */
    private $trip;

    public function __construct(Trip $trip)
    {
        $this->trip = $trip;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function getToStatus(): TripStatus
    {
        return $this->trip->getStatus();
    }
}
