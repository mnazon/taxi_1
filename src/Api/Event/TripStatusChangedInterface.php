<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;

interface TripStatusChangedInterface
{
    public function getTrip(): Trip;

    public function getToStatus(): TripStatus;
}
