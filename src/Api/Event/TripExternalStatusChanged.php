<?php

declare(strict_types=1);

namespace App\Api\Event;

use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;
use Symfony\Contracts\EventDispatcher\Event;

class TripExternalStatusChanged extends Event implements TripStatusChangedInterface
{
    /**
     * @var Trip
     */
    private $trip;
    /**
     * @var TripStatus
     */
    private $fromStatus;
    /**
     * @var TripStatus
     */
    private $toStatus;

    public function __construct(TripStatus $fromStatus, TripStatus $toStatus, Trip $trip)
    {
        $this->trip = $trip;
        $this->fromStatus = $fromStatus;
        $this->toStatus = $toStatus;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function getToStatus(): TripStatus
    {
        return $this->toStatus;
    }

    public function getFromStatus(): TripStatus
    {
        return $this->fromStatus;
    }
}
