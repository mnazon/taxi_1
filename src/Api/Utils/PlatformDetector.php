<?php

namespace App\Api\Utils;

use App\Api\Enum\DevicePlatform;
use Symfony\Component\HttpFoundation\Request;

class PlatformDetector
{
    public function detectPlatformByRequest(Request $request): ?DevicePlatform
    {
        $userAgent = $request->headers->get('user_agent');
        $platform = null;
        if (preg_match('/(Android|okhttp)/i', $userAgent)) {
            $platform = DevicePlatform::ANDROID();
        } elseif (preg_match('/(iOS|iPhone|iPad|iPod)/i', $userAgent)) {
            $platform = DevicePlatform::IOS();
        }

        return $platform;
    }
}
