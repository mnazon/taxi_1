<?php

declare(strict_types=1);

namespace App\Api\Utils;

use App\Api\DTO\AppVersionDto;
use Symfony\Component\HttpFoundation\Request;

class AppVersionDetector
{
    private const APP_VERSION_HEADER = 'X-APP-VERSION';

    public function detectByRequest(Request $request): ?AppVersionDto
    {
        $appVersion = $request->headers->get(self::APP_VERSION_HEADER);
        $platform = (new PlatformDetector())->detectPlatformByRequest($request);
        if (is_numeric($appVersion) && $platform) {
            return new AppVersionDto($platform, (int) $appVersion);
        }

        return null;
    }
}
