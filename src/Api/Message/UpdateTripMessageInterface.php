<?php

namespace App\Api\Message;

use App\Api\Enum\TripStatus;
use Money\Money;

interface UpdateTripMessageInterface
{
    public function getOldTripStatus(): TripStatus;

    public function getTripId(): int;

    public function getNewTripStatus(): TripStatus;

    public function getCancelReason(): ?\App\Api\Enum\CancelReason;

    public function getTaxiProviderId(): int;

    public function getPosition(): ?\App\Api\DTO\Trip\Position;

    public function getArriveAt(): ?\DateTimeImmutable;

    public function getCarInfo(): ?\App\Api\DTO\Vehicle;

    public function getDriverPhone(): ?string;

    public function getCost(): Money;

    public function getIsInArchive(): ?bool;
}
