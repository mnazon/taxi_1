<?php

namespace App\Api\Message;

use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\Enum\TripStatus;
use Money\Money;

class UpdateTripMessage implements UpdateTripMessageInterface
{
    /**
     * @var TripStatus
     */
    private $oldTripStatus;
    /**
     * @var int
     */
    private $tripId;
    /**
     * @var TripStatus
     */
    private $newTripStatus;
    /**
     * @var \App\Api\Enum\CancelReason|null
     */
    private $cancelReason;
    /**
     * @var int
     */
    private $taxiProviderId;
    /**
     * @var \App\Api\DTO\Trip\Position|null
     */
    private $position;
    /**
     * @var \DateTimeImmutable|null
     */
    private $arriveAt;
    /**
     * @var \App\Api\DTO\Vehicle|null
     */
    private $carInfo;
    /**
     * @var string|null
     */
    private $driverPhone;
    /**
     * @var Money
     */
    private $cost;
    /**
     * @var bool|null
     */
    private $isInArchive;

    public static function fromResponse(ExternalTripResponse $externalTripResponse, int $tripId, TripStatus $oldTripStatus)
    {
        $o = new self();
        $o->oldTripStatus = $oldTripStatus;
        $o->tripId = $tripId;
        $o->newTripStatus = $externalTripResponse->getTripStatus();
        $o->cancelReason = $externalTripResponse->getCancelReason();
        $o->taxiProviderId = $externalTripResponse->getTaxiProviderId();
        $o->position = $externalTripResponse->getPosition();
        $o->arriveAt = $externalTripResponse->getArriveAt();
        $o->carInfo = $externalTripResponse->getCarInfo();
        $o->driverPhone = $externalTripResponse->getDriverPhone();
        $o->cost = $externalTripResponse->getCost();
        $o->isInArchive = $externalTripResponse->isInArchive();

        return $o;
    }

    public function getOldTripStatus(): TripStatus
    {
        return $this->oldTripStatus;
    }

    public function getTripId(): int
    {
        return $this->tripId;
    }

    public function getNewTripStatus(): TripStatus
    {
        return $this->newTripStatus;
    }

    public function getCancelReason(): ?\App\Api\Enum\CancelReason
    {
        return $this->cancelReason;
    }

    public function getTaxiProviderId(): int
    {
        return $this->taxiProviderId;
    }

    public function getPosition(): ?\App\Api\DTO\Trip\Position
    {
        return $this->position;
    }

    public function getArriveAt(): ?\DateTimeImmutable
    {
        return $this->arriveAt;
    }

    public function getCarInfo(): ?\App\Api\DTO\Vehicle
    {
        return $this->carInfo;
    }

    public function getDriverPhone(): ?string
    {
        return $this->driverPhone;
    }

    public function getCost(): Money
    {
        return $this->cost;
    }

    public function getIsInArchive(): ?bool
    {
        return $this->isInArchive;
    }

    public function setOldTripStatus(TripStatus $oldTripStatus): self
    {
        $this->oldTripStatus = $oldTripStatus;

        return $this;
    }

    public function setTripId(int $tripId): self
    {
        $this->tripId = $tripId;

        return $this;
    }

    public function setNewTripStatus(TripStatus $newTripStatus): self
    {
        $this->newTripStatus = $newTripStatus;

        return $this;
    }

    public function setCancelReason(?\App\Api\Enum\CancelReason $cancelReason): self
    {
        $this->cancelReason = $cancelReason;

        return $this;
    }

    public function setTaxiProviderId(int $taxiProviderId): self
    {
        $this->taxiProviderId = $taxiProviderId;

        return $this;
    }

    public function setPosition(?\App\Api\DTO\Trip\Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function setArriveAt(?\DateTimeImmutable $arriveAt): self
    {
        $this->arriveAt = $arriveAt;

        return $this;
    }

    public function setCarInfo(?\App\Api\DTO\Vehicle $carInfo): self
    {
        $this->carInfo = $carInfo;

        return $this;
    }

    public function setDriverPhone(?string $driverPhone): self
    {
        $this->driverPhone = $driverPhone;

        return $this;
    }

    public function setCost(Money $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function setIsInArchive(?bool $isInArchive): self
    {
        $this->isInArchive = $isInArchive;

        return $this;
    }
}
