<?php

declare(strict_types=1);

namespace App\Api\DBAL\Types;

use App\Api\Enum\TaxiAdditionalService;

class AdditionalServicesType extends \App\DBAL\Types\AbstractSetType
{
    public static function getChoices(): array
    {
        return [
            TaxiAdditionalService::COURIER_DELIVERY => TaxiAdditionalService::COURIER_DELIVERY,
            TaxiAdditionalService::BAGGAGE => TaxiAdditionalService::BAGGAGE,
            TaxiAdditionalService::CONDITIONER => TaxiAdditionalService::CONDITIONER,
            TaxiAdditionalService::ANIMAL => TaxiAdditionalService::ANIMAL,
        ];
    }
}
