<?php

namespace App\Api\MessageHandler;

use App\Api\Message\TestMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class TestMessageHandler implements MessageHandlerInterface
{
    public function __invoke(TestMessage $message)
    {
        printf('Handled message "%s" at %s.'.PHP_EOL, $message->getContent(), date(DATE_ATOM));
    }
}
