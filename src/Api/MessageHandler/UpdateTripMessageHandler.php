<?php

namespace App\Api\MessageHandler;

use App\Api\Message\UpdateTripMessageInterface;
use App\Api\Service\Trip\UpdateTripHandler;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateTripMessageHandler implements MessageHandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var UpdateTripHandler
     */
    private $updateTripHandler;

    public function __construct(UpdateTripHandler $updateTripHandler)
    {
        $this->updateTripHandler = $updateTripHandler;

        $this->logger = new NullLogger();
    }

    public function __invoke(UpdateTripMessageInterface $message)
    {
        $this->updateTripHandler->handle($message);
    }
}
