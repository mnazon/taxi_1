<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class DeviceData implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="64")
     * @Groups("api")
     * @SWG\Property(description="unique device token, that will generate for each device the same at ")
     *
     * @var string
     */
    private $deviceToken;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="100")
     * @Groups("api")
     *
     * @var string
     */
    private $firebaseRegistrationToken;

    public function __construct(string $deviceToken = '', string $firebaseRegistrationToken = '')
    {
        $this->deviceToken = $deviceToken;
        $this->firebaseRegistrationToken = $firebaseRegistrationToken;
    }

    public function getDeviceToken(): string
    {
        return $this->deviceToken;
    }

    public function getFirebaseRegistrationToken(): string
    {
        return $this->firebaseRegistrationToken;
    }
}
