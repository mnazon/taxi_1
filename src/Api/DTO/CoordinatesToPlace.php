<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\DTO\Geo\Coordinates;
use App\Api\Entity\Place;

class CoordinatesToPlace
{
    /**
     * @var Coordinates
     */
    private $coordinates;
    /**
     * @var Place
     */
    private $place;

    public function __construct(Coordinates $coordinates, Place $place)
    {
        $this->coordinates = $coordinates;
        $this->place = $place;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function withPlace(Place $place): self
    {
        return new self($this->coordinates, $place);
    }
}
