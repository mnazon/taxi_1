<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\DTO\Geo\Coordinates;
use App\Api\Enum\CarStatus;

class CarPosition
{
    /**
     * @var Coordinates
     */
    private $coordinates;
    /**
     * @var CarStatus
     */
    private $status;

    public function __construct(Coordinates $coordinates, CarStatus $status)
    {
        $this->coordinates = $coordinates;
        $this->status = $status;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getStatus(): CarStatus
    {
        return $this->status;
    }
}
