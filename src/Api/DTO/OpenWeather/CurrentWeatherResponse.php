<?php

declare(strict_types=1);

namespace App\Api\DTO\OpenWeather;

class CurrentWeatherResponse
{
    /**
     * @var int
     */
    private $temperature;
    /**
     * @var int
     */
    private $windSpeed;
    /**
     * @var float
     */
    private $precipitationRain1h;
    /**
     * @var float
     */
    private $precipitationRain3h;
    /**
     * @var float
     */
    private $precipitationSnow1h;
    /**
     * @var float
     */
    private $precipitationSnow3h;
    /**
     * @var int
     */
    private $cityId;
    /**
     * @var int
     */
    private $visibility;
    /**
     * @var int
     */
    private $weatherId;
    /**
     * @var array
     */
    private $weather;

    public function __construct(
        int $cityId,
        int $temperature,
        int $windSpeed,
        int $visibility,
        int $weatherId,
        array $weather,
        ?float $precipitationRain1h,
        ?float $precipitationRain3h,
        ?float $precipitationSnow1h,
        ?float $precipitationSnow3h
    ) {
        $this->cityId = $cityId;
        $this->temperature = $temperature;
        $this->windSpeed = $windSpeed;
        $this->precipitationRain1h = $precipitationRain1h;
        $this->precipitationRain3h = $precipitationRain3h;
        $this->precipitationSnow1h = $precipitationSnow1h;
        $this->precipitationSnow3h = $precipitationSnow3h;
        $this->visibility = $visibility;
        $this->weatherId = $weatherId;
        $this->weather = $weather;
    }

    public function getTemperature(): ?int
    {
        return $this->temperature;
    }

    public function getWindSpeed(): ?int
    {
        return $this->windSpeed;
    }

    public function getPrecipitationRain1h(): ?float
    {
        return $this->precipitationRain1h;
    }

    public function getPrecipitationRain3h(): ?float
    {
        return $this->precipitationRain3h;
    }

    public function getPrecipitationSnow1h(): ?float
    {
        return $this->precipitationSnow1h;
    }

    public function getPrecipitationSnow3h(): ?float
    {
        return $this->precipitationSnow3h;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function getVisibility(): int
    {
        return $this->visibility;
    }

    public function getWeatherId(): int
    {
        return $this->weatherId;
    }

    public function getWeather(): array
    {
        return $this->weather;
    }
}
