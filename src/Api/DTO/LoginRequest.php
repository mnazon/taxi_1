<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class LoginRequest
{
    /**
     * @Assert\NotBlank()
     * @PhoneNumber(type="mobile")
     *
     * @var string
     * @Groups({"api"})
     */
    private $phone;
    /**
     * @Assert\NotBlank()
     *
     * @var string
     * @Groups({"api"})
     */
    private $password;

    public function __construct(string $phone = '', string $password = '')
    {
        $this->phone = $phone;
        $this->password = $password;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
