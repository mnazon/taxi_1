<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Enum\DevicePlatform;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationRequest implements ArgumentResolvableInterface, IpAwareInterface
{
    use IpAwareTrait;

    /**
     * @Assert\NotBlank()
     * @AssertPhoneNumber(type="mobile")
     * @Groups({"api"})
     *
     * @var string
     */
    private $phone;

    /**
     * @Assert\Range(min="1000", max="9999")
     * @Assert\NotBlank()
     * @Groups({"api"})
     *
     * @var string
     */
    private $confirmationCode;

    /**
     * @Assert\Choice(callback={"\App\Api\Enum\DevicePlatform", "validValues"})
     * @Groups({"api"})
     *
     * @var string|null
     */
    private $platform;

    public function __construct(string $phone = '', string $confirmationCode = '', ?string $platform = null)
    {
        $this->phone = $phone;
        $this->confirmationCode = $confirmationCode;
        $this->platform = $platform;
    }

    public function getConfirmationCode(): string
    {
        return $this->confirmationCode;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getPlatform(): ?DevicePlatform
    {
        if (\is_string($this->platform)) {
            $this->platform = DevicePlatform::createFromLiteral($this->platform);
        }

        return $this->platform;
    }

    public function setPlatform(DevicePlatform $platform): void
    {
        $this->platform = $platform;
    }
}
