<?php

declare(strict_types=1);

namespace App\Api\DTO;

class User
{
    /**
     * @var string
     */
    private $fullName;
    /**
     * @var string
     */
    private $phone;

    public function __construct(string $fullName, string $phone)
    {
        $this->fullName = $fullName;
        $this->phone = $phone;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
