<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

class Position implements \JsonSerializable
{
    /**
     * @var float
     */
    private $lat;
    /**
     * @var float
     */
    private $lng;
    /**
     * @var string
     */
    private $status;
    /**
     * @var int|null
     */
    private $bearing;
    /**
     * @var int|null
     */
    private $speed;

    public function __construct(float $lat, float $lng, string $status, int $bearing = null, int $speed = null)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->status = $status;
        $this->bearing = $bearing;
        $this->speed = $speed;
    }

    public function jsonSerialize()
    {
        return [
            'lat' => $this->lat,
            'lng' => $this->lng,
            'status' => $this->status,
            'bearing' => $this->bearing,
            'speed' => $this->speed,
        ];
    }
}
