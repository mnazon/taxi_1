<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use Symfony\Component\Validator\Constraints as Assert;

class AdditionalCostRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     *
     * @var int|null
     */
    private $tripId;
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     * @Assert\Range(min="0", max="1000")
     *
     * @var float|null
     */
    private $additionalCost;

    /**
     * @param int   $tripId
     * @param float $additionalCost
     */
    public function __construct($tripId, $additionalCost)
    {
        $this->tripId = $tripId;
        $this->additionalCost = $additionalCost;
    }

    public function getTripId(): int
    {
        return (int) $this->tripId;
    }

    public function getAdditionalCost(): float
    {
        return (float) $this->additionalCost;
    }
}
