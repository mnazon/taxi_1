<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\DTO\WebSocketSign;
use Symfony\Component\Serializer\Annotation\Groups;

class CreateTripResponse
{
    /**
     * @var int
     * @Groups("api")
     */
    private $tripId;
    /**
     * @var string
     * @Groups("api")
     */
    private $webSocketChannel;
    /**
     * @var WebSocketSign
     * @Groups("api")
     */
    private $webSocketSign;

    public function __construct(
        int $tripId,
        string $webSocketChannel,
        WebSocketSign $webSocketSign
    ) {
        $this->tripId = $tripId;
        $this->webSocketChannel = $webSocketChannel;
        $this->webSocketSign = $webSocketSign;
    }

    public function getTripId(): int
    {
        return $this->tripId;
    }

    public function getWebSocketChannel(): string
    {
        return $this->webSocketChannel;
    }

    public function getWebSocketSign(): WebSocketSign
    {
        return $this->webSocketSign;
    }
}
