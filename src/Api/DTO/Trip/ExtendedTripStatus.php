<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;

class ExtendedTripStatus
{
    /** @var TripStatus */
    private $tripStatus;
    /** @var CancelReason|null */
    private $cancelReason;

    public function __construct(TripStatus $tripStatus, ?CancelReason $cancelReason)
    {
        $this->tripStatus = $tripStatus;
        $this->cancelReason = $cancelReason;
    }

    public function getTripStatus(): TripStatus
    {
        return $this->tripStatus;
    }

    public function getCancelReason(): ?CancelReason
    {
        return $this->cancelReason;
    }
}
