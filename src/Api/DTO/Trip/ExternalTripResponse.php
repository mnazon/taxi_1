<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\DTO\Vehicle;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use Money\Money;

class ExternalTripResponse
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var TripStatus
     */
    private $tripStatus;
    /**
     * @var int
     */
    private $taxiProviderId;
    /**
     * @var Position|null
     */
    private $position;
    /**
     * @var \DateTimeImmutable|null
     */
    private $arriveAt;
    /**
     * @var Vehicle|null
     */
    private $carInfo;
    /**
     * @var string|null
     */
    private $driverPhone;
    /**
     * @var ExtendedTripStatus
     */
    private $extendedTripStatus;
    /**
     * @var Money
     */
    private $cost;
    /**
     * @var bool|null
     */
    private $isInArchive;

    public function __construct(
        string $id,
        int $taxiProviderId,
        ExtendedTripStatus $extendedTripStatus,
        Money $cost,
        ?\DateTimeImmutable $arriveAt = null,
        ?Vehicle $carInfo = null,
        ?string $driverPhone = null,
        ?Position $position = null,
        ?bool $isInArchive = null //this field for evos provider - maybe better to replace in some payload data
    ) {
        $this->id = $id;
        $this->tripStatus = $extendedTripStatus->getTripStatus();
        $this->taxiProviderId = $taxiProviderId;
        $this->position = $position;
        $this->arriveAt = $arriveAt;
        $this->carInfo = $carInfo;
        $this->driverPhone = $driverPhone;
        $this->extendedTripStatus = $extendedTripStatus;
        $this->cost = $cost;
        $this->isInArchive = $isInArchive;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTripStatus(): TripStatus
    {
        return $this->tripStatus;
    }

    public function getCancelReason(): ?CancelReason
    {
        return $this->extendedTripStatus->getCancelReason();
    }

    public function getTaxiProviderId(): int
    {
        return $this->taxiProviderId;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function getArriveAt(): ?\DateTimeImmutable
    {
        return $this->arriveAt;
    }

    public function getCarInfo(): ?Vehicle
    {
        return $this->carInfo;
    }

    public function getDriverPhone(): ?string
    {
        return $this->driverPhone;
    }

    public function getCost(): Money
    {
        return $this->cost;
    }

    public function isInArchive(): ?bool
    {
        return $this->isInArchive;
    }
}
