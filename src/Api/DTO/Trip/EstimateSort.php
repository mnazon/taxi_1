<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\Enum\EstimateSortField;
use App\Api\Enum\SortDirection;
use Symfony\Component\Validator\Constraints as Assert;

class EstimateSort
{
    /**
     * @Assert\Choice(callback={"\App\Api\Enum\EstimateSortField", "toArray"}, choices="{1,2,4,5,6}")
     *
     * @var string
     */
    private $field;
    /**
     * @Assert\Choice(callback={"\App\Api\Enum\SortDirection", "toArray"})
     *
     * @var string
     */
    private $direction;

    public function __construct(string $field, string $direction)
    {
        $this->field = $field;
        $this->direction = $direction;
    }

    public function getField(): EstimateSortField
    {
        return new EstimateSortField($this->field);
    }

    public function getDirection(): SortDirection
    {
        return new SortDirection($this->direction);
    }
}
