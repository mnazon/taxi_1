<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

class ExternalTripCollection
{
    /**
     * @var ExternalTripResponse[]
     */
    private $data = [];

    public function add(ExternalTripResponse $tripResponse)
    {
        $this->data[] = $tripResponse;
    }

    /**
     * @return ExternalTripResponse[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function find(string $externalId, int $taxiProviderId): ?ExternalTripResponse
    {
        $dataToIterate = $this->data;
        foreach ($dataToIterate as $item) {
            $isFounded = $item->getId() === $externalId
                && $item->getTaxiProviderId() === $taxiProviderId;

            if ($isFounded) {
                return $item;
            }
        }

        return null;
    }
}
