<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\DTO\ArgumentResolvableInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition(description="We cancel trip with trip cancel request object")
 */
class TripCancelRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\Choice(callback={"\App\Api\Enum\TripCancellationReason", "toArray"})
     * @Groups("api")
     *
     * @var int
     */
    private $tripCancellationReason;

    /**
     * @Assert\Type("string")
     * @Groups("api")
     *
     * @var string|null
     */
    private $message;

    public function getTripCancellationReason(): int
    {
        return $this->tripCancellationReason;
    }

    /**
     * @return TripCancelRequest
     */
    public function setTripCancellationReason(int $tripCancellationReason): self
    {
        $this->tripCancellationReason = $tripCancellationReason;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return TripCancelRequest
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
