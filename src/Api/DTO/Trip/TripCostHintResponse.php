<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use Symfony\Component\Serializer\Annotation\Groups;

class TripCostHintResponse
{
    /**
     * @Groups("api")
     *
     * @var float
     */
    private $baseCost;

    /**
     * @Groups("api")
     *
     * @var float
     */
    private $additionalCost;

    public function __construct(float $baseCost, float $additionalCost)
    {
        $this->baseCost = $baseCost;
        $this->additionalCost = $additionalCost;
    }

    public function getBaseCost(): float
    {
        return $this->baseCost;
    }

    public function getAdditionalCost(): float
    {
        return $this->additionalCost;
    }
}
