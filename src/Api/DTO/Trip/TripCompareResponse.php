<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use Symfony\Component\Serializer\Annotation\Groups;

class TripCompareResponse
{
    /**
     * @Groups("api")
     *
     * @var bool
     */
    private $isPriceChanged;
    /**
     * @Groups("api")
     *
     * @var float
     */
    private $baseCost;
    /**
     * @Groups("api")
     *
     * @var float
     */
    private $additionalCost;

    public function __construct(bool $isPriceChanged, float $baseCost, float $additionalCost)
    {
        $this->isPriceChanged = $isPriceChanged;
        $this->baseCost = $baseCost;
        $this->additionalCost = $additionalCost;
    }

    public function isPriceChanged(): bool
    {
        return $this->isPriceChanged;
    }

    public function getBaseCost(): float
    {
        return $this->baseCost;
    }

    public function getAdditionalCost(): float
    {
        return $this->additionalCost;
    }
}
