<?php

declare(strict_types=1);

namespace App\Api\DTO\Trip;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\HttpDTO\EstimateTripRequest;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition(description="We compare summary price with trip request object")
 */
class TripCompareRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     * @SWG\Property(description="summary price")
     * @Groups("api")
     *
     * @var float
     */
    private $baseCost;
    /**
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @Groups("api")
     * @SWG\Property(description="trip request object for comparing")
     *
     * @var EstimateTripRequest
     */
    private $trip;
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     * @Assert\Type(type="int")
     * @Groups("api")
     *
     * @var int
     */
    private $taxiProviderId;

    public function getBaseCost(): float
    {
        return $this->baseCost;
    }

    public function setBaseCost(float $price)
    {
        $this->baseCost = $price;
    }

    public function getTrip(): EstimateTripRequest
    {
        return $this->trip;
    }

    public function setTrip(EstimateTripRequest $trip)
    {
        $this->trip = $trip;
    }

    public function getTaxiProviderId(): int
    {
        return $this->taxiProviderId;
    }

    public function setTaxiProviderId(int $taxiProviderId)
    {
        $this->taxiProviderId = $taxiProviderId;
    }
}
