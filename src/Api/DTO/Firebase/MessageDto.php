<?php

declare(strict_types=1);

namespace App\Api\DTO\Firebase;

use App\Api\Entity\NotificationToken;
use Kreait\Firebase\Messaging\AndroidConfig;
use Kreait\Firebase\Messaging\ApnsConfig;
use Kreait\Firebase\Messaging\MessageData;
use Kreait\Firebase\Messaging\Notification;

class MessageDto
{
    /**
     * @var NotificationToken
     */
    private $notificationToken;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $body;
    /**
     * @var array
     */
    private $data;
    /**
     * @var array
     */
    private $androidConfig;
    /**
     * @var array
     */
    private $apnsConfig;

    public function __construct(
        NotificationToken $notificationToken,
        string $title,
        ?string $body = null,
        array $data = [],
        array $androidConfig = [],
        array $apnsConfig = []
    ) {
        $this->notificationToken = $notificationToken;
        $this->title = $title;
        $this->body = $body;
        $this->data = $data;
        $this->androidConfig = $androidConfig;
        $this->apnsConfig = $apnsConfig;
    }

    public function getNotificationToken(): NotificationToken
    {
        return $this->notificationToken;
    }

    public function getNotification(): Notification
    {
        return Notification::create($this->title, $this->body);
    }

    public function getMessageData(): ?MessageData
    {
        return $this->data ? MessageData::fromArray($this->data) : null;
    }

    public function getAndroidConfig(): ?AndroidConfig
    {
        return $this->androidConfig ? AndroidConfig::fromArray($this->androidConfig) : null;
    }

    public function getApnsConfig(): ?ApnsConfig
    {
        return $this->apnsConfig ? ApnsConfig::fromArray($this->apnsConfig) : null;
    }
}
