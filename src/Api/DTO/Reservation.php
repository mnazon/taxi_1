<?php

declare(strict_types=1);

namespace App\Api\DTO;

class Reservation
{
    /**
     * @var \DateTime
     */
    private $dateTime;

    public function __construct(\DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }
}
