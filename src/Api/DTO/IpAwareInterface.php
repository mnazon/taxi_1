<?php

declare(strict_types=1);

namespace App\Api\DTO;

interface IpAwareInterface
{
    public function getIp(): ?string;

    public function getIpLong(): ?int;

    public function setIp(string $ip);
}
