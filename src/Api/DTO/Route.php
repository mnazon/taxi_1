<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\DTO\Geo\Place;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class Route
{
    /**
     * @Assert\Count(min="2")
     * @Groups("api")
     *
     * @var Place[]
     */
    private $places = [];
    /**
     * @Groups("api")
     *
     * @var string|null
     */
    private $entranceFrom;

    /**
     * @return Place[]
     */
    public function getPoints(): ?array
    {
        return $this->places;
    }

    /**
     * @return Place[]
     */
    public function getPlaces(): array
    {
        return $this->places;
    }

    public function addPlace(Place $point)
    {
        $this->places[] = $point;
    }

    public function hasPlaces(): bool
    {
        return \count($this->places) > 0;
    }

    /**
     * @param Place[] $places
     *
     * @return Route
     */
    public function setPlaces($places): self
    {
        foreach ($places as $place) {
            if (!$place instanceof Place) {
                throw new \InvalidArgumentException('Wrong instance of place setup');
            }
        }
        $this->places = $places;

        return $this;
    }

    public function getEntranceFrom(): ?string
    {
        return $this->entranceFrom;
    }

    /**
     * @return Route
     */
    public function setEntranceFrom(?string $entranceFrom): self
    {
        $this->entranceFrom = $entranceFrom;

        return $this;
    }
}
