<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Enum\TaxiProvider;

class CreateOrderRequest
{
    /**
     * @var CostRequest
     */
    private $costRequest;
    /**
     * @var string
     */
    private $taxiProvider;

    public function setTaxiProvider(string $taxiProvider)
    {
        $this->taxiProvider = $taxiProvider;
    }

    public function getTaxiProvider(): TaxiProvider
    {
        return new TaxiProvider($this->taxiProvider);
    }

    public function setCostRequest(CostRequest $costRequest)
    {
        $this->costRequest = $costRequest;
    }

    public function getCostRequest(): CostRequest
    {
        return $this->costRequest;
    }
}
