<?php

declare(strict_types=1);

namespace App\Api\DTO;

class OrderCostCollection
{
    private $data = [];

    public function add(EstimatedTripItem $orderCostResponse)
    {
        $this->data[] = $orderCostResponse;
    }

    /**
     * @return EstimatedTripItem[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
