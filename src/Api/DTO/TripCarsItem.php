<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Entity\Trip;
use Doctrine\Common\Collections\ArrayCollection;

class TripCarsItem
{
    /**
     * @var ArrayCollection
     */
    private $carPositions;
    /**
     * @var Trip
     */
    private $trip;

    public function __construct(Trip $trip)
    {
        $this->trip = $trip;
        $this->carPositions = new ArrayCollection();
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function addCarPosition(CarPosition $carPosition): self
    {
        if (!$this->carPositions->contains($carPosition)) {
            $this->carPositions->add($carPosition);
        }

        return $this;
    }

    public function getCarPositions(): ArrayCollection
    {
        return $this->carPositions;
    }
}
