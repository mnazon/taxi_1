<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Enum\DevicePlatform;

class AppVersionDto
{
    /**
     * @var DevicePlatform
     */
    private $platform;
    /**
     * @var int
     */
    private $appVersion;

    public function __construct(DevicePlatform $platform, int $appVersion)
    {
        $this->platform = $platform;
        $this->appVersion = $appVersion;
    }

    public function getPlatform(): DevicePlatform
    {
        return $this->platform;
    }

    public function getAppVersion(): int
    {
        return $this->appVersion;
    }
}
