<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Entity\TaxiProvider;
use App\Api\Enum\CurrencySign;
use Symfony\Component\Serializer\Annotation\Groups;

class EstimatedTripItem
{
    /**
     * @var string
     * @Groups("api")
     */
    private $id;
    /**
     * @var float
     * @Groups("api")
     */
    private $cost;
    /**
     * @var string
     * @Groups("api")
     */
    private $currency;
    /**
     * @todo remove this logic, make instead - Money object and use it with cost and CurrencyObject
     * @Groups({"api"})
     *
     * @var string
     */
    private $currencySign = CurrencySign::UKR;
    /**
     * @var int
     * @Groups("api")
     */
    private $percentage;
    /**
     * @var TaxiProvider
     * @Groups("api")
     */
    private $taxiProvider;

    public function __construct(string $id, float $cost, string $currency, TaxiProvider $taxiProvider)
    {
        $this->id = $id;
        $this->cost = $cost;
        $this->currency = $currency;
        $this->taxiProvider = $taxiProvider;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCost(): float
    {
        return $this->cost;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getTaxiProvider(): TaxiProvider
    {
        return $this->taxiProvider;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    /**
     * @return EstimatedTripItem
     */
    public function setPercentage(int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getCurrencySign(): string
    {
        return $this->currencySign;
    }
}
