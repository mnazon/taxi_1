<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class AddVoteDto implements ArgumentResolvableInterface
{
    /**
     * @Groups("api")
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     *
     * @var int
     */
    private $tripId;
    /**
     * @Groups("api")
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="5")
     *
     * @var int
     */
    private $value;
    /**
     * @Groups("api")
     * @Assert\Length(max="2000")
     *
     * @var string|null
     */
    private $comment;

    public function __construct(int $tripId = 0, int $value = 0, ?string $comment = null)
    {
        $this->tripId = $tripId;
        $this->value = $value;
        $this->comment = $comment;
    }

    public function getTripId(): int
    {
        return $this->tripId;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
