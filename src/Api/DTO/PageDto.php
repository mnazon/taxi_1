<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class PageDto implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     *
     * @var int
     */
    private $page;
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     *
     * @var int
     */
    private $perPage;

    public function __construct(int $page = 1, int $perPage = 20)
    {
        $this->page = $page;
        $this->perPage = $perPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getLimit(): int
    {
        return $this->perPage;
    }

    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }
}
