<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

class WebSocketSign
{
    /**
     * @var int
     * @Groups("api")
     */
    private $userId;
    /**
     * @var int
     * @Groups("api")
     */
    private $timestamp;
    /**
     * @var string
     * @Groups("api")
     */
    private $websocketToken;

    public function __construct(int $userId, int $timestamp, string $websocketToken)
    {
        $this->userId = $userId;
        $this->timestamp = $timestamp;
        $this->websocketToken = $websocketToken;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getWebsocketToken(): string
    {
        return $this->websocketToken;
    }
}
