<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Enum\CarType;
use Symfony\Component\Validator\Constraints as Assert;

class CostRequest
{
    /**
     * @Assert\Choice(callback={"\App\Api\Enum\CarType", "toArray"})
     *
     * @var string
     */
    private $carType;
    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     *
     * @var Route
     */
    private $route;
    /**
     * @var Reservation|null
     */
    private $reservation;
    /**
     * @var User|null
     */
    private $user;

    public function __construct(
        string $carType,
        Route $route,
        ?Reservation $reservation = null,
        ?User $user = null
    ) {
        $this->carType = $carType;
        $this->route = $route;
        $this->reservation = $reservation;
        $this->user = $user;
    }

    /**
     * @param Route $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    public function setCarType(string $carType)
    {
        $this->carType = $carType;
    }

    public function setReservation(?Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    public function setUser(?User $user)
    {
        $this->user = $user;
    }

    public function getCarType(): CarType
    {
        return new CarType($this->carType);
    }

    public function getRoute(): Route
    {
        return $this->route;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}
