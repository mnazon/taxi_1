<?php

declare(strict_types=1);

namespace App\Api\DTO\Translation;

class Message
{
    /**
     * @var string
     */
    private $messageTemplate;
    /**
     * @var array
     */
    private $parameters;
    /**
     * @var int
     */
    private $plural;
    /**
     * @var string
     */
    private $domain;

    public function __construct(
        string $messageTemplate,
        array $parameters = [],
        int $plural = null,
        string $domain = null
    ) {
        $this->messageTemplate = $messageTemplate;
        $this->parameters = $parameters;
        $this->plural = $plural;
        $this->domain = $domain;
    }

    public function getMessageTemplate(): string
    {
        return $this->messageTemplate;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return int
     */
    public function getPlural(): ?int
    {
        return $this->plural;
    }

    /**
     * @return string
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }
}
