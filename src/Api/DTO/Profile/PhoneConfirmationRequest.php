<?php

declare(strict_types=1);

namespace App\Api\DTO\Profile;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\IpAwareInterface;
use App\Api\DTO\IpAwareTrait;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class PhoneConfirmationRequest implements ArgumentResolvableInterface, IpAwareInterface
{
    use IpAwareTrait;
    /**
     * @AssertPhoneNumber(type="mobile")
     * @Groups({"api"})
     *
     * @var string
     */
    private $phone;
    /**
     * @Assert\Range(min="1000", max="9999")
     * @Assert\NotBlank()
     * @Groups({"api"})
     *
     * @var string
     */
    private $confirmationCode;

    public function __construct(string $phone = '', string $confirmationCode = '')
    {
        $this->phone = $phone;
        $this->confirmationCode = $confirmationCode;
    }

    public function getConfirmationCode(): string
    {
        return $this->confirmationCode;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
