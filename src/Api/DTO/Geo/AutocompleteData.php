<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @SWG\Definition(description="coordinates can be null, if coordinates null - get coordinates by placeId")
 */
class AutocompleteData
{
    /**
     * @Groups("api")
     *
     * @var string
     */
    private $mainText;
    /**
     * @Groups("api")
     *
     * @var string
     */
    private $secondaryText;
    /**
     * @Groups("api")
     *
     * @var string
     */
    private $placeId;
    /**
     * @Groups("api")
     *
     * @var bool
     */
    private $isExactAddress;
    /**
     * @var string|null
     */
    private $phantomStreetAddress;
    /**
     * @Groups("api")
     *
     * @var Coordinates|null
     */
    private $coordinates;

    public function __construct(
        string $mainText,
        string $secondaryText,
        string $placeId,
        bool $isExactAddress = true,
        ?string $phantomStreetAddress = null,
        ?Coordinates $coordinates = null
    ) {
        $this->mainText = $mainText;
        $this->secondaryText = $secondaryText;
        $this->placeId = $placeId;
        $this->isExactAddress = $isExactAddress;
        $this->phantomStreetAddress = $phantomStreetAddress;
        $this->coordinates = $coordinates;
    }

    public function getMainText(): string
    {
        return $this->mainText;
    }

    /**
     * @Groups("api")
     */
    public function getSelectedText(): string
    {
        if ($this->phantomStreetAddress) {
            $result = sprintf('%s, %s', $this->mainText, $this->phantomStreetAddress);
        } else {
            if (!$this->isExactAddress) {
                $result = sprintf('%s, ', $this->mainText);
            } else {
                $result = $this->mainText;
            }
        }

        return $result;
    }

    /**
     * @param bool $withPhantormAddress
     */
    public function getSecondaryText($withPhantormAddress = true): string
    {
        $prepend = '';
        if ($withPhantormAddress && !empty($this->phantomStreetAddress)) {
            $prepend = $this->phantomStreetAddress.', ';
        }

        return $prepend.$this->secondaryText;
    }

    public function getPlaceId(): string
    {
        return $this->placeId;
    }

    public function isExactAddress(): bool
    {
        return $this->isExactAddress;
    }

    public function getPhantomStreetAddress(): ?string
    {
        return $this->phantomStreetAddress;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }
}
