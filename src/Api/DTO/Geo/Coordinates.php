<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class Coordinates
{
    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="-90", max="90")
     * @Groups({"api"})
     *
     * @var float
     */
    private $lat;
    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="-180", max="180")
     * @Groups({"api"})
     *
     * @var float
     */
    private $lng;

    public function __construct(float $lat, float $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }
}
