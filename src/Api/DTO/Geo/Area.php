<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

class Area
{
    /**
     * @var Coordinates
     */
    private $coordinates;
    /**
     * @var int
     */
    private $radius;

    public function __construct(Coordinates $coordinates, int $radius)
    {
        $this->coordinates = $coordinates;
        $this->radius = $radius;
    }

    /**
     * @return \App\Api\DTO\Geo\Coordinates
     */
    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getRadius(): int
    {
        return $this->radius;
    }
}
