<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

use Symfony\Component\Validator\Constraints as Assert;

class Search
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3")
     *
     * @var string
     */
    private $query;
    /**
     * @Assert\Range(min="1", max="200")
     *
     * @var int
     */
    private $limit;
    /**
     * @var int
     */
    private $offset;

    public function __construct(string $query, int $limit = 10, int $offset = 0)
    {
        $this->query = $query;
        $this->limit = (int) $limit;
        $this->offset = $offset;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
