<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

class Street
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $altName;

    public function __construct(string $name, string $altName)
    {
        $this->name = $name;
        $this->altName = $altName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAltName(): string
    {
        return $this->altName;
    }
}
