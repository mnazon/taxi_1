<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class Place
{
    /**
     * @SWG\Property(description="depracated")
     * @Groups({"api"})
     *
     * @var string
     */
    private $name;
    /**
     * @SWG\Property(description="nullable, depracated")
     * @Groups({"api"})
     *
     * @var string|null
     */
    private $locality;
    /**
     * @Assert\NotBlank()
     * @SWG\Property(description="nullable")
     * @Groups({"api"})
     *
     * @var Coordinates|null
     */
    private $coordinates;
    /**
     * @Groups({"api"})
     *
     * @var bool
     */
    private $isExactAddress;
    /**
     * @Groups({"api"})
     *
     * @var string
     */
    private $mainText;
    /**
     * @Groups({"api"})
     *
     * @var string
     */
    private $secondaryText;
    /**
     * @Groups({"api"})
     * @SWG\Property(description="nullable")
     *
     * @var string|null
     */
    private $phantomStreetAddress;
    /**
     * @Groups({"api"})
     * @SWG\Property(description="nullable")
     *
     * @var string|null
     */
    private $placeId;

    public function __construct(
        string $mainText = '',
        string $secondaryText = '',
        bool $isExactAddress = true,
        ?string $placeId = null,
        ?string $phantomStreetAddress = null,
        ?Coordinates $coordinates = null
    ) {
        $this->name = $mainText;
        $this->locality = $secondaryText;
        $this->coordinates = $coordinates;
        $this->isExactAddress = $isExactAddress;
        $this->mainText = $mainText;
        $this->secondaryText = $secondaryText;
        $this->phantomStreetAddress = $phantomStreetAddress;
        $this->placeId = $placeId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality)
    {
        $this->locality = $locality;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function isExactAddress(): bool
    {
        return $this->isExactAddress;
    }

    public function getMainText(): string
    {
        return !empty($this->mainText) ? $this->mainText : $this->name;
    }

    public function getSecondaryText(): string
    {
        return !empty($this->secondaryText) ? $this->secondaryText : $this->locality;
    }

    public function getPhantomStreetAddress(): ?string
    {
        return $this->phantomStreetAddress;
    }

    public function getPlaceId(): ?string
    {
        return $this->placeId;
    }
}
