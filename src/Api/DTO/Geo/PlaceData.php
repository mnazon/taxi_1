<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class PlaceData
{
    /**
     * @Assert\NotBlank()
     *
     * @Groups("api")
     *
     * @var string
     */
    private $name;
    /**
     * @SWG\Property(description="depracated")
     *
     * @Groups("api")
     *
     * @var string|null
     */
    private $locality;
    /**
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @Groups("api")
     *
     * @var Coordinates
     */
    private $coordinates;

    public function __construct(
        string $name,
        Coordinates $coordinates
    ) {
        $this->name = $name;
        $this->coordinates = $coordinates;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    /**
     * @param string $locality
     */
    public function setLocality(?string $locality)
    {
        $this->locality = $locality;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }
}
