<?php

declare(strict_types=1);

namespace App\Api\DTO\Geo;

class GeoObject
{
    /**
     * @var Street|null
     */
    private $street;
    /**
     * @var Place|null
     */
    private $place;

    public function __construct(?Street $street, ?Place $place)
    {
        $this->street = $street;
        $this->place = $place;
    }

    public function getStreet(): ?Street
    {
        return $this->street;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }
}
