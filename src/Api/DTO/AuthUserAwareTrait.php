<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Entity\User;

trait AuthUserAwareTrait
{
    /**
     * @var User
     */
    private $userEntity;

    public function setUser(User $user)
    {
        $this->userEntity = $user;
    }

    public function getUser(): ?User
    {
        return $this->userEntity;
    }
}
