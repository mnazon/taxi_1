<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

class Vehicle implements \JsonSerializable
{
    /**
     * @var string
     * @Groups({"api"})
     */
    private $makeAndModel;
    /**
     * @var string
     * @Groups({"api"})
     */
    private $numberPlate;
    /**
     * @var string
     * @Groups({"api"})
     */
    private $color;

    public function __construct(string $numberPlate, string $makeAndModel, string $color)
    {
        $this->makeAndModel = $makeAndModel;
        $this->numberPlate = $numberPlate;
        $this->color = $color;
    }

    public function getMakeAndModel(): string
    {
        return $this->makeAndModel;
    }

    public function getNumberPlate(): string
    {
        return $this->numberPlate;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function jsonSerialize()
    {
        return [
            'numberPlate' => $this->numberPlate,
            'makeAndModel' => $this->makeAndModel,
            'color' => $this->color,
        ];
    }
}
