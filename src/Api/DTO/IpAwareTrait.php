<?php

declare(strict_types=1);

namespace App\Api\DTO;

use Symfony\Component\Validator\Constraints as Assert;

trait IpAwareTrait
{
    /**
     * @Assert\Ip(version="4")
     *
     * @var string
     */
    private $ip;

    public function getIpLong(): ?int
    {
        return !empty($this->ip) ? ip2long($this->ip) : null;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }
}
