<?php

declare(strict_types=1);

namespace App\Api\DTO\phone;

use Symfony\Component\Serializer\Annotation\Groups;

class SendPhoneResponseDto implements \JsonSerializable
{
    /**
     * @var int
     * @Groups("api")
     */
    private $nextAttemptInSeconds;
    /**
     * Do not pass this code throw.
     *
     * @var int
     */
    private $confirmationCode;
    /**
     * @var bool
     */
    private $needSendConfirmationCodeInResponse = false;

    public function __construct(int $nextAttemptInSeconds, int $confirmationCode)
    {
        $this->nextAttemptInSeconds = $nextAttemptInSeconds;
        $this->confirmationCode = $confirmationCode;
    }

    public function setNeedSendConfirmationCodeInResponse(bool $needSendConfirmationCodeInResponse)
    {
        $this->needSendConfirmationCodeInResponse = $needSendConfirmationCodeInResponse;
    }

    public function jsonSerialize()
    {
        $data = [
            'nextAttemptInSeconds' => $this->nextAttemptInSeconds,
        ];
        if ($this->needSendConfirmationCodeInResponse) {
            $data['code'] = $this->confirmationCode;
        }

        return $data;
    }
}
