<?php

declare(strict_types=1);

namespace App\Api\DTO\phone;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\IpAwareInterface;
use App\Api\DTO\IpAwareTrait;
use libphonenumber\PhoneNumber;
use Symfony\Component\Validator\Constraints as Assert;

class PhoneConfirmationDto implements ArgumentResolvableInterface, IpAwareInterface
{
    use IpAwareTrait;

    /**
     * @Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber()
     *
     * @var string
     */
    private $phone;
    /**
     * @var string
     * @Assert\Type(type="numeric")
     * @Assert\Range(min="1000", max="9999")
     */
    private $code;
    /**
     * @Assert\Ip(version="4")
     *
     * @var string
     */
    private $ip;

    /**
     * PhoneConfirmationDto constructor.
     */
    public function __construct(string $phone, string $code, string $ip)
    {
        $this->phone = $phone;
        $this->code = $code;
        $this->ip = $ip;
    }

    public function getCode(): int
    {
        return (int) ($this->code);
    }

    public function getPhone(): PhoneNumber
    {
        return (new PhoneNumber())->setRawInput($this->phone);
    }
}
