<?php

declare(strict_types=1);

namespace App\Api\DTO\phone;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\IpAwareInterface;
use App\Api\DTO\IpAwareTrait;
use libphonenumber\PhoneNumber;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class SendPhoneConfirmationDTO implements ArgumentResolvableInterface, IpAwareInterface
{
    use IpAwareTrait;
    /**
     * @Assert\NotBlank()
     * @AssertPhoneNumber(type="mobile")
     * @Groups("api")
     *
     * @var string
     */
    private $phone;

    public function __construct(string $phone = '')
    {
        $this->phone = $phone;
    }

    public function getPhone(): PhoneNumber
    {
        return (new PhoneNumber())->setRawInput($this->phone);
    }
}
