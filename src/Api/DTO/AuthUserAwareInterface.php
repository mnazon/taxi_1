<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Entity\User;

interface AuthUserAwareInterface
{
    public function setUser(User $user);

    public function getUser(): ?User;
}
