<?php

declare(strict_types=1);

namespace App\Api\Admin;

use App\Api\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Filter\NumberFilter;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Sonata\Form\Type\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdmin extends AbstractAdmin
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        parent::__construct($code, $class, $baseControllerName);
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $notRequiredOption = [
            'required' => false,
        ];

        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');

        $rolesChoices = self::flattenRoles($roles);
        $formMapper
            ->add('fullName', TextType::class, $notRequiredOption)
            ->add('phone', TextType::class)
            ->add('email', EmailType::class, $notRequiredOption)
            ->add('roles', ChoiceType::class, [
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $rolesChoices,
                 ]
            )
            ->add('plainPassword', PasswordType::class, $notRequiredOption)
            ->add('createdAt', DateTimeType::class, ['input' => 'datetime_immutable'])
            ->add('isPolicyAccepted', BooleanType::class, $notRequiredOption)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', NumberFilter::class)
            ->add('phone', StringFilter::class)
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('fullName')
            ->add('email')
            ->add('plainPassword')
            ->add('createdAt')
            ->add('isPolicyAccepted')
        ;
    }

    public function preUpdate($object)
    {
        /** @var User $object */
        if ($object->getPlainPassword()) {
            $password = $this->userPasswordEncoder->encodePassword($object, $object->getPlainPassword());
            $object->setPassword($password);
        }
    }

    protected static function flattenRoles($rolesHierarchy)
    {
        $flatRoles = [];
        foreach ($rolesHierarchy as $roles) {
            if (empty($roles)) {
                continue;
            }

            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    $flatRoles[$role] = $role;
                }
            }
        }

        return $flatRoles;
    }
}
