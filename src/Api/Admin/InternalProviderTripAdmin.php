<?php

declare(strict_types=1);

namespace App\Api\Admin;

use App\Api\Enum\TripStatus;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class InternalProviderTripAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('cost')
            ->add('additionalCost')
            ->add('currency')
            ->add(
                'vehicleAsString',
                TextareaType::class,
                [
                    'help' => '{"numberPlate":"АА0000КН", "makeAndModel":"Tesla Model S", "color":"white"}',
                ]
            )
            ->add(
                'arriveAt',
                null,
                [
                    'required' => true,
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                [
                    'expanded' => true,
                    'choices' => TripStatus::values(),
                    'choice_value' => function (?TripStatus $value) {
                        return $value;
                    },
                ]
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('cost')
            ->add('additionalCost')
            ->add('currency')
            ->add('status')
            ->add('createdAt')
            ->add('arriveAt')
            ->add(
                '_action',
                'actions',
                [
                    'label' => false,
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ]
            );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status');
    }
}
