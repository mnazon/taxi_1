<?php

declare(strict_types=1);

namespace App\Api\Admin;

use App\Api\Entity\TaxiProvider;
use App\Api\Enum\TaxiPlatform;
use App\Api\Repository\TaxiProviderRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter;
use Sonata\DoctrineORMAdminBundle\Filter\NumberFilter;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Sonata\Form\Type\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TaxiProviderAdmin extends AbstractAdmin
{
    /**
     * @var string
     */
    private $kernelRoot;
    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;

    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        string $kernelRoot,
        TaxiProviderRepository $taxiProviderRepository
    ) {
        parent::__construct($code, $class, $baseControllerName);
        $this->kernelRoot = $kernelRoot;
        $this->taxiProviderRepository = $taxiProviderRepository;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $options = ['required' => false];
        /** @var $subject TaxiProvider */
        if (($subject = $this->getSubject()) && $subject->getIcon()) {
            $path = $subject->getIconUrl();
            $options['help'] = '<img src="'.$path.'" />';
        }

        $formMapper
            ->add('name', TextType::class)
            ->add('slug', TextType::class)
            //->add('icon', 'image')
            ->add('iconFile', FileType::class, $options)
            ->add('enabled', BooleanType::class, ['transform' => true])
            ->add('taxiPlatform', ChoiceType::class, [
                'expanded' => true,
                'choices' => TaxiPlatform::toArray(),
            ])
            ->add(
                'authorizers',
                CollectionType::class,
                [
                    'by_reference' => false,
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                ]
            )
            ->add('customSortWeight', IntegerType::class, ['required' => false])
            ->add('createdAt', DateTimeType::class,
                  ['widget' => 'single_text', 'attr' => ['readonly' => true]]
            )
        ;

        if (!$this->getSubject()->getId()) {
            $formMapper->add('createdAt', DateTimeType::class,
                ['widget' => 'single_text', 'input' => 'datetime_immutable']
            );
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', NumberFilter::class)
            ->add('name', StringFilter::class)
            ->add('enabled', BooleanFilter::class)
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('name')
            ->add('slug')
            ->add('enabled')
            ->add('createdAt')
            ->add('taxiPlatform')
        ;
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    public function postPersist($object)
    {
        $this->afterSaving();
    }

    public function postUpdate($object)
    {
        $this->afterSaving();
    }

    private function afterSaving()
    {
        $this->taxiProviderRepository->flushEnabledCache();
    }

    private function manageFileUpload(TaxiProvider $image)
    {
        if ($image->getIconFile()) {
            $image->upload($this->kernelRoot);
        }
    }
}
