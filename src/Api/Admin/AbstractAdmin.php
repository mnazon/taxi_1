<?php

declare(strict_types=1);

namespace App\Api\Admin;

class AbstractAdmin extends \Sonata\AdminBundle\Admin\AbstractAdmin
{
    public const ATTR_WITH_READ_ONLY = [
        'attr' => ['readonly' => true],
    ];
}
