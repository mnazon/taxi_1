<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Search;
use App\Api\Service\Geo\GoogleGeoProvider;
use App\Api\Service\Geo\VisicomGeoProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestGeoCommand extends Command
{
    protected static $defaultName = 'test:geo';
    /**
     * @var VisicomGeoProvider
     */
    private $visicomGeoProvider;
    /**
     * @var GoogleGeoProvider
     */
    private $googleGeoProvider;

    public function __construct(VisicomGeoProvider $visicomGeoProvider, GoogleGeoProvider $googleGeoProvider)
    {
        parent::__construct(self::$defaultName);
        $this->visicomGeoProvider = $visicomGeoProvider;
        $this->googleGeoProvider = $googleGeoProvider;
    }

    public function configure()
    {
//        $this
//            ->addOption('lat', null, InputOption::VALUE_REQUIRED)
//            ->addOption('lng', null, InputOption::VALUE_REQUIRED);
    }

    /**
     * @return int|void|null
     *
     * @throws \App\Api\Exception\WrongProviderResponse
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
//        $lat = (float) $input->getOption('lat');
//        $lng = (float) $input->getOption('lng');
//        $coordinates = new Coordinates($lat, $lng);
//        $res = $this->visicomGeoProvider->reverseGeocodingByCoordinates($coordinates);
        $res = $this->visicomGeoProvider->findStreetsAndPlaces(new Search('Боулінг-клуб "Дельтаплан"'));
        var_dump($res);
//        $res = $this->googleGeoProvider->reverseGeocodingByCoordinates($coordinates);
//        var_dump($res);
    }
}
