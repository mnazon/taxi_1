<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\Repository\TaxiProviderRepository;
use App\Api\Repository\TripStatusLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaxiProviderSuccessTripRatioManager extends Command
{
    protected static $defaultName = 'taxiProvider:success-trip-ratio';
    /**
     * @var TripStatusLogRepository
     */
    private $tripStatusLogRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;

    public function __construct(
        TripStatusLogRepository $tripStatusLogRepository,
        TaxiProviderRepository $taxiProviderRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct(self::$defaultName);
        $this->tripStatusLogRepository = $tripStatusLogRepository;
        $this->entityManager = $entityManager;
        $this->taxiProviderRepository = $taxiProviderRepository;
    }

    public function configure()
    {
        $this
            ->setDescription('Update success trip ratio');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Updating taxiProvide score... ');

        $data = $this->tripStatusLogRepository->getExecutedToCanceledTripRatio();

        $taxiProviders = $this->taxiProviderRepository->findByEnabled(true);
        foreach ($taxiProviders as $taxiProvider) {
            $taxiProviderId = $taxiProvider->getId();
            $percent = $data[$taxiProviderId] ?? null;
            if ($taxiProvider->getPercentage() != $percent) {
                $taxiProvider->setPercentage($percent);
            }
            $this->entityManager->persist($taxiProvider);
        }

        $this->entityManager->flush();
        $this->taxiProviderRepository->flushEnabledCache();

        $output->writeln('TaxiProvider score updated.');
    }
}
