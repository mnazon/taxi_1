<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\DTO\PageDto;
use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\Enum\TripStatus;
use App\Api\Repository\TripStatusLogRepository;
use App\Api\Service\TaxiProvider\TripClientFactory;
use App\Api\Service\Trip\CheckExternalStatusNotifier;
use function GuzzleHttp\Promise\all;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClosedTripCheckerCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $defaultName = 'trip:closed-trip-checker';
    /**
     * @var TripStatusLogRepository
     */
    private $tripStatusLogRepository;
    /**
     * @var TripClientFactory
     */
    private $tripClientFactory;

    /**
     * @var CheckExternalStatusNotifier
     */
    private $checkExternalStatusNotifier;

    public function __construct(
        TripStatusLogRepository $tripStatusLogRepository,
        TripClientFactory $tripClientFactory,
        CheckExternalStatusNotifier $checkExternalStatusNotifier
    ) {
        parent::__construct(self::$defaultName);
        $this->tripStatusLogRepository = $tripStatusLogRepository;
        $this->tripClientFactory = $tripClientFactory;
        $this->checkExternalStatusNotifier = $checkExternalStatusNotifier;
        $this->logger = new NullLogger();
    }

    public function configure()
    {
        $this
            ->setDescription('Check on update trips status after it closed')
            ->addOption('fromTime', null, InputOption::VALUE_REQUIRED, 'from time', '-20 minutes')
            ->addOption('toTime', null, InputOption::VALUE_REQUIRED, 'to time', '-1 minute');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $tripStatus = new TripStatus(TripStatus::CANCELED);
        $startDate = new \DateTime($input->getOption('fromTime'));
        $endDate = new \DateTime($input->getOption('toTime'));

        $datePeriod = new \DatePeriod($startDate, new \DateInterval('P1M'), $endDate);
        $page = 0;
        $perPage = 100;
        do {
            ++$page;
            $pageDto = new PageDto($page, $perPage);
            $tripStatusLogs = $this->tripStatusLogRepository->findByStatusAndCreatedAtPeriod($tripStatus, $datePeriod, $pageDto);
            $externalTripCollection = $this->getExternalTripCollection($tripStatusLogs);

            foreach ($tripStatusLogs as $tripStatusLog) {
                $trip = $tripStatusLog->getTrip();
                $tripResponse = $externalTripCollection->find($trip->getExternalId(), $trip->getTaxiProvider()->getId());
                if (!$tripResponse) {
                    continue;
                }

                $this->checkExternalStatusNotifier->checkAndNotify($tripResponse, $trip);
            }
        } while (\count($tripStatusLogs) === $perPage);
    }

    private function getExternalTripCollection(array $tripStatusLogs): ExternalTripCollection
    {
        $promises = [];
        $externalTripCollection = new ExternalTripCollection();
        foreach ($tripStatusLogs as $tripStatusLog) {
            $trip = $tripStatusLog->getTrip();
            $taxiProviderId = $trip->getTaxiProvider()->getId();
            $client = $this->tripClientFactory->createFromProviderId($taxiProviderId);
            $promises[] = $client->find($trip->getExternalId(), $externalTripCollection);
        }

        all($promises)->wait(false);

        return $externalTripCollection;
    }
}
