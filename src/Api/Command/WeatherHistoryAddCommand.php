<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\DTO\OpenWeather\CurrentWeatherResponse;
use App\Api\Entity\WeatherHistory;
use App\Api\Exception\WeatherWrongDataException;
use App\Api\Repository\WeatherHistoryRepository;
use App\Api\Service\OpenWeatherClient;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WeatherHistoryAddCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $defaultName = 'weather:history:add';
    private const KIEV_ID = 703448;

    /**
     * @var WeatherHistoryRepository
     */
    private $weatherHistoryRepository;

    /**
     * @var OpenWeatherClient
     */
    private $openWeatherClient;

    public function __construct(
        OpenWeatherClient $openWeatherClient,
        WeatherHistoryRepository $weatherHistoryRepository
    ) {
        parent::__construct(self::$defaultName);
        $this->weatherHistoryRepository = $weatherHistoryRepository;
        $this->openWeatherClient = $openWeatherClient;
        $this->logger = new NullLogger();
    }

    public function configure()
    {
        $this
            ->setDescription('Add current weather to history.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $currentWeatherResponse = $this->openWeatherClient->current(self::KIEV_ID);
            $this->createWeatherHistoryByCurrentWeather($currentWeatherResponse);
        } catch (WeatherWrongDataException $e) {
            $this->logger->warning('Invalid response from OpenWeather', ['exception' => $e]);
            throw $e;
        }
    }

    private function createWeatherHistoryByCurrentWeather(CurrentWeatherResponse $response): void
    {
        $weatherHistory = (new WeatherHistory())
            ->setTemperature($response->getTemperature())
            ->setWindSpeed($response->getWindSpeed())
            ->setVisibility($response->getVisibility())
            ->setWeatherId($response->getWeatherId())
            ->setWeather($response->getWeather())
            ->setPrecipitationRain1h($response->getPrecipitationRain1h())
            ->setPrecipitationRain3h($response->getPrecipitationRain3h())
            ->setPrecipitationSnow1h($response->getPrecipitationSnow1h())
            ->setPrecipitationSnow3h($response->getPrecipitationSnow3h());

        $this->weatherHistoryRepository->save($weatherHistory);
    }
}
