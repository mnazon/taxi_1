<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TaxiProviderAuthInterface;
use App\Api\Enum\TaxiPlatform;
use App\Api\Repository\TaxiProviderAuthRepository;
use App\Api\Repository\TaxiProviderRepository;
use Doctrine\ORM\EntityManagerInterface;
use function GuzzleHttp\Promise\all;
use Http\Client\Exception\NetworkException;
use Http\Client\HttpAsyncClient;
use Http\Message\MessageFactory;
use Http\Promise\Promise;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaxiProviderHostsCheckerCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    use LockableTrait;

    protected static $defaultName = 'taxiProvider:hosts-checker';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;

    /**
     * @var HttpAsyncClient
     */
    private $httpAsyncClient;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var TaxiProviderAuthRepository
     */
    private $taxiProviderAuthRepository;

    /**
     * @var bool
     */
    private $willAuthorizersBeUpdated = false;

    public function __construct(
        HttpAsyncClient $httpAsyncClient,
        MessageFactory $messageFactory,
        TaxiProviderRepository $taxiProviderRepository,
        TaxiProviderAuthRepository $taxiProviderAuthRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct(self::$defaultName);
        $this->entityManager = $entityManager;
        $this->taxiProviderRepository = $taxiProviderRepository;
        $this->httpAsyncClient = $httpAsyncClient;
        $this->messageFactory = $messageFactory;
        $this->logger = new NullLogger();
        $this->taxiProviderAuthRepository = $taxiProviderAuthRepository;
    }

    public function configure()
    {
        $this
            ->setDescription('Сhecking Evos hosts for activity');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            return;
        }

        $taxiPlatform = new TaxiPlatform(TaxiPlatform::EVOS);
        $inactiveProviders = $this->taxiProviderRepository
            ->findEnabledProvidersWithoutActiveAuthorizersByTaxiPlatform($taxiPlatform);
        $this->tryToActivateAuthorizersByProviders($inactiveProviders);

        /** @var TaxiProviderAuthInterface[] $activeAuthorizers */
        $activeAuthorizers = $this->taxiProviderAuthRepository
            ->findActiveAuthorizersWithEnabledProviderByTaxiPlatform($taxiPlatform);
        $inactiveAuthorizersData = $this->findInactiveAuthorizers($activeAuthorizers);
        $this->authorizersDeactivation($inactiveAuthorizersData);
    }

    private function tryToActivateAuthorizersByProviders(array $providers)
    {
        foreach ($providers as $provider) {
            $authorizers = $provider->getAuthorizers();
            $this->findAndActivateFirstActiveAuthorizer($authorizers);
        }

        $this->flush();
        $this->entityManager->clear();
    }

    private function findInactiveAuthorizers(array $authorizers)
    {
        $promises = [];
        $inactiveAuthorizersData = [];
        foreach ($authorizers as $authorizer) {
            $onRejected = function (\Exception $e) use ($authorizer, &$inactiveAuthorizersData) {
                $item = [
                    'authorizer' => $authorizer,
                    'context' => ['exception' => $e],
                ];
                $inactiveAuthorizersData[] = $item;
            };

            try {
                $promises[] = $this->sendAndCheckHealthCheckRequest($authorizer, $onRejected);
            } catch (\Exception $e) {
                $onRejected($e);
            }
        }

        try {
            all($promises)->wait(false);
        } catch (NetworkException $e) {
            $uri = $e->getRequest()->getUri();
            $this->logger->warning('Response waiting timeout.', ['uri' => $uri]);
        }

        return $inactiveAuthorizersData;
    }

    private function authorizersDeactivation(array $inactiveAuthorizersData)
    {
        foreach ($inactiveAuthorizersData as $data) {
            /** @var TaxiProviderAuthInterface $authorizer */
            $inactiveAuthorizer = $data['authorizer'];
            /** @var TaxiProvider $provider */
            $provider = $inactiveAuthorizer->getTaxiProvider();
            $authorizers = $this->taxiProviderAuthRepository->findInactiveAuthorizersByProvider($provider);
            $activeAuthorizer = $this->findAndActivateFirstActiveAuthorizer($authorizers);
            if (!$activeAuthorizer) {
                $this->logProviderDeactivation($provider->getName(), $data['context']);
            }
            // inactiveAuthorizer deactivation
            $this->changeAuthorizerActivity($inactiveAuthorizer, false);
        }

        $this->flush();
    }

    private function findAndActivateFirstActiveAuthorizer(iterable $authorizers)
    {
        $activeAuthorizer = $this->findActiveAuthorizer($authorizers);
        if ($activeAuthorizer && !$activeAuthorizer->isActive()) {
            $this->changeAuthorizerActivity($activeAuthorizer, true);
        }

        return $activeAuthorizer;
    }

    private function findActiveAuthorizer(iterable $authorizers)
    {
        /** @var TaxiProviderAuthInterface $authorizer */
        foreach ($authorizers as $authorizer) {
            try {
                $promise = $this->sendAndCheckHealthCheckRequest($authorizer);
                $promise->wait(true);
            } catch (\Exception $e) {
                continue;
            }

            return $authorizer;
        }

        return null;
    }

    /**
     * @throws \Exception
     */
    private function sendAndCheckHealthCheckRequest(TaxiProviderAuthInterface $authorizer, callable $onRejected = null): Promise
    {
        $headers = $authorizer->buildAuthHeader();
        $baseUrl = $authorizer->getBaseUrl();
        $uri = '/api/version';
        $uri = $baseUrl.$uri;

        $request = $this->messageFactory->createRequest('GET', $uri, $headers);
        $promise = $this->httpAsyncClient->sendAsyncRequest($request);
        $promise->then(null, $onRejected);

        return $promise;
    }

    private function changeAuthorizerActivity(TaxiProviderAuthInterface $authorizer, bool $isActive)
    {
        if ($authorizer->isActive() === $isActive) {
            return;
        }

        $authorizer->setIsActive($isActive);
        $this->willAuthorizersBeUpdated = true;
    }

    private function flush()
    {
        if (!$this->willAuthorizersBeUpdated) {
            return;
        }

        $this->entityManager->flush();
        $this->taxiProviderRepository->flushEnabledCache();
        $this->willAuthorizersBeUpdated = false;
    }

    private function logProviderDeactivation(string $providerName, array $context)
    {
        $this->logger->error(
            sprintf('Provider "%s" has no active authorizers.', $providerName),
            $context
        );
    }
}
