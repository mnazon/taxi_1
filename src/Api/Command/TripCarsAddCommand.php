<?php

declare(strict_types=1);

namespace App\Api\Command;

use App\Api\DTO\CarPosition;
use App\Api\DTO\Geo\Area;
use App\Api\DTO\PageDto;
use App\Api\DTO\TripCarsItem;
use App\Api\Entity\Trip;
use App\Api\Entity\TripCars;
use App\Api\Enum\CarStatus;
use App\Api\Exception\UnsupportedTaxiPlatformException;
use App\Api\Exception\WrongAuthorizationArgumentException;
use App\Api\Repository\TripCarsRepository;
use App\Api\Repository\TripRepository;
use App\Api\Service\Geo\GeoSupportChecker;
use App\Api\Service\TaxiProvider\TripCarsClientFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use function GuzzleHttp\Promise\all;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TripCarsAddCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $defaultName = 'trip:cars:add';
    private const BATCH_SIZE = 50;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TripCarsRepository
     */
    private $tripCarsRepository;
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var TripCarsClientFactory
     */
    private $tripCarsClientFactory;

    /**
     * @var GeoSupportChecker
     */
    private $geoSupportChecker;

    public static function getRadiiInMeters(): array
    {
        return [1000, 2000, 3000, 4000, 5000];
    }

    public function __construct(
        EntityManagerInterface $entityManager,
        TripCarsRepository $tripCarsRepository,
        TripRepository $tripRepository,
        TripCarsClientFactory $tripCarsClientFactory,
        GeoSupportChecker $geoSupportChecker
    ) {
        parent::__construct(self::$defaultName);
        $this->entityManager = $entityManager;
        $this->tripCarsRepository = $tripCarsRepository;
        $this->tripRepository = $tripRepository;
        $this->tripCarsClientFactory = $tripCarsClientFactory;
        $this->logger = new NullLogger();
        $this->geoSupportChecker = $geoSupportChecker;
    }

    public function configure()
    {
        $this
            ->setDescription('Add trip cars in radius');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $processedTripId = $this->tripCarsRepository->findLastProcessedTripId();
        $maxRadiusKm = max(self::getRadiiInMeters()) / 1000;
        $perPage = self::BATCH_SIZE;
        $page = 0;
        do {
            ++$page;
            $pageDto = new PageDto($page, $perPage);
            $trips = $this->tripRepository->findUnprocessedTripsByLastProcessedTripId($pageDto, $processedTripId);
            $tripCarsCollection = new ArrayCollection();
            $promises = [];
            /** @var Trip $trip */
            foreach ($trips as $trip) {
                $tripCarsItem = new TripCarsItem($trip);
                $tripCarsCollection->add($tripCarsItem);
                try {
                    $taxiProvider = $trip->getTaxiProvider();
                    $tripCarsClient = $this->tripCarsClientFactory->getClientByTaxiProvider($taxiProvider);
                } catch (UnsupportedTaxiPlatformException $exception) {
                    $this->logger->warning($exception->getMessage(), ['exception' => $exception]);
                    continue;
                }

                $clientPoint = $trip->getTripStart()->getCoordinates();
                $area = new Area($clientPoint, $maxRadiusKm);
                try {
                    $promises[] = $promise = $tripCarsClient->findCarsInArea($area, $tripCarsItem);
                } catch (WrongAuthorizationArgumentException $exception) {
                    $this->logger->warning($exception->getMessage(), ['exception' => $exception]);
                }
            }

            try {
                all($promises)->wait(false);
            } catch (\Throwable $exception) {
                $this->logger->info('Error while adding tripCars', ['exception' => $exception]);
            }

            $this->createTripCarsByTripCarsCollection($tripCarsCollection);
        } while (\count($trips) === $perPage);
    }

    private function createTripCarsByTripCarsCollection(iterable $tripCarsCollection): void
    {
        /** @var TripCarsItem $item */
        foreach ($tripCarsCollection as $item) {
            if ($item->getCarPositions()->isEmpty()) {
                //create an object with zero values and minimum radius for this trip
                $tripCars = $this->createTripCars($item->getTrip(), min(self::getRadiiInMeters()));
                $this->entityManager->persist($tripCars);
            } else {
                $this->createTripCarsByTripCarsItem($item);
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    private function createTripCarsByTripCarsItem(TripCarsItem $tripCarsItem)
    {
        $trip = $tripCarsItem->getTrip();
        $cars = $tripCarsItem->getCarPositions();
        $clientPoint = $trip->getTripStart()->getCoordinates();
        foreach (self::getRadiiInMeters() as $radius) {
            $area = new Area($clientPoint, $radius);
            $carsInArea = $cars->filter(function (CarPosition $carPosition) use ($area) {
                return $this->geoSupportChecker->isPointInsideArea($carPosition->getCoordinates(), $area);
            });

            if ($carsInArea->isEmpty()) {
                continue;
            }

            $amountFree = $this->getCarsCountByStatus($carsInArea, CarStatus::FREE);
            $amountBusy = $this->getCarsCountByStatus($carsInArea, CarStatus::BUSY);
            $amountClosed = $this->getCarsCountByStatus($carsInArea, CarStatus::CLOSED);
            $tripCars = $this->createTripCars($trip, $radius, $amountFree, $amountBusy, $amountClosed);

            $this->entityManager->persist($tripCars);
        }
    }

    private function createTripCars(
        Trip $trip,
        int $radius,
        int $amountFree = 0,
        int $amountBusy = 0,
        int $amountClosed = 0
    ): TripCars {
        return new TripCars($trip, $radius, $amountFree, $amountBusy, $amountClosed);
    }

    private function getCarsCountByStatus(ArrayCollection $carPositions, int $status): int
    {
        $carPositions = $carPositions->filter(function (CarPosition $car) use ($status) {
            return $car->getStatus()->getValue() === $status;
        });

        return $carPositions->count();
    }
}
