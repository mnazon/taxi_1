<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\AuthUserAwareInterface;
use App\Api\DTO\AuthUserAwareTrait;
use App\Api\Entity\User;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class CreateTripRequest implements ArgumentResolvableInterface, AuthUserAwareInterface
{
    use AuthUserAwareTrait;
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     * @Assert\Type(type="int")
     * @Groups("api")
     *
     * @var int
     */
    private $taxiProviderId;

    /**
     * @Assert\Type(type="string")
     * @SWG\Property(description="notification token(fcmId)")
     *
     * @Groups("api")
     *
     * @var string|null
     */
    private $notificationToken;
    /**
     * @Assert\Type(type="int")
     * @SWG\Property(description="device id to notify")
     *
     * @Groups("api")
     *
     * @var int
     */
    private $deviceId;

    /**
     * @Groups("api")
     *
     * @Assert\NotBlank()
     * @Assert\Valid()
     *
     * @var EstimateTripRequest
     */
    private $trip;

    public function getTaxiProviderId(): int
    {
        return $this->taxiProviderId;
    }

    /**
     * @param int $taxiProvider
     */
    public function setTaxiProviderId($taxiProvider)
    {
        $this->taxiProviderId = $taxiProvider;
    }

    public function getDeviceId(): ?int
    {
        return $this->deviceId;
    }

    public function setDeviceId(int $deviceId)
    {
        $this->deviceId = $deviceId;
    }

    public function setTrip(EstimateTripRequest $tripData)
    {
        $this->trip = $tripData;
    }

    public function getTrip(): EstimateTripRequest
    {
        return $this->trip;
    }

    /**
     * @return string
     */
    public function getNotificationToken(): ?string
    {
        return $this->notificationToken;
    }

    /**
     * @param string $notificationToken
     */
    public function setNotificationToken(?string $notificationToken)
    {
        $this->notificationToken = $notificationToken;
    }

    public function getUser(): User
    {
        return $this->userEntity;
    }
}
