<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class Location
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @Groups("api")
     *
     * @var string
     */
    private $name;

    /**
     * @Assert\Length(max="255")
     *
     * @var string|null
     */
    private $locality;
    /**
     * @Assert\NotBlank()
     * @Assert\Regex("/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/")
     * @Groups("api")
     *
     * @var float
     */
    private $lat;
    /**
     * @Assert\NotBlank()
     * @Assert\Regex("/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/")
     * @Groups("api")
     *
     * @var float
     */
    private $lng;

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality)
    {
        $this->locality = $locality;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public function setLng(float $lng)
    {
        $this->lng = $lng;
    }
}
