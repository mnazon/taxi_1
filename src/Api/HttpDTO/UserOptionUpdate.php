<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\AuthUserAwareInterface;
use App\Api\DTO\AuthUserAwareTrait;
use App\Api\Enum\UserOptionEnum;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class UserOptionUpdate implements ArgumentResolvableInterface, AuthUserAwareInterface
{
    use AuthUserAwareTrait;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"\App\Api\Enum\UserOptionEnum", "keys"})
     * @Groups("api")
     *
     * @var string
     */
    protected $userOption;

    /**
     * @Assert\Type("bool")
     * @Groups("api")
     *
     * @var bool
     */
    protected $value;

    public function __construct(string $userOption, bool $value)
    {
        $this->userOption = $userOption;
        $this->value = $value;
    }

    public function getUserOption(): UserOptionEnum
    {
        return UserOptionEnum::fromKey($this->userOption);
    }

    public function getValue(): bool
    {
        return $this->value;
    }
}
