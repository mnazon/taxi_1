<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\AuthUserAwareInterface;
use App\Api\DTO\AuthUserAwareTrait;
use App\Api\DTO\Geo\PlaceData;
use App\Api\Enum\CarType;
use App\Api\Enum\TaxiAdditionalService;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EstimateTripRequest implements ArgumentResolvableInterface, AuthUserAwareInterface
{
    use AuthUserAwareTrait;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"\App\Api\Enum\CarType", "toArray"})
     * @Groups("api")
     *
     * @var string
     */
    protected $carType;
    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @Groups("api")
     *
     * @var PlaceData[]
     */
    protected $route;
    /**
     * @SWG\Property(example="2018-12-28T14:45:51+0300", type="string")
     * @Assert\DateTime(format="Y-m-d\TH:i:sP")
     * @Groups("api")
     *
     * @var string|\DateTimeImmutable|null
     */
    protected $reservedAt;
    /**
     * @var string|null
     */
    protected $userFullName;
    /**
     * @Assert\NotBlank()
     * @PhoneNumber(defaultRegion="UA", type="mobile")
     * @Groups("api")
     *
     * @var string
     */
    protected $userPhone;
    /**
     * @Assert\Choice(
     *     callback={"\App\Api\Enum\TaxiAdditionalService", "toArray"},
     *     multiple=true,
     * ),
     * @Groups("api")
     *
     * @var string[]|null
     */
    protected $options;

    /**
     * @Assert\Range(min="1", max="255")
     * @Assert\Type(type="numeric")
     * @Groups("api")
     *
     * @var int|null
     */
    protected $startRouteEntranceFrom;
    /**
     * @Assert\Length(max="255")
     * @Groups("api")
     *
     * @var string|null
     */
    protected $comment;

    /**
     * @Assert\Range(min="0", max="1000")
     * @Assert\Type(type="integer")
     * @Groups("api")
     *
     * @var int|null
     */
    private $additionalCost;

    /**
     * @var string
     */
    private $phoneForTaxiProvider;

    /**
     * @return PlaceData[]
     */
    public function getRoute(): array
    {
        return $this->route;
    }

    /**
     * @param PlaceData[] $places
     */
    public function setRoute($places): self
    {
        foreach ($places as $place) {
            if (!$place instanceof PlaceData) {
                throw new \InvalidArgumentException('Wrong instance of place setup');
            }
        }
        $this->route = $places;

        return $this;
    }

    public function getCarType(): CarType
    {
        return new CarType($this->carType);
    }

    public function setCarType(string $carType)
    {
        $this->carType = strtolower($carType);
    }

    /**
     * @throws \Exception
     */
    public function getReservedAt(): ?\DateTimeImmutable
    {
        return !empty($this->reservedAt)
            ? new \DateTimeImmutable($this->reservedAt)
            : null;
    }

    public function setReservedAt(?string $reservedAt)
    {
        if (!empty($reservedAt)) {
            $nearFuture = new \DateTimeImmutable('+10 minutes');
            $reservedAtDateTime = new \DateTime($reservedAt);
            if ($reservedAtDateTime > $nearFuture) {
                $this->reservedAt = $reservedAt;
            }
        }
    }

    /**
     * @return string
     */
    public function getUserFullName(): ?string
    {
        return $this->userFullName;
    }

    public function setUserFullName(string $userFullName)
    {
        $this->userFullName = $userFullName;
    }

    public function getUserPhone(): ?string
    {
        return $this->userPhone;
    }

    public function setUserPhone(?string $userPhone)
    {
        $this->userPhone = $userPhone;
    }

    /**
     * @return string
     */
    public function getPhoneForTaxiProvider(): ?string
    {
        if (!empty($this->phoneForTaxiProvider)) {
            return $this->phoneForTaxiProvider;
        }

        return $this->getUserPhone();
    }

    public function setPhoneForTaxiProvider(string $phoneForTaxiProvider)
    {
        $this->phoneForTaxiProvider = $phoneForTaxiProvider;
    }

    /**
     * @return TaxiAdditionalService[]
     */
    public function getOptions(): array
    {
        $result = [];
        if (!empty($this->options)) {
            foreach ($this->options as $additionalService) {
                $result[] = new TaxiAdditionalService($additionalService);
            }
        }

        return $result;
    }

    public function setOptions(array $additionalServicesParam)
    {
        $additionalServices = [];
        foreach ($additionalServicesParam as $additionalService) {
            $additionalServices[] = strtolower($additionalService);
        }
        $this->options = $additionalServices;
    }

    public function getStartRouteEntranceFrom(): ?int
    {
        return $this->startRouteEntranceFrom;
    }

    public function setStartRouteEntranceFrom(int $startRouteEntranceFrom)
    {
        $this->startRouteEntranceFrom = $startRouteEntranceFrom;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    public function getAdditionalCost(): ?int
    {
        return $this->additionalCost;
    }

    public function setAdditionalCost(?int $additionalCost)
    {
        $this->additionalCost = ($additionalCost > 0) ? $additionalCost : null;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $reservedAt = $this->getReservedAt();
        if (null !== $reservedAt) {
            $maxReservationDateTime = new \DateTime('+7 days');
            if ($reservedAt > $maxReservationDateTime) {
                $data = [
                    '{{ value }}' => $reservedAt->format(\DATE_ATOM),
                    '{{ limit }}' => $maxReservationDateTime->format(\DATE_ATOM),
                ];
                $context->buildViolation('This date should be less than {{ limit }}.', $data)
                    ->atPath('reservedAt')
                    ->addViolation();
            }
        }
    }
}
