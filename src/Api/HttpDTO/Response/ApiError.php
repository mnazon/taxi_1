<?php

declare(strict_types=1);

namespace App\Api\HttpDTO\Response;

use App\Api\DTO\Translation\Message;

class ApiError
{
    /**
     * @var string
     */
    private $propertyPath;
    /**
     * @var Message
     */
    private $title;
    /**
     * @var string|null
     */
    private $code;

    public function __construct(string $code, Message $title, ?string $propertyPath = null)
    {
        $this->propertyPath = $propertyPath;
        $this->title = $title;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getPropertyPath(): ?string
    {
        return $this->propertyPath;
    }

    public function getTitle(): Message
    {
        return $this->title;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
