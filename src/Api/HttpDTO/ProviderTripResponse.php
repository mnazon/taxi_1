<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use App\Api\Entity\TaxiProvider;
use App\Api\Enum\TripStatus;

class ProviderTripResponse
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var float
     */
    private $cost;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var TaxiProvider
     */
    private $taxiProvider;
    /**
     * @var string
     */
    private $status;
    /**
     * @var \DateTimeImmutable|null
     */
    private $startSearchingCarAt;

    public function __construct(
        string $id,
        float $cost,
        string $currency,
        TripStatus $status,
        TaxiProvider $taxiProvider,
        \DateTimeImmutable $startSearchingCarAt = null
    ) {
        $this->id = $id;
        $this->cost = $cost;
        $this->currency = $currency;
        $this->taxiProvider = $taxiProvider;
        $this->status = $status;
        $this->startSearchingCarAt = $startSearchingCarAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get sum cost.
     */
    public function getTotalCost(): float
    {
        return $this->cost;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getTaxiProvider(): TaxiProvider
    {
        return $this->taxiProvider;
    }

    public function getStatus(): TripStatus
    {
        return $this->status;
    }

    public function getStartSearchingCarAt(): ?\DateTimeImmutable
    {
        return $this->startSearchingCarAt;
    }
}
