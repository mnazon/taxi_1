<?php

declare(strict_types=1);

namespace App\Api\HttpDTO;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\AuthUserAwareInterface;
use App\Api\DTO\AuthUserAwareTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProfileUpdate implements ArgumentResolvableInterface, AuthUserAwareInterface
{
    use AuthUserAwareTrait;

    /**
     * @Assert\Email()
     * @Groups("api")
     *
     * @var string|null
     */
    private $email;
    /**
     * @Assert\Length(min="2", max="100")
     * @Assert\Regex(pattern="/[^a-zа-я_ іїєґё\-\d]+/iu", match=false, message="Can contain only words, letters, hyphen, underscore")
     * @Groups("api")
     *
     * @var string|null
     */
    private $fullName;

    /**
     * @Assert\IsTrue()
     * @Groups("api")
     *
     * @var bool|null
     */
    private $isPolicyAccepted;

    public function __construct(string $email = null, string $fullName = null, bool $isPolicyAccepted = null)
    {
        $this->email = $email;
        $this->fullName = $fullName;
        $this->isPolicyAccepted = $isPolicyAccepted;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @return bool
     */
    public function isPolicyAccepted(): ?bool
    {
        return (bool) $this->isPolicyAccepted;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $fullNameBefore = $this->getUser()->getFullName();
        if ('' === $this->fullName && !empty($fullNameBefore)) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('fullName')
                ->addViolation();
        }
    }
}
