<?php

declare(strict_types=1);

namespace App\Api\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateRangeValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (null === $value) {
            return;
        }
        if (\is_string($value)) {
            $value = new \DateTime($value);
        }

        if (!($value instanceof \DateTimeInterface)) {
            $this->context->addViolation($constraint->invalidMessage, [
                '{{ value }}' => $value,
            ]);

            return;
        }

        if (null !== $constraint->max && $value > $constraint->max) {
            $this->context->addViolation($constraint->maxMessage, [
                '{{ value }}' => $value,
                '{{ limit }}' => $this->formatDate($constraint->max),
            ]);
        }

        if (null !== $constraint->min && $value < $constraint->min) {
            $this->context->addViolation($constraint->minMessage, [
                '{{ value }}' => $value,
                '{{ limit }}' => $this->formatDate($constraint->min),
            ]);
        }
    }

    protected function formatDate($date)
    {
        $formatter = new \IntlDateFormatter(
            null,
            \IntlDateFormatter::SHORT,
            \IntlDateFormatter::NONE,
            date_default_timezone_get(),
            \IntlDateFormatter::GREGORIAN
        );

        return $this->processDate($formatter, $date);
    }

    protected function processDate(\IntlDateFormatter $formatter, \Datetime $date): string
    {
        return $formatter->format((int) $date->format('U'));
    }
}
