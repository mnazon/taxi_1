<?php

declare(strict_types=1);

namespace App\Api\Collection;

use App\Api\Entity\TaxiProviderRating;

class TaxiProviderRatingsMap
{
    /**
     * @var TaxiProviderRating[]
     */
    private $data;

    /**
     * Add element to map, key is - providerId, value - entity.
     */
    public function add(TaxiProviderRating $taxiProviderRating)
    {
        $taxiProviderId = $taxiProviderRating->getTaxiProvider()->getId();
        if ($this->containsKey($taxiProviderId)) {
            throw new \OutOfBoundsException('key already exist');
        }
        $this->data[$taxiProviderId] = $taxiProviderRating;
    }

    public function containsKey($key)
    {
        return isset($this->data[$key]);
    }

    public function get($key): TaxiProviderRating
    {
        return $this->data[$key];
    }

    public function find($key): ?TaxiProviderRating
    {
        return $this->data[$key] ?? null;
    }
}
