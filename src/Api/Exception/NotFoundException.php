<?php

namespace App\Api\Exception;

class NotFoundException extends \RuntimeException
{
}
