<?php

declare(strict_types=1);

namespace App\Api\Exception;

use Throwable;

class SmsSendException extends \RuntimeException
{
    private $phoneNumber;

    public function __construct($phoneNumber, string $message = 'Cannot send sms', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
}
