<?php

declare(strict_types=1);

namespace App\Api\Exception;

use App\Api\Enum\ApiErrorCode;
use App\Api\HttpDTO\Response\ApiProblem;
use App\Api\Service\ApiProblemBuilder;
use Throwable;

class ApiErrorCodeException extends \Exception implements ApiProblemExceptionInterface
{
    /** @var ApiProblem */
    private $apiProblem;
    /**
     * @var int
     */
    private $statusCode;
    /**
     * @var array
     */
    private $headers;

    public function __construct(ApiErrorCode $apiError, int $statusCode = 400, array $headers = [], string $message = '', int $code = 0, Throwable $previous = null)
    {
        $apiProblemBuilder = new ApiProblemBuilder();
        $apiProblemBuilder->setApiErrorCode($apiError);
        $this->apiProblem = $apiProblemBuilder->build();

        parent::__construct($message, $code, $previous);
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getApiProblem(): ApiProblem
    {
        return $this->apiProblem;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getHeaders()
    {
        return $this->headers;
    }
}
