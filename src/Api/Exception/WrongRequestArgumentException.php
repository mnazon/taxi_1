<?php

declare(strict_types=1);

namespace App\Api\Exception;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class WrongRequestArgumentException extends \Exception implements HttpExceptionInterface
{
    /**
     * @var array
     */
    private $headers;
    /**
     * @var int
     */
    private $statusCode;
    private $property;

    public function __construct(string $property, string $message = '', $statusCode = 400, int $code = 0, array $headers = [], \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->headers = $headers;
        $this->statusCode = $statusCode;
        $this->property = $property;
    }

    public function getProperty(): string
    {
        return $this->property;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getHeaders()
    {
        return $this->headers;
    }
}
