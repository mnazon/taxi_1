<?php

declare(strict_types=1);

namespace App\Api\Exception;

use Psr\Http\Message\ResponseInterface;
use Throwable;

class WrongProviderResponse extends \Exception
{
    /**
     * @var ResponseInterface
     */
    private $response;

    public function __construct(ResponseInterface $response, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }
}
