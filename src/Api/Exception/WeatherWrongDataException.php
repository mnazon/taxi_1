<?php

declare(strict_types=1);

namespace App\Api\Exception;

class WeatherWrongDataException extends \InvalidArgumentException
{
}
