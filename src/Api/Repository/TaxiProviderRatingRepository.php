<?php

namespace App\Api\Repository;

use App\Api\Collection\TaxiProviderRatingsMap;
use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TaxiProviderRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxiProviderRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxiProviderRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxiProviderRating[]    findAll()
 * @method TaxiProviderRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxiProviderRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxiProviderRating::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByProviderIds(array $providerIds): TaxiProviderRatingsMap
    {
        $taxiProviders = [];
        foreach ($providerIds as $providerId) {
            $taxiProviders[] = $this->getEntityManager()->getReference(
                TaxiProvider::class,
                $providerId
            );
        }

        $taxiProviderRatings = $this->createQueryBuilder('t')
            ->andWhere('t.taxiProvider IN (:taxiProviders)')
            ->setParameter('taxiProviders', $taxiProviders)
            ->getQuery()
            ->getResult()
        ;

        $collection = new TaxiProviderRatingsMap();
        foreach ($taxiProviderRatings as $taxiProviderRating) {
            $collection->add($taxiProviderRating);
        }

        return $collection;
    }

    public function getByProviderId(int $providerId): TaxiProviderRating
    {
        $taxiProvider = $this->getEntityManager()->getReference(
            TaxiProvider::class,
            $providerId
        );

        $taxiProviderRating = $this->createQueryBuilder('t')
            ->andWhere('t.taxiProvider = :taxiProvider')
            ->setParameter('taxiProvider', $taxiProvider)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
            ;

        if (null === $taxiProviderRating) {
            $taxiProviderRating = new TaxiProviderRating(
                0,
                0,
                $taxiProvider
            );
        }

        return $taxiProviderRating;
    }

    public function save(TaxiProviderRating $taxiProviderRating)
    {
        $this->getEntityManager()->persist($taxiProviderRating);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return TaxiProviderRating[] Returns an array of TaxiProviderRating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaxiProviderRating
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
