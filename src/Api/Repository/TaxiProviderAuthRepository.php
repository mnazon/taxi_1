<?php

namespace App\Api\Repository;

use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TaxiProviderAuth;
use App\Api\Enum\TaxiPlatform;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxiProviderAuth|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxiProviderAuth|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxiProviderAuth[]    findAll()
 * @method TaxiProviderAuth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxiProviderAuthRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxiProviderAuth::class);
    }

    /**
     * @return TaxiProviderAuth[]
     */
    public function findActiveAuthorizersWithEnabledProviderByTaxiPlatform(TaxiPlatform $taxiPlatform): array
    {
        $qb = $this->createQueryBuilder('auth')
            ->addCriteria(self::getIsActiveCriteria(true))
            ->join('auth.taxiProvider', 'p')
            ->addSelect('p')
            ->andWhere('p.enabled = 1')
            ->andWhere('p.taxiPlatform = :taxiPlatform')
            ->setParameter('taxiPlatform', $taxiPlatform->getValue());

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @return TaxiProviderAuth[]
     */
    public function findInactiveAuthorizersByProvider(TaxiProvider $provider): array
    {
        $qb = $this->createQueryBuilder('auth')
            ->addCriteria(self::getIsActiveCriteria(false))
            ->join('auth.taxiProvider', 'p')
            ->addSelect('p')
            ->andWhere('p = :provider')
            ->setParameter('provider', $provider)
        ;

        return $qb->getQuery()
            ->getResult();
    }

    public static function getIsActiveCriteria(bool $isActive)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('isActive', $isActive))
        ;

        return $criteria;
    }
}
