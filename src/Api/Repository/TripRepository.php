<?php

namespace App\Api\Repository;

use App\Api\DTO\PageDto;
use App\Api\Entity\TaxiProvider;
use App\Api\Entity\Trip;
use App\Api\Entity\User;
use App\Api\Enum\TripStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trip[]    findAll()
 * @method Trip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trip::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Trip $ride): Trip
    {
        $this->getEntityManager()->persist($ride);
        $this->getEntityManager()->flush();

        return $ride;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByExternalId(string $externalId, int $taxiProviderId): ?Trip
    {
        $em = $this->getEntityManager();
        $criteria = [
            'externalId' => $externalId,
            'taxiProvider' => $em->getReference(TaxiProvider::class, $taxiProviderId),
        ];

        return $this->findOneBy($criteria);
    }

    /**
     * @return Trip[]
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByUserId(int $userId, PageDto $pageDto): array
    {
        $user = $this->getEntityManager()->getReference(User::class, $userId);

        return $this->createQueryBuilder('r')
            ->select('r')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.id', 'DESC')
            ->setFirstResult($pageDto->getOffset())
            ->setMaxResults($pageDto->getLimit())
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Trip[]
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByUserAndStatuses(int $userId, array $statuses): array
    {
        $user = $this->getEntityManager()->getReference(User::class, $userId);

        return $this->createQueryBuilder('r')
            ->select('r', 'request', 'route')
            ->leftJoin('r.request', 'request')
            ->leftJoin('request.route', 'route')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->andWhere('r.status IN (:statuses)')
            ->setParameter('statuses', $statuses)
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param int $processedTripId
     *
     * @return Trip[]
     */
    public function findUnprocessedTripsByLastProcessedTripId(PageDto $pageDto, ?int $processedTripId): array
    {
        $qb = $this->createQueryBuilder('t');
        $qb
            ->select('t', 'request', 'route')
            ->leftJoin('t.request', 'request')
            ->leftJoin('request.route', 'route')
            ->orderBy('t.id', 'ASC')
            ->setFirstResult($pageDto->getOffset())
            ->setMaxResults($pageDto->getLimit())
        ;

        if (null !== $processedTripId) {
            $qb
                ->andWhere('t.id > :tripId')
                ->setParameter('tripId', $processedTripId)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function countByUserAndStatus(User $user, TripStatus $status): int
    {
        return $this->count(['user' => $user, 'status' => (int) $status->getValue()]);
    }
}
