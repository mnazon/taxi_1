<?php

namespace App\Api\Repository;

use App\Api\Entity\UserDevice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserDevice|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDevice|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDevice[]    findAll()
 * @method UserDevice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDeviceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserDevice::class);
    }

    public function save(UserDevice $userDevice)
    {
        $this->getEntityManager()->persist($userDevice);
        $this->getEntityManager()->flush();
    }

    public function findByDeviceToken(string $deviceToken): ?UserDevice
    {
        return $this->createQueryBuilder('ud')
            ->andWhere('ud.deviceToken = :deviceToken')
            ->setParameter('deviceToken', $deviceToken)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
