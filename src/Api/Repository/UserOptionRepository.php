<?php

namespace App\Api\Repository;

use App\Api\Entity\User;
use App\Api\Entity\UserOption;
use App\Api\Enum\UserOptionEnum;
use App\Api\HttpDTO\UserOptionUpdate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOption[]    findAll()
 * @method UserOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserOption::class);
    }

    public function save(UserOption $userOption)
    {
        $this->getEntityManager()->persist($userOption);
        $this->getEntityManager()->flush($userOption);
    }

    public function findOneByUserAndOption(User $user, UserOptionEnum $userOption): ?UserOption
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->andWhere('u.userOption = :userOption')
            ->setParameters([
                'user' => $user,
                'userOption' => (int) $userOption->getValue(),
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return UserOption[]
     */
    public function findByUserAndOptions(User $user, array $options): array
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->andWhere('u.userOption IN (:userOptions)')
            ->setParameters([
                'user' => $user,
                'userOptions' => $options,
            ]);

        return $qb->getQuery()
            ->getResult();
    }

    public function update(UserOptionUpdate $userOptionDto): int
    {
        $query = $this->createQueryBuilder('u')
            ->update()
            ->set('u.value', ':value')
            ->andWhere('u.user = :user')
            ->andWhere('u.userOption = :userOption')
            ->setParameters([
                'user' => $userOptionDto->getUser(),
                'userOption' => $userOptionDto->getUserOption()->getValue(),
                'value' => (int) $userOptionDto->getValue(),
            ])
            ->getQuery()
        ;

        $countUpdated = $query->execute();

        return $countUpdated;
    }
}
