<?php

namespace App\Api\Repository;

use App\Api\Entity\TripCars;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripCars|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripCars|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripCars[]    findAll()
 * @method TripCars[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripCarsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripCars::class);
    }

    public function findLastProcessedTripId(): ?int
    {
        $result = $this->createQueryBuilder('t')
            ->select('IDENTITY(t.trip) AS tripId')
            ->orderBy('t.id', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;

        return $result ? (int) $result['tripId'] : null;
    }
}
