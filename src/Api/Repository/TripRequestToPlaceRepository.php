<?php

namespace App\Api\Repository;

use App\Api\Entity\TripRequestToPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripRequestToPlace|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripRequestToPlace|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripRequestToPlace[]    findAll()
 * @method TripRequestToPlace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripRequestToPlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripRequestToPlace::class);
    }
}
