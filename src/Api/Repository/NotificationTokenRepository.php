<?php

namespace App\Api\Repository;

use App\Api\Entity\NotificationToken;
use App\Api\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException as ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NotificationToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotificationToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotificationToken[]    findAll()
 * @method NotificationToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotificationToken::class);
    }

    // /**
    //  * @return NotificationToken[] Returns an array of NotificationToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @throws ORMException
     */
    public function getByFcmToken(string $fcmToken, User $user): NotificationToken
    {
        $entity = $this->findOneByFcmToken($fcmToken);
        if (!$entity) {
            $entity = new NotificationToken();
            $entity->setUser($user);
            $entity->setFcmToken($fcmToken);
            $this->save($entity);
        }

        return $entity;
    }

    public function findOneByFcmToken($fcmToken): ?NotificationToken
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.fcmToken = :fcmToken')
            ->setParameter('fcmToken', $fcmToken)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function save(NotificationToken $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }
}
