<?php

namespace App\Api\Repository;

use App\Api\Entity\User;
use App\Api\Entity\UserLoginStatistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserLoginStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLoginStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLoginStatistic[]    findAll()
 * @method UserLoginStatistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLoginStatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLoginStatistic::class);
    }

    public function findLastByUser(User $user): ?UserLoginStatistic
    {
        $result = $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->setParameter('user', $user)
            ->orderBy('u.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    /**
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(UserLoginStatistic $statistic)
    {
        $this->getEntityManager()->persist($statistic);
        $this->getEntityManager()->flush($statistic);
    }
}
