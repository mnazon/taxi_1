<?php

declare(strict_types=1);

namespace App\Api\Repository;

use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TripCostFactor;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripCostFactor|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripCostFactor|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripCostFactor[]    findAll()
 * @method TripCostFactor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripCostFactorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripCostFactor::class);
    }

    public function findOneByDate(DateTimeInterface $date, TaxiProvider $taxiProvider = null): ?TripCostFactor
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.dateOfWeek = :dateOfWeek')
            ->andWhere('t.timeFrom <= :time')
            ->andWhere('t.timeTo > :time')
            ->setParameters([
                'dateOfWeek' => (int) $date->format('N'),
                'time' => $date->format('H:i:s'),
            ]);

        if (null !== $taxiProvider) {
            $query->andWhere('t.taxiProvider = :taxiProvider')
                ->setParameter('taxiProvider', $taxiProvider);
        } else {
            $query->andWhere('t.taxiProvider is null');
        }

        return $query->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
