<?php

namespace App\Api\Repository;

use App\Api\Entity\Place;
use App\Api\Entity\User;
use App\Api\Entity\UserTopPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException as OptimisticLockException;
use Doctrine\ORM\ORMException as ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTopPlace|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTopPlace|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTopPlace[]    findAll()
 * @method UserTopPlace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTopPlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTopPlace::class);
    }

    /**
     * @param UserTopPlace ...$userTopPlaces
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateUserTopPlaces(UserTopPlace ...$userTopPlaces): array
    {
        if (empty($userTopPlaces)) {
            throw new \InvalidArgumentException('Need to set up at least on UserTopPlace');
        }
        $em = $this->getEntityManager();
        $savedEntities = [];
        foreach ($userTopPlaces as $newUserTopPlace) {
            $userTopPlace = $this->findByUserAndPlace($newUserTopPlace->getUser(), $newUserTopPlace->getPlace());
            if (!$userTopPlace) {
                $userTopPlace = $newUserTopPlace->setTripCount(1);
            } else {
                $userTopPlace->incrementTripCount();
                $userTopPlace->setCoordinates($newUserTopPlace->getCoordinates());
            }
            $savedEntities[] = $userTopPlace;
            $em->persist($userTopPlace);
        }
        $em->flush();

        return $savedEntities;
//            ++$increment;
//            $qb->orWhere("u.user = :user{$increment} AND u.place = :place{$increment}")
//                ->setParameter("user{$increment}", $userTopPlace->getUser())
//                ->setParameter("place{$increment}", $userTopPlace->getPlace())
//            ;
    }

    public function findTopPlaces(int $userId, int $limit = 3)
    {
        $user = $this->getEntityManager()->getReference(User::class, $userId);
        $query = $this->createQueryBuilder('utp')
            ->join('utp.place', 'place')
            ->addSelect('place')
            ->where('utp.user = :user')
            ->setParameter('user', $user)
            ->orderBy('utp.tripCount', 'desc')
            ->setMaxResults($limit)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @expectedException \NonUniqueResultException
     */
    private function findByUserAndPlace(User $user, Place $place): ?UserTopPlace
    {
        $result = $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->andWhere('u.place = :place')
            ->setParameter('user', $user)
            ->setParameter('place', $place)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    //public function addExi

    // /**
    //  * @return UserTopPlace[] Returns an array of UserTopPlace objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTopPlace
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
