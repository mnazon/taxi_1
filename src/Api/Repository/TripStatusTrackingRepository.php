<?php

namespace App\Api\Repository;

use App\Api\Entity\TripStatusTracking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\DeadlockException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

/**
 * @method TripStatusTracking|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripStatusTracking|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripStatusTracking[]    findAll()
 * @method TripStatusTracking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripStatusTrackingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripStatusTracking::class);
    }

    /**
     * @throws DeadlockException
     */
    public function lockAndFindByStatusesAndRetryAt(
        array $statuses,
        \DateTimeImmutable $retryAt,
        Uuid $lockId,
        int $limit = 15
    ): array {
        $retryCount = 0;
        do {
            try {
                $updateQuery = $this->createQueryBuilder('t')
                    ->update(TripStatusTracking::class, 't')
                    ->set('t.lockId', ':setLockIdValue')
                    ->setParameter('setLockIdValue', $lockId->getHex())
                    ->andWhere('t.lockId is null')
                    ->andWhere('t.retryAt <= :retryAt')
                    ->andWhere('t.status IN (:statuses)')
                    ->setParameter('statuses', $statuses)
                    ->setParameter('retryAt', $retryAt)
                    ->setMaxResults($limit)
                    ->getQuery();
                $countUpdated = $updateQuery->execute();
            } catch (DeadlockException $e) {
                $countUpdated = null;
                ++$retryCount;
                if ($retryCount > 3) {
                    throw $e;
                }
                usleep(100);
            }
        } while (null === $countUpdated);

        if (0 === $countUpdated) {
            return [];
        }

        return $this->createQueryBuilder('t')
            ->andWhere('t.lockId <= :lockId')
            ->setParameter('lockId', $lockId->getHex())
            ->getQuery()
            ->getResult()
            ;
    }

    public function unlock(Uuid $lockId): int
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->update(TripStatusTracking::class, 't')
            ->set('t.lockId', ':setLockId')
            ->setParameter('setLockId', null)
            ->where('t.lockId = :lockId')
            ->setParameter('lockId', $lockId->getHex())
            ->getQuery();

        return $query->execute();
    }

    public function findByTripId(int $tripId): ?TripStatusTracking
    {
        return $this->findOneBy(['trip' => $tripId]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(TripStatusTracking $tripStatusTracking): TripStatusTracking
    {
        $this->getEntityManager()->persist($tripStatusTracking);
        $this->getEntityManager()->flush();

        return $tripStatusTracking;
    }

    // /**
    //  * @return TripStatusTracking[] Returns an array of TripStatusTracking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TripStatusTracking
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
