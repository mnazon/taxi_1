<?php

namespace App\Api\Repository;

use App\Api\Entity\User;
use App\Api\Entity\UserAppVersion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAppVersion|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAppVersion|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAppVersion[]    findAll()
 * @method UserAppVersion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAppVersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAppVersion::class);
    }

    public function findOneByUser(User $user): ?UserAppVersion
    {
        $resultCacheId = $this->buildCacheKeyByUser($user);

        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->enableResultCache(86400, $resultCacheId)
            ->getOneOrNullResult()
            ;
    }

    public function save(UserAppVersion $object)
    {
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush($object);
        $this->flushCacheByUser($object->getUser());
    }

    public function flushCacheByUser(User $user)
    {
        $this->getEntityManager()
            ->getConfiguration()
            ->getResultCacheImpl()
            ->delete($this->buildCacheKeyByUser($user))
        ;
    }

    private function buildCacheKeyByUser(User $user): string
    {
        return sprintf('user_app_version_%s', $user->getId());
    }
}
