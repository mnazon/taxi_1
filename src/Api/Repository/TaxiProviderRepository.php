<?php

namespace App\Api\Repository;

use App\Api\Entity\TaxiProvider;
use App\Api\Enum\TaxiPlatform;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxiProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxiProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxiProvider[]    findAll()
 * @method TaxiProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxiProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxiProvider::class);
    }

    /**
     * @return TaxiProvider[]
     */
    public function findByEnabled(bool $enabled): array
    {
        $resultCacheId = $this->buildEnabledCacheKey($enabled);

        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.enabled = :enabled')
            ->setParameter('enabled', (int) $enabled)
            ->orderBy('t.id', 'ASC');
        $this->withAuthorizersCondition($qb, true);

        return $qb->getQuery()
            ->useResultCache(true, 3600, $resultCacheId)
            ->getResult();
    }

    /**
     * @return TaxiProvider[]
     */
    public function findEnabledProvidersWithoutActiveAuthorizersByTaxiPlatform(TaxiPlatform $taxiPlatform): array
    {
        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.enabled = 1')
            ->andWhere('t.taxiPlatform = :taxiPlatform')
            ->setParameter('taxiPlatform', $taxiPlatform->getValue());
        $this->withAuthorizersCondition($qb, false);

        return $qb->getQuery()
            ->getResult();
    }

    public function flushEnabledCache()
    {
        $cache = $this->getEntityManager()
            ->getConfiguration()
            ->getResultCacheImpl();

        $cache->delete($this->buildEnabledCacheKey(true));
        $cache->delete($this->buildEnabledCacheKey(false));
    }

    private function buildEnabledCacheKey(bool $enabled): string
    {
        $resultCacheId = sprintf('enabled_taxi_provider_%s', (int) $enabled);

        return $resultCacheId;
    }

    private function withAuthorizersCondition(QueryBuilder $qb, bool $withActiveAuthorizer)
    {
        $qb->join('t.authorizers', 'auth');

        if ($withActiveAuthorizer) {
            $qb
                ->addSelect('auth')
                ->andWhere('auth.isActive = 1');
        } else {
            $qb
                ->groupBy('t')
                ->andHaving('SUM(auth.isActive) = 0');
        }
    }
}
