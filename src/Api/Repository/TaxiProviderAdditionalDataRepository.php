<?php

namespace App\Api\Repository;

use App\Api\Entity\TaxiProviderAdditionalData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxiProviderAdditionalData|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxiProviderAdditionalData|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxiProviderAdditionalData[]    findAll()
 * @method TaxiProviderAdditionalData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxiProviderAdditionalDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxiProviderAdditionalData::class);
    }
}
