<?php

namespace App\Api\Repository;

use App\Api\Entity\User;
use App\Api\Entity\UserSuccessTripCount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSuccessTripCount|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSuccessTripCount|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSuccessTripCount[]    findAll()
 * @method UserSuccessTripCount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSuccessTripCountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSuccessTripCount::class);
    }

    public function findOneByUser(User $user): ?UserSuccessTripCount
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function save(UserSuccessTripCount $successTripCount)
    {
        $this->getEntityManager()->persist($successTripCount);
        $this->getEntityManager()->flush($successTripCount);
    }
}
