<?php

namespace App\Api\Repository;

use App\Api\Entity\User;
use App\Api\Entity\UserToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserToken[]    findAll()
 * @method UserToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserToken::class);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByAccessToken(string $accessToken): ?UserToken
    {
        return $this->createQueryBuilder('ut')
            ->andWhere('ut.accessToken = :accessToken')
            ->setParameter('accessToken', $accessToken)
            ->orderBy('ut.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByUser(User $user): ?UserToken
    {
        return $this->createQueryBuilder('ut')
            ->andWhere('ut.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function save(UserToken $userToken)
    {
        $this->getEntityManager()->persist($userToken);
        $this->getEntityManager()->flush($userToken);
    }
}
