<?php

namespace App\Api\Repository;

use App\Api\DTO\Geo\PlaceData;
use App\Api\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException as NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    /**
     * @param int ...$ids
     *
     * @return Place[]
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function findByIds(int ...$ids): array
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.id IN(:ids)')
            ->setParameter('ids', $ids)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Place $place)
    {
        $this->getEntityManager()->persist($place);
        $this->getEntityManager()->flush();
    }

    /***
     * @param PlaceData $place
     *
     * @return Place|null
     * @expectedException NonUniqueResultException
     */
    public function checkIfExist(PlaceData $place): ?Place
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.lat = :lat')
            ->andWhere('p.lng = :lng')
            ->andWhere('p.name = :name')
            ->andWhere('p.locality = :locality')
            ->setParameter('lat', $place->getCoordinates()->getLat())
            ->setParameter('lng', $place->getCoordinates()->getLng())
            ->setParameter('name', $place->getName())
            ->setParameter('locality', $place->getLocality())
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @expectedException \NonUniqueResultException
     */
    public function findByExternalPlaceId(string $placeId): ?Place
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.externalId = :placeId')
            ->setParameter('placeId', $placeId)
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}
