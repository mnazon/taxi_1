<?php

namespace App\Api\Repository;

use App\Api\Entity\WeatherHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeatherHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherHistory[]    findAll()
 * @method WeatherHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeatherHistory::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(WeatherHistory $weatherHistory)
    {
        $this->getEntityManager()->persist($weatherHistory);
        $this->getEntityManager()->flush($weatherHistory);
    }
}
