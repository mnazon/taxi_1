<?php

namespace App\Api\Repository;

use App\Api\Entity\Phone;
use App\Api\Entity\PhoneConfirmation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PhoneConfirmation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneConfirmation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneConfirmation[]    findAll()
 * @method PhoneConfirmation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneConfirmationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneConfirmation::class);
    }

    // /**
    //  * @return PhoneConfirmation[] Returns an array of PhoneConfirmation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneConfirmation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByPhonePeriodOfSendAndCode(
        Phone $phone,
        \DateTimeInterface $sendStart,
        \DateTimeInterface $sendEnd,
        int $confirmationCode = null
    ): ?PhoneConfirmation {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.phone = :phone')
            ->setParameter('phone', $phone)
            ->andWhere('p.sentAt BETWEEN :start AND :end')
            ->setParameter('start', $sendStart)
            ->setParameter('end', $sendEnd)
            ->setMaxResults(1)
            ->orderBy('p.id', 'DESC')
        ;
        if (null !== $confirmationCode) {
            $qb->andWhere('p.code = :code')
                ->setParameter('code', $confirmationCode);
        }
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    public function findLastByPhone(Phone $phone): ?PhoneConfirmation
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.phone = :phone')
            ->setParameter('phone', $phone)
            ->setMaxResults(1)
            ->orderBy('p.id', 'DESC')
        ;

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}
