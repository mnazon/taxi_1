<?php

namespace App\Api\Repository;

use App\Api\DTO\PageDto;
use App\Api\Entity\TaxiProvider;
use App\Api\Entity\Trip;
use App\Api\Entity\TripVote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripVote|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripVote|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripVote[]    findAll()
 * @method TripVote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripVoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripVote::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(TripVote $tripVote)
    {
        if (null !== $tripVote->getId()) {
            throw new \RuntimeException('Cannot update entity');
        }

        $this->getEntityManager()->persist($tripVote);
        $this->getEntityManager()->flush($tripVote);
    }

    public function findByTrip(Trip $trip): ?TripVote
    {
        return $this->createQueryBuilder('tv')
            ->where('tv.voter = :user')
            ->andWhere('tv.trip = :trip')
            ->setParameter('user', $trip->getUser()->getId())
            ->setParameter('trip', $trip->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return TripVote[]
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByTaxiProvider(int $taxiProviderId, PageDto $pageDto): array
    {
        $taxiProvider = $this->getEntityManager()->getReference(TaxiProvider::class, $taxiProviderId);
        //todo optimize request, add caching, add user data fetching

        $query = $this->createQueryBuilder('tv')
            ->select('tv', 'user')
            ->join('tv.voter', 'user')
            ->andWhere('tv.taxiProvider = :taxiProvider')
            ->setParameter('taxiProvider', $taxiProvider)
            ->orderBy('tv.id', 'DESC')
            ->setMaxResults($pageDto->getLimit())
            ->setFirstResult($pageDto->getOffset())
            ->getQuery();

        return $query->getResult();
    }

    // /**
    //  * @return TripVote[] Returns an array of TripVote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TripVote
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
