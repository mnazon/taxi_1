<?php

namespace App\Api\Repository;

use App\Api\DTO\PageDto;
use App\Api\Entity\TripStatusLog;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripStatusLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripStatusLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripStatusLog[]    findAll()
 * @method TripStatusLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripStatusLogRepository extends ServiceEntityRepository
{
    private const MIN_TRIP_COUNT = 10;
    private const RATE_DAY_INTERVAL = 30;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripStatusLog::class);
    }

    /**
     * @return TripStatusLog[]
     */
    public function findByStatusAndCreatedAtPeriod(TripStatus $tripStatus, \DatePeriod $datePeriod, PageDto $pageDto): array
    {
        //add index for createdAt
        $queryBuilder = $this->createQueryBuilder('ts')
            ->join('ts.trip', 'trip')
            ->addSelect('trip')
            ->andWhere('ts.status = :status')
            ->setParameter('status', $tripStatus->getValue())
            ->andWhere('ts.createdAt >= :dateTimeFrom')
            ->setParameter('dateTimeFrom', $datePeriod->getStartDate())
            ->andWhere('ts.createdAt <= :dateTimeTo')
            ->setParameter('dateTimeTo', $datePeriod->getEndDate())
            ->setParameter('status', $tripStatus->getValue())
            ->setFirstResult($pageDto->getOffset())
            ->setMaxResults($pageDto->getLimit());

        $data = $queryBuilder
            ->getQuery()
            ->getResult()
            ;

        return $data;
    }

    public function save(TripStatusLog $tripStatusLog)
    {
        $this->getEntityManager()->persist($tripStatusLog);
        $this->getEntityManager()->flush();
    }

    public function getExecutedToCanceledTripRatio()
    {
        $canceledTripCountByProvider = $this->getCanceledTripCount();
        $successTripCountByProvider = $this->getSuccessTripCount();

        $result = [];
        $allTripCount = $successTripCountByProvider;
        foreach ($canceledTripCountByProvider as $providerId => $count) {
            if (!isset($allTripCount[$providerId])) {
                $allTripCount[$providerId] = 0;
            }
            $allTripCount[$providerId] += $count;
        }

        foreach ($allTripCount as $providerId => $count) {
            $successTripCount = $successTripCountByProvider[$providerId] ?? 0;
            if ($count > self::MIN_TRIP_COUNT) {
                $result[$providerId] = (int) (round($successTripCount / $count, 2) * 100);
            }
        }

        return $result;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getCanceledTripCount(): array
    {
        $connection = $this->getEntityManager()->getConnection();
        $canceledSql = '
            SELECT taxiProvider_id, count(1) as count FROM
                    (SELECT trip.createdAt,
                               trip.id,
                               trip.taxiProvider_id,
                               trip.cancelReason,
                               GROUP_CONCAT(tsl.status
                                            ORDER BY tsl.createdAt) as st,
                               GROUP_CONCAT(tsl.createdAt
                                            ORDER BY tsl.createdAt) as created,
                               SUM(case
                                       when tsl.status = 5 then 1
                                       else 0
                                   end) as count_car_search,
                               UNIX_TIMESTAMP(max(tsl.createdAt)) - UNIX_TIMESTAMP(trip.createdAt) as tripTimeSearch
                        FROM trip
                        JOIN tripStatusLog tsl on trip.id = tsl.trip_id
                        WHERE trip.status = :canceledStatus
                          and trip.createdAt BETWEEN NOW() - INTERVAL :rateDayInterval DAY AND NOW()
                        GROUP BY tsl.trip_id) as canceledTrip
             WHERE (canceledTrip.st like :searchAndCancel #if user was wait to long and car not found at all
                     and canceledTrip.st not like :runningAndCanceled
                     and canceledTrip.cancelReason = :canceledByUserFromApplication
                     and canceledTrip.tripTimeSearch > :tooLongWaitTime)
             or ((canceledTrip.st like :runningAndCanceled #if provider was search for multiple times
                  or canceledTrip.st like :foundAndCanceled)
                 and canceledTrip.count_car_search > 1) 
             or (canceledTrip.cancelReason <> :canceledByUserFromApplication #if provider was cancel the trip and was not found the car in some period of time
                 and canceledTrip.st not like :runningAndCanceled
                 and canceledTrip.st not like :foundAndCanceled)
          GROUP BY taxiProvider_id
        ';
        $query = $connection->prepare($canceledSql);

        $query->bindValue('runningAndCanceled', '%'.TripStatus::RUNNING.','.TripStatus::CANCELED);
        $query->bindValue('foundAndCanceled', '%'.TripStatus::CAR_FOUND.','.TripStatus::CANCELED);
        $query->bindValue('searchAndCancel', '%'.TripStatus::SEARCH_FOR_CAR.','.TripStatus::CANCELED);
        $query->bindValue('canceledByUserFromApplication', CancelReason::RIDER_CANCEL_FROM_APPLICATION);
        $query->bindValue('canceledStatus', TripStatus::CANCELED);
        $query->bindValue('tooLongWaitTime', 180);
        $query->bindValue('rateDayInterval', self::RATE_DAY_INTERVAL);
        $query->execute();
        $canceledResult = $query->fetchAll(AbstractQuery::HYDRATE_ARRAY);

        $result = [];
        foreach ($canceledResult as $item) {
            $result[$item['taxiProvider_id']] = $item['count'];
        }

        return $result;
    }

    /**
     * @return mixed
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getSuccessTripCount()
    {
        $connection = $this->getEntityManager()->getConnection();
        $executedSql = '
            SELECT taxiProvider_id, count(1) as count FROM
                    (SELECT trip.createdAt,
                            trip.id,
                            trip.taxiProvider_id,
                            GROUP_CONCAT(tsl.status
                                         ORDER BY tsl.createdAt) as st,
                            GROUP_CONCAT(tsl.createdAt
                                         ORDER BY tsl.createdAt) as created,
                            trip.cancelReason,
                            SUM(case
                                    when tsl.status = :searching then 1
                                    else 0
                                end) as count_car_search
                     FROM trip
                     JOIN tripStatusLog tsl on trip.id = tsl.trip_id
            WHERE
                trip.createdAt BETWEEN NOW() - INTERVAL :rateDayInterval DAY AND NOW()
            GROUP BY tsl.trip_id) t1
                  where (((t1.st like :runningAndCanceled
                          or t1.st like :foundAndCanceled)
                          and count_car_search < 2)
                         or t1.st like :likeExecuted)
            GROUP BY taxiProvider_id
        ';
        $query = $connection->prepare($executedSql);
        $params = [
            'likeExecuted' => '%'.TripStatus::EXECUTED,
            'searching' => TripStatus::SEARCH_FOR_CAR,
            'runningAndCanceled' => '%'.TripStatus::RUNNING.','.TripStatus::CANCELED,
            'foundAndCanceled' => '%'.TripStatus::CAR_FOUND.','.TripStatus::CANCELED,
            'rateDayInterval' => self::RATE_DAY_INTERVAL,
        ];
        $query->execute($params);
        $successResult = $query->fetchAll(FetchMode::ASSOCIATIVE);
        $result = [];
        foreach ($successResult as $item) {
            $result[$item['taxiProvider_id']] = $item['count'];
        }

        return $result;
    }
}
