<?php

namespace App\Api\Repository;

use App\Api\Entity\Notification;
use App\Api\Entity\Trip;
use App\Api\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    /**
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Notification $voteNotification)
    {
        $this->getEntityManager()->persist($voteNotification);
        $this->getEntityManager()->flush($voteNotification);
    }

    public function findByTrip(Trip $trip): ?Notification
    {
        return $this->createQueryBuilder('n')
            ->where('n.user = :user')
            ->andWhere('n.trip = :trip')
            ->setParameter('user', $trip->getUser()->getId())
            ->setParameter('trip', $trip->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function findLastNotCanceledByUserId(int $userId): ?Notification
    {
        $user = $this->getEntityManager()->getReference(User::class, $userId);

        $query = $this->createQueryBuilder('n')
            ->andWhere('n.user = :user')
            ->andWhere('n.canceledAt IS NULL')
            ->setParameter('user', $user)
            ->orderBy('n.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @return mixed
     *
     * @throws ORMException
     */
    public function cancelByUserId(int $userId): int
    {
        $user = $this->getEntityManager()->getReference(User::class, $userId);

        $query = $this->createQueryBuilder('n')
            ->update(Notification::class, 'n')
            ->set('n.canceledAt', ':currentTime')
            ->setParameter('currentTime', new \DateTimeImmutable())
            ->andWhere('n.user = :user')
            ->andWhere('n.canceledAt IS NULL')
            ->setParameter('user', $user)
            ->getQuery();

        $countUpdated = $query->execute();

        return $countUpdated;
    }

    // /**
    //  * @return Notification[] Returns an array of Notification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notification
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
