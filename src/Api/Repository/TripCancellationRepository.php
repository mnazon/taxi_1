<?php

namespace App\Api\Repository;

use App\Api\Entity\TripCancellation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TripCancellationRepository extends ServiceEntityRepository
{
    /**
     * TripCancellationRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripCancellation::class);
    }
}
