<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190226153330 extends AbstractMigration
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'CREATE TABLE tripRequestToPlace (
                id INT unsigned AUTO_INCREMENT NOT NULL,
                place_id INT NOT NULL,
                tripRequest_id INT NOT NULL, 
                INDEX IDX_25F3E5A4DA6A219 (place_id), 
                INDEX IDX_25F3E5A45DA05B3F (tripRequest_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('CREATE TABLE place (
            id INT AUTO_INCREMENT NOT NULL, 
            mainText VARCHAR(150) NOT NULL, 
            secondaryText VARCHAR(255) NOT NULL, 
            lat NUMERIC(11, 8) NOT NULL, 
            lng NUMERIC(11, 8) NOT NULL, 
            PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_25F3E5A4DA6A219 FOREIGN KEY (place_id) REFERENCES tripRequest (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_25F3E5A45DA05B3F FOREIGN KEY (tripRequest_id) REFERENCES place (id)');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripRequestToPlace DROP FOREIGN KEY FK_25F3E5A45DA05B3F');
        $this->addSql('DROP TABLE tripRequestToPlace');
        $this->addSql('DROP TABLE Place');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
    }
}
