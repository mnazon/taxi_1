<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301120644 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tripPlace');

        $this->addSql('
            ALTER TABLE place 
              ADD streetNumber VARCHAR(15) DEFAULT NULL, 
              ADD street VARCHAR(100) DEFAULT NULL, 
              ADD locality VARCHAR(70) DEFAULT NULL, 
              ADD admLevel1 VARCHAR(50) NOT NULL, 
              ADD approximate TINYINT(1) NOT NULL, 
              ADD name VARCHAR(150) DEFAULT NULL, 
              DROP mainText, 
              CHANGE secondarytext externalId VARCHAR(255) NOT NULL
        ');
        $this->addSql('CREATE INDEX externalId ON place (externalId(16))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tripPlace (id INT AUTO_INCREMENT NOT NULL, trip_id INT DEFAULT NULL, request_id INT NOT NULL, name VARCHAR(150) NOT NULL COLLATE utf8mb4_unicode_ci, lat NUMERIC(11, 8) NOT NULL, lng NUMERIC(11, 8) NOT NULL, INDEX IDX_C3C19FB9427EB8A5 (request_id), INDEX IDX_C3C19FB9A5BC2E0E (trip_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tripPlace ADD CONSTRAINT FK_C3C19FB9427EB8A5 FOREIGN KEY (request_id) REFERENCES tripRequest (id)');
        $this->addSql('ALTER TABLE tripPlace ADD CONSTRAINT FK_C3C19FB9A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX externalId ON place');
        $this->addSql('ALTER TABLE place ADD mainText VARCHAR(150) NOT NULL COLLATE utf8mb4_unicode_ci, DROP streetNumber, DROP street, DROP locality, DROP admLevel1, DROP approximate, DROP name, CHANGE externalid secondaryText VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa45da05b3f TO IDX_25F3E5A45DA05B3F');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa4da6a219 TO IDX_25F3E5A4DA6A219');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
    }
}
