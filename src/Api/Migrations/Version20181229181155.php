<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181229181155 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE tripStatusLog (
                id INT AUTO_INCREMENT NOT NULL, 
                trip_id INT NOT NULL, 
                status TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', 
                createdAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
                INDEX IDX_BEA41F7BA5BC2E0E (trip_id), 
                PRIMARY KEY(id)
            ) 
            DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE tripStatusLog 
          ADD CONSTRAINT FK_BEA41F7BA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tripStatusLog');
    }
}
