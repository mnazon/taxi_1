<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208161353 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
        $this->addSql('ALTER TABLE trip RENAME INDEX fk_7656f53bd9feee25 TO IDX_7656F53BD9FEEE25');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(16))');
        $this->addSql('ALTER TABLE trip RENAME INDEX idx_7656f53bd9feee25 TO FK_7656F53BD9FEEE25');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId)');
    }
}
