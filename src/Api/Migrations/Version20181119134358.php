<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181119134358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE phone (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(20) NOT NULL, createdAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', confirmedAt DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phoneConfirmation (
        id INT AUTO_INCREMENT NOT NULL, 
        phone_id INT NOT NULL, 
        code SMALLINT UNSIGNED NOT NULL, 
        sentAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        confirmedAt DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        ip INT NOT NULL, INDEX IDX_8EFC17373B7323CB (phone_id), 
        PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE phoneConfirmation ADD CONSTRAINT FK_8EFC17373B7323CB FOREIGN KEY (phone_id) REFERENCES phone (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE phoneConfirmation DROP FOREIGN KEY FK_8EFC17373B7323CB');
        $this->addSql('DROP TABLE phone');
        $this->addSql('DROP TABLE phoneConfirmation');
    }
}
