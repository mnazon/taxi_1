<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190227080633 extends AbstractMigration
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE userTopPlace (
                id INT AUTO_INCREMENT NOT NULL, 
                user_id INT NOT NULL, 
                place_id INT NOT NULL, 
                tripCount SMALLINT NOT NULL,
                INDEX IDX_9D29673BA76ED395 (user_id), 
                INDEX IDX_9D29673BDA6A219 (place_id), 
                PRIMARY KEY(id)
             ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('ALTER TABLE userTopPlace ADD CONSTRAINT FK_9D29673BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE userTopPlace ADD CONSTRAINT FK_9D29673BDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE userTopPlace');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
    }
}
