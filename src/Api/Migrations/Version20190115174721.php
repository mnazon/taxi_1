<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190115174721 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tripRequest (
        id INT AUTO_INCREMENT NOT NULL, 
        carType VARCHAR(15) NOT NULL, 
        reservedAt DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        userFullName VARCHAR(100) DEFAULT NULL, 
        userPhone VARCHAR(20) DEFAULT NULL, 
        options SET(\'courier\', \'baggage\', \'conditioner\', \'animal\') DEFAULT NULL COMMENT \'(DC2Type:AdditionalServicesType)\', 
        startRouteEntranceFrom TINYINT DEFAULT NULL COMMENT \'(DC2Type:tinyint)\', 
        comment VARCHAR(255) DEFAULT NULL, 
        PRIMARY KEY(id)) 
        DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE trip ADD request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B427EB8A5 FOREIGN KEY (request_id) REFERENCES tripRequest (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7656F53B427EB8A5 ON trip (request_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B427EB8A5');
        $this->addSql('DROP TABLE tripRequest');
        $this->addSql('DROP INDEX UNIQ_7656F53B427EB8A5 ON trip');
        $this->addSql('ALTER TABLE trip DROP request_id');
    }
}
