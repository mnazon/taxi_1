<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191108215050 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE internalProviderTrip (id INT AUTO_INCREMENT NOT NULL, cost NUMERIC(8, 2) NOT NULL, additionalCost NUMERIC(8, 2) DEFAULT NULL, currency VARCHAR(4) NOT NULL, vehicle JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', status TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', createdAt DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripCars ADD CONSTRAINT FK_F457526AA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('DROP TABLE internalProviderTrip');
    }
}
