<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190327121155 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripVote ADD taxiProvider_id SMALLINT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE tripVote ADD CONSTRAINT FK_3B80CA1AF865BD96 FOREIGN KEY (taxiProvider_id) REFERENCES taxiProvider (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX externalId ON place');
        $this->addSql('CREATE INDEX externalId ON place (externalId(16))');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD PRIMARY KEY (id)');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
        $this->addSql('ALTER TABLE tripVote DROP FOREIGN KEY FK_3B80CA1AF865BD96');
        $this->addSql('DROP INDEX IDX_3B80CA1AF865BD96 ON tripVote');
        $this->addSql('ALTER TABLE tripVote DROP taxiProvider_id');
    }
}
