<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181118062302 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE place (
            id INT AUTO_INCREMENT NOT NULL, 
            trip_id INT NOT NULL, 
            name VARCHAR(150) NOT NULL, 
            lat NUMERIC(11, 8) NOT NULL, 
            lng NUMERIC(11, 8) NOT NULL, 
            INDEX IDX_741D53CDA5BC2E0E (trip_id), 
            PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('CREATE TABLE taxiProvider (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(70) NOT NULL, slug VARCHAR(70) NOT NULL, enabled TINYINT(1) NOT NULL, createdAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip (id INT AUTO_INCREMENT NOT NULL, externalId VARCHAR(50) NOT NULL, cost NUMERIC(8, 2) NOT NULL, currency VARCHAR(4) NOT NULL, createdAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', vehicle VARCHAR(100) DEFAULT NULL, driverPhone VARCHAR(20) DEFAULT NULL, closeReason TINYINT DEFAULT NULL COMMENT \'(DC2Type:tinyint)\', status TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', taxiProvider_id SMALLINT UNSIGNED NOT NULL, INDEX IDX_7656F53BF865BD96 (taxiProvider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BF865BD96 FOREIGN KEY (taxiProvider_id) REFERENCES taxiProvider (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BF865BD96');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CDA5BC2E0E');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE taxiProvider');
        $this->addSql('DROP TABLE trip');
    }
}
