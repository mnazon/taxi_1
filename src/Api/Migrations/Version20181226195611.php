<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181226195611 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE taxiProviderRating RENAME INDEX uniq_8473ad47f865bd96 TO UNIQ_D9351577F865BD96');
        $this->addSql('ALTER TABLE tripVote ADD comment VARCHAR(2000) DEFAULT NULL');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX idx_c2f6a84cebb4b8ad TO IDX_3B80CA1AEBB4B8AD');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX uniq_c2f6a84ca5bc2e0e TO UNIQ_3B80CA1AA5BC2E0E');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE taxiProviderRating RENAME INDEX uniq_d9351577f865bd96 TO UNIQ_8473AD47F865BD96');
        $this->addSql('ALTER TABLE tripVote DROP comment');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX uniq_3b80ca1aa5bc2e0e TO UNIQ_C2F6A84CA5BC2E0E');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX idx_3b80ca1aebb4b8ad TO IDX_C2F6A84CEBB4B8AD');
    }
}
