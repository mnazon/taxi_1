<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200221001136 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripRequestToPlace ADD lat NUMERIC(11, 8) DEFAULT NULL, ADD lng NUMERIC(11, 8) DEFAULT NULL, ADD routePointNumber SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa45da05b3f TO IDX_992A8AA412FD58EF');
        $this->addSql('ALTER TABLE userTopPlace ADD lat NUMERIC(11, 8) DEFAULT NULL, ADD lng NUMERIC(11, 8) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripRequestToPlace DROP lat, DROP lng, DROP routePointNumber');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa412fd58ef TO IDX_992A8AA45DA05B3F');
        $this->addSql('ALTER TABLE userTopPlace DROP lat, DROP lng');
    }
}
