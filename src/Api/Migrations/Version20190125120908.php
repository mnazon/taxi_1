<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125120908 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tripPlace (id INT AUTO_INCREMENT NOT NULL, trip_id INT NOT NULL, name VARCHAR(150) NOT NULL, lat NUMERIC(11, 8) NOT NULL, lng NUMERIC(11, 8) NOT NULL, INDEX IDX_C3C19FB9A5BC2E0E (trip_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tripPlace ADD CONSTRAINT FK_C3C19FB9A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('DROP TABLE place');
        $this->addSql('ALTER TABLE phoneConfirmation CHANGE sentAt sentAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE place (id INT AUTO_INCREMENT NOT NULL, trip_id INT NOT NULL, name VARCHAR(150) NOT NULL COLLATE utf8mb4_unicode_ci, lat NUMERIC(11, 8) NOT NULL, lng NUMERIC(11, 8) NOT NULL, INDEX IDX_741D53CDA5BC2E0E (trip_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('DROP TABLE tripPlace');
        $this->addSql('ALTER TABLE phoneConfirmation CHANGE sentAt sentAt DATETIME NOT NULL COMMENT \'(DC2Type:datetimetz_immutable)\'');
    }
}
