<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208154922 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE notificationToken 
            (id INT AUTO_INCREMENT NOT NULL, 
            user_id INT NOT NULL, fcmToken VARCHAR(255) NOT NULL, 
            INDEX IDX_7BEFB51EA76ED395 (user_id), 
            INDEX fcmToken (fcmToken(16)), 
            PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE notificationToken 
ADD CONSTRAINT FK_7BEFB51EA76ED395 
FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip ADD notificationToken_id INT DEFAULT NULL');
        $this->addSql('
            ALTER TABLE trip ADD CONSTRAINT FK_7656F53BD9FEEE25 
            FOREIGN KEY (notificationToken_id) 
            REFERENCES notificationToken (id)
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BD9FEEE25');
        $this->addSql('DROP TABLE notificationToken');
        $this->addSql('DROP INDEX IDX_7656F53BD9FEEE25 ON trip');
        $this->addSql('ALTER TABLE trip DROP notificationToken_id');
    }
}
