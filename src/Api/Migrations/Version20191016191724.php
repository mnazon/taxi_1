<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191016191724 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE weatherHistory (id INT AUTO_INCREMENT NOT NULL, temperature TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', windSpeed TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', precipitationRain1h NUMERIC(4, 2) DEFAULT NULL, precipitationRain3h NUMERIC(4, 2) DEFAULT NULL, precipitationSnow1h NUMERIC(4, 2) DEFAULT NULL, precipitationSnow3h NUMERIC(4, 2) DEFAULT NULL, createdAt DATETIME NOT NULL, INDEX created_at_index (createdAt), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE weatherHistory');
    }
}
