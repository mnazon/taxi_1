<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190326061752 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripRequestToPlace DROP FOREIGN KEY FK_25F3E5A45DA05B3F');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP FOREIGN KEY FK_25F3E5A4DA6A219');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_992A8AA45DA05B3F FOREIGN KEY (tripRequest_id) REFERENCES tripRequest (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_992A8AA4DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');

        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_25f3e5a45da05b3f TO IDX_992A8AA45DA05B3F');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_25f3e5a4da6a219 TO IDX_992A8AA4DA6A219');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX externalId ON place');
        $this->addSql('CREATE INDEX externalId ON place (externalId(16))');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP FOREIGN KEY FK_992A8AA45DA05B3F');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP FOREIGN KEY FK_992A8AA4DA6A219');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_25F3E5A45DA05B3F FOREIGN KEY (tripRequest_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD CONSTRAINT FK_25F3E5A4DA6A219 FOREIGN KEY (place_id) REFERENCES tripRequest (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa4da6a219 TO IDX_25F3E5A4DA6A219');
        $this->addSql('ALTER TABLE tripRequestToPlace RENAME INDEX idx_992a8aa45da05b3f TO IDX_25F3E5A45DA05B3F');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
    }
}
