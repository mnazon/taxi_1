<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181219110042 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
          ALTER TABLE userToken DROP INDEX accessToken, 
          ADD UNIQUE INDEX UNIQ_B952B2ED350A9822 (accessToken)'
        );
        $this->addSql('ALTER TABLE userToken ADD createdAt DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE userToken DROP INDEX UNIQ_B952B2ED350A9822, ADD INDEX accessToken (accessToken)');
        $this->addSql('ALTER TABLE userToken DROP createdAt');
    }
}
