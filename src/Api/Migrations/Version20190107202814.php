<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190107202814 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
CREATE TABLE userDevice 
(id INT AUTO_INCREMENT NOT NULL, 
user_id INT NOT NULL, 
deviceToken VARCHAR(64) NOT NULL, 
firebaseRegistrationToken VARCHAR(100) NOT NULL, 
INDEX IDX_6679A47CA76ED395 (user_id), 
INDEX deviceToken (deviceToken), 
PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
');
        $this->addSql('ALTER TABLE userDevice 
ADD CONSTRAINT FK_6679A47CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)
');
        $this->addSql('ALTER TABLE trip ADD device_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B94A4C7D4 FOREIGN KEY (device_id) REFERENCES userDevice (id)');
        $this->addSql('CREATE INDEX IDX_7656F53B94A4C7D4 ON trip (device_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B94A4C7D4');
        $this->addSql('DROP TABLE userDevice');
        $this->addSql('DROP INDEX IDX_7656F53B94A4C7D4 ON trip');
        $this->addSql('ALTER TABLE trip DROP device_id');
    }
}
