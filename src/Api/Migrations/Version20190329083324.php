<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329083324 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification RENAME INDEX idx_user TO IDX_BF5476CAA76ED395');
        $this->addSql('ALTER TABLE notification RENAME INDEX uniq_trip TO UNIQ_BF5476CAA5BC2E0E');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification RENAME INDEX idx_bf5476caa76ed395 TO IDX_user');
        $this->addSql('ALTER TABLE notification RENAME INDEX uniq_bf5476caa5bc2e0e TO UNIQ_trip');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX externalId ON place');
        $this->addSql('CREATE INDEX externalId ON place (externalId(16))');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD PRIMARY KEY (id)');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX idx_3b80ca1af865bd96 TO FK_3B80CA1AF865BD96');
    }
}
