<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181206113308 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD username VARCHAR(100) NOT NULL, CHANGE phone phone VARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_2da17977444f97dd TO UNIQ_8D93D649444F97DD');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE RegistrationRequest');
        $this->addSql('ALTER TABLE user DROP username, CHANGE phone phone VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d649444f97dd TO UNIQ_2DA17977444F97DD');
    }
}
