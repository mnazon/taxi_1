<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190729073348 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE taxiProviderAuth (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, isActive TINYINT(1) NOT NULL, auth JSON NOT NULL COMMENT \'(DC2Type:json_array)\', createdAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', taxiProvider_id SMALLINT UNSIGNED NOT NULL, INDEX IDX_67C73DC2F865BD96 (taxiProvider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE taxiProviderAuth ADD CONSTRAINT FK_67C73DC2F865BD96 FOREIGN KEY (taxiProvider_id) REFERENCES taxiProvider (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE taxiProviderAuth');
    }
}
