<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200328131918 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE taxiProviderAdditionalData ADD standardTariffName VARCHAR(50) DEFAULT NULL');
        $this->addSql(<<<Mysql
UPDATE taxiProviderAdditionalData 
SET standardTariffName=? 
WHERE taxiProvider_id IN (
    SELECT id 
    FROM taxiProvider 
    WHERE slug=?
    )
Mysql
            , ['Приложение', 'Opti']);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE taxiProviderAdditionalData DROP standardTariffName');
    }
}
