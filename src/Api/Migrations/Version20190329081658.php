<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329081658 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE notification 
            (
                id INT AUTO_INCREMENT NOT NULL, 
                user_id INT NOT NULL, 
                trip_id INT NOT NULL, 
                canceledAt DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
                INDEX IDX_user (user_id), 
                UNIQUE INDEX UNIQ_trip (trip_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('DROP INDEX externalId ON place');
        $this->addSql('CREATE INDEX externalId ON place (externalId(16))');
        $this->addSql('ALTER TABLE tripRequestToPlace DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE tripRequestToPlace ADD PRIMARY KEY (id)');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
        $this->addSql('ALTER TABLE tripVote RENAME INDEX idx_3b80ca1af865bd96 TO FK_3B80CA1AF865BD96');
    }
}
