<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190204152545 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tripStatusTracking ADD lockId CHAR(32) DEFAULT NULL');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId)');
        $this->addSql('CREATE INDEX retryAt ON tripStatusTracking (retryAt)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('DROP INDEX retryAt ON tripStatusTracking');
        $this->addSql('ALTER TABLE tripStatusTracking DROP lockId');
    }
}
