<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181219000209 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX accessToken ON userToken (accessToken)');
        $this->addSql('ALTER TABLE userToken RENAME INDEX uniq_e297fda0a76ed395 TO UNIQ_B952B2EDA76ED395');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX accessToken ON userToken');
        $this->addSql('ALTER TABLE userToken RENAME INDEX uniq_b952b2eda76ed395 TO UNIQ_E297FDA0A76ED395');
    }
}
