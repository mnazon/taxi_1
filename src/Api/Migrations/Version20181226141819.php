<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181226141819 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE taxiProviderRating (id INT AUTO_INCREMENT NOT NULL, rate DOUBLE PRECISION DEFAULT NULL, numVotes INT NOT NULL, taxiProvider_id SMALLINT UNSIGNED NOT NULL, UNIQUE INDEX UNIQ_8473AD47F865BD96 (taxiProvider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tripVote (id INT AUTO_INCREMENT NOT NULL, voter_id INT NOT NULL, trip_id INT NOT NULL, value TINYINT NOT NULL COMMENT \'(DC2Type:tinyint)\', createdAt DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C2F6A84CEBB4B8AD (voter_id), UNIQUE INDEX UNIQ_C2F6A84CA5BC2E0E (trip_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE taxiProviderRating ADD CONSTRAINT FK_8473AD47F865BD96 FOREIGN KEY (taxiProvider_id) REFERENCES taxiProvider (id)');
        $this->addSql('ALTER TABLE tripVote ADD CONSTRAINT FK_C2F6A84CEBB4B8AD FOREIGN KEY (voter_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tripVote ADD CONSTRAINT FK_C2F6A84CA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE taxiProviderRating');
        $this->addSql('DROP TABLE tripVote');
    }
}
