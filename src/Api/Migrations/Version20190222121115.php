<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190222121115 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('Delete from tripPlace');
        $this->addSql('
            ALTER TABLE tripPlace
            ADD request_id INT NOT NULL,
            CHANGE trip_id trip_id INT DEFAULT NULL
        ');

        $this->addSql('ALTER TABLE tripPlace 
          ADD CONSTRAINT FK_C3C19FB9427EB8A5 
          FOREIGN KEY (request_id) REFERENCES tripRequest (id)
        ');
        $this->addSql('CREATE INDEX IDX_C3C19FB9427EB8A5 ON tripPlace (request_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fcmToken ON notificationToken');
        $this->addSql('CREATE INDEX fcmToken ON notificationToken (fcmToken(20))');
        $this->addSql('ALTER TABLE tripPlace DROP FOREIGN KEY FK_C3C19FB9427EB8A5');
        $this->addSql('DROP INDEX IDX_C3C19FB9427EB8A5 ON tripPlace');
        $this->addSql('ALTER TABLE tripPlace DROP request_id, CHANGE trip_id trip_id INT NOT NULL');
        $this->addSql('ALTER TABLE tripRequest ADD additionalCost SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('DROP INDEX lockId ON tripStatusTracking');
        $this->addSql('CREATE INDEX lockId ON tripStatusTracking (lockId(8))');
    }
}
