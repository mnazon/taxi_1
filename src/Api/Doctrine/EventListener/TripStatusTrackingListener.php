<?php

declare(strict_types=1);

namespace App\Api\Doctrine\EventListener;

use App\Api\Entity\Trip;
use App\Api\Service\TripStatusTrackingService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TripStatusTrackingListener implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var TripStatusTrackingService
     */
    private $tripStatusTrackingService;

    public function __construct(
        TripStatusTrackingService $tripStatusTrackingService
    ) {
        $this->tripStatusTrackingService = $tripStatusTrackingService;

        $this->logger = new NullLogger();
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $trip = $args->getEntity();
        if ($trip instanceof Trip) {
            if ($args->hasChangedField('status')) {
                $this->prePersist($args);
            }
        }
    }

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $trip = $eventArgs->getEntity();
        if ($trip instanceof Trip) {
            $statusTracking = $this->tripStatusTrackingService->createStatusTracking($trip);
            if ($statusTracking) {
                $eventArgs->getEntityManager()->persist($statusTracking);
            }
        }
    }
}
