<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Search;
use App\Api\Entity\Place;
use App\Api\Exception\WrongProviderResponse;
use App\Api\Exception\WrongRequestArgumentException;
use Http\Client\Common\PluginClient;
use Http\Message\MessageFactory;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class VisicomGeoProvider implements GeoProviderInterface
{
    private const CATEGORY_STREET = 'adr_street';
    private const CATEGORY_ADDRESS = 'adr_address';
    private const CATEGORY_POI = 'poi';

    private const CATEGORY_PAYMENT_TERMINAL = 'poi_payment_terminal';
    private const CATEGORY_TAXI_SERVICE = 'poi_taxi_service';
    private const CATEGORY_COUNTRY = 'adm_country';
    private const CATEGORY_ADM_LEVEL1 = 'adm_level1';
    private const CATEGORY_ADM_LEVEL2 = 'adm_level2';
    private const CATEGORY_ADM_LEVEL3 = 'adm_level3';
    private const CATEGORY_ADM_DISTRICT = 'adm_district';
    private const CATEGORY_ADM_SETTLEMENT = 'adm_settlement';
    private const CATEGORY_STOW = 'tpm_stow';

    private static $filteredCategories = [
        self::CATEGORY_TAXI_SERVICE,
        self::CATEGORY_COUNTRY,
        self::CATEGORY_ADM_LEVEL1,
        self::CATEGORY_ADM_LEVEL2,
        self::CATEGORY_ADM_LEVEL3,
        self::CATEGORY_ADM_DISTRICT,
        self::CATEGORY_STOW,
        self::CATEGORY_PAYMENT_TERMINAL,
    ];

    private static $searchCategories = [
        self::CATEGORY_ADDRESS,
        self::CATEGORY_STREET,
        self::CATEGORY_POI,
        self::CATEGORY_ADM_SETTLEMENT,
    ];

    /**
     * @var PluginClient
     */
    private $pluginClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;
    /**
     * @var GeoSupportChecker
     */
    private $geoSupportChecker;
    /**
     * @var PlaceEntityFactory
     */
    private $placeEntityFactory;

    public function __construct(
        PluginClient $pluginClient,
        MessageFactory $messageFactory,
        GeoSupportChecker $geoSupportChecker,
        PlaceEntityFactory $placeEntityFactory
    ) {
        $this->pluginClient = $pluginClient;
        $this->messageFactory = $messageFactory;
        $this->geoSupportChecker = $geoSupportChecker;
        $this->placeEntityFactory = $placeEntityFactory;
    }

    /**
     * @param Coordinates|null $coordinates
     *
     * @return Place
     *
     * @throws WrongProviderResponse
     * @throws \Exception
     */
    public function reverseGeocodingByCoordinates(Coordinates $coordinates): ?Place
    {
        $params = [
            'r' => '500',
            'n' => $coordinates->getLng().','.$coordinates->getLat(),
        ];

        //https://api.visicom.ua/uk/products/data-api/data-api-references/search
        $uri = sprintf('/data-api/4.0/uk/search/%s.json?', implode(',', self::$searchCategories));
        $request = $this->messageFactory->createRequest('GET', $uri.http_build_query($params));
        $promise = $this->pluginClient->sendAsyncRequest($request);
        $response = $promise->wait(true);

        $features = $this->getFeaturesDataFromResponse($response);
        if (!$features) {
            return null;
        }

        $nearestPlaceOnStreet = null;
        $nearestFeatures = [];

        $nearestStreet = $this->extractNearestFeatureByCategory($features, self::CATEGORY_STREET);
        if ($nearestStreet) {
            $nearestPlaceOnStreet = $this->extractNearestFeatureByCategory(
                $features,
                self::CATEGORY_ADDRESS,
                $nearestStreet['id']
            );

            if ($nearestPlaceOnStreet) {
                $nearestFeatures[] = $nearestPlaceOnStreet; //the street is not needed if there is a place on it
            } else {
                $nearestFeatures[] = $nearestStreet;
            }
        }

        $nearestPlace = $this->extractNearestFeatureByCategory($features, self::CATEGORY_ADDRESS);
        if ($nearestPlace) {
            $nearestFeatures[] = $nearestPlace;
        }

        $nearestObject = $this->extractNearestFeatureByCategory($features, self::CATEGORY_POI);
        if ($nearestObject) {
            $nearestFeatures[] = $nearestObject;
        }

        if (!$nearestFeatures) {
            $settlement = $this->extractNearestFeatureByCategory($features, self::CATEGORY_ADM_SETTLEMENT);
            if ($settlement) {
                $nearestFeatures[] = $settlement;
            }
        }

        if (empty($nearestFeatures)) {
            return null;
        }

        uasort($nearestFeatures, function (array $featureCurrent, array $featureNext) {
            $featureCurrentDist = $featureCurrent['properties']['dist_meters'];
            $featureNextDist = $featureNext['properties']['dist_meters'];

            return $featureCurrentDist <=> $featureNextDist;
        });

        $place = $this->fromFeatureToPlace(reset($nearestFeatures));

        return $place;
    }

    /**
     * @throws WrongProviderResponse
     */
    public function findStreetsAndPlaces(Search $search): array
    {
        $query = $search->getQuery();
        $supportedArea = $this->geoSupportChecker->getSupportedArea();
        $params = [
            't' => $query,
            'n' => "{$supportedArea->getCoordinates()->getLng()},{$supportedArea->getCoordinates()->getLat()}",
            'r' => $supportedArea->getRadius(),
            'l' => $search->getLimit(),
        ];
        $response = $this->sendGeocodeRequestAndGetResponse($params);
        $features = $this->getFeaturesDataFromResponse($response);

        $objects = [];
        foreach ($features as $feature) {
            $properties = $feature['properties'];
            $mainCategory = $this->extractMainCategoryFromFeatureProperties($properties);

            if ($this->isCategoryFiltered($mainCategory)) {
                continue;
            }

            $isExactAddress = false;
            $districtFormatted = isset($properties['zone']) ? $properties['zone'].', ' : '';

            switch ($mainCategory) {
                case self::CATEGORY_STREET:
                    $mainText = $properties['type'].' '.$properties['name'];
                    $secondaryText = sprintf('%s%s, %s', $districtFormatted, $properties['settlement'], $properties['country']);
                    break;
                case self::CATEGORY_ADDRESS:
                    $isExactAddress = true;
                    $mainText = $properties['street'].' '.$properties['name'];
                    $secondaryText = sprintf('%s%s', $districtFormatted, $properties['settlement']);
                    break;
                case self::CATEGORY_ADM_SETTLEMENT:
                    $mainText = $properties['name'];
                    $secondaryText = sprintf('%s, %s', $properties['level2'], $properties['level1']);
                    break;
                default:
                    $isExactAddress = true;
                    $mainText = $properties['vitrine'] ?? $properties['name'];
                    $secondaryText = $properties['address_info'] ?? $properties['address'] ?? '';
            }

            $objects[] = new AutocompleteData(
                $mainText,
                $secondaryText,
                $feature['id'],
                $isExactAddress,
                null,
                $this->extractCoordinatesFromFeatureData($feature)
            );
        }

        return $objects;
    }

    /**
     * @throws WrongProviderResponse
     * @throws WrongRequestArgumentException
     */
    public function findPlaceDetails(string $placeId): Place
    {
        //https://api.visicom.ua/uk/products/data-api/data-api-references/feature
        $uri = sprintf('/data-api/4.0/uk/feature/%s.json?', $placeId);
        $request = $this->messageFactory->createRequest('GET', $uri);
        $promise = $this->pluginClient->sendAsyncRequest($request);
        $response = $promise->wait(true);
        $place = $this->fromResponseToPlace($response);

        if (!$place) {
            throw new WrongRequestArgumentException('placeId', sprintf('Place with id "%s" not found', $placeId));
        }

        return $place;
    }

    /**
     * @throws WrongProviderResponse
     * @throws WrongRequestArgumentException
     */
    public function geocodingByPlaceId(string $placeId): ?Place
    {
        return $this->findPlaceDetails($placeId);
    }

    /**
     * @throws \Exception
     */
    private function sendGeocodeRequestAndGetResponse(array $params): ResponseInterface
    {
        //https://api.visicom.ua/uk/products/data-api/data-api-references/geocode
        $uri = '/data-api/4.0/uk/geocode.json?';
        $request = $this->messageFactory->createRequest('GET', $uri.http_build_query($params));
        $promise = $this->pluginClient->sendAsyncRequest($request);
        $response = $promise->wait(true);

        return $response;
    }

    /**
     * @throws WrongProviderResponse
     */
    private function fromResponseToPlace(ResponseInterface $response): ?Place
    {
        $features = $this->getFeaturesDataFromResponse($response);
        if (!$features) {
            return null;
        }

        $feature = reset($features);

        return $this->fromFeatureToPlace($feature);
    }

    private function getFeaturesDataFromResponse(ResponseInterface $response): array
    {
        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new WrongProviderResponse($response, 'Wrong status code from visicom');
        }

        try {
            $contents = $response->getBody()->getContents();
            $data = \GuzzleHttp\json_decode($contents, true);
        } catch (\Throwable $e) {
            throw new WrongProviderResponse($response, 'Wrong response body from visicom', 0, $e);
        }

        $type = $data['type'] ?? null;
        $features = [];
        switch ($type) {
            case 'Feature':
                $features[] = $data;
                break;
            case 'FeatureCollection':
                $features = $data['features'];
                break;
        }

        return $features;
    }

    private function extractCoordinatesFromFeatureData(array $feature): ?Coordinates
    {
        $coordinates = null;
        if (isset($feature['geo_centroid']['coordinates'])) {
            [$lng, $lat] = $feature['geo_centroid']['coordinates'];
            $coordinates = new Coordinates($lat, $lng);
        }

        return $coordinates;
    }

    private function isCategoryFiltered(string $category): bool
    {
        return \in_array($category, self::$filteredCategories);
    }

    private function fromFeatureToPlace(array $feature): ?Place
    {
        $properties = $feature['properties'] ?? null;
        if (!\is_array($properties)) {
            return null;
        }

        $street = $streetNumber = $name = null;
        $settlement = $properties['settlement'] ?? null;
        $district = $properties['zone'] ?? null;
        $admLevel1 = $properties['level1'] ?? null;
        $formattedAddressData = [];
        $mainCategory = $this->extractMainCategoryFromFeatureProperties($properties);
        $placeCoordinates = $this->extractCoordinatesFromFeatureData($feature);

        switch ($mainCategory) {
            case self::CATEGORY_STREET:
                $street = $properties['name'];
                $formattedAddressData[] = $properties['type'].' '.$street;

                if ($district) {
                    $formattedAddressData[] = $district;
                }

                if ($settlement) {
                    $formattedAddressData[] = $settlement;
                }

                $formattedAddressData[] = $properties['country'];

                $formattedAddress = implode(', ', $formattedAddressData);
                break;
            case self::CATEGORY_ADDRESS:
                $street = $properties['street'] ?? null;
                $streetType = $properties['street_type'] ?? null;
                $streetNumber = $properties['name'] ?? null;

                $formattedAddressData[] = $streetType ? $streetType.' '.$street : $street;
                if ($streetNumber) {
                    $formattedAddressData[] = $streetNumber;
                }

                if ($district) {
                    $formattedAddressData[] = $district;
                }

                if ($settlement) {
                    $formattedAddressData[] = $settlement;
                }

                $formattedAddress = implode(', ', $formattedAddressData);
                break;
            case self::CATEGORY_ADM_SETTLEMENT:
                $name = $properties['name'];
                if ($name) {
                    $formattedAddressData[] = $name;
                }

                $district = $properties['level2'] ?? null;
                if ($district) {
                    $formattedAddressData[] = $district;
                }

                if ($admLevel1) {
                    $formattedAddressData[] = $admLevel1;
                }

                $formattedAddress = implode(', ', $formattedAddressData);
                break;
            default:
                $name = $properties['vitrine'] ?? $properties['name'];
                $formattedAddress = $properties['address_info'] ?? $properties['address'] ?? null;
                if ($formattedAddress) {
                    $explodedAddress = explode(',', $formattedAddress);
                    if (\count($explodedAddress) > 2) {
                        $settlement = trim($explodedAddress[0]);
                        $street = isset($explodedAddress[1]) ? trim($explodedAddress[1]) : null;
                        $streetNumber = isset($explodedAddress[2]) ? trim($explodedAddress[2]) : null;
                    } else {
                        $street = isset($explodedAddress[0]) ? trim($explodedAddress[0]) : null;
                        $streetNumber = isset($explodedAddress[1]) ? trim($explodedAddress[1]) : null;
                    }
                }
        }

        $place = $this->placeEntityFactory->create(
            $name,
            $streetNumber,
            $street,
            $settlement,
            $district,
            $admLevel1,
            $feature['id'],
            $placeCoordinates,
            false,
            $formattedAddress
        );

        return $place;
    }

    private function extractNearestFeatureByCategory(array $features, string $category, string $streetId = null): ?array
    {
        /** @var array $feature */
        foreach ($features as $feature) {
            $properties = $feature['properties'];
            $mainCategory = $this->extractMainCategoryFromFeatureProperties($properties);
            if ($this->isCategoryFiltered($mainCategory)) {
                continue;
            }

            if (false !== strpos($mainCategory, $category)) {
                if (!$streetId || isset($properties['street_id']) && $properties['street_id'] === $streetId) {
                    return $feature;
                }
            }
        }

        return null;
    }

    private function extractMainCategoryFromFeatureProperties(array $featureProperties): ?string
    {
        if (!isset($featureProperties['categories']) && !\is_string($featureProperties['categories'])) {
            return null;
        }

        $categories = explode(',', $featureProperties['categories']);
        $mainCategory = reset($categories);

        return $mainCategory;
    }
}
