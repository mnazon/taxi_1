<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\Coordinates;
use App\Api\Entity\Place as Place;
use App\Api\Exception\WrongProviderResponse;

interface ReverseGeocodingInterface
{
    /**
     * @param Coordinates|null $coordinates
     *
     * @return Place
     *
     * @throws WrongProviderResponse
     * @throws \Exception
     */
    public function reverseGeocodingByCoordinates(Coordinates $coordinates): ?Place;
}
