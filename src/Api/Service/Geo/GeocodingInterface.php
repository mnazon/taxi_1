<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\Entity\Place as Place;
use App\Api\Exception\WrongProviderResponse;

interface GeocodingInterface
{
    /**
     * @return Place
     *
     * @throws WrongProviderResponse
     * @throws \Exception
     */
    public function geocodingByPlaceId(string $placeId): ?Place;
}
