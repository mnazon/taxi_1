<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\Search;
use App\Api\Entity\Place as Place;

interface GeoProviderInterface extends GeocodingInterface, ReverseGeocodingInterface
{
    /**
     * @return AutocompleteData[]
     */
    public function findStreetsAndPlaces(Search $search): array;

    public function findPlaceDetails(string $placeId): Place;
}
