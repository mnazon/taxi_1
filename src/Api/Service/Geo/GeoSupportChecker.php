<?php

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\Area;
use App\Api\DTO\Geo\Coordinates;

class GeoSupportChecker
{
    const EARTH_RADIUS = 6371000;

    public function isSupported(Coordinates $coordinates): bool
    {
        return $this->isPointInsideArea($coordinates, $this->getSupportedArea());
    }

    public function getSupportedArea(): Area
    {
        $areaCenter = new Coordinates(50.443719, 30.522868);
        $radiusMeters = 40000;

        return new Area($areaCenter, $radiusMeters);
    }

    public function isPointInsideArea(Coordinates $point, Area $area): bool
    {
        // convert from degrees to radians
        $latFrom = deg2rad($point->getLat());
        $lngFrom = deg2rad($point->getLng());
        $latTo = deg2rad($area->getCoordinates()->getLat());
        $lngTo = deg2rad($area->getCoordinates()->getLng());
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lngTo - $lngFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $area->getRadius() >= $angle * self::EARTH_RADIUS;
    }
}
