<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\Coordinates;
use App\Api\Entity\Place;

class PlaceEntityFactory
{
    private const UTF_ENCODING = 'UTF-8';

    public function create(
        ?string $name,
        ?string $streetNumber,
        ?string $street,
        ?string $locality,
        ?string $district,
        ?string $admLevel1,
        string $externalId,
        Coordinates $coordinates,
        bool $approximate = false,
        ?string $formattedAddress = null
    ): Place {
        return new Place(
            $name ? $this->adhereLength($name, 255) : null,
            $streetNumber,
            $street,
            $locality,
            $district,
            $admLevel1,
            $externalId,
            $coordinates,
            $approximate,
            $formattedAddress
        );
    }

    private function adhereLength(string $string, int $length, string $postfix = '..'): string
    {
        if (mb_strlen($string, self::UTF_ENCODING) <= $length) {
            return $string;
        }

        $length -= mb_strlen($postfix, self::UTF_ENCODING);
        $tmp = mb_substr($string, 0, $length, self::UTF_ENCODING);

        return mb_substr($tmp, 0, mb_strripos($tmp, ' ', 0, self::UTF_ENCODING), self::UTF_ENCODING).$postfix;
    }
}
