<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Search;
use App\Api\Entity\Place;
use App\Api\Exception\WrongProviderResponse;
use Http\Client\Common\PluginClient;
use Http\Client\Exception as Exception;
use Http\Message\MessageFactory;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GoogleGeoProvider implements GeoProviderInterface
{
    /**
     * @var PluginClient
     */
    private $pluginClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;
    /**
     * @var AdapterInterface
     */
    private $adapter;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var GeoSupportChecker
     */
    private $geoSupportChecker;

    public function __construct(
        TranslatorInterface $translator,
        AdapterInterface $adapter,
        PluginClient $pluginClient,
        MessageFactory $messageFactory,
        GeoSupportChecker $geoSupportChecker
    ) {
        $this->pluginClient = $pluginClient;
        $this->messageFactory = $messageFactory;
        $this->adapter = $adapter;
        $this->translator = $translator;
        $this->geoSupportChecker = $geoSupportChecker;
    }

    /**
     * @return Place
     *
     * @throws WrongProviderResponse
     * @throws \Exception
     */
    public function geocoding(?string $placeId = null, Coordinates $coordinates = null): ?Place
    {
        if ((!empty($placeId) && null !== $coordinates)
            || (empty($placeId) && null === $coordinates)
        ) {
            throw new \InvalidArgumentException('Wrong argument applied');
        }

        $promise = $this->requestGeocoding($placeId, $coordinates);
        $response = $promise->wait(true);
        $placeData = $this->fromResponseToPlace($response);

        return $placeData;
    }

    /**
     * @param Coordinates ...$coordinates
     *
     * @return Place[]
     *
     * @throws \Exception
     */
    public function geocodingByCoordinates(Coordinates ...$coordinates): array
    {
        $promises = [];
        foreach ($coordinates as $coordinate) {
            $promises[] = $this->requestGeocoding(null, $coordinate);
        }

        $places = [];
        foreach ($promises as $promise) {
            /** @var $promise \Http\Promise\Promise */
            $response = $promise->wait(true);
            /* @var $response ResponseInterface */
            $places[] = $this->fromResponseToPlace($response);
        }

        return $places;
    }

    /**
     * @return AutocompleteData[]
     *
     * @throws WrongProviderResponse
     * @throws Exception
     */
    public function findStreetsAndPlaces(Search $search): array
    {
        $query = $search->getQuery();
        //$sessionToken = $this->retrieveSessionToken($query);
        $supportedArea = $this->geoSupportChecker->getSupportedArea();
        $params = [
            'input' => $query,
            //'sessiontoken' => $sessionToken,
            'location' => "{$supportedArea->getCoordinates()->getLat()},{$supportedArea->getCoordinates()->getLng()}",
            'radius' => $supportedArea->getRadius(),
            'strictbounds' => '1',
            'language' => 'uk',
        ];
        $uri = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
        $request = $this->messageFactory->createRequest(
            'GET',
            $uri.http_build_query($params)
        );
        $promise = $this->pluginClient->sendAsyncRequest($request);
        $response = $promise->wait();
        $data = json_decode($response->getBody()->getContents(), true);
        if (!isset($data['status']) || !\in_array($data['status'], ['OK', 'ZERO_RESULTS'], true)) {
            throw new WrongProviderResponse($response, 'Wrong response status');
        }

        $placeIds = [];
        foreach ($data['predictions'] as $row) {
            $placeIds[] = $row['place_id'];
        }

        $objects = [];
        foreach ($data['predictions'] as $row) {
            $placeId = $row['place_id'];
            $types = $row['types'];
            if ($isCity = \in_array('locality', $types)) {
                continue;
            }
            //$locality = $this->findCity($row);
            $mainText = $row['structured_formatting']['main_text'];
            $wordThatFilterFromSecondaryText = [
                'Україна',
            ];
            $secondaryText = str_replace($wordThatFilterFromSecondaryText, '', $row['structured_formatting']['secondary_text']);
            $firstTerm = $row['terms'][0]['value'];
            $isNonExistingAddress = strpos($firstTerm, ',');
            $isExactAddress = !\in_array('route', $types);
            $phantomStreetAddress = null;
            if ($isNonExistingAddress) {
                $mainTextData = explode(',', $mainText);
                $mainText = reset($mainTextData);
                $phantomStreetAddress = trim(end($mainTextData));
            }

            $objects[] = new AutocompleteData(
                $this->prettifyMainText($mainText, $row),
                trim($secondaryText, ', '),
                $placeId,
                $isExactAddress,
                $phantomStreetAddress
            );
        }

        return $objects;
    }

    private function prettifyMainText(string $mainText, array $rowData): string
    {
        //prettify transit_station
        $isStation = \in_array('transit_station', $rowData['types'], true);
        if ($isStation) {
            $wordToSearch = $this->translator->trans('station');
            $isWordInTextAlready = false !== strpos($mainText, $wordToSearch);
            if (!$isWordInTextAlready) {
                $mainText = sprintf('%s %s', $wordToSearch, $mainText);
            }
        }

        return $mainText;
    }

    public function findPlaceDetails(string $placeId): Place
    {
        $params = [
            'fields' => 'address_component,geometry,name,alt_ids,formatted_address,vicinity,types,place_id',
            'language' => 'uk',
            'placeid' => $placeId,
        ];
        $url = 'https://maps.googleapis.com/maps/api/place/details/json?'.http_build_query($params);
        $request = $this->messageFactory->createRequest('GET', $url);
        $promise = $this->pluginClient->sendAsyncRequest($request);
        $response = $promise->wait(true);
        $place = $this->fromResponseToPlace($response, true);

        return $place;
    }

    /**
     * @return string
     */
    private function findCity($placeData): ?string
    {
        $locality = null;
        foreach ($placeData['address_components'] as $addressComponent) {
            if (\in_array('locality', $addressComponent['types'])) {
                $locality = $addressComponent['short_name'];
                break;
            }
            if (\in_array('administrative_area_level_1', $addressComponent['types'])) {
                $locality = $addressComponent['short_name'];
                break;
            }
        }

        return $locality;
    }

    private function findAdressComponent(array $placeData, string $component): ?string
    {
        $componentValue = null;
        foreach ($placeData['address_components'] as $addressComponent) {
            if (\in_array($component, $addressComponent['types'], true)) {
                $componentValue = $addressComponent['short_name'];
                break;
            }
        }

        return $componentValue;
    }

    /**
     * @param $queryWithoutLastLetter
     */
    private function buildCacheKeyForSessionToken($queryWithoutLastLetter): string
    {
        return 'google_geo_session_key_'.md5($queryWithoutLastLetter);
    }

    /**
     * @return mixed|string
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function retrieveSessionToken(string $query)
    {
        $queryWithoutLastLetter = substr($query, 0, \strlen($query) - 2);
        $cacheKeyPrev = $this->buildCacheKeyForSessionToken($queryWithoutLastLetter);
        $cacheItem = $this->adapter->getItem($cacheKeyPrev);
        if ($cacheItem->isHit()) {
            $sessionToken = $cacheItem->get();
        } else {
            $cacheKeyCurrent = $this->buildCacheKeyForSessionToken($query);
            $cacheItem = $this->adapter->getItem($cacheKeyCurrent);
            $sessionToken = Uuid::uuid4()->toString();
            $cacheItem->set($sessionToken)
                ->expiresAfter(60);
            $result = $this->adapter->save($cacheItem);
        }

        return $sessionToken;
    }

    /**
     * @param Coordinates $coordinates
     * @param             $placeId
     *
     * @throws \Exception
     */
    private function requestGeocoding($placeId = null, Coordinates $coordinates = null): \Http\Promise\Promise
    {
        $params = [
            'fields' => 'address_component,geometry,name,alt_ids,formatted_address,vicinity,types,adr_address',
            'language' => 'uk',
        ];
        if ($placeId) {
            $params['place_id'] = $placeId;
        }
        if ($coordinates) {
            $params['latlng'] = $coordinates->getLat().','.$coordinates->getLng();
        }
        $uri = 'https://maps.googleapis.com/maps/api/geocode/json?';
        $request = $this->messageFactory->createRequest(
            'GET',
            $uri.http_build_query($params)
        );
        $promise = $this->pluginClient->sendAsyncRequest($request);

        return $promise;
    }

    /**
     * @param $response
     *
     * @return Place
     *
     * @throws WrongProviderResponse
     */
    private function fromResponseToPlace(ResponseInterface $response, $isPlaceDetails = false): ?Place
    {
        $data = json_decode($response->getBody()->getContents(), true);
        if (!isset($data['status']) || !\in_array($data['status'], ['OK', 'ZERO_RESULTS'], true)) {
            throw new WrongProviderResponse($response, 'Wrong response status');
        }
        if ('ZERO_RESULTS' === $data['status']) {
            return null;
        }
        $result = ($isPlaceDetails) ? $data['result'] : $data['results'][0];
        if (empty($result)) {
            return null;
        }
        $isPlace = \in_array('establishment', $result['types'] ?? [], true);
        //https://developers.google.com/maps/documentation/geocoding/intro#place-id
        $placeData = new Place(
            ($isPlaceDetails && $isPlace) ? $result['name'] : $this->findAdressComponent($result, 'establishment'),
            $this->findAdressComponent($result, 'street_number'),
            $this->findAdressComponent($result, 'route'),
            $this->findAdressComponent($result, 'locality'),
            null,
            $this->findAdressComponent($result, 'administrative_area_level_1'),
            $result['place_id'],
            new Coordinates(
                $result['geometry']['location']['lat'],
                $result['geometry']['location']['lng']
            ),
            isset($result['geometry']['location_type']) ? 'ROOFTOP' !== $result['geometry']['location_type'] : false,
            $result['formatted_address'] ?? null
        );

        return $placeData;
    }

    /**
     * @throws WrongProviderResponse
     */
    public function reverseGeocodingByCoordinates(Coordinates $coordinates): ?Place
    {
        return $this->geocoding(null, $coordinates);
    }

    /**
     * @throws WrongProviderResponse
     */
    public function geocodingByPlaceId(string $placeId): ?Place
    {
        return $this->geocoding($placeId);
    }
}
