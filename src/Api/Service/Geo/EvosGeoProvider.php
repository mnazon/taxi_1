<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Place;
use App\Api\DTO\Geo\Search;
use App\Api\Entity\Place as PlaceEntity;
use App\Api\Exception\WrongProviderResponse;
use Http\Client\Common\PluginClient;
use Http\Message\MessageFactory;
use Symfony\Component\HttpFoundation\Response;

class EvosGeoProvider implements GeoProviderInterface
{
    /**
     * @var PluginClient
     */
    private $httpClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;

    public function __construct(PluginClient $httpAsyncClient, MessageFactory $messageFactory)
    {
        $this->httpClient = $httpAsyncClient;
        $this->messageFactory = $messageFactory;
    }

    public function geocoding(?string $placeId = null, Coordinates $coordinates = null): ?PlaceEntity
    {
        $query = [
            'r' => '1000',
            'lat' => $coordinates->getLat(),
            'lng' => $coordinates->getLng(),
        ];
        $uri = '/api/geodata/nearest?'.http_build_query($query);
        $headers = [
            'Accept-Language' => 'UK-UA',
        ];
        $request = $this->messageFactory->createRequest('GET', $uri, $headers);
        $response = $this->httpClient->sendRequest($request);

        $data = $this->assertValidResponse($response);

        if (!empty($data['geo_streets']['geo_street'])) {
            $street = $data['geo_streets']['geo_street'][0];
            $place = new PlaceEntity(
                $street['name'],
                reset($street['houses'])['house'],
                $street['name'],
                null,
                null,
                null,
                'evos',
                $coordinates
            );
        }
        if (!empty($data['geo_objects']['geo_object'])) {
            $object = $data['geo_objects']['geo_object'][0];
            $place = new PlaceEntity(
                $object['name'],
                null,
                null,
                null,
                null,
                null,
                'evos',
                $coordinates
            );
        }

        return $place ?? null;
    }

    /**
     * @throws WrongProviderResponse
     * @throws \Exception
     */
    public function findStreetsAndPlaces(Search $search): array
    {
        $query = [
            'q' => $search->getQuery(),
            'limit' => round($search->getLimit() / 2),
            'offset' => $search->getOffset(),
        ];
        $uri = '/api/geodata/search?'.http_build_query($query);
        $headers = [
            'Accept-Language' => 'UK-UA',
        ];
        $request = $this->messageFactory->createRequest('GET', $uri, $headers);
        $response = $this->httpClient->sendRequest($request);

        $data = $this->assertValidResponse($response);

        $geoObjects = [];
        if (!empty($data['geo_objects']['geo_object'])) {
            foreach ($data['geo_objects']['geo_object'] as $place) {
                $geoObjects[] = new Place(
                    $place['name'],
                    '',
                    true,
                    'fake_id',
                    null,
                    new Coordinates($place['lat'], $place['lng'])
                );
            }
        }

        if (!empty($data['geo_streets']['geo_street'])) {
            foreach ($data['geo_streets']['geo_street'] as $street) {
                $result = preg_match('/(.*?)\((.*?)\)$/', $street['name'], $matches);
                if ($result) {
                    $streetName = $matches[1];
                    $locality = $matches[2];
                } else {
                    $streetName = $street['name'];
                    $locality = '';
                }

                $geoObjects[] = new Place(
                    $streetName,
                    $locality,
                    false,
                    'street_id'
                );
            }
        }

        return \array_slice($geoObjects, 0, $search->getLimit());
    }

    /**
     * @param $response
     *
     * @return mixed
     *
     * @throws WrongProviderResponse
     */
    private function assertValidResponse($response): array
    {
        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new WrongProviderResponse($response, 'Wrong status code from geo');
        }

        try {
            $contents = $response->getBody()->getContents();
            $data = \GuzzleHttp\json_decode($contents, true);
        } catch (\Throwable $e) {
            throw new WrongProviderResponse($response, 'Wrong response body from geo', 0, $e);
        }

        $isStructureValid = isset($data['geo_objects']['geo_object']) &&
            isset($data['geo_streets']['geo_street']);

        if (!$isStructureValid) {
            throw new WrongProviderResponse($response, 'Wrong response structure from geo');
        }

        return $data;
    }

    public function findPlaceDetails(string $placeId): PlaceEntity
    {
        throw new \RuntimeException('Not implemented yet');
    }

    /**
     * @throws WrongProviderResponse
     */
    public function reverseGeocodingByCoordinates(Coordinates $coordinates): ?PlaceEntity
    {
        return $this->geocoding(null, $coordinates);
    }

    /**
     * @throws \RuntimeException
     */
    public function geocodingByPlaceId(string $placeId): ?PlaceEntity
    {
        throw new \RuntimeException('Not implemented yet');
    }
}
