<?php

declare(strict_types=1);

namespace App\Api\Service\Geo;

use App\Api\DTO\CoordinatesToPlace;
use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Search;
use App\Api\Entity\Place;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\WrongProviderResponse;
use App\Api\Repository\PlaceRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\CacheItem;

class CompositeGeoProviderDecorator implements GeoProviderInterface
{
    /**
     * @var GoogleGeoProvider
     */
    private $geoProvider;
    /**
     * @var AdapterInterface
     */
    private $cacheAdapter;
    /**
     * @var PlaceRepository
     */
    private $placeRepository;
    /**
     * @var GeoSupportChecker
     */
    private $geoSupportChecker;
    /**
     * @var EvosGeoProvider
     */
    private $evosGeoProvider;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var VisicomGeoProvider
     */
    private $visicomGeoProvider;
    /**
     * @var VisicomGeoProvider
     */
    private $mainProvider;

    public function __construct(
        PlaceRepository $placeRepository,
        GoogleGeoProvider $geoProvider,
        AdapterInterface $cacheAdapter,
        EvosGeoProvider $evosGeoProvider,
        GeoSupportChecker $geoSupportChecker,
        LoggerInterface $logger,
        VisicomGeoProvider $visicomGeoProvider,
        VisicomGeoProvider $mainProvider
    ) {
        if (!$geoProvider instanceof GeoProviderInterface) {
            throw new \InvalidArgumentException('Wrong instance');
        }
        $this->geoProvider = $geoProvider;
        $this->cacheAdapter = $cacheAdapter;
        $this->placeRepository = $placeRepository;
        $this->geoSupportChecker = $geoSupportChecker;
        $this->evosGeoProvider = $evosGeoProvider;
        $this->logger = $logger;
        $this->visicomGeoProvider = $visicomGeoProvider;
        $this->mainProvider = $mainProvider;
    }

    /**
     * @return AutocompleteData[]
     *
     * @throws WrongProviderResponse
     * @throws \Http\Client\Exception
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Http\Client\Exception
     */
    public function findStreetsAndPlaces(Search $search): array
    {
        $increment = 17;
        $key = sprintf('geo_object_result_%s_%s', $increment, md5(\get_class($this->visicomGeoProvider).serialize($search)));
        try {
            $item = $this->cacheAdapter->getItem($key);
        } catch (\Throwable $t) {
            $item = new CacheItem();
            //todo - invalidate all items in this tag
            //https://symfony.com/doc/current/components/cache/cache_invalidation.html#cache-component-tags
        }

        if (!$item->isHit()) {
            $this->logger->info('[geo] miss', ['query' => $search->getQuery()]);
            $result = $this->mainProvider->findStreetsAndPlaces($search);
            $serializedData = $this->serializeAutoCompleteData($result);
            $item->set($serializedData)
                ->expiresAfter(3600 * 24 * 60);
            $this->cacheAdapter->save($item);
        } else {
            $this->logger->info('[geo] hit', ['query' => $search->getQuery()]);
            $cachedData = $item->get();
            $result = $this->desirializeAutocompleteData($cachedData);
        }

        return $result;
    }

    /**
     * @return Place
     *
     * @throws WrongProviderResponse
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function geocodingByPlaceId(string $placeId): ?Place
    {
        $key = sprintf('cached_place_id_%s', md5($placeId));
        $placeIdCacheItem = $this->retrieveCacheItem($key);
        if (!$placeIdCacheItem->isHit()) {
            $savedTime = 3600 * 24 * 1;
            $placeEntity = $this->findPlaceDetails($placeId);
            //save place for placeId key
            $serializedPlaceData = $this->serializePlaceData($placeEntity);
            $placeIdCacheItem->set($serializedPlaceData)
                ->expiresAfter($savedTime);
            $this->cacheAdapter->save($placeIdCacheItem);

            //save place for coordinates key
            $coordinatesCacheKey = $this->createCacheKeyForCoordinates($placeEntity->getCoordinates());
            $coordinatesCacheItem = $this->retrieveCacheItem($coordinatesCacheKey);
            $coordinatesCacheItem->set($serializedPlaceData)
                ->expiresAfter($savedTime);
            $this->cacheAdapter->save($coordinatesCacheItem);
        } else {
            $placeEntity = $this->desirializePlaceData($placeIdCacheItem->get());
        }

        return $placeEntity;
    }

    /**
     * @return Place|null
     *
     * @throws ApiErrorCodeException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function geocodingByCoordinates(
        Coordinates $coordinates,
        ReverseGeocodingInterface $reverseGeocoding = null
    ): ?CoordinatesToPlace {
        if (!$this->geoSupportChecker->isSupported($coordinates)) {
            throw new ApiErrorCodeException(ApiErrorCode::LOCATION_NOT_SUPPORTED());
        }

        $placeEntity = $this->getPlaceDataFromCache(
            $coordinates,
            function ($coordinates) use ($reverseGeocoding) {
                return $this->mainProvider->reverseGeocodingByCoordinates($coordinates);
            }
        );

        $place = null;
        if ($placeEntity) {
            $place = new CoordinatesToPlace($coordinates, $placeEntity);
        }

        return $place;
    }

    /**
     * @param Coordinates ...$coordinates
     *
     * @return CoordinatesToPlace[]
     *
     * @throws WrongProviderResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \App\Api\Exception\ApiErrorCodeException
     */
    public function geocodingByRouteAndSave(Coordinates ...$coordinates): array
    {
        $result = [];
        foreach ($coordinates as $coordinate) {
            $coordinatesToPlace = $this->geocodingByCoordinates($coordinate);
            if (!$coordinatesToPlace) {
                continue;
            }

            $place = $coordinatesToPlace->getPlace();
            $existPlace = $this->placeRepository->findByExternalPlaceId($place->getExternalId());
            if (!$existPlace) {
                $this->placeRepository->save($place);
                $result[] = $coordinatesToPlace;
            } else {
                $result[] = $coordinatesToPlace->withPlace($existPlace);
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findPlaceDetails(string $placeId): Place
    {
        $isVisicomId = ctype_upper(preg_replace('/\d/', '', $placeId));
        if ($isVisicomId) {
            $place = $this->visicomGeoProvider->findPlaceDetails($placeId);
        } else {
            $place = $this->geoProvider->findPlaceDetails($placeId);
        }

        return  $place;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseGeocodingByCoordinates(Coordinates $coordinates): ?Place
    {
        return $this->mainProvider->reverseGeocodingByCoordinates($coordinates);
    }

    private function serializePlaceData(Place $place): array
    {
        $result = [
            'name' => $place->getName(),
            'streetNumber' => $place->getStreetNumber(),
            'street' => $place->getStreet(),
            'locality' => $place->getLocality(),
            'district' => $place->getDistrict(),
            'adminLevel1' => $place->getAdmLevel1(),
            'externalId' => $place->getExternalId(),
            'coordinates' => [
                'lat' => $place->getCoordinates()->getLat(),
                'lng' => $place->getCoordinates()->getLng(),
            ],
            'approximate' => (int) $place->isApproximate(),
            'formatted_address' => $place->getFormattedAddress(),
        ];

        return $result;
    }

    /**
     * @param $cachedData
     *
     * @return array
     */
    private function desirializePlaceData($cachedData): Place
    {
        $placeData = new Place(
            $cachedData['name'],
            $cachedData['streetNumber'],
            $cachedData['street'],
            $cachedData['locality'],
            $cachedData['district'],
            $cachedData['adminLevel1'],
            $cachedData['externalId'],
            new Coordinates(
                $cachedData['coordinates']['lat'],
                $cachedData['coordinates']['lng']
            ),
            (bool) $cachedData['approximate'],
            $cachedData['formatted_address'] ?? null
        );

        return $placeData;
    }

    /**
     * @param AutocompleteData[] $places
     */
    private function serializeAutoCompleteData(array $places): array
    {
        $result = [];
        foreach ($places as $place) {
            $result[] = [
                'mainText' => $place->getMainText(),
                'secondaryText' => $place->getSecondaryText(false),
                'phantomStreetAddress' => $place->getPhantomStreetAddress(),
                'isExactAddress' => (int) $place->isExactAddress(),
                'placeId' => $place->getPlaceId(),
            ];
        }

        return $result;
    }

    /**
     * @param $cachedData
     */
    private function desirializeAutocompleteData($cachedData): array
    {
        $result = [];
        foreach ($cachedData as $row) {
            $result[] = new AutocompleteData(
                $row['mainText'],
                $row['secondaryText'],
                $row['placeId'],
                (bool) $row['isExactAddress'],
                $row['phantomStreetAddress']
            );
        }

        return $result;
    }

    /**
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getPlaceDataFromCache(Coordinates $coordinates, \Closure $dataProvider = null): ?Place
    {
        $key = $this->createCacheKeyForCoordinates($coordinates);
        $placeDataCacheItem = $this->retrieveCacheItem($key);

        if (!$placeDataCacheItem->isHit() && null !== $dataProvider) {
            $placeEntity = $dataProvider->call($this, $coordinates);
            if (!$placeEntity) {
                return null;
            }
            $serializedData = $this->serializePlaceData($placeEntity);
            $placeDataCacheItem->set($serializedData)
                ->expiresAfter(3600 * 24 * 1);
            $this->cacheAdapter->save($placeDataCacheItem);
        } else {
            $placeEntity = $this->desirializePlaceData($placeDataCacheItem->get());
        }

        return $placeEntity;
    }

    /**
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function retrieveCacheItem(string $key): CacheItem
    {
        try {
            $placeDataCacheItem = $this->cacheAdapter->getItem($key);
        } catch (\Throwable $t) {
            $placeDataCacheItem = new CacheItem();
            //todo - log here
        }

        return $placeDataCacheItem;
    }

    private function createCacheKeyForCoordinates(Coordinates $coordinates): string
    {
        $increment = 19;
        $key = sprintf('by_coordinates_%s_%s', $increment, md5(serialize($coordinates)));

        return $key;
    }
}
