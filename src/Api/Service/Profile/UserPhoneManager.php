<?php

declare(strict_types=1);

namespace App\Api\Service\Profile;

use App\Api\DTO\phone\PhoneConfirmationDto;
use App\Api\DTO\Profile\PhoneConfirmationRequest;
use App\Api\Entity\User;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\PhoneConfirmationException;
use App\Api\Repository\UserRepository;
use App\Api\Service\Phone\PhoneConfirmationManager;

class UserPhoneManager
{
    /**
     * @var PhoneConfirmationManager
     */
    private $phoneConfirmationManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        PhoneConfirmationManager $phoneConfirmationManager,
        UserRepository $userRepository
    ) {
        $this->phoneConfirmationManager = $phoneConfirmationManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws ApiErrorCodeException                 if wrong confirmation code
     * @throws PhoneConfirmationException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function change(PhoneConfirmationRequest $changePhoneRequest, User $user): User
    {
        $this->assertValidConfirmationCode($changePhoneRequest);

        $phoneWhichChanges = $changePhoneRequest->getPhone();
        $userWithSuchPhone = $this->userRepository->findByPhone($phoneWhichChanges);
        if (null !== $userWithSuchPhone) {
            throw new ApiErrorCodeException(ApiErrorCode::USER_DUPLICATE_PHONE());
        }

        $user->setPhone($phoneWhichChanges);
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @throws ApiErrorCodeException
     * @throws PhoneConfirmationException
     * @throws \Doctrine\ORM\ORMException
     */
    private function assertValidConfirmationCode(PhoneConfirmationRequest $registrationRequest): void
    {
        $confirmation = new PhoneConfirmationDto(
            $registrationRequest->getPhone(),
            $registrationRequest->getConfirmationCode(),
            $registrationRequest->getIp()
        );

        $confirmResult = $this->phoneConfirmationManager->confirm($confirmation);

        if (!$confirmResult) {
            throw new ApiErrorCodeException(ApiErrorCode::PHONE_CONFIRMATION_CODE());
        }
    }
}
