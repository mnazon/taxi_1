<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Translation\Message;
use App\Api\Enum\ApiErrorCode;
use App\Api\HttpDTO\Response\ApiError;
use App\Api\HttpDTO\Response\ApiProblem;

class ApiProblemBuilder
{
    /**
     * @var ApiErrorCode
     */
    private $apiErrorCode;

    public function setApiErrorCode(ApiErrorCode $apiErrorCode)
    {
        $this->apiErrorCode = $apiErrorCode;
    }

    public function build()
    {
        $apiError = new ApiError(
            $this->apiErrorCode->getValue(),
            new Message($this->apiErrorCode->getTitle())
        );

        $apiProblem = new ApiProblem(
            $this->apiErrorCode->getUrl(),
            $this->apiErrorCode->getTitle()
        );
        $apiProblem->addError($apiError);

        return $apiProblem;
    }
}
