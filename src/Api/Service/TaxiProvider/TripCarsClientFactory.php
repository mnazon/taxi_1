<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\Entity\TaxiProvider;
use App\Api\Enum\TaxiPlatform;
use App\Api\Exception\UnsupportedTaxiPlatformException;
use App\Api\Service\TaxiProvider\Evos\TripCarsClient;

class TripCarsClientFactory
{
    /**
     * @var TripCarsClient
     */
    private $evosTripCarsClient;
    /**
     * @var TripCarsClientInterface[]
     */
    private $clients = [];
    /**
     * @var array
     */
    private static $supportedTaxiPlatforms;

    public static function supportedTaxiPlatforms(): array
    {
        if (!self::$supportedTaxiPlatforms) {
            self::$supportedTaxiPlatforms = [
                new TaxiPlatform(TaxiPlatform::EVOS),
            ];
        }

        return self::$supportedTaxiPlatforms;
    }

    public function __construct(TripCarsClient $evosTripCarsClient)
    {
        $this->evosTripCarsClient = $evosTripCarsClient;
    }

    public function getClientByTaxiProvider(TaxiProvider $taxiProvider): TripCarsClientInterface
    {
        $taxiProviderKey = spl_object_hash($taxiProvider);
        if (!isset($this->clients[$taxiProviderKey])) {
            $client = $this->createFromEntity($taxiProvider);
            $this->clients[$taxiProviderKey] = $client;
        }

        return $this->clients[$taxiProviderKey];
    }

    public function createFromEntity(TaxiProvider $taxiProvider): TripCarsClientInterface
    {
        $this->assertTaxiPlatformSupport($taxiProvider);

        $client = clone $this->evosTripCarsClient;
        $client->setTaxiProvider($taxiProvider);

        return $client;
    }

    private function assertTaxiPlatformSupport(TaxiProvider $taxiProvider): void
    {
        $taxiPlatform = $taxiProvider->getTaxiPlatform();
        if (!\in_array($taxiPlatform, self::supportedTaxiPlatforms())) {
            throw new UnsupportedTaxiPlatformException(sprintf('Taxi platform of "%s" provider is not supported', $taxiProvider->getName()));
        }
    }
}
