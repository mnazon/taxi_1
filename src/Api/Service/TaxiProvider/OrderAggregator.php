<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\EstimatedTripItem;
use App\Api\DTO\OrderCostCollection;
use App\Api\DTO\Trip\EstimateSort;
use App\Api\Enum\EstimateSortField;
use App\Api\Enum\SortDirection;
use App\Api\HttpDTO\EstimateTripRequest;
use App\Api\Repository\TaxiProviderRatingRepository;
use App\Api\Repository\TaxiProviderRepository;
use function GuzzleHttp\Promise\all;
use Psr\Log\LoggerInterface;

class OrderAggregator
{
    /**
     * @var TripClientFactory
     */
    private $orderClientFactory;
    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;
    /**
     * @var TaxiProviderRatingRepository
     */
    private $taxiProviderRatingRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        TripClientFactory $orderClientFactory,
        TaxiProviderRepository $taxiProviderRepository,
        TaxiProviderRatingRepository $taxiProviderRatingRepository,
        LoggerInterface $logger
    ) {
        $this->orderClientFactory = $orderClientFactory;
        $this->taxiProviderRepository = $taxiProviderRepository;
        $this->taxiProviderRatingRepository = $taxiProviderRatingRepository;
        $this->logger = $logger;
    }

    public function cost(
        EstimateTripRequest $costRequest,
        EstimateSort $sort
    ): OrderCostCollection {
        $allProviderNames = $clients = [];
        $taxiProviders = $this->taxiProviderRepository->findByEnabled(true);
        foreach ($taxiProviders as $taxiProvider) {
            $clients[] = $this->orderClientFactory->createFromEntity($taxiProvider);
            $allProviderNames[] = $taxiProvider->getName();
        }
        $requestStartTime = microtime(true);
        $tripCostCollection = $this->getClientOrderCostCollection($costRequest, $clients);
        $requestExecutionTime = microtime(true) - $requestStartTime;
        $this->appendRatingDataAndSuccessTripRatio($tripCostCollection);
        $tripCostCollection = $this->sort($tripCostCollection, $sort);

        $this->logResponse($tripCostCollection, $allProviderNames, $requestExecutionTime);

        return $tripCostCollection;
    }

    public function getClientEsitmate(int $taxiProviderId, EstimateTripRequest $costRequest): EstimatedTripItem
    {
        $taxiProvider = $this->taxiProviderRepository->find($taxiProviderId);
        $clients = [
            $this->orderClientFactory->createFromEntity($taxiProvider),
        ];
        $orderCostCollection = $this->getClientOrderCostCollection($costRequest, $clients);
        $data = $orderCostCollection->getData();

        return reset($data);
    }

    public function sort(OrderCostCollection $orderCostCollection, EstimateSort $sort): OrderCostCollection
    {
        $data = $orderCostCollection->getData();
        $sortFiled = $sort->getField()->getValue();
        $sortDirectionValue = SortDirection::ASC === $sort->getDirection()->getValue()
            ? true : false;
        if (EstimateSortField::PERCENT === $sortFiled) {
            $sortFunction = function (EstimatedTripItem $first, EstimatedTripItem $second) use ($sortDirectionValue) {
                return ($first->getPercentage() < $second->getPercentage()) !== $sortDirectionValue;
            };
        } elseif (EstimateSortField::PRICE === $sortFiled) {
            $sortFunction = function (EstimatedTripItem $first, EstimatedTripItem $second) use ($sortDirectionValue) {
                return ($first->getCost() > $second->getCost()) === $sortDirectionValue;
            };
        } elseif (EstimateSortField::RATING === $sortFiled) {
            $sortFunction = function (EstimatedTripItem $first, EstimatedTripItem $second) use ($sortDirectionValue) {
                $firstRating = $first->getTaxiProvider()->getRating();
                $secondRating = $second->getTaxiProvider()->getRating();
                if (null === $firstRating || null === $secondRating) {
                    return 0;
                }
                $firstHaveBiggerRate = $firstRating->getRate() > $secondRating->getRate();

                return $firstHaveBiggerRate === $sortDirectionValue;
            };
        } elseif (EstimateSortField::SMART === $sortFiled) {
            $sortFunction = function (EstimatedTripItem $first, EstimatedTripItem $second) use ($sortDirectionValue) {
                $firstPercentage = null !== $first->getPercentage() ? $first->getPercentage() : rand(0, 5);
                $secondPercentage = null !== $second->getPercentage() ? $second->getPercentage() : rand(0, 5);

                $firstCustomWeight = $first->getTaxiProvider()->getCustomSortWeight();
                $secondCustomWeight = $second->getTaxiProvider()->getCustomSortWeight();
                $firstWeight = $firstCustomWeight ? rand($firstCustomWeight / 2, $firstCustomWeight) : 0;
                $secondWeight = $secondCustomWeight ? rand($secondCustomWeight / 2, $secondCustomWeight) : 0;

                $firstRatio = (($firstPercentage + $firstWeight) / $first->getCost());
                $secondRatio = (($secondPercentage + $secondWeight) / $second->getCost());

                $firstRatioBigger = $firstRatio > $secondRatio;

                return $firstRatioBigger === $sortDirectionValue;
            };
        } else {
            throw new \RuntimeException('Not valid sort field');
        }

        $result = usort($data, $sortFunction);
        $newCollection = new OrderCostCollection();
        foreach ($data as $item) {
            $newCollection->add($item);
        }

        return $newCollection;
    }

    public function status()
    {
    }

    private function getClientOrderCostCollection(EstimateTripRequest $costRequest, array $clients): OrderCostCollection
    {
        //todo - set up 1 second as timeout
        $promises = [];
        $orderCostCollection = new OrderCostCollection();
        $costRequest->setUserPhone(null); //do not transfer phone while do estimate request!
        /** @var TripAdapterInterface $client */
        foreach ($clients as $client) {
            $promises[] = $promise = $client->cost(
                $costRequest,
                $orderCostCollection
            );
        }

        try {
            all($promises)->wait();
        } catch (\Throwable $e) {
            $this->logger->info('Error while cost calculation', ['exception' => $e]);
        }

        //todo - get data for car availability percentage

        return $orderCostCollection;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    private function appendRatingDataAndSuccessTripRatio(OrderCostCollection $orderCostCollection): void
    {
        $providers = [];
        foreach ($orderCostCollection->getData() as $providerCostData) {
            $providers[] = $providerCostData->getTaxiProvider()->getId();
        }

        $taxiProviderRatings = $this->taxiProviderRatingRepository->findByProviderIds($providers);
        foreach ($orderCostCollection->getData() as $providerCostData) {
            $taxiProvider = $providerCostData->getTaxiProvider();
            if ($taxiProvider->getPercentage()) {
                $providerCostData->setPercentage($taxiProvider->getPercentage());
            }
            $providerRatingData = $taxiProviderRatings->find($taxiProvider->getId());
            if ($providerRatingData) {
                $taxiProvider->setRating($providerRatingData);
            }
        }
    }

    private function logResponse(OrderCostCollection $tripCostCollection, array $allProviderNames, float $requestExecutionTime): void
    {
        $providersThatRespond = [];
        foreach ($tripCostCollection->getData() as $tripCostResponse) {
            $providersThatRespond[] = $tripCostResponse->getTaxiProvider()->getName();
        }

        $countProviderWhoAnswered = [
            'all' => \count($allProviderNames),
            'current' => \count($tripCostCollection->getData()),
            'withNoResponse' => array_diff($allProviderNames, $providersThatRespond),
            'requestExecutionTime' => $requestExecutionTime,
        ];
        $this->logger->info('providers-estimate-by-count', $countProviderWhoAnswered);
    }
}
