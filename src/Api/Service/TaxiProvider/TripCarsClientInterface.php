<?php

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\Geo\Area;
use App\Api\DTO\TripCarsItem;
use App\Api\Entity\TaxiProvider;
use App\Api\Exception\WrongAuthorizationArgumentException;
use Http\Promise\Promise;

interface TripCarsClientInterface
{
    public function setTaxiProvider(TaxiProvider $taxiProvider);

    /**
     * @throws WrongAuthorizationArgumentException when the authorization for the provider is incorrect or empty
     */
    public function findCarsInArea(Area $area, TripCarsItem $tripCarsItem): Promise;
}
