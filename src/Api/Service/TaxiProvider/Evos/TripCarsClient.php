<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider\Evos;

use App\Api\DTO\CarPosition;
use App\Api\DTO\Geo\Area;
use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\TripCarsItem;
use App\Api\Entity\TaxiProvider;
use App\Api\Enum\CarStatus;
use App\Api\Service\TaxiProvider\TripCarsClientInterface;
use Http\Client\HttpAsyncClient;
use Http\Message\MessageFactory;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class TripCarsClient implements TripCarsClientInterface
{
    /**
     * @var TaxiProvider
     */
    private $taxiProvider;
    /**
     * @var HttpAsyncClient
     */
    private $httpAsyncClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        HttpAsyncClient $httpAsyncClient,
        MessageFactory $messageFactory,
        LoggerInterface $logger
    ) {
        $this->httpAsyncClient = $httpAsyncClient;
        $this->messageFactory = $messageFactory;
        $this->logger = $logger;
    }

    public function setTaxiProvider(TaxiProvider $taxiProvider)
    {
        $this->taxiProvider = $taxiProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function findCarsInArea(Area $area, TripCarsItem $tripCarsItem): Promise
    {
        /** @var RequestInterface $request */
        $request = $this->buildRequest($area);
        $promise = $this->httpAsyncClient->sendAsyncRequest($request);

        $promise->then(
            function (ResponseInterface $response) use ($tripCarsItem) {
                $content = $response->getBody()->getContents();
                $responseBody = \GuzzleHttp\json_decode($content, true);
                foreach ($responseBody as $carPositionData) {
                    $lat = $carPositionData['drivercar_position']['lat'];
                    $lng = $carPositionData['drivercar_position']['lng'];
                    $carPosition = new CarPosition(
                        new Coordinates($lat, $lng),
                        CarStatus::createFromLiteral($carPositionData['state'])
                    );
                    $tripCarsItem->addCarPosition($carPosition);
                }

                return $response;
            },
            function (\Exception $exception) {
                $this->logger->error('Invalid response', ['exception' => $exception]);
            }
        );

        return $promise;
    }

    private function buildRequest(Area $area): RequestInterface
    {
        $authorizer = $this->taxiProvider->getAuthorizer();
        $headers = $authorizer->buildAuthHeader();
        $baseUrl = $authorizer->getBaseUrl();
        $params = [
            'lat' => $area->getCoordinates()->getLat(),
            'lng' => $area->getCoordinates()->getLng(),
            'radius' => $area->getRadius(),
        ];
        $uri = $baseUrl.'/api/drivers/position?'.http_build_query($params);

        $request = $this->messageFactory->createRequest('GET', $uri, $headers);

        return $request;
    }
}
