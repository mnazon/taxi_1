<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider\Evos;

use App\Api\DTO\CoordinatesToPlace;
use App\Api\DTO\EstimatedTripItem;
use App\Api\DTO\OrderCostCollection;
use App\Api\DTO\Trip\ExtendedTripStatus;
use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\DTO\Trip\Position;
use App\Api\DTO\Vehicle;
use App\Api\Entity\Place;
use App\Api\Entity\TaxiProvider;
use App\Api\Enum\CancelReason;
use App\Api\Enum\CarType;
use App\Api\Enum\TaxiAdditionalService;
use App\Api\Enum\TripStatus;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\CarAlreadyFoundException;
use App\Api\Exception\WrongProviderResponse;
use App\Api\HttpDTO\CreateTripRequest;
use App\Api\HttpDTO\EstimateTripRequest;
use App\Api\HttpDTO\ProviderTripResponse;
use App\Api\Service\TaxiProvider\TripAdapterInterface;
use Http\Client\HttpAsyncClient;
use Http\Message\MessageFactory;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use Money\Currency;
use Money\Parser\DecimalMoneyParser;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Lock\Exception\LockConflictedException;
use Symfony\Component\Lock\Lock;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\PersistingStoreInterface;
use Symfony\Component\Lock\Store\RetryTillSaveStore;

class TripAdapter implements TripAdapterInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private const CLOSED_REASON_MAP = [
        //-1 => null, //Выполняется
        //0 => CancelReason::SUCCESSFUL_EXECUTED, //Выполнен
        1 => CancelReason::RIDER_CANCEL, //Отказ клиента
        2 => CancelReason::DRIVER_CANCEL, //Отказ водителя
        3 => CancelReason::DISPATCHER_CANCEL, //Отмена по вине диспетчера
        4 => CancelReason::NO_CARS_AVAILABLE, //Нет машины
        5 => CancelReason::COST_CALCULATION, //Просчет
        6 => CancelReason::DID_NOT_ARRANGE_TARIFF, //Отказ клиента не устроил тариф
        7 => CancelReason::DID_NOT_ARRANGE_TIME, //Отказ клиента не устроило время
        8 => CancelReason::THROW_ON_COZ, //Перекинут на СОЗ (выполнен)
        9 => CancelReason::THROW_ON_COZ, //'Перекинут на СОЗ (выполнен)'
    ];
    /**
     * @var HttpAsyncClient
     */
    private $httpClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var TaxiProvider
     */
    private $taxiProvider;
    /**
     * @var HttpAsyncClient
     */
    private $costCalculationClient;
    /**
     * @var DecimalMoneyParser
     */
    private $decimalMoneyParser;
    /**
     * @var LockFactory
     */
    private $lockFactory;
    /**
     * @var HttpAsyncClient
     */
    private $httpAsyncFindTripClient;

    public function __construct(
        HttpAsyncClient $httpAsyncClient,
        HttpAsyncClient $httpAsyncFindTripClient,
        HttpAsyncClient $costCalculationClient,
        MessageFactory $messageFactory,
        DecimalMoneyParser $decimalMoneyParser,
        PersistingStoreInterface $store
    ) {
        $this->httpClient = $httpAsyncClient;
        $this->httpAsyncFindTripClient = $httpAsyncFindTripClient;
        $this->costCalculationClient = $costCalculationClient;
        $this->messageFactory = $messageFactory;
        $this->decimalMoneyParser = $decimalMoneyParser;
        $this->lockFactory = new LockFactory(new RetryTillSaveStore($store));

        $this->logger = new NullLogger();
    }

    public function setTaxiProvider(TaxiProvider $taxiProvider)
    {
        $this->taxiProvider = $taxiProvider;
    }

    /**
     * We can return Promise with the assoc onFullfield response mapper.
     *
     * @throws \Exception
     */
    public function cost(
        EstimateTripRequest $estimateTripRequest,
        OrderCostCollection $orderCostCollection
    ): Promise {
        $body = $this->fromCostRequestToBody($estimateTripRequest);
        $bodyJson = json_encode($body);
        $uri = '/api/weborders/cost';
        $request = $this->createAuthRequest('POST', $uri, $bodyJson);
        $promise = $this->costCalculationClient->sendAsyncRequest($request);

        $promise->then(
            function (ResponseInterface $response) use ($orderCostCollection) {
                try {
                    $content = $response->getBody()->getContents();
                    $responseBody = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

                    if (!isset(
                        $responseBody['dispatching_order_uid'],
                        $responseBody['order_cost'],
                        $responseBody['currency']
                    )) {
                        throw new WrongProviderResponse($response, 'Response must contains dispatching_order_uid, order_cost, currency fields.');
                    }
                    $orderCostResponse = new EstimatedTripItem(
                        $responseBody['dispatching_order_uid'],
                        (float) $responseBody['order_cost'],
                        trim($responseBody['currency'], '. '),
                        $this->taxiProvider
                    );

                    $orderCostCollection->add($orderCostResponse);

                    return $response;
                } catch (\Exception $exception) {
                    $this->logger->error('Invalid response', [
                        'exception' => $exception,
                    ]);
                }
            },
            function (\Exception $exception) {
                $data = ['exception' => $exception];
                $this->logger->error('Invalid response', $data);
            }
        );

        return $promise;
    }

    /**
     * @throws ApiErrorCodeException
     * @throws WrongProviderResponse
     * @throws \Throwable
     */
    public function updateAdditionalCost(string $externalTripId, float $additionalCost)
    {
        $lock = $this->createExternalTripLock($externalTripId, $this->taxiProvider->getId(), 10);

        if ($lock->acquire(true)) {
            try {
                $uri = "/api/weborders/{$externalTripId}/cost/additional";
                $bodyJson = json_encode([
                    'id' => $externalTripId,
                    'amount' => $additionalCost,
                ], JSON_THROW_ON_ERROR, 512);
                $request = $this->createAuthRequest('PUT', $uri, $bodyJson);

                $promise = $this->httpClient->sendAsyncRequest($request);

                $response = $promise->wait(true);
                //$content = $response->getBody()->getContents();

                //{"id":0,"links":[],"dispatching_order_uid":"1701b151fe1d41c8a7324765195f9ca0","order_cost":"181","add_cost":"5","currency":" грн.","order_car_info":null,"driver_phone":null,"drivercar_position":null,"required_time":null,"close_reason":-1,"cancel_reason_comment":null,"order_is_archive":false,"route_address_from":{"name":"тест1.","number":null},"route_address_to":{"name":"тест2.","number":null},"driver_execution_status":0,"create_date_time":"2019-01-09T13:19:24.713","find_car_timeout":570,"find_car_delay":0,"execution_status":"SearchesForCar"}
                // can't change price, because trip status was changed.
                if (400 === $response->getStatusCode()) {
                    throw new CarAlreadyFoundException();
                }
                $this->assertValidResponseCode(200, $response);
            } catch (CarAlreadyFoundException $exception) {
                throw $exception;
            } catch (\Exception $exception) {
                $this->logger->error('Invalid response', [
                    'exception' => $exception,
                ]);
                throw $exception;
            } finally {
                $lock->release();
            }
        }
    }

    /**
     * @throws WrongProviderResponse
     * @throws \Http\Client\Exception
     */
    public function cancel(string $externalTripId): int
    {
        $uri = '/api/weborders/cancel/'.$externalTripId;
        $request = $this->createAuthRequest('PUT', $uri);
        $promise = $this->httpClient->sendAsyncRequest($request);
        $response = $promise->wait(true);
        $body = $response->getBody()->getContents();
        $data = \GuzzleHttp\json_decode($body, true);
        if (!isset($data['order_client_cancel_result'])
            || 1 !== $data['order_client_cancel_result']
        ) {
            throw new WrongProviderResponse($response, 'No order_client_cancel_result field');
        }

        return $data['order_client_cancel_result'];
    }

    /**
     * @param CoordinatesToPlace[] $coordinatesToPLaces
     *
     * @throws \Throwable
     */
    public function create(CreateTripRequest $createTripRequest, array $coordinatesToPLaces): ProviderTripResponse
    {
        $body = $this->fromCostRequestToBody($createTripRequest->getTrip(), $coordinatesToPLaces);
        $bodyJson = json_encode($body);
        $uri = '/api/weborders';
        $request = $this->createAuthRequest('POST', $uri, $bodyJson);

        try {
            $promise = $this->httpClient->sendAsyncRequest($request);
        } catch (\Throwable $e) {
            throw $e;
        }

        $providerTripCreationResponse = null;
        $promise->then(
            function (ResponseInterface $response) use (&$providerTripCreationResponse, $createTripRequest) {
                $reservedAt = $createTripRequest->getTrip()->getReservedAt();
                $status = (null === $reservedAt)
                    ? TripStatus::SEARCH_FOR_CAR
                    : TripStatus::WAITING_FOR_CAR_SEARCH;

                //todo - handle error response
                $content = $response->getBody()->getContents();
                $responseBody = \GuzzleHttp\json_decode($content, true);
                //{"dispatching_order_uid":"c5af30abde5140e88b8117a7768bbdf3",
                //"discount_trip":false,
                //"find_car_timeout":570,"find_car_delay":0,
                //"order_cost":"35","currency":" грн.",
                //"route_address_from":{"name":"test32","number":null,"lat":50.3776027860752,"lng":30.468147321812},
                //"route_address_to":{"name":"test21","number":null,"lat":50.377595950743,"lng":30.4681473383544}}
                //todo - нужно реализовать предварительный заказ, тогда будет статус - WAITING_FOR_CAR_SEARCH
                $delay = $responseBody['find_car_delay'];
                $startSearchingCarAt = new \DateTimeImmutable();
                if ($delay > 0) {
                    $startSearchingCarAt = $startSearchingCarAt->modify("+ $delay seconds");
                }

                //@todo - this huck for regsat if it will be another huck - move to another class
                $externalTripId = $responseBody['dispatching_order_uid'];
                if (!isset($responseBody['order_cost'])) {
                    $collection = new ExternalTripCollection();
                    $findPromise = $this->find($externalTripId, $collection);
                    $findPromise->wait(false);
                    $tripInfo = $collection->find($externalTripId, $this->taxiProvider->getId());

                    $cost = $tripInfo->getCost();
                    $costAmount = ((int) $cost->getAmount()) / 100;
                    $currency = 'грн';
                } else {
                    $costAmount = (float) $responseBody['order_cost'];
                    $currency = trim($responseBody['currency'], '. ');
                }
                $tripStatus = new TripStatus($status);

                $providerTripCreationResponse = new ProviderTripResponse(
                    $externalTripId,
                    $costAmount,
                    $currency,
                    $tripStatus,
                    $this->taxiProvider,
                    $startSearchingCarAt
                );

                return $response;
            }
        );

        $promise->wait(true);

        return $providerTripCreationResponse;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function find(string $externalTripId, ExternalTripCollection $externalTripCollection): Promise
    {
        $lock = $this->createExternalTripLock($externalTripId, $this->taxiProvider->getId(), 3);

        if ($lock->acquire()) {
            $uri = '/api/weborders/'.$externalTripId;
            $request = $this->createAuthRequest('GET', $uri);

            return $this->httpAsyncFindTripClient
                ->sendAsyncRequest($request)
                ->then(
                    function (ResponseInterface $response) use ($externalTripCollection, $lock) {
                        //todo - handle error response
                        $content = $response->getBody()->getContents();
                        $responseBody = \GuzzleHttp\json_decode($content, true);
                        $externalTrip = $this->createExterternalTripInfo($responseBody, $this->taxiProvider);
                        $externalTripCollection->add($externalTrip);
                        $lock->release();

                        return $externalTrip;
                    },
                    function (\Exception $exception) use ($lock) {
                        $data = ['exception' => $exception];
                        $this->logger->error('Invalid response', $data);
                        $lock->release();
                    }
                );
        }

        return new RejectedPromise(
            new LockConflictedException(
                sprintf('Trip %s/%s not found.', $this->taxiProvider->getId(), $externalTripId)
            )
        );
    }

    /**
     * @throws \Exception
     */
    private function createExterternalTripInfo(array $responseBody, TaxiProvider $taxiProvider): ExternalTripResponse
    {
        $position = null;
        if (isset($responseBody['drivercar_position'])) {
            $position = new Position(
                $responseBody['drivercar_position']['lat'],
                $responseBody['drivercar_position']['lng'],
                $responseBody['drivercar_position']['status'],
                $responseBody['drivercar_position']['bearing'],
                $responseBody['drivercar_position']['speed']
            );
        }

        $vehicle = $this->extractVehicle($responseBody);

        $cost = $this->decimalMoneyParser->parse(
            $responseBody['order_cost'],
            new Currency('UAH')
        );
        $extendedTripStatus = $this->toExtendedStatus($responseBody);
        $tripStatus = new ExternalTripResponse(
            $responseBody['dispatching_order_uid'],
            $taxiProvider->getId(),
            $extendedTripStatus,
            $cost,
            !empty($responseBody['required_time']) ? new \DateTimeImmutable($responseBody['required_time']) : null,
            $vehicle,
            !empty($responseBody['driver_phone']) ? $responseBody['driver_phone'] : null,
            $position,
            $responseBody['order_is_archive']
        );

        return $tripStatus;
    }

    /**
     * @param CoordinatesToPlace[]|null $coordinatesToPlaces
     *
     * @throws \Exception
     */
    private function fromCostRequestToBody(EstimateTripRequest $estimateTripRequest, array $coordinatesToPlaces = null): array
    {
        $reservation = $estimateTripRequest->getReservedAt();
        $route = $estimateTripRequest->getRoute();

        $body = [];
        $body['reservation'] = null !== $reservation;
        if ($reservation) {
            $body['required_time'] = $reservation->format('Y-m-d\TH:i:s');
        }
        if ($estimateTripRequest->getUserFullName()) {
            $body['user_full_name'] = $estimateTripRequest->getUserFullName();
        }
        if ($estimateTripRequest->getPhoneForTaxiProvider()) {
            $body['user_phone'] = $estimateTripRequest->getPhoneForTaxiProvider();
        }
        if (!empty($coordinatesToPlaces)) {
            foreach ($coordinatesToPlaces as $coordinatesToPlace) {
                if (!$coordinatesToPlace instanceof CoordinatesToPlace) {
                    throw new \InvalidArgumentException('Wrong place instance');
                }
                $placeName = $this->composeRouteName($coordinatesToPlace->getPlace());
                $routeArray = [
                    'name' => $placeName,
                    'lat' => $coordinatesToPlace->getCoordinates()->getLat(),
                    'lng' => $coordinatesToPlace->getCoordinates()->getLng(),
                ];
                $body['route'][] = $routeArray;
            }
        } else {
            foreach ($route as $place) {
                $routeArray = [
                    'name' => sprintf('(%s), %s', $place->getLocality(), $place->getName()),
                    'lat' => $place->getCoordinates()->getLat(),
                    'lng' => $place->getCoordinates()->getLng(),
                ];
                $body['route'][] = $routeArray;
            }
        }
        if ($estimateTripRequest->getAdditionalCost()) {
            $body['add_cost'] = $estimateTripRequest->getAdditionalCost();
        }

        $carType = $estimateTripRequest->getCarType();

        if (CarType::STANDART === $carType->getValue()) {
            $providerData = $this->taxiProvider->getAdditionalData();
            if ($providerData && $providerData->getStandardTariffName()) {
                $body['flexible_tariff_name'] = $providerData->getStandardTariffName();
            }
        }
        $body['wagon'] = CarType::WAGON === $carType->getValue();
        $body['minibus'] = CarType::MINIBUS === $carType->getValue();
        $body['premium'] = CarType::PREMIUM === $carType->getValue();

        $additionalServices = $estimateTripRequest->getOptions();
        foreach ($additionalServices as $additionalService) {
            switch ($additionalService->getValue()) {
                case TaxiAdditionalService::BAGGAGE:
                    $body['baggage'] = true;
                    break;
                case TaxiAdditionalService::ANIMAL:
                    $body['animal'] = true;
                    break;
                case TaxiAdditionalService::CONDITIONER:
                    $body['conditioner'] = true;
                    break;
                case TaxiAdditionalService::COURIER_DELIVERY:
                    $body['courier_delivery'] = true;
                    break;
            }
        }
        $body['route_address_entrance_from'] = $estimateTripRequest->getStartRouteEntranceFrom();
        $body['comment'] = $estimateTripRequest->getComment();

        $authorizer = $this->taxiProvider->getAuthorizer();
        $body['taxiColumnId'] = $authorizer->getTaxiColumnId();
        $keyPb = $authorizer->getKeyPb();
        if (!empty($keyPb)) {
            $body['key_pb'] = $keyPb;
        }

        return $body;
    }

    /**
     * @throws WrongProviderResponse
     */
    private function assertValidResponseCode(int $expectedResponseCode, ResponseInterface $response)
    {
        if ($expectedResponseCode !== $response->getStatusCode()) {
            throw new WrongProviderResponse($response, 'Wrong response code');
        }
    }

    private function toExtendedStatus(array $data): ExtendedTripStatus
    {
        return new ExtendedTripStatus(
            $this->toStatus($data),
            $this->toCancelReason($data)
        );
    }

    public function toStatus(array $data): TripStatus
    {
        $map = [
            'WaitingCarSearch' => TripStatus::WAITING_FOR_CAR_SEARCH,
            'SearchesForCar' => TripStatus::SEARCH_FOR_CAR,
            'CarFound' => TripStatus::CAR_FOUND,
            'Running' => TripStatus::RUNNING,
            'Canceled' => TripStatus::CANCELED,
            'CostCalculation' => TripStatus::CANCELED,
            'Executed' => TripStatus::EXECUTED,
        ];

        if (isset($data['execution_status']) && isset($map[$data['execution_status']])) {
            if (!$this->isClosed($data)
                && TripStatus::CANCELED === $map[$data['execution_status']]
            ) {
                $status = new TripStatus(TripStatus::SEARCH_FOR_CAR);
            } else {
                $status = new TripStatus($map[$data['execution_status']]);
            }
        } else {
            $status = new TripStatus(TripStatus::UNDEFINED);
            $this->logger->error(sprintf('Unknown trip status %s', $data['execution_status'] ?? '---'));
        }

        return $status;
    }

    private function isClosed(array $data): bool
    {
        return -1 !== $data['close_reason'];
    }

    public function toCancelReason(array $data): ?CancelReason
    {
        return \array_key_exists($data['close_reason'], self::CLOSED_REASON_MAP)
            ? new CancelReason(self::CLOSED_REASON_MAP[$data['close_reason']])
            : null;
    }

    /**
     * @param $bodyJson
     */
    private function createAuthRequest(string $method, string $uri, $bodyJson = null): RequestInterface
    {
        $authorizer = $this->taxiProvider->getAuthorizer();
        $headers = $authorizer->buildAuthHeader();
        $baseUrl = $authorizer->getBaseUrl();
        $uri = $baseUrl.$uri;

        $request = $this->messageFactory->createRequest($method, $uri, $headers, $bodyJson);

        return $request;
    }

    public function extractVehicle(array $responseBody): ?Vehicle
    {
        $vehicle = null;
        $carInfo = $responseBody['order_car_info'];
        if (!empty($carInfo)) {
            $vehicle = $this->checkOnSpecialOrderCardInfoFormat($carInfo);
            if ($vehicle) {
                return $vehicle;
            }

            //unified string format
            $numberPlate = '';
            if (preg_match('/([A-ZА-Я]{1,3}\d{3,5}[A-ZА-Я]{0,4})/u', $responseBody['order_car_info'], $matches)) {
                $numberPlate = $matches[0];
            }
            $platePosition = strpos($carInfo, $numberPlate);
            if (false !== $platePosition) {
                $carInfo = substr($carInfo, $platePosition + \strlen($numberPlate));
            }
            $carInfo = trim($carInfo, ' ,');
            $color = preg_match('/[а-я_іїєґё\-]+/iu', $carInfo, $matches)
                ? $matches[0]
                : '';
            $carInfo = str_replace($color, '', $carInfo);
            $carInfo = trim($carInfo, ' ,');
            $makeAndModel = preg_replace("/\(.*\)|,.*/", '', $carInfo);

            $vehicle = new Vehicle(
                $numberPlate,
                trim($makeAndModel),
                strtolower($color)
            );
        }

        return $vehicle;
    }

    private function checkOnSpecialOrderCardInfoFormat(string $carInfo): ?Vehicle
    {
        $stringPatterns = [
            '\d+:\d+\s\:\s([A-ZА-Я0-9]{4,10}) ([а-я_іїєґё\-\.\-]+).*?([a-zа-я_іїєґё \-0-9]+)\s\(' => ['color' => 2, 'numberPlate' => 1, 'makeAndModel' => 3],
            '([A-ZА-Я0-9]{4,10}), ([а-я_іїєґё\-\.\-]+),.*?([a-zа-я_іїєґё \-0-9]+),\s\+' => ['color' => 2, 'numberPlate' => 1, 'makeAndModel' => 3],
        ];

        foreach ($stringPatterns as $pattern => $positionOfCharacters) {
            if (preg_match("/$pattern/ui", $carInfo, $matches)) {
                $numberPlate = $matches[$positionOfCharacters['numberPlate']];
                $color = $matches[$positionOfCharacters['color']];
                $makeAndModel = $matches[$positionOfCharacters['makeAndModel']];

                $vehicle = new Vehicle(
                    $numberPlate,
                    trim($makeAndModel),
                    strtolower($color)
                );
                break;
            }
        }

        return $vehicle ?? null;
    }

    private function composeRouteName(Place $place): string
    {
        $districtAndLocalityFormattedData = [];
        if ($place->getDistrict()) {
            $districtAndLocalityFormattedData[] = $place->getDistrict();
        }

        if ($place->getLocality()) {
            $districtAndLocalityFormattedData[] = $place->getLocality();
        }

        $districtAndLocalityFormatted = implode(' ', $districtAndLocalityFormattedData);
        if ($place->isObject()) {
            $additionalData = [];
            if ($place->getStreet()) {
                $additionalData[] = $place->getStreet();
            }
            if ($place->getStreetNumber()) {
                $additionalData[] = $place->getStreetNumber();
            }
            $additionalInfo = trim(implode(' ', $additionalData));
            if (!empty($additionalInfo)) {
                $placeName = sprintf(
                    '%s (%s) (%s)',
                    $place->getObjectName(),
                    $additionalInfo,
                    $districtAndLocalityFormatted
                );
            } else {
                $placeName = sprintf('%s (%s)', $place->getObjectName(), $districtAndLocalityFormatted);
            }
        } else {
            if ($place->getStreet() && $place->getStreetNumber()) {
                $placeName = sprintf(
                    '%s %s (%s)',
                    $place->getStreet(),
                    $place->getStreetNumber(),
                    $districtAndLocalityFormatted
                );
            } else {
                $placeName = sprintf(
                    '(%s)',
                    $districtAndLocalityFormatted
                );
            }
        }

        return $placeName;
    }

    /**
     * @return bool
     */
    public function createExternalTripLock(string $externalTripId, int $providerId, float $ttl = 1): Lock
    {
        if ($ttl < 0) {
            throw new \InvalidArgumentException('Invalid ttl value');
        }
        $tripStateLockKey = sprintf('ext-trip-state-lock-%s-%s', $providerId, $externalTripId);

        return $this->lockFactory->createLock($tripStateLockKey, $ttl);
    }
}
