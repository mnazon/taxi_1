<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\CoordinatesToPlace;
use App\Api\DTO\Trip\AdditionalCostRequest;
use App\Api\Entity\NotificationToken;
use App\Api\Entity\Trip;
use App\Api\Entity\TripRequest;
use App\Api\Entity\TripRequestToPlace;
use App\Api\Entity\User;
use App\Api\Enum\ApiErrorCode;
use App\Api\Event\TripCreated;
use App\Api\Event\TripExternalForceStatusChanged;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\CarAlreadyFoundException;
use App\Api\HttpDTO\CreateTripRequest;
use App\Api\HttpDTO\ProviderTripResponse;
use App\Api\Repository\NotificationTokenRepository;
use App\Api\Repository\TaxiProviderRepository;
use App\Api\Repository\TripRepository;
use App\Api\Service\Geo\CompositeGeoProviderDecorator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CreateTripManager
{
    /**
     * @var TripClientFactory
     */
    private $orderClientFactory;
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var NotificationTokenRepository
     */
    private $notificationTokenRepository;
    /**
     * @var bool
     */
    private $createTestTrip;
    /**
     * @var CompositeGeoProviderDecorator
     */
    private $geoProviderDecorator;

    public function __construct(
        TripClientFactory $orderClientFactory,
        TripRepository $rideRepository,
        TaxiProviderRepository $taxiProviderRepository,
        CompositeGeoProviderDecorator $geoProviderDecorator,
        EventDispatcherInterface $eventDispatcher,
        NotificationTokenRepository $notificationTokenRepository,
        bool $createTestTrip = true
    ) {
        $this->orderClientFactory = $orderClientFactory;
        $this->tripRepository = $rideRepository;
        $this->taxiProviderRepository = $taxiProviderRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->createTestTrip = $createTestTrip;
        $this->notificationTokenRepository = $notificationTokenRepository;
        $this->geoProviderDecorator = $geoProviderDecorator;
    }

    /**
     * @throws \Throwable
     */
    public function create(CreateTripRequest $createTripRequest): Trip
    {
        $this->handleTestTrip($createTripRequest);
        $notificationToken = $this->assertValidNotificationToken($createTripRequest);
        $coordinatesToPlace = $this->assertValidPlaces($createTripRequest);

        $taxiProviderId = $createTripRequest->getTaxiProviderId();
        $taxiProvider = $this->taxiProviderRepository->find($taxiProviderId);
        $orderClient = $this->orderClientFactory->createFromEntity($taxiProvider);

        $response = $orderClient->create($createTripRequest, $coordinatesToPlace);

        try {
            $trip = $this->saveRideResponse($response, $createTripRequest, $coordinatesToPlace, $notificationToken);

            $tripCreated = new TripCreated($trip);
            $this->eventDispatcher->dispatch($tripCreated);
        } catch (\Throwable $e) {
            throw $e;
        }

        return $trip;
    }

    /**
     * @throws HttpException
     * @throws ApiErrorCodeException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function updateAdditionalCost(AdditionalCostRequest $additionalCostRequest, User $user): Trip
    {
        $tripId = $additionalCostRequest->getTripId();
        $trip = $this->tripRepository->find($tripId);
        if (!$trip) {
            throw new HttpException(404, 'Trip not found');
        }
        if ($trip->getUser()->getId() !== $user->getId()) {
            throw new HttpException(401, 'Invalid operation, wrong user');
        }
        if (!$trip->getStatus()->isCarSearching()) {
            throw new ApiErrorCodeException(ApiErrorCode::ADDITIONAL_COST_CAR_FOUND());
        }

        try {
            $additionalCost = $additionalCostRequest->getAdditionalCost();
            $orderClient = $this->orderClientFactory->createFromEntity($trip->getTaxiProvider());

            $orderClient->updateAdditionalCost($trip->getExternalId(), $additionalCost);
            $trip->getRequest()->setAdditionalCost($additionalCost);
            $this->tripRepository->save($trip);
        } catch (CarAlreadyFoundException $e) {
            $this->eventDispatcher->dispatch(
                new TripExternalForceStatusChanged($trip)
            );
            throw new ApiErrorCodeException(ApiErrorCode::CAR_ALREADY_FOUND());
        } catch (\Throwable $e) {
            //@todo - try to handle this situation
            throw $e;
        }

        return $trip;
    }

    /**
     * @param CoordinatesToPlace[] $coordinatesToPlaces
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveRideResponse(
        ProviderTripResponse $response,
        CreateTripRequest $createTripRequest,
        array $coordinatesToPlaces,
        ?NotificationToken $notificationToken
    ): Trip {
        $tripRequest = $createTripRequest->getTrip();
        $tripRequestEntity = new TripRequest();
        $this->createRoute($coordinatesToPlaces, $tripRequestEntity);

        $tripRequestEntity->setCarType($tripRequest->getCarType()->getValue())
            ->setUserFullName($tripRequest->getUserFullName())
            ->setUserPhone($tripRequest->getUserPhone())
            ->setReservedAt($tripRequest->getReservedAt())
            ->setStartRouteEntranceFrom($tripRequest->getStartRouteEntranceFrom())
            ->setOptions($tripRequest->getOptions())
            ->setAdditionalCost($tripRequest->getAdditionalCost())
            ->setComment($tripRequest->getComment());

        $baseCost = $response->getTotalCost() - $tripRequest->getAdditionalCost();

        $trip = new Trip();
        $trip->setExternalId($response->getId())
            ->setCost($baseCost)
            ->setStartSearchingCarAt($response->getStartSearchingCarAt())
            ->setCurrency($response->getCurrency())
            ->setTaxiProvider($response->getTaxiProvider())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setStatus($response->getStatus())
            ->setUser($createTripRequest->getUser())
            ->setNotificationToken($notificationToken)
            ->setRequest($tripRequestEntity);

        return $this->tripRepository->save($trip);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    private function assertValidNotificationToken(CreateTripRequest $createTripRequest): ?NotificationToken
    {
        $notificationToken = null;
        if ($createTripRequest->getNotificationToken()) {
            $notificationToken = $this->notificationTokenRepository->getByFcmToken(
                $createTripRequest->getNotificationToken(),
                $createTripRequest->getUser()
            );
//            $isUserEquals = $notificationToken->getUser()->getId() === $createTripRequest->getUser()->getId();
//            if (!$isUserEquals) {
//                throw new \RuntimeException('Wrong token applied');
//            }
        }

        return $notificationToken;
    }

    private function handleTestTrip(CreateTripRequest $createTripRequest): void
    {
        if ($this->createTestTrip) {
            $tripRequest = $createTripRequest->getTrip();
            $tripRequest->setComment('ТЕСТ '.$tripRequest->getComment());
            $route = $tripRequest->getRoute();
            foreach ($route as $place) {
                $place->setName('ТЕСТ '.$place->getName());
            }
            $tripRequest->setPhoneForTaxiProvider('11111111');
        }
    }

    /**
     * @return CoordinatesToPlace[]
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assertValidPlaces(CreateTripRequest $createTripRequest): array
    {
        $tripRequest = $createTripRequest->getTrip();
        $route = $tripRequest->getRoute();
        $coordinates = [];
        foreach ($route as $placeData) {
            $coordinates[] = $placeData->getCoordinates();
        }

        $coordinatesToPlace = $this->geoProviderDecorator->geocodingByRouteAndSave(...$coordinates);
        if (\count($coordinatesToPlace) !== \count($coordinates)) {
            throw new \RuntimeException('Invalid response from geo');
        }

        return $coordinatesToPlace;
    }

    private function createRoute(
        array $coordinatesToPlaces,
        TripRequest $tripRequestEntity
    ): void {
        $routePointNumber = 0;
        /* @var CoordinatesToPlace $placeData */
        foreach ($coordinatesToPlaces as $coordinatesToPlace) {
            ++$routePointNumber;
            $tripRequestToPlace = new TripRequestToPlace(
                $tripRequestEntity,
                $coordinatesToPlace->getPlace(),
                $coordinatesToPlace->getCoordinates(),
                $routePointNumber
            );

            $tripRequestEntity->addToRoute($tripRequestToPlace);
        }
    }
}
