<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\DTO\Trip\TripCancelRequest;
use App\Api\Entity\Trip;
use App\Api\Entity\TripCancellation as TripCancellationEntity;
use App\Api\Enum\ApiErrorCode;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripInternalStatusChanged;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Repository\TripRepository;
use App\Api\Service\Trip\CheckExternalStatusNotifier;
use App\Api\Service\Trip\ExternalTripStateLocker;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class TripCancellation
{
    /**
     * @var TripClientFactory
     */
    private $clientFactory;
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /** @var LoggerInterface */
    private $logger;
    /**
     * @var ExternalTripStateLocker
     */
    private $tripStateLocker;
    /**
     * @var CheckExternalStatusNotifier
     */
    private $checkExternalStatusNotifier;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        TripClientFactory $clientFactory,
        TripRepository $tripRepository,
        ExternalTripStateLocker $tripStateLocker,
        EventDispatcherInterface $dispatcher,
        CheckExternalStatusNotifier $checkExternalStatusNotifier,
        EntityManagerInterface $entityManager
    ) {
        $this->clientFactory = $clientFactory;
        $this->tripRepository = $tripRepository;
        $this->dispatcher = $dispatcher;
        $this->tripStateLocker = $tripStateLocker;
        $this->checkExternalStatusNotifier = $checkExternalStatusNotifier;
        $this->entityManager = $entityManager;

        $this->logger = new NullLogger();
    }

    /**
     * @throws ApiErrorCodeException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cancel(Trip $trip, ?TripCancelRequest $tripCancelRequest)
    {
        $lock = $this->tripStateLocker->create($trip, 10);
        try {
            $lock->acquire(true);
            $this->entityManager->refresh($trip);

            $dataToLog = ['tripId' => $trip->getId(), 'externalId' => $trip->getExternalId()];
            if (TripStatus::EXECUTED === $trip->getStatus()->getValue()) {
                throw new ApiErrorCodeException(ApiErrorCode::BAD_REQUEST_DATA());
            }

            if (!$trip->canCancel()) {
                $this->logger->error('Can\'t cancel trip', $dataToLog);
                throw new ApiErrorCodeException(ApiErrorCode::TRIP_CANNOT_CANCEL_TIME_IS_UP());
            }
            $taxiProviderId = $trip->getTaxiProvider()->getId();
            $client = $this->clientFactory->createFromProviderId($taxiProviderId);
            try {
                $this->logger->info('Start cancel trip', $dataToLog);
                $client->cancel($trip->getExternalId());
                $this->logger->info('Cancel trip finished', $dataToLog);
            } catch (\Throwable $e) {
                throw new \RuntimeException('Cannot cancel on provider side', 0, $e);
            }

            $status = new TripStatus(TripStatus::CANCELED);
            $trip->setCancelReason(new CancelReason(CancelReason::RIDER_CANCEL_FROM_APPLICATION))
                ->setStatus($status);

            if ($tripCancelRequest && !$trip->getCancellationReason()) {
                $cancelReason = TripCancellationEntity::createFromRequest($tripCancelRequest);
                $trip->setCancellationReason($cancelReason);
            }

            $this->tripRepository->save($trip);

            $event = new TripInternalStatusChanged($trip, $status);
            $this->dispatcher->dispatch($event);

            $this->checkExternalTripStatusAndNotify($trip, $client, $taxiProviderId);
        } catch (\Throwable $throwable) {
            throw $throwable;
        } finally {
            $lock->release();
        }
    }

    private function checkExternalTripStatusAndNotify(
        Trip $trip,
        TripAdapterInterface $client,
        ?int $taxiProviderId
    ): void {
        try {
            $externalTripCollection = new ExternalTripCollection();
            $promise = $client->find($trip->getExternalId(), $externalTripCollection);
            $promise->wait();
            $tripResponse = $externalTripCollection->find($trip->getExternalId(), $taxiProviderId);
            $this->checkExternalStatusNotifier->checkAndNotify($tripResponse, $trip);
        } catch (\Throwable $e) {
            $this->logger->error('Can not check trip after cancellation', ['trip' => $trip->getId()]);
        }
    }
}
