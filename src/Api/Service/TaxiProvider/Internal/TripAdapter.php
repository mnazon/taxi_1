<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider\Internal;

use App\Api\DTO\EstimatedTripItem;
use App\Api\DTO\OrderCostCollection;
use App\Api\DTO\Trip\ExtendedTripStatus;
use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\Entity\InternalProviderTrip;
use App\Api\Entity\TaxiProvider;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\CarAlreadyFoundException;
use App\Api\HttpDTO\CreateTripRequest;
use App\Api\HttpDTO\EstimateTripRequest;
use App\Api\HttpDTO\ProviderTripResponse;
use App\Api\Repository\InternalProviderTripRepository;
use App\Api\Service\TaxiProvider\TripAdapterInterface;
use Http\Promise\FulfilledPromise;
use Http\Promise\Promise;
use Money\Currency;
use Money\Money;
use Ramsey\Uuid\Uuid;

class TripAdapter implements TripAdapterInterface
{
    private const FAKE_DRIVER_PHONE = '+380501234567';
    private const FAKE_TRIP_COST = 50;
    private const FAKE_VEHICLE_DATA = [
        'numberPlate' => 'АА0000КН',
        'makeAndModel' => 'Tesla Model S',
        'color' => 'white',
    ];

    /**
     * @var InternalProviderTripRepository
     */
    private $internalTripRepository;

    /**
     * @var TaxiProvider
     */
    private $taxiProvider;

    public function __construct(InternalProviderTripRepository $internalTripRepository)
    {
        $this->internalTripRepository = $internalTripRepository;
    }

    public function setTaxiProvider(TaxiProvider $taxiProvider)
    {
        $this->taxiProvider = $taxiProvider;
    }

    public function cost(
        EstimateTripRequest $estimateTripRequest,
        OrderCostCollection $orderCostCollection
    ): Promise {
        $orderCostResponse = new EstimatedTripItem(
            Uuid::NIL,
            (float) self::FAKE_TRIP_COST,
            'грн',
            $this->taxiProvider
        );
        $orderCostCollection->add($orderCostResponse);

        return new FulfilledPromise([]);
    }

    /**
     * @throws ApiErrorCodeException
     * @throws \Throwable
     */
    public function updateAdditionalCost(string $externalTripId, float $additionalCost)
    {
        $internalTrip = $this->getInternalTrip($externalTripId);
        if ($internalTrip->getStatus()->isCarFound()) {
            throw new CarAlreadyFoundException();
        }

        $internalTrip->setAdditionalCost($additionalCost);
        $this->internalTripRepository->save($internalTrip);
    }

    public function cancel(string $externalTripId): int
    {
        $internalTrip = $this->getInternalTrip($externalTripId);
        $internalTrip->setStatus(new TripStatus(TripStatus::CANCELED));
        $this->internalTripRepository->save($internalTrip);
        $cancelResult = 1;

        return $cancelResult;
    }

    public function create(CreateTripRequest $createTripRequest, array $places): ProviderTripResponse
    {
        $reservedAt = $createTripRequest->getTrip()->getReservedAt();
        $tripStatus = null === $reservedAt ? TripStatus::SEARCH_FOR_CAR : TripStatus::WAITING_FOR_CAR_SEARCH;

        $trip = $createTripRequest->getTrip();
        $internalTrip = (new InternalProviderTrip())
            ->setCost((float) self::FAKE_TRIP_COST)
            ->setAdditionalCost($trip->getAdditionalCost())
            ->setArriveAt(new \DateTimeImmutable('+ 1 minutes'))
            ->setVehicle(self::FAKE_VEHICLE_DATA)
            ->setCurrency('грн')
            ->setStatus(new TripStatus($tripStatus))
        ;

        $this->internalTripRepository->save($internalTrip);

        $providerTripCreationResponse = new ProviderTripResponse(
            (string) $internalTrip->getId(),
            $internalTrip->getCost(),
            $internalTrip->getCurrency(),
            $internalTrip->getStatus(),
            $this->taxiProvider,
            $internalTrip->getCreatedAt()
        );

        return $providerTripCreationResponse;
    }

    public function find(string $externalTripId, ExternalTripCollection $externalTripCollection): Promise
    {
        $internalTrip = $this->getInternalTrip($externalTripId);
        $externalTrip = $this->createExternalTripInfo($internalTrip, $this->taxiProvider);
        $externalTripCollection->add($externalTrip);

        return new FulfilledPromise($externalTrip);
    }

    public function toStatus(array $data): TripStatus
    {
        return new TripStatus((int) $data['status']);
    }

    public function toCancelReason(array $data): ?CancelReason
    {
        return \in_array($data['cancelReason'], CancelReason::toArray())
            ? new CancelReason((int) $data['cancelReason'])
            : null;
    }

    private function createExternalTripInfo(
        InternalProviderTrip $internalTrip,
        TaxiProvider $taxiProvider
    ): ExternalTripResponse {
        $cost = new Money($internalTrip->getCost(), new Currency('UAH'));
        $status = $internalTrip->getStatus()->getValue();
        $extendedStatusData = [
            'status' => $status,
            'cancelReason' => TripStatus::CANCELED === $status ? CancelReason::RIDER_CANCEL : null,
        ];
        $extendedTripStatus = $this->toExtendedStatus($extendedStatusData);

        $tripStatus = new ExternalTripResponse(
            (string) $internalTrip->getId(),
            $taxiProvider->getId(),
            $extendedTripStatus,
            $cost,
            $internalTrip->getArriveAt(),
            $internalTrip->getVehicle(),
            self::FAKE_DRIVER_PHONE,
            null,
            TripStatus::EXECUTED === $status
        );

        return $tripStatus;
    }

    private function toExtendedStatus(array $data): ExtendedTripStatus
    {
        return new ExtendedTripStatus(
            $this->toStatus($data),
            $this->toCancelReason($data)
        );
    }

    private function getInternalTrip(string $id): InternalProviderTrip
    {
        $internalTrip = $this->internalTripRepository->find((int) $id);
        if (null === $internalTrip) {
            throw new \RuntimeException('Trip not found');
        }

        return $internalTrip;
    }
}
