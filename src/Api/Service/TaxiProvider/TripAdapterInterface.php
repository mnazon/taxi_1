<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\DTO\CoordinatesToPlace;
use App\Api\DTO\OrderCostCollection;
use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\Entity\TaxiProvider;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use App\Api\Exception\CarAlreadyFoundException;
use App\Api\HttpDTO\CreateTripRequest;
use App\Api\HttpDTO\EstimateTripRequest;
use App\Api\HttpDTO\ProviderTripResponse;
use Http\Promise\Promise;

interface TripAdapterInterface
{
    public function cost(
        EstimateTripRequest $estimateTripRequest,
        OrderCostCollection $orderCostCollection
    ): Promise;

    public function setTaxiProvider(TaxiProvider $taxiProvider);

    /**
     * @param CoordinatesToPlace[] $coordinatesToPLaces
     */
    public function create(CreateTripRequest $createTripRequest, array $coordinatesToPLaces): ProviderTripResponse;

    /**
     * @throws CarAlreadyFoundException
     *
     * @return mixed
     */
    public function updateAdditionalCost(string $externalTripId, float $additionalCost);

    public function cancel(string $externalTripId): int;

    public function find(string $externalTripId, ExternalTripCollection $externalTripCollection): Promise;

    //todo - remove into some mapper class
    public function toStatus(array $data): TripStatus;

    public function toCancelReason(array $data): ?CancelReason;
}
