<?php

declare(strict_types=1);

namespace App\Api\Service\TaxiProvider;

use App\Api\Enum\TaxiPlatform;
use App\Api\Enum\TaxiProvider;
use App\Api\Exception\UnsupportedTaxiPlatformException;
use App\Api\Repository\TaxiProviderRepository;

class TripClientFactory
{
    /**
     * @var Evos\TripAdapter
     */
    private $evosOrderClient;
    /**
     * @var TaxiProviderRepository
     */
    private $taxiProviderRepository;
    /**
     * @var Internal\TripAdapter
     */
    private $internalOrderClient;

    public function __construct(
        Evos\TripAdapter $evosOrderClient,
        Internal\TripAdapter $internalOrderClient,
        TaxiProviderRepository $taxiProviderRepository
    ) {
        $this->evosOrderClient = $evosOrderClient;
        $this->internalOrderClient = $internalOrderClient;
        $this->taxiProviderRepository = $taxiProviderRepository;
    }

    public function create(TaxiProvider $taxiProvider): TripAdapterInterface
    {
        $client = clone $this->evosOrderClient;
        $client->setTaxiProvider($taxiProvider);

        return $client;
    }

    public function createFromEntity(\App\Api\Entity\TaxiProvider $taxiProvider): TripAdapterInterface
    {
        $taxiPlatform = $taxiProvider->getTaxiPlatform();
        $taxiPlatform = $taxiPlatform ? $taxiPlatform->getValue() : null;
        switch ($taxiPlatform) {
            case TaxiPlatform::EVOS:
                $client = clone $this->evosOrderClient;
                break;
            case TaxiPlatform::INTERNAL:
                $client = clone $this->internalOrderClient;
                break;
            default:
                throw new UnsupportedTaxiPlatformException(sprintf('Taxi platform of "%s" provider is not supported', $taxiProvider->getName()));
        }

        $client->setTaxiProvider($taxiProvider);

        return $client;
    }

    public function createFromProviderId(int $taxiProviderId): TripAdapterInterface
    {
        $taxiProvider = $this->taxiProviderRepository->find($taxiProviderId);

        return $this->createFromEntity($taxiProvider);
    }
}
