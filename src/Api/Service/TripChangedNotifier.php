<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\Controller\V1\AbstractApiController;
use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\Event\TripStateChanged;
use Centrifugo\Centrifugo;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TripChangedNotifier implements EventSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Centrifugo
     */
    private $centrifugo;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(
        Centrifugo $centrifugo,
        NormalizerInterface $normalizer
    ) {
        $this->centrifugo = $centrifugo;
        $this->normalizer = $normalizer;

        $this->logger = new NullLogger();
    }

    public function send(Trip $trip, TripStatus $prevTripStatus = null)
    {
        $tripId = $trip->getId();
        $data = [
            'tripId' => $tripId,
            'prevStatus' => $prevTripStatus ? $prevTripStatus->getValue() : $trip->getStatus()->getValue(),
            'status' => $trip->getStatus()->getValue(),
            'arriveAt' => ($trip->getArriveAt()) ? $trip->getArriveAt()->format(\DateTime::ATOM) : null,
            'trip' => $this->normalizer->normalize($trip, 'json', ['groups' => AbstractApiController::RESPONSE_DEFAULT_GROUP]),
        ];
        $chanelName = 'trip.state_changed_'.$trip->getId();
        $dataToLog = $data;
        $this->logger->info("Start sync status throw centrifugo $tripId", $dataToLog);
        try {
            $response = $this->centrifugo->publish($chanelName, $data);

            $dataToLog['error'] = $response->getError();
            $dataToLog['body'] = $response->getBody();
            $this->logger->info("End sync status throw centrifugo $tripId", $dataToLog);
        } catch (\Throwable $e) {
            $data['exception'] = $e;
            $this->logger->error('Failed to publish to centrifugo', $data);
        }

        //todo leave only this chanel for use, remove - trip.state_changed_
        try {
            $userChanelName = 'user.trips_'.$trip->getUser()->getId();
            $response = $this->centrifugo->publish($userChanelName, $data);
        } catch (\Throwable $e) {
            $data['exception'] = $e;
            $this->logger->error('Failed to publish to centrifugo', $data);
        }
    }

    public function handleTripStatusChanged(TripExternalStatusChanged $event)
    {
        $this->send($event->getTrip(), $event->getFromStatus());
    }

    public function handleTripStateChanged(TripStateChanged $event)
    {
        $this->send($event->getTrip());
    }

    public static function getSubscribedEvents()
    {
        return [
            TripStateChanged::class => 'handleTripStateChanged',
            TripExternalStatusChanged::class => 'handleTripStatusChanged',
        ];
    }
}
