<?php

declare(strict_types=1);

namespace App\Api\Service\Trip;

use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TripCostFactor;
use App\Api\Repository\TripCostFactorRepository;
use DateTimeInterface;

class TripCostHintCalculator
{
    /** @var TripCostFactorRepository */
    private $tripCostFactorRepository;

    public function __construct(TripCostFactorRepository $costFactorRepository)
    {
        $this->tripCostFactorRepository = $costFactorRepository;
    }

    public function calculateAdditionalCost(
        float $baseCost,
        DateTimeInterface $date,
        TaxiProvider $taxiProvider = null
    ): ?int {
        $additionalCost = null;
        /** @var TripCostFactor $tripCostFactor */
        $tripCostFactor = $this->tripCostFactorRepository->findOneByDate($date, $taxiProvider);
        if (!$tripCostFactor) {
            $tripCostFactor = $this->tripCostFactorRepository->findOneByDate($date);
        }
        if ($tripCostFactor) {
            $min = $tripCostFactor->getFactorMin();
            $max = $tripCostFactor->getFactorMax();
            $costFactor = rand($min, $max);
            $additionalCost = (int) ($baseCost * $costFactor / 100);
        }

        return $additionalCost;
    }
}
