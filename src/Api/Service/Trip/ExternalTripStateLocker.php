<?php

declare(strict_types=1);

namespace App\Api\Service\Trip;

use App\Api\Entity\Trip;
use Symfony\Component\Lock\Lock;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\PersistingStoreInterface;
use Symfony\Component\Lock\Store\RetryTillSaveStore;

class ExternalTripStateLocker
{
    /**
     * @var LockFactory
     */
    private $factory;
    /**
     * @var PersistingStoreInterface
     */
    private $store;

    public function __construct(PersistingStoreInterface $store)
    {
        $this->store = new RetryTillSaveStore($store);
        $this->factory = new LockFactory($this->store);
    }

    /**
     * @return bool
     */
    public function create(Trip $trip, float $ttl = 1): Lock
    {
        if ($ttl < 0) {
            throw new \InvalidArgumentException('Invalid ttl value');
        }
        $tripStateLockKey = sprintf('trip-state-lock-%s', $trip->getId());
        $lock = $this->factory->createLock($tripStateLockKey, $ttl);

        return $lock;
    }
}
