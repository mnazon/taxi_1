<?php

declare(strict_types=1);

namespace App\Api\Service\Trip;

use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\Entity\Trip;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class CheckExternalStatusNotifier implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    public function checkAndNotify(ExternalTripResponse $tripResponse, Trip $trip)
    {
        $isStatusNotEqual = $tripResponse->getTripStatus()->getValue() !== $trip->getStatus()->getValue();
        $tripNotInArchive = false === $tripResponse->isInArchive();
        if ($isStatusNotEqual || $tripNotInArchive) {
            $dataToLog = [
                'taxiProviderId' => $tripResponse->getTaxiProviderId(),
                'externalId' => $tripResponse->getId(),
                'tripId' => $trip->getId(),
                'tripCancelReason' => $trip->getCancelReason(),
                'providerStatus' => $tripResponse->getTripStatus()->getValue(),
            ];
            $this->logger->critical('Trip status changed after cancellation or trip not in archive', $dataToLog);
        }
    }
}
