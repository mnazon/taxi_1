<?php

namespace App\Api\Service\Trip;

use App\Api\Entity\Trip;
use App\Api\Entity\TripStatusTracking;
use App\Api\Enum\CancelReason;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\Event\TripStateChanged;
use App\Api\Exception\NotFoundException;
use App\Api\Exception\UpdateTripException;
use App\Api\Message\UpdateTripMessageInterface;
use App\Api\Repository\TripRepository;
use App\Api\Repository\TripStatusTrackingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateTripHandler implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var TripStatusTrackingRepository
     */
    private $tripStatusTrackingRepository;
    /**
     * @var ExternalTripStateLocker
     */
    private $tripStateLocker;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        TripRepository $tripRepository,
        TripStatusTrackingRepository $tripStatusTrackingRepository,
        ExternalTripStateLocker $tripStateLocker,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tripRepository = $tripRepository;
        $this->tripStatusTrackingRepository = $tripStatusTrackingRepository;
        $this->tripStateLocker = $tripStateLocker;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;

        $this->logger = new NullLogger();
    }

    public function handle(UpdateTripMessageInterface $message)
    {
        $trip = $this->getTrip($message->getTripId(), true);
        $lock = $this->tripStateLocker->create($trip, 3);
        try {
            $lock->acquire(true);
            $trip = $this->getTrip($message->getTripId());

            $this->assertTripNotChanged($trip, $message->getOldTripStatus());

            if ($this->isStatusChanged($trip, $message->getNewTripStatus())) {
                $newTripStatus = $message->getNewTripStatus();

                $this->applyTripChanges($trip, $message);
                $this->triggerStatusChangedEvent($trip->getStatus(), $newTripStatus, $trip);
                $this->checkValidStatusAndReason($newTripStatus, $message->getCancelReason(), $trip);
                $this->handleTripStateChanged($trip);

                $this->entityManager->flush();
            }
        } catch (\Throwable $throwable) {
            $this->logger->critical('Error handling UpdateTripMessage', [
                'message' => $throwable->getMessage(),
                'trace' => $throwable->getTrace(),
                'exception' => $throwable,
            ]);
            throw $throwable;
        } finally {
            $lock->release();
        }
    }

    private function getTrip(int $id, bool $proxy = false): Trip
    {
        if ($proxy) {
            $trip = $this->entityManager->getReference(Trip::class, $id);
        } else {
            $trip = $this->tripRepository->find($id);
        }

        if (!$trip) {
            throw new NotFoundException(sprintf('Trip %s not found', $id));
        }

        return $trip;
    }

    private function assertTripNotChanged(Trip $trip, TripStatus $tripStatus): void
    {
        if (!$trip->getStatus()->equals($tripStatus)) {
            throw new UpdateTripException('Trip status changed until UpdateMessage incoming to Handler');
        }
    }

    private function isStatusChanged(Trip $trip, TripStatus $newTripStatus): bool
    {
        return !$trip->getStatus()->equals($newTripStatus);
    }

    private function triggerStatusChangedEvent(TripStatus $prevTripStatus, TripStatus $newTripStatus, Trip $trip)
    {
        try {
            if ($newTripStatus->isRunning() && !$prevTripStatus->isCarFound()) {
                $this->eventDispatcher->dispatch(
                    new TripExternalStatusChanged(TripStatus::CAR_FOUND(), $newTripStatus, $trip)
                );
            }
            $this->eventDispatcher->dispatch(
                new TripExternalStatusChanged($prevTripStatus, $newTripStatus, $trip)
            );
        } catch (\Throwable $e) {
            $this->logger->critical('Cannot dispatch TripExternalStatusChanged event', [
                'tripId' => $trip->getId(),
                'exception' => $e,
            ]);
            throw $e;
        }
    }

    private function checkValidStatusAndReason(TripStatus $newTripStatus, ?CancelReason $cancelReason, Trip $trip): void
    {
        if (null === $cancelReason && $newTripStatus->isCanceled()) {
            $this->logger->critical('Trip canceled without cancelReason', [
                'externalId' => $trip->getExternalId(),
                'tripId' => $trip->getId(),
            ]);
        }
    }

    private function applyTripChanges(Trip $trip, UpdateTripMessageInterface $message): void
    {
        $newTripStatus = $message->getNewTripStatus();

        $trip->setStatus($newTripStatus);

        if ($newTripStatus->isBiggerOrEqualThanCarFound()) {
            $trip->setArriveAt($message->getArriveAt());
        } else {
            $trip->setArriveAt(null);
        }

        if ($message->getCancelReason()) {
            $trip->setCancelReason($message->getCancelReason());
        }
        if ($message->getCarInfo()) {
            $trip->setVehicle($message->getCarInfo());
        }
        if ($message->getDriverPhone()) {
            $trip->setDriverPhone($message->getDriverPhone());
        }
    }

    /**
     * @throws \Throwable
     */
    private function handleTripStateChanged(Trip $trip): void
    {
        $tripStatusTracking = $this->getStatusTrackingByTripId($trip->getId());

        $isStateChanged = !$tripStatusTracking->isSentTripStateChanged() && !$trip->canCancel();
        if ($isStateChanged) {
            $tripStatusTracking->setIsSentTripStateChanged(true);
            try {
                $this->eventDispatcher->dispatch(
                    new TripStateChanged($trip)
                );
            } catch (\Throwable $e) {
                $this->logger->error('Dispatch failed', [
                    'tripId' => $trip->getId(),
                    'exception' => $e,
                ]);
                throw $e;
            }
        }
    }

    private function getStatusTrackingByTripId(int $id): TripStatusTracking
    {
        $tripTrackingData = $this->tripStatusTrackingRepository->findByTripId($id);
        if (!$tripTrackingData) {
            throw new NotFoundException(sprintf('TripStatusTracking by tripId %s not found', $id));
        }

        return $tripTrackingData;
    }
}
