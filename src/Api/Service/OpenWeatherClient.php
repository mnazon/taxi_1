<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\OpenWeather\CurrentWeatherResponse;
use App\Api\Exception\WeatherWrongDataException;
use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use KoKoKo\assert\Assert;
use KoKoKo\assert\exceptions\ArgumentException;
use KoKoKo\assert\exceptions\InvalidArrayCountException;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;

class OpenWeatherClient
{
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var MessageFactory
     */
    private $messageFactory;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(HttpClient $httpClient, MessageFactory $messageFactory, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->messageFactory = $messageFactory;
        $this->logger = $logger;
    }

    public function current(int $cityId): CurrentWeatherResponse
    {
        $request = $this->buildRequestByCityId($cityId);
        $response = $this->httpClient->sendRequest($request);

        $data = json_decode($response->getBody()->getContents(), true);
        try {
            $currentWeatherResponse = $this->checkDataAndBuildCurrentWeather($data);
        } catch (ArgumentException $e) {
            throw new WeatherWrongDataException($e->getMessage(), $e->getCode());
        }

        return $currentWeatherResponse;
    }

    private function buildRequestByCityId(int $cityId): RequestInterface
    {
        $query = [
            'id' => $cityId,
            'units' => 'metric',
        ];
        $uri = 'data/2.5/weather/?'.http_build_query($query);

        return $this->messageFactory->createRequest('GET', $uri);
    }

    private function checkDataAndBuildCurrentWeather(?array $data)
    {
        Assert::assert($data, 'data')
            ->isArray()
            ->hasKey('main')
            ->hasKey('weather')
            ->hasKey('wind')
            ->hasKey('id')
            ->hasKey('visibility')
        ;

        $weather = Assert::assert($data['weather'], 'weather')->isArray()->hasKey(0)->get();
        try {
            Assert::assert($weather, 'weather')->count(1);
        } catch (InvalidArrayCountException $e) {
            $this->logger->warning('The weather contains more than one element.', ['exception' => $e]);
        }

        $weatherFirst = Assert::assert($weather[0], 'weather')->isArray()->hasKey('id')->get();
        $main = Assert::assert($data['main'], 'main')->hasKey('temp')->get();
        $wind = Assert::assert($data['wind'], 'wind')->hasKey('speed')->get();

        $weatherId = (int) Assert::assert($weatherFirst['id'], 'weatherId')->numeric()->get();
        $cityId = (int) Assert::assert($data['id'], 'cityId')->numeric()->get();
        $temperature = (int) Assert::assert($main['temp'], 'temperature')->numeric()->get();
        $windSpeed = (int) Assert::assert($wind['speed'], 'windSpeed')->numeric()->get();
        $visibility = (int) Assert::assert($data['visibility'], 'visibility')->numeric()->get();

        $rain1h = isset($data['rain']['1h']) ? (float) $data['rain']['1h'] : null;
        $rain3h = isset($data['rain']['3h']) ? (float) $data['rain']['3h'] : null;
        $snow1h = isset($data['snow']['1h']) ? (float) $data['snow']['1h'] : null;
        $snow3h = isset($data['snow']['3h']) ? (float) $data['snow']['3h'] : null;

        $currentWeather = new CurrentWeatherResponse(
            $cityId,
            $temperature,
            $windSpeed,
            $visibility,
            $weatherId,
            $weather,
            $rain1h,
            $rain3h,
            $snow1h,
            $snow3h
        );

        return $currentWeather;
    }
}
