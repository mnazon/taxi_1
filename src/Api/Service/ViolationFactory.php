<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Translation\Message;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ViolationFactory
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function createViolation(Message $message, string $propertyPath = null): ConstraintViolation
    {
        $plural = $message->getPlural();
        if (null === $plural) {
            $transMessage = $this->translator->trans(
                $message->getMessageTemplate(),
                $message->getParameters(),
                $message->getDomain()
            );
        } else {
            $transMessage = $this->translator->transChoice(
                $message->getMessageTemplate(),
                $message->getPlural(),
                $message->getParameters(),
                $message->getDomain()
            );
        }

        return new ConstraintViolation(
            $transMessage,
            $message->getMessageTemplate(),
            $message->getParameters(),
            '',
            $propertyPath,
            ''
        );
    }

    public function createViolationList(Message $message, string $propertyPath = null)
    {
        $violation = $this->createViolation($message, $propertyPath);
        $violationList = new ConstraintViolationList();
        $violationList->add($violation);

        return $violationList;
    }
}
