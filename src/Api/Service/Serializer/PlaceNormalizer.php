<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\DTO\CoordinatesToPlace;
use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\PlaceData;
use App\Api\Entity\Place;
use App\Api\Entity\Place as PlaceEntity;
use App\Api\Entity\TripRequestToPlace;
use App\Api\Entity\UserTopPlace;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlaceNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        if ($object instanceof UserTopPlace) {
            $placeEntity = $object->getPlace();
            $place = new AutocompleteData(
                $this->getMainText($placeEntity),
                $placeEntity->getSecondaryText(),
                $placeEntity->getExternalId(),
                true,
                null,
                $object->getCoordinates()//exact coordinates of the pick-up/drop-off
            );
        } elseif ($object instanceof CoordinatesToPlace || $object instanceof TripRequestToPlace) {
            $place = new PlaceData(
                $this->getMainText($object->getPlace()),
                $object->getCoordinates()
            );
            $place->setLocality($object->getPlace()->getLocality());
        } else {
            /* @var $object PlaceEntity */
            $place = new PlaceData(
                $this->getMainText($object),
                $object->getCoordinates()
            );
            $place->setLocality($object->getLocality());
        }

        $data = $this->normalizer->normalize($place, $format, $context);

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof PlaceEntity
            || $data instanceof UserTopPlace
            || $data instanceof CoordinatesToPlace
            || $data instanceof TripRequestToPlace;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }

    /**
     * @param $place
     *
     * @return mixed
     */
    private function getMainText(Place $place)
    {
        $address = $place->getFullStreetAddress();
        if ($place->isObject()) {
            if (!empty($address)) {
                $text = sprintf('%s (%s)', $place->getName(), $address);
            } else {
                $text = $place->getName();
            }
        } else {
            $text = $place->getFullStreetAddress();
        }

        $text = empty($text) ? $place->getFormattedAddress() : $text;
        $text = empty($text) ? 'Назва не визначенна' : $text;

        return $text;
    }
}
