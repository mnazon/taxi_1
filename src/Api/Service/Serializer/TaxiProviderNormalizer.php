<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\Entity\TaxiProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TaxiProviderNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var string
     */
    private $imageHost;
    /**
     * @var ObjectNormalizer
     */
    private $normalizer;

    public function __construct(
        RequestStack $request,
        ObjectNormalizer $normalizer,
        string $imageHost
    ) {
        $this->request = $request->getCurrentRequest();
        $this->imageHost = $imageHost;
        $this->normalizer = $normalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $additionalData = null;
        /* @var $object TaxiProvider */
        if ($additionalDataObject = $object->getAdditionalData()) {
            $additionalData = $this->normalizer->normalize($additionalDataObject, $format, $context);
            $additionalData['carStandbyTime'] = $additionalData['carStandbyTime'] ?? null;
        }

        $taxiProviderData = [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'slug' => $object->getSlug(),
            'rating' => $object->getRating(),
            'additionalData' => $additionalData,
        ];

        $schemeAndHost = ($this->request) ? $this->request->getSchemeAndHttpHost() : $this->imageHost;
        $taxiProviderData['iconUrl'] = ($object->getIcon()) ? $schemeAndHost.$object->getIconUrl() : null;

        return $taxiProviderData;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof TaxiProvider;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }
}
