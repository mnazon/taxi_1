<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\Entity\UserOption;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UserOptionNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof UserOption;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        /** @var UserOption $object */
        $data = [
            'userOption' => $object->getUserOption()->getKey(),
            'value' => (bool) $object->getValue(),
        ];

        return $data;
    }
}
