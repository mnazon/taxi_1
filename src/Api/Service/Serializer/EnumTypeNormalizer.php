<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use MyCLabs\Enum\Enum;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EnumTypeNormalizer implements NormalizerInterface, DenormalizerInterface, CacheableSupportsMethodInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /* @var $object Enum */
        return $object->getValue();
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Enum;
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new $class($data);
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        if (!class_exists($type)) {
            return false;
        }

        return \in_array(Enum::class, class_parents($type));
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }
}
