<?php

namespace App\Api\Service\Serializer;

use Money\Currency;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CurrencyDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return new Currency($data);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return Currency::class === $type && 'json' === $format;
    }
}
