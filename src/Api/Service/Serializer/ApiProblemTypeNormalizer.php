<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\DTO\Translation\Message;
use App\Api\HttpDTO\Response\ApiError;
use App\Api\HttpDTO\Response\ApiProblem;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * This Normalizer implements RFC7807 {@link https://tools.ietf.org/html/rfc7807}.
 */
class ApiProblemTypeNormalizer implements NormalizerInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        /** @var ApiProblem $object */
        $data = [
            'type' => $object->getType(),
            'title' => $object->getTitle(),
        ];
        if ($object->getDetail()) {
            $data['detail'] = $this->translate($object->getDetail());
        }
        if ($object->getStatus()) {
            $data['status'] = $object->getStatus();
        }
        $data['violations'] = [];
        foreach ($object->getErrors() as $apiError) {
            $data['violations'][] = $this->normalizeError($apiError);
        }

        return $data;
    }

    private function normalizeError(ApiError $apiError)
    {
        $violationEntry = [
            'propertyPath' => $apiError->getPropertyPath(),
            'title' => $this->translate($apiError->getTitle()),
        ];
        if (null !== $type = $apiError->getCode()) {
            $violationEntry['code'] = $type;
        }

        return $violationEntry;
    }

    private function translate(Message $message): string
    {
        $plural = $message->getPlural();
        if (null === $plural) {
            $transMessage = $this->translator->trans(
                $message->getMessageTemplate(),
                $message->getParameters(),
                $message->getDomain()
            );
        } else {
            $transMessage = $this->translator->transChoice(
                $message->getMessageTemplate(),
                $message->getPlural(),
                $message->getParameters(),
                $message->getDomain()
            );
        }

        return $transMessage;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ApiProblem;
    }
}
