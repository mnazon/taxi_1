<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\Entity\TripVote;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TripVoteNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var DateTimeNormalizer
     */
    private $dateTimeNormalizer;

    public function __construct(DateTimeNormalizer $dateTimeNormalizer)
    {
        $this->dateTimeNormalizer = $dateTimeNormalizer;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        /** @var TripVote $object */
        $data = [
            'value' => $object->getValue(),
            'createdAt' => $this->dateTimeNormalizer->normalize($object->getCreatedAt(), $format, $context),
            'userName' => $object->getVoter()->getFullName(),
            'comment' => $object->getComment(),
        ];

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof TripVote;
    }
}
