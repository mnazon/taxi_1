<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\Entity\TripRequest;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TripRequestNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        /** @var TripRequest $object */
        $data = $this->normalizer->normalize($object, $format, $context);
        $data['additionalCost'] = $data['additionalCost'] ?? 0;

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof TripRequest;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return __CLASS__ === \get_class($this);
    }
}
