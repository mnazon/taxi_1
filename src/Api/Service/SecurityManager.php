<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\phone\PhoneConfirmationDto;
use App\Api\DTO\RegistrationRequest;
use App\Api\Entity\User;
use App\Api\Entity\UserToken;
use App\Api\Enum\ApiErrorCode;
use App\Api\Event\UserLoggedIn;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\Exception\PhoneConfirmationException;
use App\Api\Repository\UserRepository;
use App\Api\Repository\UserTokenRepository;
use App\Api\Service\Phone\PhoneConfirmationManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class SecurityManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var PhoneConfirmationManager
     */
    private $phoneConfirmationManager;
    /**
     * @var ViolationFactory
     */
    private $violationFactory;
    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        UserRepository $userRepository,
        UserTokenRepository $userTokenRepository,
        ValidatorInterface $validator,
        ViolationFactory $violationFactory,
        PhoneConfirmationManager $phoneConfirmationManager,
        UserPasswordEncoderInterface $userPasswordEncoder,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->validator = $validator;
        $this->phoneConfirmationManager = $phoneConfirmationManager;
        $this->violationFactory = $violationFactory;
        $this->userTokenRepository = $userTokenRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @throws ConstraintViolationListException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws PhoneConfirmationException
     * @throws \Exception
     */
    public function register(RegistrationRequest $registrationRequest): UserToken
    {
        $this->assertValidConfirmationCode($registrationRequest);

        $createdAt = new \DateTimeImmutable();
        $user = $this->buildAndSaveUser($registrationRequest, $createdAt);
        $userToken = $this->buildAndSaveUserToken($user, $createdAt);
        $this->dispatchUserLoggedIn($registrationRequest, $user);

        return $userToken;
    }

    /**
     * @throws ConstraintViolationListException
     * @throws PhoneConfirmationException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function login(User $user, RegistrationRequest $registrationRequest): UserToken
    {
        $this->assertValidConfirmationCode($registrationRequest);

        $createdAt = new \DateTimeImmutable();
        $userToken = $this->buildAndSaveUserToken($user, $createdAt);
        $this->dispatchUserLoggedIn($registrationRequest, $user);

        return $userToken;
    }

    /**
     * @throws ApiErrorCodeException
     * @throws PhoneConfirmationException
     * @throws \Doctrine\ORM\ORMException
     */
    private function assertValidConfirmationCode(RegistrationRequest $registrationRequest): void
    {
        $confirmation = new PhoneConfirmationDto(
            $registrationRequest->getPhone(),
            $registrationRequest->getConfirmationCode(),
            $registrationRequest->getIp()
        );

        $confirmResult = $this->phoneConfirmationManager->confirm($confirmation);

        if (!$confirmResult) {
            throw new ApiErrorCodeException(ApiErrorCode::PHONE_CONFIRMATION_CODE());
        }
    }

    /**
     * @throws \Exception
     */
    private function buildAndSaveUserToken(User $user, \DateTimeImmutable $createdAt): UserToken
    {
        $token = bin2hex(random_bytes(16));
        $userToken = $this->userTokenRepository->findOneByUser($user);
        if (null === $userToken) {
            $userToken = new UserToken($token, $user, $createdAt);
        } else {
            $userToken = $userToken->setAccessToken($token);
        }

        $this->userTokenRepository->save($userToken);

        return $userToken;
    }

    /**
     * @throws ConstraintViolationListException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function buildAndSaveUser(RegistrationRequest $registrationRequest, \DateTimeImmutable $createdAt): User
    {
        $user = new User();
        $user->setPhone($registrationRequest->getPhone())
            ->setCreatedAt($createdAt)
            ->setIsPolicyAccepted(false);
        $violations = $this->validator->validate($user);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }
        $this->userRepository->save($user);

        return $user;
    }

    private function dispatchUserLoggedIn(RegistrationRequest $registrationRequest, User $user)
    {
        $event = new UserLoggedIn($registrationRequest, $user);
        $this->eventDispatcher->dispatch($event);
    }
}
