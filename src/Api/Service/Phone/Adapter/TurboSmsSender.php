<?php

declare(strict_types=1);

namespace App\Api\Service\Phone\Adapter;

use App\Api\Exception\SmsSendException;
use App\Api\Service\Phone\SmsSenderAdapterInterface;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class TurboSmsSender implements SmsSenderAdapterInterface
{
    /**
     * @var \SoapClient
     */
    private $client;
    /**
     * @var PhoneNumberUtil
     */
    private $phoneNumberUtil;
    /**
     * @var string
     */
    private $sender;
    /**
     * @var string
     */
    private $authLogin;
    /**
     * @var string
     */
    private $authPass;
    /**
     * @var string
     */
    private $wsdl;

    public function __construct(
        string $authLogin,
        string $authPass,
        string $wsdl,
        string $sender,
        PhoneNumberUtil $phoneNumberUtil
    ) {
        $this->phoneNumberUtil = $phoneNumberUtil;
        $this->sender = $sender;
        $this->authLogin = $authLogin;
        $this->authPass = $authPass;
        $this->wsdl = $wsdl;
    }

    /**
     * @throws SmsSendException
     */
    public function send(PhoneNumber $phoneNumber, string $text): string
    {
        if ($this->phoneNumberUtil->isValidNumber($phoneNumber)) {
            throw new \RuntimeException('Not valid phone number');
        }

        $authData = [
            'login' => $this->authLogin,
            'password' => $this->authPass,
        ];
        $authResult = $this->getClient()->Auth($authData);
        if ('Неверный логин или пароль' === $authResult->AuthResult) {
            $message = sprintf('Cannot auth on sms gateway - %s', $authResult->AuthResult);
            throw new SmsSendException('', $message);
        } else {
            //Вы успешно авторизировались positive response
        }

        $formattedPhoneNumber = $this->phoneNumberUtil->format($phoneNumber, PhoneNumberFormat::E164);
        $smsData = [
            'sender' => $this->sender,
            'destination' => $formattedPhoneNumber,
            'text' => $text,
        ];

        try {
            $sendSmsResult = $this->getClient()->SendSMS($smsData);
        } catch (\Throwable $t) {
            throw new SmsSendException($formattedPhoneNumber, 'Cannot send sms', 0, $t);
        }

        if (!isset($sendSmsResult->SendSMSResult->ResultArray[1])
            || !\is_string($sendSmsResult->SendSMSResult->ResultArray[1])
        ) {
            $message = sprintf(
                'Cannot send sms - %s',
                var_export($sendSmsResult, true)
            );
            throw new SmsSendException($formattedPhoneNumber, $message);
        }

        return $sendSmsResult->SendSMSResult->ResultArray[1];
    }

    protected function getClient(): \SoapClient
    {
        if (null === $this->client) {
            $this->client = new \SoapClient($this->wsdl);
        }

        return $this->client;
    }
}
