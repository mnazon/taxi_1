<?php

declare(strict_types=1);

namespace App\Api\Service\Phone\Adapter;

use App\Api\Service\Phone\SmsSenderAdapterInterface;
use libphonenumber\PhoneNumber;

class DummySmsSender implements SmsSenderAdapterInterface
{
    public function send(PhoneNumber $phoneNumber, string $text): string
    {
        return 'dummy-message-id';
    }
}
