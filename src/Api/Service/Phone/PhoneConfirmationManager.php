<?php

declare(strict_types=1);

namespace App\Api\Service\Phone;

use App\Api\DTO\phone\PhoneConfirmationDto;
use App\Api\DTO\phone\SendPhoneConfirmationDTO;
use App\Api\DTO\phone\SendPhoneResponseDto;
use App\Api\Entity\Phone;
use App\Api\Entity\PhoneConfirmation;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\PhoneConfirmationException;
use App\Api\Repository\PhoneConfirmationRepository;
use App\Api\Repository\PhoneRepository;
use Doctrine\ORM\ORMException;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PhoneConfirmationManager
{
    private const CONFIRM_CODE_LIFETIME = 10;
    private const CONFIRM_CODE_RATE_LIMIT_COUNT = 5;
    private const CONFIRM_CODE_BLOCKED_TIME = 10;
    private const CONFIRM_CODE_SEND_RATE_SECONDS = 60;

    /**
     * @var PhoneNumberUtil
     */
    private $phoneNumberUtil;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;
    /**
     * @var PhoneConfirmationRepository
     */
    private $phoneConfirmationRepository;
    /**
     * @var SmsSenderAdapterInterface
     */
    private $smsSenderAdapter;

    public function __construct(
        ValidatorInterface $validator,
        PhoneRepository $phoneRepository,
        PhoneConfirmationRepository $phoneConfirmationRepository,
        PhoneNumberUtil $phoneNumberUtil,
        SmsSenderAdapterInterface $smsSenderAdapter
    ) {
        $this->phoneNumberUtil = $phoneNumberUtil;
        $this->validator = $validator;
        $this->phoneRepository = $phoneRepository;
        $this->phoneConfirmationRepository = $phoneConfirmationRepository;
        $this->smsSenderAdapter = $smsSenderAdapter;
    }

    public function setSmsSenderAdapter(SmsSenderAdapterInterface $smsSenderAdapter)
    {
        $this->smsSenderAdapter = $smsSenderAdapter;
    }

    /**
     * @throws ORMException
     * @throws ApiErrorCodeException
     * @throws \Exception
     */
    public function sendConfirmationCode(
        SendPhoneConfirmationDTO $sendPhoneConfirmationDTO
    ): SendPhoneResponseDto {
        //@todo concurrency problem or lock need
        //phone verification validator
        $violationList = $this->validator->validate($sendPhoneConfirmationDTO);
        if ($violationList->count() > 0) {
            throw new \RuntimeException('Not valid object');
        }

        $phoneNumber = $sendPhoneConfirmationDTO->getPhone();
        $phone = $this->phoneRepository->findOneByPhone($phoneNumber);
        $createdAt = new \DateTimeImmutable();
        if (!$phone) {
            $phone = new Phone($phoneNumber, $createdAt);
        }

        //@todo - add some logic how much from this ip
        //@todo - add some logic how much for this phone
        //@todo - add logic on all confirmation for period
        $this->assertValidPhoneState($phone, $createdAt);
        $confirmationCode = $this->getConfirmationCodeToSend($phone);
        $confirmation = new PhoneConfirmation();
        $confirmation->setCode($confirmationCode);
        $confirmation->setSentAt($createdAt);
        $confirmation->setIp($sendPhoneConfirmationDTO->getIpLong());
        $phone->addPhoneConfirmation($confirmation);

        try {
            //@todo - определится с текстом в сообщении с верификацией кода
            $this->smsSenderAdapter->send($phoneNumber, (string) $confirmationCode);
            $this->phoneRepository->save($phone);
        } catch (ORMException $t) {
            throw $t;
        }

        return new SendPhoneResponseDto(
            self::CONFIRM_CODE_SEND_RATE_SECONDS,
            $confirmationCode
        );
    }

    /**
     * @throws ORMException
     * @throws PhoneConfirmationException if no phone found
     * @throws ApiErrorCodeException
     *                                    or some anti spam rules failed
     */
    public function confirm(PhoneConfirmationDto $phoneConfirmationDto): bool
    {
        $phoneNumber = $phoneConfirmationDto->getPhone();
        $phoneEntity = $this->phoneRepository->findOneByPhone($phoneNumber);
        if (null === $phoneEntity) {
            throw new PhoneConfirmationException(sprintf('No phone found %s', $phoneNumber));
        }

        $passedConfirmationCode = $phoneConfirmationDto->getCode();
        $confirmationEntity = $this->assertValidConfirmationState($phoneEntity);
        $isConfirmationCodeMatch = $this->isConfirmationCodeValid($confirmationEntity, $passedConfirmationCode);

        $current = new \DateTimeImmutable();
        if ($isConfirmationCodeMatch) {
            $phoneEntity->setConfirmedAt($current);
            $confirmationEntity->setConfirmedAt($current);
        } else {
            $attempt = $phoneEntity->getConfirmationAttemptNumber();
            $phoneEntity->setConfirmationAttemptNumber(++$attempt);
            if ($attempt >= self::CONFIRM_CODE_RATE_LIMIT_COUNT) {
                $blockedUntil = $current->modify(sprintf('+%d minutes', self::CONFIRM_CODE_BLOCKED_TIME));
                $phoneEntity->setConfirmationBlockedUntil($blockedUntil);
            }
        }

        $this->phoneRepository->save($phoneEntity);

        return $isConfirmationCodeMatch;
    }

    /**
     * @throws \Exception
     */
    private function getConfirmationCodeToSend(Phone $phone): int
    {
        $currentDate = new \DateTimeImmutable();
        $phoneConfirmation = null;
        if ($phone->getId()) {
            $phoneConfirmation = $this->phoneConfirmationRepository->findOneByPhonePeriodOfSendAndCode(
                $phone,
                $currentDate->modify(sprintf('-%d minutes', self::CONFIRM_CODE_LIFETIME)),
                $currentDate
            );
        }

        if ($phoneConfirmation) {
            $nextAttemptIn = $currentDate->getTimestamp() - ($phoneConfirmation->getSentAt()->getTimestamp() + self::CONFIRM_CODE_SEND_RATE_SECONDS);
            if ($nextAttemptIn <= 0) {
                throw new ApiErrorCodeException(ApiErrorCode::CONFIRMATION_CODE_SENDING_RATE_LIMIT());
            }
        }

        $confirmationCode = null !== $phoneConfirmation ?
            $phoneConfirmation->getCode()
            : random_int(1000, 9999);

        return $confirmationCode;
    }

    /**
     * @throws ApiErrorCodeException
     * @throws PhoneConfirmationException
     */
    private function assertValidConfirmationState(Phone $phone): PhoneConfirmation
    {
        $current = new \DateTimeImmutable();
        $this->assertValidPhoneState($phone, $current);

        $confirmationEntity = $this->phoneConfirmationRepository->findLastByPhone($phone);
        if (null === $confirmationEntity) {
            throw new PhoneConfirmationException(sprintf('No confirmation code sent', $phone));
        }

        $isCodeExpired = $current->getTimestamp() - self::CONFIRM_CODE_LIFETIME * 60 >
            $confirmationEntity->getSentAt()->getTimestamp();

        //todo - remove this huck
        $isTimeLessConfirmationCode = $this->isTimeLessConfirmationCode($phone);
        if ($isCodeExpired && !$isTimeLessConfirmationCode) {
            throw new ApiErrorCodeException(ApiErrorCode::PHONE_CONFIRMATION_CODE_EXPIRED());
        }

        return $confirmationEntity;
    }

    /**
     * @throws ApiErrorCodeException
     */
    private function assertValidPhoneState(Phone $phone, \DateTimeImmutable $current): void
    {
        if (null !== $phone->getConfirmationBlockedUntil()) {
            if ($current < $phone->getConfirmationBlockedUntil()) {
                throw new ApiErrorCodeException(ApiErrorCode::PHONE_BANNED());
            } else {
                $phone->resetConfirmationTimer();
            }
        }
    }

    /**
     * @throws NumberParseException
     */
    private function isTimeLessConfirmationCode(Phone $phone): bool
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $specialPhone = $phoneUtil->parse('+380992943666');
        $isTimeLessConfirmationCode = $phone->getValue()->equals($specialPhone);

        return $isTimeLessConfirmationCode;
    }

    private function isConfirmationCodeValid(PhoneConfirmation $confirmationEntity, int $passedConfirmationCode): bool
    {
        //todo - how much confirmation for one phone, for one ip?
        $isConfirmationCodeMatch = null !== $confirmationEntity
            && $confirmationEntity->getCode() === $passedConfirmationCode;

        return $isConfirmationCodeMatch || $this->isTimeLessConfirmationCode($confirmationEntity->getPhone());
    }
}
