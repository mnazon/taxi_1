<?php

namespace App\Api\Service\Phone;

use App\Api\Exception\SmsSendException;
use libphonenumber\PhoneNumber;

interface SmsSenderAdapterInterface
{
    /**
     * @return string messageId
     *
     * @throws SmsSendException
     */
    public function send(PhoneNumber $phoneNumber, string $text): string;
}
