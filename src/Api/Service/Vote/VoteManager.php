<?php

declare(strict_types=1);

namespace App\Api\Service\Vote;

use App\Api\DTO\AddVoteDto;
use App\Api\Entity\TripVote;
use App\Api\Entity\User;
use App\Api\Event\VoteAdded;
use App\Api\Repository\TripRepository;
use App\Api\Repository\TripVoteRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class VoteManager implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var TripVoteRepository
     */
    private $tripVoteRepository;
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        TripVoteRepository $tripVoteRepository,
        TripRepository $tripRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tripVoteRepository = $tripVoteRepository;
        $this->tripRepository = $tripRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;

        $this->logger = new NullLogger();
    }

    /**
     * @throws UniqueConstraintViolationException
     * @throws NotFoundHttpException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addVote(AddVoteDto $addVoteDto, User $user): TripVote
    {
        $tripId = $addVoteDto->getTripId();
        $trip = $this->tripRepository->find($tripId);
        if (!$trip || $trip->getUser()->getId() !== $user->getId()) {
            throw new NotFoundHttpException('Trip not found');
        }
        $tripVote = $this->tripVoteRepository->findByTrip($trip);

        $this->logger->info('VoteManager.addVote.trying', [
            'message' => 'Trying create TripVote',
            'tripId' => $tripId,
            'userId' => $trip->getUser()->getId(),
            'isTripVoteExists' => (bool) $tripVote,
        ]);

        if (null !== $tripVote) {
            throw new AccessDeniedException(sprintf('Vote already exists for Trip(id=%d)', $tripId));
        }

        $tripVote = new TripVote(
            $addVoteDto->getValue(),
            $user,
            $trip,
            new \DateTimeImmutable(),
            $addVoteDto->getComment()
        );

        $this->tripVoteRepository->add($tripVote);

        $this->eventDispatcher->dispatch(new VoteAdded($tripVote));

        return $tripVote;
    }
}
