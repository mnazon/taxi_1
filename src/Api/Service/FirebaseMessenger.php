<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Firebase\MessageDto;
use Kreait\Firebase;
use Kreait\Firebase\Messaging\MessageTarget;
use Psr\Log\LoggerInterface;

class FirebaseMessenger
{
    /**
     * @var Firebase
     */
    private $firebase;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Firebase $firebase, LoggerInterface $logger)
    {
        $this->firebase = $firebase;
        $this->logger = $logger;
    }

    public function send(MessageDto $msg): array
    {
        $token = $msg->getNotificationToken()->getFcmToken();
        $message = Firebase\Messaging\CloudMessage::withTarget(MessageTarget::TOKEN, $token)
            ->withNotification($msg->getNotification());

        $msgData = $msg->getMessageData();
        if ($msgData) {
            $message = $message->withData($msgData);
        }

        $androidConfig = $msg->getAndroidConfig();
        if ($androidConfig) {
            $message = $message->withAndroidConfig($androidConfig);
        }

        $apnsConfig = $msg->getApnsConfig();
        if ($apnsConfig) {
            $message = $message->withApnsConfig($apnsConfig);
        }

        $messaging = $this->firebase->getMessaging();
        $response = $messaging->send($message);

        $logData = [
            'message' => $message->jsonSerialize(),
            'response' => $response,
        ];

        $this->logger->info('Firebase message sending', $logData);

        return $response;
    }
}
