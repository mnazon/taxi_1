<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\AppVersionDto;
use App\Api\Entity\User;
use App\Api\Entity\UserAppVersion;
use App\Api\Enum\MobileApplicationFeature;
use App\Api\Repository\UserAppVersionRepository;

class FeatureSupportChecker
{
    /**
     * @var UserAppVersionRepository
     */
    private $userAppVersionRepository;

    public function __construct(UserAppVersionRepository $userAppVersionRepository)
    {
        $this->userAppVersionRepository = $userAppVersionRepository;
    }

    public function isSupportedByUser(MobileApplicationFeature $feature, User $user): bool
    {
        $userAppVersion = $this->userAppVersionRepository->findOneByUser($user);
        if ($userAppVersion) {
            $supportedAppVersions = $feature->getSupportedAppVersions();
            foreach ($supportedAppVersions as $appVersion) {
                if ($this->appVersionsCompatible($appVersion, $userAppVersion)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function appVersionsCompatible(AppVersionDto $appVersionDto, UserAppVersion $userAppVersion): bool
    {
        $platformsAreEqual = $appVersionDto->getPlatform()->equals($userAppVersion->getPlatform());

        return $platformsAreEqual && $userAppVersion->getAppVersion() >= $appVersionDto->getAppVersion();
    }
}
