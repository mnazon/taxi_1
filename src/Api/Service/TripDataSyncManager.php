<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Trip\ExternalTripCollection;
use App\Api\DTO\Trip\ExternalTripResponse;
use App\Api\Entity\Trip;
use App\Api\Entity\TripStatusTracking;
use App\Api\Enum\TripStatus;
use App\Api\Event\ExternalTripDataRetrieved;
use App\Api\Exception\WrongAuthorizationArgumentException;
use App\Api\Message\UpdateTripMessage;
use App\Api\Service\TaxiProvider\TripClientFactory;
use App\Api\Service\Trip\UpdateTripHandler;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use function GuzzleHttp\Promise\all;
use Http\Promise\Promise;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

class TripDataSyncManager implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var bool
     */
    private $async;
    /**
     * @var UpdateTripHandler
     */
    private $updateTripHandler;
    /**
     * @var TripClientFactory
     */
    private $tripClientFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(
        UpdateTripHandler $updateTripHandler,
        TripClientFactory $tripClientFactory,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        MessageBusInterface $bus
    ) {
        $this->updateTripHandler = $updateTripHandler;
        $this->tripClientFactory = $tripClientFactory;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->bus = $bus;

        $this->logger = new NullLogger();
    }

    public function handleAsync(TripStatusTracking ...$tripsStatusTracking)
    {
        $this->async = true;
        $this->startSyncProcess($tripsStatusTracking);
    }

    /**
     * @throws Throwable
     */
    private function startSyncProcess(array $tripsStatusTracking): void
    {
        $externalTripPromises = [];

        foreach ($tripsStatusTracking as $tripStatusTracking) {
            $trip = $tripStatusTracking->getTrip();
            try {
                $externalTripPromises[] = $this->findExternalTripPromise($trip)
                    ->then($this->getResponseHandler($trip));
                $this->setNextRetryAt($trip, $tripStatusTracking);
            } catch (WrongAuthorizationArgumentException $e) {
                $this->logger->warning(
                    'Unable to get external trip due to provider authorization data problem', [
                    'providerName' => $trip->getTaxiProvider()->getName(),
                    'tripId' => $trip->getId(),
                    'exception' => $e,
                ]);

                $tripStatusTracking->setRetryAt(new DateTimeImmutable('+1 minutes'));
            } finally {
                $this->entityManager->flush();
            }
        }

        try {
            all($externalTripPromises)->wait(false);
        } catch (Throwable $e) {
            $this->logger->warning('Request failed to taxiProvider with find trip method', [
                'exception' => $e,
            ]);
        }
    }

    private function findExternalTripPromise(Trip $trip): Promise
    {
        $taxiProvider = $trip->getTaxiProvider();
        $tripClient = $this->tripClientFactory->createFromProviderId($taxiProvider->getId());

        return $tripClient->find($trip->getExternalId(), new ExternalTripCollection());
    }

    private function getResponseHandler(Trip $trip)
    {
        return function (ExternalTripResponse $externalTripResponse) use ($trip) {
            $this->handleTripExternalResponse($trip, $externalTripResponse);
            $this->raiseExternalDataRetrieved($trip, $externalTripResponse);

            return $externalTripResponse;
        };
    }

    /**
     * @throws Throwable
     */
    private function handleTripExternalResponse(Trip $trip, ExternalTripResponse $externalTripResponse)
    {
        try {
            $isExternalStatusChanged = !$trip->getStatus()->equals($externalTripResponse->getTripStatus());
            if ($isExternalStatusChanged) {
                $message = UpdateTripMessage::fromResponse($externalTripResponse, $trip->getId(), $trip->getStatus());
                if ($this->async) {
                    $this->bus->dispatch($message);
                } else {
                    $this->updateTripHandler->handle($message);
                }
            }
        } catch (Throwable $e) {
            $this->logger->error('Dispatch failed', [
                'tripId' => $trip->getId(),
                'exception' => $e,
            ]);
        }
    }

    private function raiseExternalDataRetrieved(Trip $trip, ExternalTripResponse $externalTripResponse): void
    {
        try {
            $this->eventDispatcher->dispatch(
                new ExternalTripDataRetrieved($externalTripResponse, $trip)
            );
        } catch (Throwable $e) {
            $this->logger->error('Dispatch failed', [
                'tripId' => $trip->getId(),
                'exception' => $e,
            ]);
        }
    }

    private function setNextRetryAt(Trip $trip, TripStatusTracking $tripStatusTracking): void
    {
        $arrivedAt = $trip->getArriveAt();
        $currentTime = new DateTimeImmutable();

        if ($arrivedAt) {
            $diffInSeconds = $currentTime->getTimestamp() - $arrivedAt->getTimestamp();
            if ($diffInSeconds >= -15 * 60 && $diffInSeconds < 5 * 60) {
                $nextRetryTimeInterval = 10;
            } elseif ($diffInSeconds >= 5 * 60 && $diffInSeconds < 20 * 60) {
                $nextRetryTimeInterval = 20;
            } else {
                $nextRetryTimeInterval = 60;
            }
        } else {
            if (TripStatus::SEARCH_FOR_CAR === $trip->getStatus()->getValue()) {
                $nextRetryTimeInterval = 10;
            } else {
                $nextRetryTimeInterval = 60;
            }
        }

        $modifySeconds = sprintf('+%s sec', $nextRetryTimeInterval);
        $newRetryAt = $tripStatusTracking->getRetryAt()->modify($modifySeconds);
        $tripStatusTracking->setRetryAt($newRetryAt);
    }

    public function handle(TripStatusTracking ...$tripsStatusTracking)
    {
        $this->async = false;
        $this->startSyncProcess($tripsStatusTracking);
    }
}
