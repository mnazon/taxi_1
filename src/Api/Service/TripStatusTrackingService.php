<?php

namespace App\Api\Service;

use App\Api\Entity\Trip;
use App\Api\Entity\TripStatusTracking;
use App\Api\Enum\TaxiPlatform;
use App\Api\Enum\TripStatus;
use App\Api\Repository\TripStatusTrackingRepository;

class TripStatusTrackingService
{
    /**
     * @var TripStatusTrackingRepository
     */
    private $tripStatusTrackingRepository;

    public function __construct(TripStatusTrackingRepository $tripStatusTrackingRepository)
    {
        $this->tripStatusTrackingRepository = $tripStatusTrackingRepository;
    }

    public function createStatusTracking(Trip $trip): ?TripStatusTracking
    {
        $taxiPlatform = $trip->getTaxiProvider()->getTaxiPlatform();

        if (\in_array($taxiPlatform->getValue(), [TaxiPlatform::EVOS, TaxiPlatform::INTERNAL], true)) {
            $tripStatusTracking = null;
            $status = $trip->getStatus();
            if ($trip->getId()) {
                $tripStatusTracking = $this->tripStatusTrackingRepository->findByTripId($trip->getId());
            }
            if (!$tripStatusTracking) {
                $nextRetry = $trip->getCreatedAt()->modify('+10 seconds');

                if (TripStatus::WAITING_FOR_CAR_SEARCH === $status->getValue()) {
                    $reservedAt = $trip->getRequest()->getReservedAt();
                    if ($reservedAt) {
                        $nextRetry->modify('-60 minutes');
                    }
                }

                $tripStatusTracking = (new TripStatusTracking())
                    ->setTrip($trip)
                    ->setRetryAt($nextRetry);
            }

            if ($status->getValue() !== $tripStatusTracking->getStatus()) {
                $tripStatusTracking->setStatus($status);
            }

            return $tripStatusTracking;
        }

        return null;
    }
}
