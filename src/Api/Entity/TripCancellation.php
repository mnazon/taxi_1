<?php

namespace App\Api\Entity;

use App\Api\DTO\Trip\TripCancelRequest;
use App\Api\Enum\TripCancellationReason;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripCancellationRepository")
 * @ORM\Table(name="tripCancellation")
 */
class TripCancellation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\Trip", inversedBy="cancellationReason")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     *
     * @var Trip
     */
    private $trip;

    /**
     * @ORM\Column(type="tinyint")
     *
     * @var TripCancellationReason
     */
    private $cancellationReason;

    /**
     * @ORM\Column(type="string", nullable=true, length=1000)
     *
     * @var string|null
     */
    private $message;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCancellationReason(): TripCancellationReason
    {
        if (null !== $this->cancellationReason && !($this->cancellationReason instanceof TripCancellationReason)) {
            $this->cancellationReason = new TripCancellationReason((int) $this->cancellationReason);
        }

        return $this->cancellationReason;
    }

    /**
     * @return $this
     */
    public function setCancellationReason(TripCancellationReason $cancellationReason): self
    {
        $this->cancellationReason = $cancellationReason;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return TripCancellation
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    /**
     * @return TripCancellation
     */
    public function setTrip(Trip $trip): self
    {
        $this->trip = $trip;

        return $this;
    }

    /**
     * @return static
     */
    public static function createFromRequest(TripCancelRequest $tripCancelRequest): self
    {
        $cancelReason = new self();
        $cancelReason->setCancellationReason(new TripCancellationReason($tripCancelRequest->getTripCancellationReason()));
        $cancelReason->setMessage($tripCancelRequest->getMessage());

        return $cancelReason;
    }
}
