<?php

namespace App\Api\Entity;

use App\Api\DTO\Vehicle;
use App\Api\Enum\CancelReason;
use App\Api\Enum\CurrencySign;
use App\Api\Enum\TripStatus;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripRepository")
 * @ORM\Table(name="trip", indexes={@ORM\Index(name="createdAt", columns={"createdAt"})})
 */
class Trip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api","list"})
     */
    private $id;
    //todo - setup unique with taxiProvider?
    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"api","list"})
     */
    private $externalId;
    /**
     * @SWG\Property(description="depracated - remove soon")
     * @ORM\Column(type="decimal", precision=8, scale=2)
     * @Groups({"list","api"})
     *
     * @var float
     */
    private $cost;
    /**
     * @Groups({"list","api"})
     *
     * @var float
     */
    private $baseCost;
    /**
     * @ORM\Column(type="string", length=4)
     * @Groups({"list","api"})
     */
    private $currency;
    /**
     * @todo remove this logic, make instead - Money object and use it with cost and CurrencyObject
     * @Groups({"list","api"})
     *
     * @var string
     */
    private $currencySign = CurrencySign::UKR;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     * @Groups({"list","api"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="json_array", length=100, nullable=true)
     *
     * @var Vehicle|null
     * @SWG\Property(ref="#/definitions/Vehicle")
     * @Groups({"list","api"})
     */
    private $vehicle;
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"list","api"})
     */
    private $driverPhone;
    /**
     * @ORM\Column(type="tinyint", length=1, nullable=true)
     */
    private $cancelReason;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TripCancellation", mappedBy="trip",  cascade={"persist"})
     *
     * @var TripCancellation
     */
    private $cancellationReason;

    /**
     * @var TripStatus
     * @ORM\Column(type="tinyint")
     * @Groups({"list","api"})
     */
    private $status;
    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\TaxiProvider")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list","api"})
     *
     * @var TaxiProvider
     */
    private $taxiProvider;
    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Groups({"list","api"})
     *
     * @var \DateTimeImmutable
     */
    private $arriveAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\UserDevice")
     */
    private $device;

    /**
     * @todo - remove field
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     *
     * @var float|null
     */
    private $additionalCost;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\NotificationToken")
     */
    private $notificationToken;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TripRequest", inversedBy="trip", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="tripRequestToPlace",
     *     joinColumns={
     *          @ORM\JoinColumn(name="request_id", referencedColumnName="id")
     *     },
     * )
     *
     * @Groups({"list", "api"})
     *
     * @var TripRequest
     */
    private $request;

    /**
     * @Groups({"api"})
     *
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @var \DateTimeImmutable|null
     */
    private $startSearchingCarAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        if (\is_array($this->vehicle)) {
            if (!empty($this->vehicle)) {
                $this->vehicle = new Vehicle(
                    $this->vehicle['numberPlate'],
                    $this->vehicle['makeAndModel'],
                    $this->vehicle['color']
                );
            } else {
                $this->vehicle = null;
            }
        }

        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getDriverPhone(): ?string
    {
        return $this->driverPhone;
    }

    public function setDriverPhone(?string $driverPhone): self
    {
        $this->driverPhone = $driverPhone;

        return $this;
    }

    public function getCancelReason(): ?CancelReason
    {
        if (null !== $this->cancelReason && !($this->cancelReason instanceof CancelReason)) {
            $this->cancelReason = new CancelReason((int) $this->cancelReason);
        }

        return $this->cancelReason;
    }

    public function setCancelReason(CancelReason $cancelReason): self
    {
        $this->cancelReason = $cancelReason;

        return $this;
    }

    public function getStatus(): TripStatus
    {
        return new TripStatus((int) $this->status);
    }

    public function setStatus(TripStatus $status): self
    {
        $this->status = $status->getValue();

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTaxiProvider(): ?TaxiProvider
    {
        return $this->taxiProvider;
    }

    public function setTaxiProvider(?TaxiProvider $taxiProvider): self
    {
        $this->taxiProvider = $taxiProvider;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getArriveAt(): ?\DateTimeImmutable
    {
        return $this->arriveAt;
    }

    public function setArriveAt(?\DateTimeImmutable $arriveAt): self
    {
        $this->arriveAt = $arriveAt;

        return $this;
    }

    public function getDevice(): ?UserDevice
    {
        return $this->device;
    }

    public function setDevice(?UserDevice $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getAdditionalCost(): ?float
    {
        return $this->additionalCost;
    }

    public function setAdditionalCost(?float $additionalCost): self
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    public function getNotificationToken(): ?NotificationToken
    {
        return $this->notificationToken;
    }

    public function setNotificationToken(?NotificationToken $notificationToken): self
    {
        $this->notificationToken = $notificationToken;

        return $this;
    }

    public function getCurrencySign(): string
    {
        return $this->currencySign;
    }

    public function getRequest(): ?TripRequest
    {
        return $this->request;
    }

    public function setRequest(?TripRequest $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getBaseCost(): ?float
    {
        return $this->cost;
    }

    public function getStartSearchingCarAt(): ?\DateTimeImmutable
    {
        return $this->startSearchingCarAt;
    }

    public function setStartSearchingCarAt(?\DateTimeImmutable $startSearchingCarAt): self
    {
        $this->startSearchingCarAt = $startSearchingCarAt;

        return $this;
    }

    public function canCancel(): bool
    {
        $isSuitableStatus = !$this->getStatus()->isCanceled()
            && !$this->getStatus()->isExecuted();

        $isCarArrived = false;
        if ($isSuitableStatus && null !== $this->arriveAt) {
            $currentTime = new \DateTime();
            $isCarArrived = $currentTime->getTimestamp() > $this->arriveAt->getTimestamp();
        }

        return $isSuitableStatus && !$isCarArrived;
    }

    /**
     * @Groups({"api"})
     *
     * @return string[]
     */
    public function getState(): array
    {
        //todo - maybe need to add TripState object later
        return [
            'canCancel' => $this->canCancel(),
        ];
    }

    public function getTripStart(): TripRequestToPlace
    {
        return $this->getRequest()->getRoute()->first();
    }

    public function getCancellationReason(): ?TripCancellation
    {
        return $this->cancellationReason;
    }

    public function setCancellationReason(?TripCancellation $cancellationReason): self
    {
        $this->cancellationReason = $cancellationReason;
        $cancellationReason->setTrip($this);

        return $this;
    }
}
