<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripRequestRepository")
 * @ORM\Table(name="tripRequest")
 */
class TripRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     * @Groups({"api"})
     */
    private $carType;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Groups({"api"})
     */
    private $reservedAt;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"api"})
     */
    private $userFullName;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"api"})
     */
    private $userPhone;

    /**
     * @ORM\Column(type="AdditionalServicesType", nullable=true)
     * @Groups({"api"})
     *
     * @var string[]
     */
    private $options;

    /**
     * @ORM\Column(type="tinyint", nullable=true)
     * @Groups({"api"})
     */
    private $startRouteEntranceFrom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"api"})
     */
    private $comment;
    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     * @Groups({"api", "list"})
     *
     * @var float|null
     */
    private $additionalCost;
    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\Trip", mappedBy="request")
     */
    private $trip;
    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Api\Entity\TripRequestToPlace",
     *     mappedBy="tripRequest",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     * @ORM\OrderBy({"routePointNumber"="ASC"})
     * @Groups({"api"})
     */
    private $route;

    public function __construct()
    {
        $this->route = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarType(): ?string
    {
        return $this->carType;
    }

    public function setCarType(string $carType): self
    {
        $this->carType = $carType;

        return $this;
    }

    public function getReservedAt(): ?\DateTimeImmutable
    {
        return $this->reservedAt;
    }

    public function setReservedAt(?\DateTimeImmutable $reservedAt): self
    {
        $this->reservedAt = $reservedAt;

        return $this;
    }

    public function getUserFullName(): ?string
    {
        return $this->userFullName;
    }

    public function setUserFullName(?string $userFullName): self
    {
        $this->userFullName = $userFullName;

        return $this;
    }

    public function getUserPhone(): ?string
    {
        return $this->userPhone;
    }

    public function setUserPhone(?string $userPhone): self
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getStartRouteEntranceFrom()
    {
        return $this->startRouteEntranceFrom;
    }

    public function setStartRouteEntranceFrom(?int $startRouteEntranceFrom): self
    {
        $this->startRouteEntranceFrom = $startRouteEntranceFrom;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getTrip(): ?Trip
    {
        return $this->trip;
    }

    public function setTrip(?Trip $trip): self
    {
        $this->trip = $trip;

        // set (or unset) the owning side of the relation if necessary
        $newRequest = null === $trip ? null : $this;
        if ($newRequest !== $trip->getRequest()) {
            $trip->setRequest($newRequest);
        }

        return $this;
    }

    public function getAdditionalCost(): ?float
    {
        return $this->additionalCost;
    }

    /**
     * @return TripRequest
     */
    public function setAdditionalCost(?float $additionalCost): self
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    /**
     * @return Collection|TripRequestToPlace[]
     */
    public function getRoute(): Collection
    {
        return $this->route;
    }

    public function addToRoute(TripRequestToPlace $tripRequestToPlace): self
    {
        if (!$this->route->contains($tripRequestToPlace)) {
            $this->route[] = $tripRequestToPlace;
        }

        return $this;
    }
}
