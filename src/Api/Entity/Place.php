<?php

namespace App\Api\Entity;

use App\Api\DTO\Geo\Coordinates;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\PlaceRepository")
 * @ORM\Table(name="place", indexes={
 *      @ORM\Index(name="externalId", columns={"externalId"}, options={"lengths": {16}}),
 *     })
 */
class Place
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $externalId;
    /**
     * @ORM\Column(type="decimal", precision=11, scale=8)
     *
     * @var float
     */
    private $lat;
    /**
     * @ORM\Column(type="decimal", precision=11, scale=8)
     *
     * @var float
     */
    private $lng;
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     *
     * @var string|null
     */
    private $streetNumber;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $street;
    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     *
     * @var string|null
     */
    private $locality;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $district;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @var string|null
     */
    private $admLevel1;
    /**
     * @var Coordinates
     */
    private $coordinates;
    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $approximate;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $name;
    /**
     * @var string|null
     */
    private $formattedAddress;

    public function __construct(
        ?string $name,
        ?string $streetNumber,
        ?string $street,
        ?string $locality,
        ?string $district,
        ?string $admLevel1,
        string $externalId,
        Coordinates $coordinates,
        bool $approximate = false,
        ?string $formattedAddress = null
    ) {
        $this->streetNumber = $streetNumber;
        $this->street = $street;
        $this->locality = $locality;
        $this->district = $district;
        $this->admLevel1 = $admLevel1;
        $this->externalId = $externalId;
        $this->coordinates = $coordinates;
        $this->approximate = $approximate;
        $this->name = $name;
        $this->lat = $coordinates->getLat();
        $this->lng = $coordinates->getLng();
        $this->formattedAddress = $formattedAddress;
    }

    /**
     * @return Place
     */
    public function withId(int $id): self
    {
        $new = clone $this;
        $new->id = $id;

        return $new;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function isObject()
    {
        return !empty($this->name);
    }

    public function getObjectName()
    {
        return $this->name;
    }

    public function getSecondaryText(): string
    {
        $text = [];
        if ($this->isObject()) {
            $text[] = $this->getFullStreetAddress(true);
        }

        if ($this->district) {
            $text[] = $this->district;
        }

        if ($this->locality) {
            $text[] = $this->locality;
        }

        return trim(implode(', ', $text));
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    /**
     * @return string
     */
    public function getAdmLevel1(): ?string
    {
        return $this->admLevel1;
    }

    /**
     * @Groups("api")
     */
    public function getCoordinates(): Coordinates
    {
        if (null === $this->coordinates) {
            $this->coordinates = new Coordinates(
                $this->lat,
                $this->lng
            );
        }

        return $this->coordinates;
    }

    public function setCoordinates(Coordinates $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

    public function isApproximate(): bool
    {
        return $this->approximate;
    }

    /**
     * @Groups("api")
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function getFullStreetAddress($forceWithStreetNumber = false): ?string
    {
        $text = $this->street;
        if ($this->streetNumber && ($forceWithStreetNumber || !$this->approximate)) {
            $text .= ', '.$this->streetNumber;
        }

        return $text;
    }

    public function getFormattedAddress(): ?string
    {
        return $this->formattedAddress;
    }
}
