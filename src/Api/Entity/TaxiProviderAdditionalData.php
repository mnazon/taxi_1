<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TaxiProviderAdditionalDataRepository")
 * @ORM\Table(name="taxiProviderAdditionalData")
 */
class TaxiProviderAdditionalData
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var TaxiProvider|null
     *
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TaxiProvider", inversedBy="additionalData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="taxiProvider_id", referencedColumnName="id", nullable=false)
     */
    private $taxiProvider;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $standardTariffName;

    /**
     * @var int|null
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $carStandbyTime;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTaxiProvider(): ?TaxiProvider
    {
        return $this->taxiProvider;
    }

    /**
     * @return TaxiProviderAdditionalData
     */
    public function setTaxiProvider(TaxiProvider $taxiProvider): self
    {
        $this->taxiProvider = $taxiProvider;

        return $this;
    }

    public function getCarStandbyTime(): ?int
    {
        return $this->carStandbyTime;
    }

    public function getStandardTariffName(): ?string
    {
        return $this->standardTariffName;
    }

    /**
     * @return TaxiProviderAdditionalData
     */
    public function setStandardTariffName(?string $standardTariffName): self
    {
        $this->standardTariffName = $standardTariffName;

        return $this;
    }
}
