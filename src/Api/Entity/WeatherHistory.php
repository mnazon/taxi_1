<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\WeatherHistoryRepository")
 * @ORM\Table(name="weatherHistory", indexes={@ORM\Index(name="created_at_index", columns={"createdAt"})})
 */
class WeatherHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="tinyint", length=2)
     */
    private $temperature;

    /**
     * @ORM\Column(type="tinyint", length=3)
     */
    private $windSpeed;

    /**
     * @ORM\Column(type="smallint", length=4)
     */
    private $visibility;

    /**
     * @ORM\Column(type="smallint", length=4)
     */
    private $weatherId;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $weather;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2, nullable=true)
     */
    private $precipitationRain1h;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2, nullable=true)
     */
    private $precipitationRain3h;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2, nullable=true)
     */
    private $precipitationSnow1h;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2, nullable=true)
     */
    private $precipitationSnow3h;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemperature(): ?int
    {
        return $this->temperature;
    }

    public function setTemperature(int $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getWindSpeed(): ?int
    {
        return $this->windSpeed;
    }

    public function setWindSpeed(int $windSpeed): self
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }

    public function getPrecipitationRain1h(): ?float
    {
        return $this->precipitationRain1h;
    }

    public function setPrecipitationRain1h(?float $precipitationRain1h): self
    {
        $this->precipitationRain1h = $precipitationRain1h;

        return $this;
    }

    public function getPrecipitationRain3h(): ?float
    {
        return $this->precipitationRain3h;
    }

    public function setPrecipitationRain3h(?float $precipitationRain3h): self
    {
        $this->precipitationRain3h = $precipitationRain3h;

        return $this;
    }

    public function getPrecipitationSnow1h(): ?float
    {
        return $this->precipitationSnow1h;
    }

    public function setPrecipitationSnow1h(?float $precipitationSnow1h): self
    {
        $this->precipitationSnow1h = $precipitationSnow1h;

        return $this;
    }

    public function getPrecipitationSnow3h(): ?float
    {
        return $this->precipitationSnow3h;
    }

    public function setPrecipitationSnow3h(?float $precipitationSnow3h): self
    {
        $this->precipitationSnow3h = $precipitationSnow3h;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getVisibility(): int
    {
        return $this->visibility;
    }

    public function setVisibility(int $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function getWeatherId(): int
    {
        return $this->weatherId;
    }

    public function setWeatherId($weatherId): self
    {
        $this->weatherId = $weatherId;

        return $this;
    }

    public function getWeather(): array
    {
        return $this->weather;
    }

    public function setWeather(array $weather): self
    {
        $this->weather = $weather;

        return $this;
    }
}
