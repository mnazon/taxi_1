<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\NotificationTokenRepository")
 * @ORM\Table(name="notificationToken", indexes={
 *     @ORM\Index(name="fcmToken", columns={"fcmToken"}, options={"lengths": {20} }),
 * })
 */
class NotificationToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fcmToken;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFcmToken(): ?string
    {
        return $this->fcmToken;
    }

    public function setFcmToken(string $fcmToken): self
    {
        $this->fcmToken = $fcmToken;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
