<?php

declare(strict_types=1);

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripCostFactorRepository")
 * @ORM\Table(name="tripCostFactor")
 */
class TripCostFactor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $factorMin;

    /**
     * @ORM\Column(type="smallint")
     */
    private $factorMax;

    /**
     * @ORM\Column(type="time")
     */
    private $timeFrom;

    /**
     * @ORM\Column(type="time")
     */
    private $timeTo;

    /**
     * @ORM\Column(type="tinyint")
     */
    private $dateOfWeek;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\TaxiProvider")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var TaxiProvider
     */
    private $taxiProvider;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfWeek(): ?int
    {
        return $this->dateOfWeek;
    }

    public function setDateOfWeek($dateOfWeek): self
    {
        $this->dateOfWeek = $dateOfWeek;

        return $this;
    }

    public function getFactorMin(): ?int
    {
        return $this->factorMin;
    }

    public function setFactorMin(int $factorMin): self
    {
        $this->factorMin = $factorMin;

        return $this;
    }

    public function getFactorMax(): ?int
    {
        return $this->factorMax;
    }

    public function setFactorMax(int $factorMax): self
    {
        $this->factorMax = $factorMax;

        return $this;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->timeFrom;
    }

    public function setTimeFrom(\DateTimeInterface $timeFrom): self
    {
        $this->timeFrom = $timeFrom;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->timeTo;
    }

    public function setTimeTo(\DateTimeInterface $timeTo): self
    {
        $this->timeTo = $timeTo;

        return $this;
    }
}
