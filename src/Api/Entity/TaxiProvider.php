<?php

namespace App\Api\Entity;

use App\Api\Enum\TaxiPlatform;
use App\Api\Exception\WrongAuthorizationArgumentException;
use App\Api\Repository\TaxiProviderAuthRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition(description="rating - nullable")
 * @ORM\Entity(repositoryClass="App\Api\Repository\TaxiProviderRepository")
 * @ORM\Table(name="taxiProvider")
 */
class TaxiProvider
{
    public const WEB_PATH_TO_IMAGE = '/upload/providers/';
    private const PATH_TO_IMAGE = '/public'.self::WEB_PATH_TO_IMAGE;

    /**
     * @Groups({"api"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $id;

    /**
     * @Groups({"api"})
     * @ORM\Column(type="string", length=70)
     */
    private $name;

    /**
     * @Groups({"api"})
     * @ORM\Column(type="string", length=70)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @Groups({"api"})
     * @ORM\Column(type="tinyint")
     *
     * @var TaxiPlatform
     * @SWG\Property(ref="#/definitions/TaxiPlatform")
     */
    private $taxiPlatform;

    /**
     * @Groups({"api"})
     *
     * @var string
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $icon;

    /**
     * @var UploadedFile|null
     * @Assert\File(mimeTypes={"image/png"}, )
     */
    private $iconFile;

    /**
     * @Groups({"api"})
     * @SWG\Property(description="nullable")
     *
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TaxiProviderRating", mappedBy="taxiProvider", cascade={"persist", "remove"})
     *
     * @var TaxiProviderRating
     */
    private $rating;

    /**
     * @ORM\Column(type="tinyint", nullable=true)
     *
     * @var int|null
     */
    private $percentage;

    /**
     * @var TaxiProviderAuth[]
     *
     * @ORM\OneToMany(targetEntity="App\Api\Entity\TaxiProviderAuth", mappedBy="taxiProvider", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $authorizers;

    /**
     * @Groups({"api"})
     * @SWG\Property(description="nullable")
     *
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TaxiProviderAdditionalData", mappedBy="taxiProvider", fetch="EAGER", cascade={"persist", "remove"})
     *
     * @var TaxiProviderAdditionalData
     */
    private $additionalData;

    /**
     * @ORM\Column(type="tinyint", nullable=true, options={"unsigned"=true})
     * @Assert\Range(min=0, max=255)
     *
     * @var int|null
     */
    private $customSortWeight;

    public function __construct()
    {
        $this->authorizers = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return TaxiPlatform
     */
    public function getTaxiPlatform(): ?TaxiPlatform
    {
        if (is_numeric($this->taxiPlatform)) {
            return new TaxiPlatform((int) $this->taxiPlatform);
        }

        return $this->taxiPlatform;
    }

    public function setTaxiPlatform(int $taxiPlatform)
    {
        $this->taxiPlatform = new TaxiPlatform($taxiPlatform);
    }

    public function getRating(): ?TaxiProviderRating
    {
        return $this->rating;
    }

    public function setRating(TaxiProviderRating $taxiProviderRating): self
    {
        $this->rating = $taxiProviderRating;

        return $this;
    }

    public function getIconFile(): ?UploadedFile
    {
        return $this->iconFile;
    }

    public function setIconFile(?UploadedFile $iconFile)
    {
        $this->iconFile = $iconFile;
    }

    public function upload($basePath)
    {
        $iconFile = $this->getIconFile();
        if (null === $iconFile) {
            return;
        }

        $fileName = md5(uniqid()).'.'.$iconFile->guessExtension();
        $iconFile->move($basePath.self::PATH_TO_IMAGE, $fileName);

        $this->icon = $fileName; //$this->getIconFile()->getClientOriginalName();
        $this->setIconFile(null);
    }

    /**
     * @Groups({"api"})
     */
    public function getIconUrl(): string
    {
        return self::WEB_PATH_TO_IMAGE.$this->icon;
    }

    /**
     * @return string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(?int $percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return ArrayCollection|TaxiProviderAuth[]
     */
    public function getAuthorizers()
    {
        return $this->authorizers;
    }

    public function getAuthorizer(): TaxiProviderAuthInterface
    {
        $activeAuthorizerCriteria = TaxiProviderAuthRepository::getIsActiveCriteria(true);
        $activeAuthorizer = $this->authorizers->matching($activeAuthorizerCriteria)->current();

        if (!$activeAuthorizer) {
            throw new WrongAuthorizationArgumentException(sprintf('Taxi provider "%s" has no active authorizers', $this->getName()));
        }

        return $activeAuthorizer;
    }

    /**
     * @return $this
     */
    public function addAuthorizer(TaxiProviderAuth $taxiProviderAuth)
    {
        if (!$this->authorizers->contains($taxiProviderAuth)) {
            $this->authorizers->add($taxiProviderAuth);
            $taxiProviderAuth->setTaxiProvider($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAuthorizer(TaxiProviderAuth $taxiProviderAuth)
    {
        if ($this->authorizers->contains($taxiProviderAuth)) {
            $this->authorizers->removeElement($taxiProviderAuth);
            if ($taxiProviderAuth->getTaxiProvider() === $this) {
                $taxiProviderAuth->setTaxiProvider(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name ?: '';
    }

    public function getAdditionalData(): ?TaxiProviderAdditionalData
    {
        return $this->additionalData;
    }

    /**
     * @return TaxiProvider
     */
    public function setAdditionalData(TaxiProviderAdditionalData $additionalData): self
    {
        $this->additionalData = $additionalData;

        // set the owning side of the relation if necessary
        if ($this !== $additionalData->getTaxiProvider()) {
            $additionalData->setTaxiProvider($this);
        }

        return $this;
    }

    public function getCustomSortWeight(): ?int
    {
        return $this->customSortWeight;
    }

    public function setCustomSortWeight(?int $customSortWeight): self
    {
        $this->customSortWeight = $customSortWeight;

        return $this;
    }
}
