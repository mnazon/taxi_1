<?php

namespace App\Api\Entity;

use App\Api\DTO\AppVersionDto;
use App\Api\Enum\DevicePlatform;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserAppVersionRepository")
 * @ORM\Table(name="userAppVersion")
 * @ORM\HasLifecycleCallbacks
 */
class UserAppVersion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="tinyint", options={"unsigned"=true})
     */
    private $platform;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $appVersion;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct(User $user, DevicePlatform $devicePlatform, int $appVersion)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->onPreUpdate();
        $this->user = $user;
        $this->platform = $devicePlatform;
        $this->appVersion = $appVersion;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getPlatform(): DevicePlatform
    {
        if (!($this->platform instanceof DevicePlatform)) {
            $this->platform = new DevicePlatform((int) $this->platform);
        }

        return $this->platform;
    }

    public function setPlatform(DevicePlatform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getAppVersion(): ?int
    {
        return $this->appVersion;
    }

    public function setAppVersion(int $appVersion): self
    {
        $this->appVersion = $appVersion;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function equals(AppVersionDto $appVersion): bool
    {
        $platform = $appVersion->getPlatform();
        $isNewRecord = null === $this->id;

        return !$isNewRecord
            && $platform->equals($this->getPlatform())
            && $this->getAppVersion() === $appVersion->getAppVersion();
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
