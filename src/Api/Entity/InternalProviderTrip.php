<?php

namespace App\Api\Entity;

use App\Api\DTO\Vehicle;
use App\Api\Enum\TripStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\InternalProviderTripRepository")
 * @ORM\Table(name="internalProviderTrip")
 */
class InternalProviderTrip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $cost;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $additionalCost;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $currency;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $vehicle;

    /**
     * @ORM\Column(type="tinyint")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @var \DateTimeImmutable|null
     */
    private $arriveAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        if (\is_array($this->vehicle)) {
            if (!empty($this->vehicle)) {
                $this->vehicle = new Vehicle(
                    $this->vehicle['numberPlate'],
                    $this->vehicle['makeAndModel'],
                    $this->vehicle['color']
                );
            } else {
                $this->vehicle = null;
            }
        }

        return $this->vehicle;
    }

    public function setVehicle(array $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function setVehicleAsString(string $vehicle): self
    {
        $this->vehicle = \GuzzleHttp\json_decode($vehicle);

        return $this;
    }

    public function getVehicleAsString(): ?string
    {
        return $this->vehicle ? \GuzzleHttp\json_encode($this->vehicle, JSON_UNESCAPED_UNICODE) : null;
    }

    public function getStatus(): TripStatus
    {
        return new TripStatus((int) $this->status);
    }

    public function setStatus(TripStatus $status): self
    {
        $this->status = $status->getValue();

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getAdditionalCost(): ?float
    {
        return $this->additionalCost;
    }

    public function setAdditionalCost(?float $additionalCost): self
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    public function getArriveAt(): ?\DateTimeImmutable
    {
        return $this->arriveAt;
    }

    public function setArriveAt(\DateTimeImmutable $arriveAt): self
    {
        $this->arriveAt = $arriveAt;

        return $this;
    }
}
