<?php

namespace App\Api\Entity;

use App\Api\DTO\Geo\Coordinates;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserTopPlaceRepository")
 * @ORM\Table(name="userTopPlace")
 */
class UserTopPlace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Place", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $place;

    /**
     * @ORM\Column(type="smallint")
     */
    private $tripCount = 0;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lng;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getTripCount(): ?int
    {
        return $this->tripCount;
    }

    public function incrementTripCount()
    {
        ++$this->tripCount;

        return $this;
    }

    public function setTripCount(int $tripCount): self
    {
        $this->tripCount = $tripCount;

        return $this;
    }

    public function getLat(): float
    {
        return $this->lat ?: $this->getPlace()->getLat();
    }

    public function getLng(): float
    {
        return $this->lng ?: $this->getPlace()->getLng();
    }

    public function getCoordinates(): Coordinates
    {
        return new Coordinates($this->getLat(), $this->getLng());
    }

    public function setCoordinates(Coordinates $coordinates): self
    {
        $this->lat = $coordinates->getLat();
        $this->lng = $coordinates->getLng();

        return $this;
    }
}
