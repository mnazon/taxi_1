<?php

namespace App\Api\Entity;

use App\Api\Exception\WrongAuthorizationArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @SWG\Definition(description="Taxi provider auth")
 * @ORM\Entity(repositoryClass="App\Api\Repository\TaxiProviderAuthRepository")
 * @ORM\Table(name="taxiProviderAuth")
 */
class TaxiProviderAuth implements TaxiProviderAuthInterface
{
    /**
     * @Groups({"api"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=false)
     * @SWG\Property(type="string")
     */
    private $auth;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\TaxiProvider", inversedBy="authorizers", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $taxiProvider;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->isActive = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getAuth(): array
    {
        return $this->auth;
    }

    /**
     * @throws \InvalidArgumentException when not setup taxiColumnId
     */
    public function getTaxiColumnId(): string
    {
        $authData = $this->getAuth();
        if (!isset($authData['taxiColumnId'])) {
            throw new WrongAuthorizationArgumentException('wrong taxiColumnId');
        }

        return $authData['taxiColumnId'];
    }

    public function getBaseUrl(): string
    {
        $authData = $this->getAuth();
        if (!isset($authData['baseUrl']) || empty($authData['baseUrl'])) {
            throw new WrongAuthorizationArgumentException('wrong baseUrl');
        }

        return $authData['baseUrl'];
    }

    public function buildAuthHeader(): array
    {
        $authData = $this->getAuth();
        if (!isset($authData['username']) || empty($authData['username'])) {
            throw new WrongAuthorizationArgumentException('wrong username');
        }

        if (!isset($authData['password']) || empty($authData['password'])) {
            throw new WrongAuthorizationArgumentException('wrong password');
        }

        $header = sprintf(
            'Basic %s',
            base64_encode(sprintf('%s:%s', $authData['username'], $authData['password']))
        );

        return ['Authorization' => $header];
    }

    public function getKeyPb(): ?string
    {
        $authData = $this->getAuth();

        return $authData['keyPb'] ?? null;
    }

    public function setAuth($auth = null): self
    {
        $this->auth = $auth;

        return $this;
    }

    public function setAuthAsString($auth)
    {
        $this->auth = \GuzzleHttp\json_decode($auth);
    }

    public function getAuthAsString()
    {
        return \GuzzleHttp\json_encode($this->auth);
    }

    public function getTaxiProvider(): TaxiProvider
    {
        return $this->taxiProvider;
    }

    public function setTaxiProvider(TaxiProvider $taxiProvider): self
    {
        $this->taxiProvider = $taxiProvider;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }
}
