<?php

namespace App\Api\Entity;

use App\Api\Enum\TripStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripStatusTrackingRepository")
 * @ORM\Table(name="tripStatusTracking", indexes={
 *     @ORM\Index(name="lockId", columns={"lockId"}, options={"lengths": {8}}),
 *     @ORM\Index(name="retryAt", columns={"retryAt"}),
 *     @ORM\Index(name="status", columns={"status"}),
 * })
 */
class TripStatusTracking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\Trip")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trip;

    /**
     * @ORM\Column(type="tinyint")
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $statusUpdatedAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $retryAt;

    /**
     * @ORM\Column(type="string", length=32, nullable=true, options={"fixed"=true})
     */
    private $lockId;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}))
     */
    private $isSentTripStateChanged = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrip(): ?Trip
    {
        return $this->trip;
    }

    public function setTrip(Trip $trip): self
    {
        $this->trip = $trip;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(TripStatus $status): self
    {
        $this->status = $status->getValue();
        $this->setStatusUpdatedAt(new \DateTimeImmutable());

        return $this;
    }

    public function getStatusUpdatedAt(): ?\DateTimeInterface
    {
        return $this->statusUpdatedAt;
    }

    public function setStatusUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->statusUpdatedAt = $updatedAt;

        return $this;
    }

    public function getRetryAt(): \DateTimeImmutable
    {
        return $this->retryAt;
    }

    /**
     * @return TripStatusTracking
     */
    public function setRetryAt(\DateTimeImmutable $retryAt): self
    {
        $this->retryAt = $retryAt;

        return $this;
    }

    public function getLockId(): ?string
    {
        return $this->lockId;
    }

    public function isSentTripStateChanged(): ?bool
    {
        return $this->isSentTripStateChanged;
    }

    public function setIsSentTripStateChanged(bool $isSentTripStateChanged): self
    {
        $this->isSentTripStateChanged = $isSentTripStateChanged;

        return $this;
    }
}
