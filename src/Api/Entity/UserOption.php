<?php

namespace App\Api\Entity;

use App\Api\Enum\UserOptionEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserOptionRepository")
 * @ORM\Table(name="userOption")
 */
class UserOption
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="tinyint")
     */
    private $userOption;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "1"})
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct(User $user, UserOptionEnum $userOption, string $value)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->user = $user;
        $this->userOption = (int) $userOption->getValue();
        $this->value = $value;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserOption(): UserOptionEnum
    {
        return new UserOptionEnum((int) $this->userOption);
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
}
