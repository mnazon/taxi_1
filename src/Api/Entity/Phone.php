<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use libphonenumber\PhoneNumber;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\PhoneRepository")
 * @ORM\Table(name="phone", indexes={@Index(name="phone_index", columns={"value"})})
 */
class Phone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="phone_number", length=20)
     */
    private $value;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $confirmedAt;
    /**
     * @ORM\OneToMany(targetEntity="App\Api\Entity\PhoneConfirmation", mappedBy="phone", cascade={"persist"})
     * @Assert\Valid()
     */
    private $phoneConfirmations;
    /**
     * @ORM\Column(type="tinyint", options={"default" : 0})
     */
    private $confirmationAttemptNumber = 0;
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $confirmationBlockedUntil;

    public function __construct(
        PhoneNumber $phone,
        \DateTimeInterface $createdAt
    ) {
        $this->value = $phone;
        $this->createdAt = $createdAt;
        $this->phoneConfirmations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?PhoneNumber
    {
        return $this->value;
    }

    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getConfirmedAt(): ?\DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function setConfirmedAt(?\DateTimeInterface $verifiedAt): self
    {
        $this->resetConfirmationTimer();
        $this->confirmedAt = $verifiedAt;

        return $this;
    }

    /**
     * @return Collection|PhoneConfirmation[]
     */
    public function getPhoneConfirmations(): Collection
    {
        return $this->phoneConfirmations;
    }

    public function addPhoneConfirmation(PhoneConfirmation $phoneConfirmation): self
    {
        if (!$this->phoneConfirmations->contains($phoneConfirmation)) {
            $this->phoneConfirmations[] = $phoneConfirmation;
            $phoneConfirmation->setPhone($this);
        }

        return $this;
    }

    public function removePhoneConfirmation(PhoneConfirmation $phoneConfirmation): self
    {
        if ($this->phoneConfirmations->contains($phoneConfirmation)) {
            $this->phoneConfirmations->removeElement($phoneConfirmation);
            // set the owning side to null (unless already changed)
            if ($phoneConfirmation->getPhone() === $this) {
                $phoneConfirmation->setPhone(null);
            }
        }

        return $this;
    }

    public function getConfirmationAttemptNumber()
    {
        return $this->confirmationAttemptNumber;
    }

    public function setConfirmationAttemptNumber($confirmationAttemptNumber): self
    {
        $this->confirmationAttemptNumber = $confirmationAttemptNumber;

        return $this;
    }

    public function getConfirmationBlockedUntil(): ?\DateTimeInterface
    {
        return $this->confirmationBlockedUntil;
    }

    public function setConfirmationBlockedUntil(?\DateTimeInterface $confirmationBlockedUntil): self
    {
        $this->confirmationBlockedUntil = $confirmationBlockedUntil;

        return $this;
    }

    public function resetConfirmationTimer()
    {
        $this->confirmationAttemptNumber = 0;
        $this->confirmationBlockedUntil = null;
    }
}
