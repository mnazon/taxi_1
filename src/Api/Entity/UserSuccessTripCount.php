<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserSuccessTripCountRepository")
 * @ORM\Table(name="userSuccessTripCount")
 */
class UserSuccessTripCount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="smallint")
     */
    private $count;

    public function __construct(User $user, int $successTripCount = 0)
    {
        $this->user = $user;
        $this->count = $successTripCount;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function incrementCount()
    {
        ++$this->count;

        return $this;
    }
}
