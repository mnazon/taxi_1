<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripCarsRepository")
 * @ORM\Table(name="tripCars", indexes={@ORM\Index(name="created_at_index", columns={"createdAt"})})
 */
class TripCars
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Trip")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Trip
     */
    private $trip;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $radius;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $amountFree;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $amountBusy;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $amountClosed;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable
     */
    private $createdAt;

    public function __construct(
        Trip $trip,
        int $radius,
        int $amountFree = 0,
        int $amountBusy = 0,
        int $amountClosed = 0
    ) {
        $this->trip = $trip;
        $this->radius = $radius;
        $this->amountFree = $amountFree;
        $this->amountBusy = $amountBusy;
        $this->amountClosed = $amountClosed;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function getRadius(): int
    {
        return $this->radius;
    }

    public function getAmountFree(): int
    {
        return $this->amountFree;
    }

    public function getAmountBusy(): int
    {
        return $this->amountBusy;
    }

    public function getAmountClosed(): int
    {
        return $this->amountClosed;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }
}
