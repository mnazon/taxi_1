<?php

namespace App\Api\Entity;

interface TaxiProviderAuthInterface
{
    public function getAuth(): array;

    public function getBaseUrl(): string;

    public function getTaxiColumnId();

    public function getKeyPb();

    public function isActive(): bool;

    public function setIsActive(bool $isActive);

    public function buildAuthHeader(): array;

    public function getTaxiProvider(): TaxiProvider;
}
