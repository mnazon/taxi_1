<?php

namespace App\Api\Entity;

use App\Api\DTO\Geo\Coordinates;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripRequestToPlaceRepository")
 * @ORM\Table(name="tripRequestToPlace")
 */
class TripRequestToPlace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\TripRequest", inversedBy="route")
     * @ORM\JoinColumn(name="triprequest_id", referencedColumnName="id", nullable=false)
     */
    private $tripRequest;
    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private $place;
    /**
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lat;
    /**
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=true)
     */
    private $lng;
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $routePointNumber;

    public function __construct(
        TripRequest $tripRequest,
        Place $place,
        ?Coordinates $coordinates = null,
        ?int $routePointNumber = null
    ) {
        $this->tripRequest = $tripRequest;
        $this->place = $place;
        $this->routePointNumber = $routePointNumber;
        if ($coordinates) {
            $this->lat = $coordinates->getLat();
            $this->lng = $coordinates->getLng();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTripRequest(): ?TripRequest
    {
        return $this->tripRequest;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function getLat(): float
    {
        return $this->lat ?: $this->getPlace()->getLat();
    }

    public function getLng(): float
    {
        return $this->lng ?: $this->getPlace()->getLng();
    }

    public function getRoutePointNumber(): ?int
    {
        return $this->routePointNumber;
    }

    public function getCoordinates(): Coordinates
    {
        return new Coordinates($this->getLat(), $this->getLng());
    }
}
