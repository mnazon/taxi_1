<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TripVoteRepository")
 * @ORM\Table(name="tripVote")
 */
class TripVote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="tinyint")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voter;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\Trip")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trip;
    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\TaxiProvider")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var TaxiProvider
     */
    private $taxiProvider;
    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     *
     * @var string|null
     */
    private $comment;

    public function __construct(
        int $value,
        User $voter,
        Trip $trip,
        \DateTimeImmutable $createdAt,
        string $comment = null
    ) {
        $this->value = $value;
        $this->voter = $voter;
        $this->trip = $trip;
        $this->taxiProvider = $trip->getTaxiProvider();
        $this->createdAt = $createdAt;
        $this->comment = $comment;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getVoter(): User
    {
        return $this->voter;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getTrip(): Trip
    {
        return $this->trip;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
