<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\TaxiProviderRatingRepository")
 * @ORM\Table(name="taxiProviderRating")
 */
class TaxiProviderRating implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rate = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $numVotes = 0;
    /**
     * @ORM\OneToOne(targetEntity="App\Api\Entity\TaxiProvider", inversedBy="rating", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $taxiProvider;

    public function __construct(float $rate, int $numVotes, TaxiProvider $taxiProvider)
    {
        $this->rate = $rate;
        $this->numVotes = $numVotes;
        $this->taxiProvider = $taxiProvider;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getNumVotes(): int
    {
        return $this->numVotes;
    }

    public function addVote(TripVote $tripVote)
    {
        $currentVoteSum = $this->numVotes * $this->rate;

        ++$this->numVotes;
        $this->rate = ($currentVoteSum + $tripVote->getValue()) / $this->numVotes;
    }

    public function getTaxiProvider(): ?TaxiProvider
    {
        return $this->taxiProvider;
    }

    public function jsonSerialize()
    {
        return [
            'rate' => round($this->rate, 1),
            'numVotes' => $this->numVotes,
        ];
    }
}
