<?php

namespace App\Api\Entity;

use App\Api\Enum\DevicePlatform;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserLoginStatisticRepository")
 * @ORM\Table(name="userLoginStatistic")
 */
class UserLoginStatistic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="tinyint", length=1, nullable=false)
     */
    private $platform;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct(User $user, ?DevicePlatform $devicePlatform)
    {
        $this->user = $user;
        $this->platform = $devicePlatform;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getPlatform(): ?DevicePlatform
    {
        if (null !== $this->platform && !($this->platform instanceof DevicePlatform)) {
            $this->platform = new DevicePlatform((int) $this->platform);
        }

        return $this->platform;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
}
