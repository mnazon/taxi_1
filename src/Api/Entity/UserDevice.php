<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\UserDeviceRepository")
 * @ORM\Table(name="userDevice", indexes={@Index(name="deviceToken", columns={"deviceToken"})})
 */
class UserDevice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $deviceToken;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firebaseRegistrationToken;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeviceToken(): ?string
    {
        return $this->deviceToken;
    }

    public function setDeviceToken(string $deviceId): self
    {
        $this->deviceToken = $deviceId;

        return $this;
    }

    public function getFirebaseRegistrationToken(): ?string
    {
        return $this->firebaseRegistrationToken;
    }

    public function setFirebaseRegistrationToken(string $firebaseRegistrationToken): self
    {
        $this->firebaseRegistrationToken = $firebaseRegistrationToken;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
