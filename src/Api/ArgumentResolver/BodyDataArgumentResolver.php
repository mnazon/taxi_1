<?php

declare(strict_types=1);

namespace App\Api\ArgumentResolver;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\AuthUserAwareInterface;
use App\Api\DTO\IpAwareInterface;
use App\Api\Entity\User;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\ConstraintViolationListException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyDataArgumentResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Serializer
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TokenStorageInterface $tokenStorage
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->tokenStorage = $tokenStorage;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return \in_array($request->getMethod(), ['POST', 'PUT'])
            //&& $this->isJson($request->getContent())
            && null !== $argument->getType()
            && class_exists($argument->getType())
            && \in_array(ArgumentResolvableInterface::class, class_implements($argument->getType()), true);
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $data = $request->getContent();
        try {
            $object = $this->serializer->deserialize($data, $argument->getType(), 'json');
        } catch (NotNormalizableValueException $e) {
            $apiErrorCode = ApiErrorCode::BAD_REQUEST_DATA()
                ->setTitle($e->getMessage());
            throw new ApiErrorCodeException($apiErrorCode);
        }

        if ($object instanceof AuthUserAwareInterface) {
            $token = $this->tokenStorage->getToken();
            $user = null === $token ?: $token->getUser();
            if ($user instanceof User) {
                $object->setUser($user);
            }
        }

        if ($object instanceof IpAwareInterface) {
            if ($request->getClientIp()) {
                $object->setIp($request->getClientIp());
            }
        }
        $violations = $this->validator->validate($object);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        yield $object;
    }
}
