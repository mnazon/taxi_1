<?php

namespace App\Api\ArgumentResolver;

use App\Api\DTO\Trip\TripCancelRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OptionalBodyDataArgumentResolver extends BodyDataArgumentResolver
{
    const SUPPORTED_CLASSES = [TripCancelRequest::class];

    /**
     * OptionalBodyDataArgumentResolver constructor.
     */
    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($validator, $serializer, $tokenStorage);
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return  \in_array($argument->getType(), self::SUPPORTED_CLASSES)
           && parent::supports($request, $argument);
    }

    /**
     * @throws \App\Api\Exception\ApiErrorCodeException
     * @throws \App\Api\Exception\ConstraintViolationListException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $data = $request->getContent();
        if ('' === $data) {
            yield null;
        } else {
            yield from parent::resolve($request, $argument);
        }
    }
}
