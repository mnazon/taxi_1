<?php

declare(strict_types=1);

namespace App\Api\ArgumentResolver;

use App\Api\DTO\PageDto;
use App\Api\Exception\ConstraintViolationListException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PageDtoArgumentResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return PageDto::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $pageDto = new PageDto(
            (int) $request->query->get('page', 1),
            (int) $request->query->get('perPage', 20)
        );

        $violations = $this->validator->validate($pageDto);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        yield $pageDto;
    }
}
