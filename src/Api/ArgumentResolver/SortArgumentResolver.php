<?php

declare(strict_types=1);

namespace App\Api\ArgumentResolver;

use App\Api\DTO\Trip\EstimateSort;
use App\Api\Enum\EstimateSortField;
use App\Api\Enum\SortDirection;
use App\Api\Exception\ConstraintViolationListException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SortArgumentResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return EstimateSort::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $sortData = $request->query->get('sort', []);
        $sort = new EstimateSort(
            $sortData['field'] ?? EstimateSortField::SMART,
            $sortData['direction'] ?? SortDirection::DESC
        );
        $violations = $this->validator->validate($sort);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        yield $sort;
    }
}
