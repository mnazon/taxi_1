<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Entity\NotificationToken;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalStatusChanged;
use Kreait\Firebase;
use Kreait\Firebase\Messaging\MessageTarget;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CarSearchingPushNotifier implements EventSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private const CLICK_ACTION_OPEN_FIND_CAR_SCREEN = 'OPEN_FIND_CAR_SCREEN';

    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var Firebase\Messaging
     */
    private $firebaseMessaging;

    public function __construct(Firebase\Messaging $firebaseMessaging, TranslatorInterface $translator)
    {
        $this->logger = new NullLogger();
        $this->translator = $translator;
        $this->firebaseMessaging = $firebaseMessaging;
    }

    public static function getSubscribedEvents()
    {
        return [
            TripExternalStatusChanged::class => 'sendPushNotification',
        ];
    }

    public function sendPushNotification(TripExternalStatusChanged $tripStatusChanged)
    {
        $trip = $tripStatusChanged->getTrip();
        $notificationToken = $trip->getNotificationToken();
        if (null === $notificationToken) {
            $this->logger->info('No notificationToken for trip '.$trip->getId());

            return;
        }

        $message = $this->getFilledMessage($tripStatusChanged, $notificationToken);
        if (null === $message) {
            return;
        }

        $logData = [
            'tripId' => $trip->getId(),
            'fromStatus' => $tripStatusChanged->getFromStatus()->getValue(),
            'toStatus' => $tripStatusChanged->getToStatus()->getValue(),
            'userId' => $trip->getUser()->getId(),
            'message' => json_encode($message),
        ];
        try {
            $start = microtime(true);
            $response = $this->firebaseMessaging->send($message);
            $logData['response'] = $response;
            $logData['time'] = microtime(true) - $start;
            $this->logger->info('Car searching push notifier', $logData);
        } catch (\Throwable $e) {
            $logData['exception'] = $e;
            $this->logger->error('Error while sending notification to firebase', $logData);
        }
    }

    private function getFilledMessage(TripExternalStatusChanged $tripStatusChanged, NotificationToken $notificationToken): ?Firebase\Messaging\CloudMessage
    {
        $trip = $tripStatusChanged->getTrip();
        $vehicle = $trip->getVehicle();
        $fromStatus = $tripStatusChanged->getFromStatus()->getValue();
        $toStatus = $tripStatusChanged->getToStatus()->getValue();
        $bodyParams = [
            '%taxiProvider%' => $trip->getTaxiProvider()->getName(),
        ];
        if ($vehicle) {
            $bodyParams['%color%'] = $vehicle->getColor();
            $bodyParams['%make_and_model%'] = $vehicle->getMakeAndModel();
            $bodyParams['%number_plate%'] = $vehicle->getNumberPlate();
        }

        $token = $notificationToken->getFcmToken();
        $message = Firebase\Messaging\CloudMessage::withTarget(MessageTarget::TOKEN, $token);
        $apnsConfig = $androidConfig = $notificationData = [];

        $this->addSoundAndVibroSettings($androidConfig, $apnsConfig);

        $data = [
            'tripId' => (string) $trip->getId(),
            'status' => (string) $trip->getStatus()->getValue(),
        ];

        if (TripStatus::CANCELED === $toStatus) {
            $androidConfig['notification']['click_action'] = self::CLICK_ACTION_OPEN_FIND_CAR_SCREEN;
        }
        if (TripStatus::CAR_FOUND === $toStatus) {
            $bodyParams['%time%'] = $trip->getArriveAt()->format('H:i');
            $title = $this->translator->trans('car_found');
            $body = $this->translator->trans('car_will_at_%time%_%color%_%make_and_model%_%number_plate%', $bodyParams);
        } elseif (TripStatus::CAR_FOUND === $fromStatus) {
            if (TripStatus::CANCELED === $toStatus) {
                $title = $this->translator->trans('canceled_by_driver');
                $body = $this->translator->trans('driver_cannot_arrive_%make_and_model%_%number_plate%', $bodyParams);
            } elseif (TripStatus::SEARCH_FOR_CAR === $toStatus) {
                $title = $this->translator->trans('canceled_by_driver_search_new_car');
                $body = $this->translator->trans('driver_cannot_arrive_search_new_car_%make_and_model%_%number_plate%', $bodyParams);
            }
        } elseif (TripStatus::CANCELED === $toStatus) {
            $title = $this->translator->trans('car_not_found');
            $body = $this->translator->trans('car_not_with_%taxiProvider%', $bodyParams);
        }

        if (!isset($title) || !isset($body)) {
            return null;
        }

        $notificationData['title'] = $title;
        $notificationData['body'] = $body;
        $message = $message->withNotification($notificationData);
        if (!empty($data)) {
            $message = $message->withData($data);
        }
        if (!empty($apnsConfig)) {
            $message = $message->withApnsConfig($apnsConfig);
        }
        if (!empty($androidConfig)) {
            $message = $message->withAndroidConfig($androidConfig);
        }

        return $message;
    }

    /**
     * @return array
     */
    private function addSoundAndVibroSettings(array &$androidConfig, array &$apnsConfig): void
    {
        $androidConfig['notification']['sound'] = 'default';
        $androidConfig['notification']['default_vibrate_timings'] = true;
        $apnsConfig['payload']['aps']['sound'] = 'default';
    }
}
