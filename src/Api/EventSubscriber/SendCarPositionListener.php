<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Event\ExternalTripDataRetrieved;
use Centrifugo\Centrifugo;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SendCarPositionListener implements EventSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Centrifugo
     */
    private $centrifugo;

    public function __construct(Centrifugo $centrifugo)
    {
        $this->centrifugo = $centrifugo;
        $this->logger = new NullLogger();
    }

    public static function getSubscribedEvents()
    {
        return [
            ExternalTripDataRetrieved::class => 'sendCarPositionToClient',
        ];
    }

    public function sendCarPositionToClient(ExternalTripDataRetrieved $externalTripDataRetrieved)
    {
        $trip = $externalTripDataRetrieved->getTrip();
        $externalTripData = $externalTripDataRetrieved->getExternalTripResponse();
        if ($externalTripData->getPosition()) {
            $data = [
                'tripId' => $trip->getId(),
                'location' => $externalTripData->getPosition()->jsonSerialize(),
            ];
            $chanelName = 'trip.car_location_'.$trip->getId();

            //todo - need to use redis client
            try {
                $reponse = $this->centrifugo->publish($chanelName, $data);
            } catch (\Throwable $e) {
                $data['exception'] = $e;
                $this->logger->error('Car position publish failed', $data);
            }
        }
    }
}
