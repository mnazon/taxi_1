<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\DTO\AppVersionDto;
use App\Api\Entity\User;
use App\Api\Entity\UserAppVersion;
use App\Api\Repository\UserAppVersionRepository;
use App\Api\Utils\AppVersionDetector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\PersistingStoreInterface;
use Symfony\Component\Security\Core\Security;

class UserAppVersionCollectorListener implements EventSubscriberInterface
{
    /**
     * @var UserAppVersionRepository
     */
    private $userAppVersionRepo;

    /**
     * @var LockFactory
     */
    private $lockFactory;
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security,
        UserAppVersionRepository $userAppVersionRepo,
        PersistingStoreInterface $redisStore
    ) {
        $this->userAppVersionRepo = $userAppVersionRepo;
        $this->lockFactory = new LockFactory($redisStore);
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => 'handle',
        ];
    }

    public function handle(RequestEvent $arg)
    {
        $request = $arg->getRequest();
        $user = $this->security->getUser();
        if (!$user instanceof User) {
            return;
        }

        $appVersion = (new AppVersionDetector())->detectByRequest($request);
        if ($appVersion) {
            $lockKey = sprintf('user-app-version-%s', $user->getId());
            $lock = $this->lockFactory->createLock($lockKey, 1);
            try {
                if ($lock->acquire(false)) {
                    $this->checkUserAppVersion($user, $appVersion);
                }
            } finally {
                $lock->release();
            }
        }
    }

    /**
     * @param $user
     */
    private function checkUserAppVersion($user, AppVersionDto $appVersion): void
    {
        $userAppVersion = $this->userAppVersionRepo->findOneByUser($user);
        if (!$userAppVersion) {
            $userAppVersion = new UserAppVersion($user, $appVersion->getPlatform(), $appVersion->getAppVersion());
        }

        if (!$userAppVersion->equals($appVersion)) {
            $userAppVersion
                ->setAppVersion($appVersion->getAppVersion())
                ->setPlatform($appVersion->getPlatform());
            $this->userAppVersionRepo->save($userAppVersion);
        }
    }
}
