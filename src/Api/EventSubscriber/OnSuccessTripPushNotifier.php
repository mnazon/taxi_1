<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\DTO\Firebase\MessageDto;
use App\Api\Entity\Notification;
use App\Api\Entity\NotificationToken;
use App\Api\Entity\Trip;
use App\Api\Entity\User;
use App\Api\Entity\UserOption;
use App\Api\Enum\MobileApplicationFeature;
use App\Api\Enum\UserOptionEnum;
use App\Api\Event\SuccessTripCountInitialized;
use App\Api\Repository\NotificationRepository;
use App\Api\Repository\UserOptionRepository;
use App\Api\Service\FeatureSupportChecker;
use App\Api\Service\FirebaseMessenger;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OnSuccessTripPushNotifier implements EventSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private const MIN_SUCCESS_TRIPS_FOR_PUSH_SENDING = 3;
    private const CLICK_ACTION_RATE_APP = 'ACTION_RATE_APP';

    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var FirebaseMessenger
     */
    private $firebaseMessenger;
    /**
     * @var UserOptionRepository
     */
    private $userOptionRepository;
    /**
     * @var FeatureSupportChecker
     */
    private $featureSupportChecker;
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    public function __construct(
        UserOptionRepository $userOptionRepository,
        FirebaseMessenger $firebaseMessenger,
        TranslatorInterface $translator,
        FeatureSupportChecker $featureSupportChecker,
        NotificationRepository $notificationRepository
    ) {
        $this->userOptionRepository = $userOptionRepository;
        $this->firebaseMessenger = $firebaseMessenger;
        $this->translator = $translator;
        $this->logger = new NullLogger();
        $this->featureSupportChecker = $featureSupportChecker;
        $this->notificationRepository = $notificationRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            SuccessTripCountInitialized::class => 'handle',
        ];
    }

    public function handle(SuccessTripCountInitialized $countCreatedOrUpdated)
    {
        $successTripCount = $countCreatedOrUpdated->getSuccessTripCount();
        $trip = $countCreatedOrUpdated->getTrip();
        $notificationToken = $trip->getNotificationToken();
        if (null === $notificationToken) {
            return;
        }

        if (self::MIN_SUCCESS_TRIPS_FOR_PUSH_SENDING <= $successTripCount->getCount()) {
            $user = $trip->getUser();
            $this->checkAndCreateAppRateViewOptionForUser($user);

            $messageHasBeenSent = $this->checkAndSendAppRatePushMessage($notificationToken, $user);
            if ($messageHasBeenSent) {
                return; //do not send other messages
            }
        }
        $this->saveNotificationEntity($trip);
        $this->sendTripVotePushMessage($notificationToken, $trip);
    }

    private function checkAndCreateAppRateViewOptionForUser(User $user): void
    {
        $rateViewFeature = MobileApplicationFeature::APP_RATE_VIEW();
        if ($this->featureSupportChecker->isSupportedByUser($rateViewFeature, $user)) {
            $option = UserOptionEnum::APP_RATE_VIEW();
            $userOption = $this->userOptionRepository->findOneByUserAndOption($user, $option);
            if (!$userOption) {
                $this->saveUserOption($user, $option);
            }
        }
    }

    private function saveUserOption(User $user, UserOptionEnum $option)
    {
        $userOption = new UserOption($user, $option, '1');
        $this->userOptionRepository->save($userOption);
    }

    private function checkAndSendAppRatePushMessage(NotificationToken $notificationToken, User $user): bool
    {
        $messageHasBeenSent = false;
        $pushFeature = MobileApplicationFeature::APP_RATE_PUSH_NOTIFICATION();
        if ($this->featureSupportChecker->isSupportedByUser($pushFeature, $user)) {
            $option = UserOptionEnum::APP_RATE_PUSH_NOTIFICATION();
            $userOption = $this->userOptionRepository->findOneByUserAndOption($user, $option);
            if (!$userOption) {
                $androidConfig['notification']['click_action'] = self::CLICK_ACTION_RATE_APP;
                $androidConfig['notification']['sound'] = 'default';
                $androidConfig['notification']['default_vibrate_timings'] = true;
                $apnsConfig['payload']['aps']['sound'] = 'default';
                $title = $this->translator->trans('like_to_use_app');
                $body = $this->translator->trans('write_vote_for_app');

                $message = new MessageDto($notificationToken, $title, $body, [], $androidConfig, $apnsConfig);
                $messageHasBeenSent = $this->sendVoteMessage($message, 'app');
                if ($messageHasBeenSent) {
                    $this->saveUserOption($user, $option);
                }
            }
        }

        return $messageHasBeenSent;
    }

    private function sendVoteMessage(MessageDto $message, string $voteFor): bool
    {
        try {
            $start = microtime(true);
            $response = $this->firebaseMessenger->send($message);
            $logData = [
                'time' => microtime(true) - $start,
                'response' => $response,
            ];
            $this->logger->debug(sprintf('Vote for %s push', $voteFor), $logData);

            return true;
        } catch (\Throwable $e) {
            $this->logger->error('Error while sending notification to firebase', ['exception' => $e]);

            return false;
        }
    }

    private function sendTripVotePushMessage(NotificationToken $notificationToken, Trip $trip): bool
    {
        $title = $this->translator->trans('write_vote_for_trip');
        $data = [
            'tripId' => (string) $trip->getId(),
        ];
        $message = new MessageDto($notificationToken, $title, null, $data);

        return $this->sendVoteMessage($message, 'trip');
    }

    private function saveNotificationEntity(Trip $trip): void
    {
        $vote = $this->notificationRepository->findByTrip($trip);
        if (!$vote) {
            $vote = new Notification();
            $vote->setTrip($trip)
                ->setUser($trip->getUser());
            $this->notificationRepository->save($vote);
        }
    }
}
