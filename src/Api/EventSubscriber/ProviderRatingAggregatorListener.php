<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Event\VoteAdded;
use App\Api\Repository\TaxiProviderRatingRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProviderRatingAggregatorListener implements EventSubscriberInterface
{
    /**
     * @var TaxiProviderRatingRepository
     */
    private $taxiProviderRatingRepository;

    public function __construct(
        TaxiProviderRatingRepository $taxiProviderRatingRepository
    ) {
        $this->taxiProviderRatingRepository = $taxiProviderRatingRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            VoteAdded::class => 'handle',
        ];
    }

    public function handle(VoteAdded $voteAdded)
    {
        $tripVote = $voteAdded->getTripVote();
        $providerId = $tripVote->getTrip()->getTaxiProvider()->getId();
        $taxiProviderRating = $this->taxiProviderRatingRepository->getByProviderId($providerId);
        $taxiProviderRating->addVote($tripVote);
        $this->taxiProviderRatingRepository->save($taxiProviderRating);
    }
}
