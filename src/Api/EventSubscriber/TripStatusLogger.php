<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Entity\TripStatusLog;
use App\Api\Event\TripCreated;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\Event\TripInternalStatusChanged;
use App\Api\Event\TripStatusChangedInterface;
use App\Api\Repository\TripStatusLogRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TripStatusLogger implements EventSubscriberInterface
{
    /**
     * @var TripStatusLogRepository
     */
    private $tripStatusLogRepository;

    public function __construct(TripStatusLogRepository $tripStatusLogRepository)
    {
        $this->tripStatusLogRepository = $tripStatusLogRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            TripExternalStatusChanged::class => 'handle',
            TripInternalStatusChanged::class => 'handle',
            TripCreated::class => 'handle',
        ];
    }

    public function handle(TripStatusChangedInterface $event)
    {
        if (!$event instanceof TripStatusChangedInterface) {
            throw new \InvalidArgumentException('Wrong event instance');
        }

        $trip = $event->getTrip();
        $tripStatusLog = new TripStatusLog();
        $tripStatusLog->setTrip($trip);
        $tripStatusLog->setStatus($event->getToStatus()->getValue());
        $tripStatusLog->setCreatedAt(new \DateTimeImmutable());

        $this->tripStatusLogRepository->save($tripStatusLog);
    }
}
