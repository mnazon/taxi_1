<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Entity\UserTopPlace;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\Repository\UserTopPlaceRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserTopPlacesListener implements EventSubscriberInterface
{
    /**
     * @var UserTopPlaceRepository
     */
    private $userTopPlaceRepository;

    public function __construct(UserTopPlaceRepository $userTopPlaceRepository)
    {
        $this->userTopPlaceRepository = $userTopPlaceRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            TripExternalStatusChanged::class => 'onTripExternalStatusChanged',
        ];
    }

    /**
     * @return UserTopPlace[]
     */
    public function onTripExternalStatusChanged(TripExternalStatusChanged $tripStatusChanged): array
    {
        $savedUserTopPlaces = [];
        if (TripStatus::EXECUTED === $tripStatusChanged->getToStatus()->getValue()) {
            $trip = $tripStatusChanged->getTrip();
            $userTopPlaces = [];
            $route = $trip->getRequest()->getRoute();
            foreach ($route as $requestToPlace) {
                $userTopPlaces[] = (new UserTopPlace())
                    ->setUser($trip->getUser())
                    ->setPlace($requestToPlace->getPlace())
                    ->setCoordinates($requestToPlace->getCoordinates());
            }

            if (!empty($userTopPlaces)) {
                $savedUserTopPlaces = $this->userTopPlaceRepository->updateUserTopPlaces(...$userTopPlaces);
            }
        }

        return $savedUserTopPlaces;
    }
}
