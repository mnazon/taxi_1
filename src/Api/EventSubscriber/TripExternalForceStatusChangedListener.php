<?php

namespace App\Api\EventSubscriber;

use App\Api\Event\TripExternalForceStatusChanged;
use App\Api\Repository\TripStatusTrackingRepository;
use App\Api\Service\TripDataSyncManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TripExternalForceStatusChangedListener implements EventSubscriberInterface
{
    /**
     * @var TripDataSyncManager
     */
    private $tripDataSyncManager;
    /**
     * @var TripStatusTrackingRepository
     */
    private $tripStatusTrackingRepository;

    public function __construct(
        TripDataSyncManager $tripDataSyncManager,
        TripStatusTrackingRepository $tripStatusTrackingRepository
    ) {
        $this->tripDataSyncManager = $tripDataSyncManager;
        $this->tripStatusTrackingRepository = $tripStatusTrackingRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            TripExternalForceStatusChanged::class => 'handle',
        ];
    }

    /**
     * @throws \Throwable
     */
    public function handle(TripExternalForceStatusChanged $externalForceStatusChanged)
    {
        // we already locked trip in CreateTripManager, so no need to lock tripstatus
        $trip = $externalForceStatusChanged->getTrip();
        $trackingData = $this->tripStatusTrackingRepository->findByTripId($trip->getId());
        $this->tripDataSyncManager->handle($trackingData);
    }
}
