<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\DTO\Translation\Message;
use App\Api\Enum\ApiErrorCode;
use App\Api\Enum\ApiProblemType;
use App\Api\Exception\ApiProblemExceptionInterface;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\Exception\WrongRequestArgumentException;
use App\Api\HttpDTO\Response\ApiError;
use App\Api\HttpDTO\Response\ApiProblem;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ApiExceptionSubscriber implements EventSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var bool
     */
    private $formatHandledException;

    public function __construct(SerializerInterface $serializer, $formatHandledException)
    {
        $this->serializer = $serializer;
        $this->formatHandledException = $formatHandledException;
        $this->logger = new NullLogger();
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', -100],
        ];
    }

    /**
     * @throws ConstraintViolationListException
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $e = $event->getThrowable();
        if ($e instanceof ConstraintViolationListException) {
            $apiProblem = $e->getConstraintViolationList();
        } elseif ($e instanceof ApiProblemExceptionInterface) {
            $apiProblem = $e->getApiProblem();
        } else {
            $title = 'Internal error';
            if ($e instanceof WrongRequestArgumentException) {
                $title = $e->getMessage();
                $apiError = new ApiError(ApiErrorCode::BAD_REQUEST_DATA, new Message($title), $e->getProperty());
            } elseif ($e instanceof HttpExceptionInterface) {
                $title = $e->getMessage();
                $apiError = new ApiError(ApiErrorCode::BAD_REQUEST_DATA, new Message($title));
            } else {
                $apiError = new ApiError(ApiErrorCode::INTERNAL_ERROR, new Message($title));
            }

            $apiProblem = new ApiProblem(
                ApiProblemType::COMMON,
                $title
            );
            $apiProblem->addError($apiError);
        }

        $statusCode = 500;
        $headers = [];
        $isHandledException = $e instanceof HttpExceptionInterface;
        if ($e instanceof HttpExceptionInterface) {
            $statusCode = $e->getStatusCode();
            $headers = $e->getHeaders();
        }

        if (!$isHandledException) {
            $this->logger->error('Not handled exception', ['exception' => $e]);
        } else {
            $this->logger->info('Handled exception', ['exception' => $e]);
        }

        if (!$this->formatHandledException || !$isHandledException) {
            throw $e;
        }

        $data = $this->serializer->serialize($apiProblem, 'json');
        $response = new JsonResponse(
            $data,
            $statusCode,
            $headers,
            true
        );

        $response->headers->set('Content-Type', 'application/problem+json');
        $event->setResponse($response);
    }
}
