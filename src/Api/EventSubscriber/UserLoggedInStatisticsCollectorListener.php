<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Entity\User;
use App\Api\Entity\UserLoginStatistic;
use App\Api\Enum\DevicePlatform;
use App\Api\Event\UserLoggedIn;
use App\Api\Repository\UserLoginStatisticRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserLoggedInStatisticsCollectorListener implements EventSubscriberInterface
{
    /**
     * @var UserLoginStatisticRepository
     */
    private $loginStatisticRepository;

    public function __construct(UserLoginStatisticRepository $loginStatisticRepository)
    {
        $this->loginStatisticRepository = $loginStatisticRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserLoggedIn::class => 'onUserLoggedIn',
        ];
    }

    public function onUserLoggedIn(UserLoggedIn $loginOrRegisterEvent): void
    {
        $user = $loginOrRegisterEvent->getUser();
        $registrationRequest = $loginOrRegisterEvent->getRegistrationRequest();
        $devicePlatform = $registrationRequest->getPlatform();

        $userLoginStatistic = $this->createUserLoginStatistic($user, $devicePlatform);

        $this->loginStatisticRepository->save($userLoginStatistic);
    }

    private function createUserLoginStatistic(User $user, ?DevicePlatform $devicePlatform): UserLoginStatistic
    {
        return new UserLoginStatistic($user, $devicePlatform);
    }
}
