<?php

declare(strict_types=1);

namespace App\Api\EventSubscriber;

use App\Api\Entity\User;
use App\Api\Entity\UserSuccessTripCount;
use App\Api\Enum\TripStatus;
use App\Api\Event\SuccessTripCountInitialized;
use App\Api\Event\TripExternalStatusChanged;
use App\Api\Repository\TripRepository;
use App\Api\Repository\UserSuccessTripCountRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class SuccessTripCountListener implements EventSubscriberInterface
{
    /**
     * @var UserSuccessTripCountRepository
     */
    private $successTripCountRepository;
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        UserSuccessTripCountRepository $successTripCountRepository,
        TripRepository $tripRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->successTripCountRepository = $successTripCountRepository;
        $this->tripRepository = $tripRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public static function getSubscribedEvents()
    {
        return [
            TripExternalStatusChanged::class => 'onTripExternalStatusChanged',
        ];
    }

    public function onTripExternalStatusChanged(TripExternalStatusChanged $tripStatusChanged)
    {
        $isTripExecuted = TripStatus::EXECUTED === $tripStatusChanged->getToStatus()->getValue();
        if (!$isTripExecuted) {
            return;
        }

        $trip = $tripStatusChanged->getTrip();
        $user = $trip->getUser();
        $successTripCount = $this->initSuccessTripCount($user);
        $this->successTripCountRepository->save($successTripCount);

        $event = new SuccessTripCountInitialized($successTripCount, $trip);
        $this->eventDispatcher->dispatch($event);
    }

    private function initSuccessTripCount(User $user): UserSuccessTripCount
    {
        $successTripCount = $this->successTripCountRepository->findOneByUser($user);
        if (null === $successTripCount) {
            $countSuccessTrip = $this->tripRepository->countByUserAndStatus(
                $user,
                new TripStatus(TripStatus::EXECUTED)
            );
            $successTripCount = new UserSuccessTripCount($user, $countSuccessTrip);
        } else {
            $successTripCount->incrementCount();
        }

        return $successTripCount;
    }
}
