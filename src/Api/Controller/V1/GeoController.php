<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Geo\Coordinates;
use App\Api\DTO\Geo\Place;
use App\Api\DTO\Geo\PlaceData;
use App\Api\DTO\Geo\Search;
use App\Api\Exception\WrongProviderResponse as WrongProviderResponse;
use App\Api\Service\Geo\CompositeGeoProviderDecorator;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GeoController extends AbstractApiController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var CompositeGeoProviderDecorator
     */
    private $geoProvider;

    public function __construct(
        SerializerInterface $serializer,
        CompositeGeoProviderDecorator $geoProvider,
        ValidatorInterface $validator
    ) {
        $this->serializer = $serializer;
        parent::__construct($validator);
        $this->geoProvider = $geoProvider;
    }

    /**
     * Find places and streets.
     *
     * @Route("/places", methods={"GET"})
     * @SWG\Parameter(
     *     name="query",
     *     in="query",
     *     type="string",
     *     required=true,
     * ),
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="number",
     *     required=false,
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns street or place objects",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=AutocompleteData::class, groups={"api"}))
     *     ),
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Response(
     *     response="500",
     *     description="internal server error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     *
     * @SWG\Tag(name="geo")
     * @Security(name="UserAuth")
     *
     * @throws WrongProviderResponse
     * @throws \App\Api\Exception\ConstraintViolationListException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Http\Client\Exception
     */
    public function objectsSearch(Request $request): JsonResponse
    {
        $search = new Search(
            $request->query->get('query', ''),
            (int) $request->query->get('limit', 10)
        );
        $this->assertValidData($search);

        $result = $this->geoProvider->findStreetsAndPlaces($search);

        return $this->serialize($result, 200);
    }

    /**
     * Get place info by id.
     *
     * @Route("/places/{placeId}", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="placeId",
     *     in="path",
     *     type="string",
     *     required=true,
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns place data",
     *     @Model(type=PlaceData::class, groups={"api"})
     * ),
     *
     * @SWG\Tag(name="geo")
     * @Security(name="UserAuth")
     *
     * @throws WrongProviderResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function retrieve(string $placeId): JsonResponse
    {
        $result = $this->geoProvider->geocodingByPlaceId($placeId);

        return $this->serialize($result);
    }

    /**
     * Get place info by coordinates.
     *
     * @Route("/geocoding", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="lat",
     *     in="query",
     *     type="number",
     *     required=true,
     * ),
     * @SWG\Parameter(
     *     name="lng",
     *     in="query",
     *     type="number",
     *     required=true,
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns place data",
     *     @Model(type=PlaceData::class, groups={"api"})
     * ),
     *
     * @SWG\Tag(name="geo")
     * @Security(name="UserAuth")
     *
     * @throws WrongProviderResponse
     * @throws \App\Api\Exception\ConstraintViolationListException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \App\Api\Exception\ApiErrorCodeException
     */
    public function reverseGeocoding(Request $request): JsonResponse
    {
        $coordinates = new Coordinates(
            (float) $request->query->get('lat'),
            (float) $request->query->get('lng')
        );
        $this->assertValidData($coordinates);
        $place = $this->geoProvider->geocodingByCoordinates($coordinates);

        return $this->serialize($place);
    }
}
