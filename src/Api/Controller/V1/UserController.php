<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\Centrifugo\CentrifugoCryptoManager;
use App\Api\DTO\Geo\AutocompleteData;
use App\Api\DTO\Profile\PhoneConfirmationRequest;
use App\Api\DTO\WebSocketSign;
use App\Api\Entity\Notification;
use App\Api\Entity\User;
use App\Api\Entity\UserOption;
use App\Api\Enum\UserOptionEnum;
use App\Api\HttpDTO\ProfileUpdate;
use App\Api\HttpDTO\UserOptionUpdate;
use App\Api\Repository\NotificationRepository;
use App\Api\Repository\UserOptionRepository;
use App\Api\Repository\UserRepository;
use App\Api\Repository\UserTopPlaceRepository;
use App\Api\Service\Profile\UserPhoneManager;
use App\Api\Service\SecurityManager;
use Kreait\Firebase;
use Kreait\Firebase\Messaging\MessageTarget;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request as RequestAlias;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractApiController
{
    /**
     * @var SecurityManager
     */
    private $registrationManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var UserPhoneManager
     */
    private $userPhoneManager;
    /**
     * @var CentrifugoCryptoManager
     */
    private $centrifugoCryptoManager;
    /**
     * @var UserTopPlaceRepository
     */
    private $userTopPlaceRepository;
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    public function __construct(
        ValidatorInterface $validator,
        SecurityManager $registrationManager,
        UserRepository $userRepository,
        UserTopPlaceRepository $userTopPlaceRepository,
        UserPhoneManager $userPhoneManager,
        CentrifugoCryptoManager $centrifugoCryptoManager,
        NotificationRepository $notificationRepository,
        LoggerInterface $logger
    ) {
        $this->registrationManager = $registrationManager;
        $this->userRepository = $userRepository;

        parent::__construct($validator);
        $this->logger = $logger;
        $this->userPhoneManager = $userPhoneManager;
        $this->centrifugoCryptoManager = $centrifugoCryptoManager;
        $this->userTopPlaceRepository = $userTopPlaceRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Get the most visited user places.
     *
     * @Route("/users/{userId}/top-places/{limit}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns get objects",
     *     @SWG\Items(ref=@Model(type=AutocompleteData::class, groups={"api"})),
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getLastTopPlaces(int $userId, int $limit = 3): JsonResponse
    {
        $user = $this->assertHasAccess($userId);
        $places = $this->userTopPlaceRepository->findTopPlaces($user->getId(), $limit);

        return $this->serialize($places, Response::HTTP_OK);
    }

    /**
     * @Route("/users/{userId}", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns get objects",
     *     @Model(type=User::class, groups={"api"})
     * ),
     *
     * @SWG\Response(
     *     response=401,
     *     description="If try to call for not current auth user",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     */
    public function profile(int $userId): JsonResponse
    {
        $user = $this->assertHasAccess($userId);

        return $this->serialize($user);
    }

    /**
     * Get websocket sign.
     *
     * @Route("/users/{userId}/ws", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns get objects",
     *     @Model(type=WebSocketSign::class, groups={"api"})
     * ),
     *
     * @SWG\Response(
     *     response=401,
     *     description="If try to call for not current auth user",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     */
    public function webSocketSign(int $userId): JsonResponse
    {
        $user = $this->assertHasAccess($userId);
        $sign = $this->centrifugoCryptoManager->createClientSign($user->getId());

        return $this->serialize($sign);
    }

    /**
     * Validate confirmation code, and change phone number.
     *
     * @Route("/users/phone/change", methods={"POST"})
     * @SWG\Parameter(
     *     name="phoneData",
     *     in="body",
     *     description="Data to change phone",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          ref=@Model(type=PhoneConfirmationRequest::class, groups={"api"})
     *      ),
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="If phone successfully updated",
     *     @Model(type=User::class, groups={"api"})
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error if phone already used or wrong confirmation code",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Tag(name="users")
     *
     * @Security(name="UserAuth")
     *
     * @throws \App\Api\Exception\ApiErrorCodeException
     * @throws \App\Api\Exception\PhoneConfirmationException
     * @throws \Doctrine\ORM\ORMException
     */
    public function changePhone(PhoneConfirmationRequest $request): JsonResponse
    {
        $user = $this->userPhoneManager->change($request, $this->getUser());

        return $this->serialize($user, Response::HTTP_OK);
    }

    /**
     * Update user profile.
     *
     * @Route("/users/{userId}", methods={"PUT"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Profile data to update",
     *     required=true,
     *     format="application/json",
     *     @Model(type=ProfileUpdate::class, groups={"api"}),
     * ),
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @SWG\Response(
     *     response=200,
     *     description="Returns get objects",
     *     @Model(type=User::class, groups={"api"})
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     */
    public function updateProfile(int $userId, ProfileUpdate $profileUpdate): JsonResponse
    {
        $user = $this->assertHasAccess($userId);

        if (null !== $profileUpdate->getFullName()
            && $user->getFullName() !== $profileUpdate->getFullName()
        ) {
            $user->setFullName($profileUpdate->getFullName());
        }
        if (null !== $profileUpdate->getEmail()
            && $user->getEmail() !== $profileUpdate->getEmail()
        ) {
            $user->setEmail($profileUpdate->getEmail());
        }
        if (false === $user->getIsPolicyAccepted()
            && $profileUpdate->isPolicyAccepted()) {
            $user->setIsPolicyAccepted($profileUpdate->isPolicyAccepted());
        }

        $this->userRepository->save($user);

        return $this->serialize($user, Response::HTTP_OK);
    }

    /**
     * Get Last Notification for user.
     *
     * @Route("/users/{userId}/last-notification", methods={"POST"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns notification objects",
     *     @Model(type=Notification::class, groups={"api"})
     * ),
     *
     * @SWG\Response(
     *     response=403,
     *     description="Access denied",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function lastNotification(int $userId): JsonResponse
    {
        $this->assertHasAccess($userId);
        $notification = $this->notificationRepository->findLastNotCanceledByUserId($userId);

        return $this->serialize($notification, Response::HTTP_OK);
    }

    /**
     * Update all users Notifications as canceled(viewed).
     *
     * @Route("/users/{userId}/notifications/cancel", methods={"PUT"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="if operation success"
     * ),
     *
     * @SWG\Response(
     *     response=403,
     *     description="Access denied",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function cancelAllNotifications(int $userId): JsonResponse
    {
        $this->assertHasAccess($userId);
        $result = $this->notificationRepository->cancelByUserId($userId);

        return $this->serialize(null, Response::HTTP_OK);
    }

    /**
     * Send message to device via push.
     *
     * @Route("/push/{fcmToken}/send", methods={"POST"})
     * @SWG\Parameter(
     *     name="pushData",
     *     in="query",
     *     type="string",
     *     description="Put json string with index 'data','notification','android','apns','webpush'",
     *     required=true,
     *     format="application/json"
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="If phone successfully updated",
     *     @Model(type=User::class, groups={"api"})
     * ),
     * @Security(name="UserAuth")
     */
    public function testPush(string $fcmToken, RequestAlias $request, Firebase\Messaging $firebaseMessaging)
    {
        $pushData = $request->query->get('pushData');
        $data = \GuzzleHttp\json_decode($pushData, true);
        $data[MessageTarget::TOKEN] = $fcmToken;

        $message = Firebase\Messaging\CloudMessage::fromArray($data);
        $result = $firebaseMessaging->send($message);

        return $this->serialize($result);
    }

    /**
     * Get Options for user.
     *
     * @Route("/users/{userId}/options", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns user options",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=UserOptionUpdate::class, groups={"api"}))
     *     )
     * ),
     *
     * @SWG\Response(
     *     response=403,
     *     description="Access denied",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function options(int $userId, UserOptionRepository $optionRepository): JsonResponse
    {
        $user = $this->assertHasAccess($userId);
        $options = $optionRepository->findByUserAndOptions(
            $this->getUser(),
            UserOptionEnum::getAvailableOptions()
        );
        $options = $this->hydrateOptionsWithDefaultValue($options, $user);

        return $this->serialize($options, Response::HTTP_OK);
    }

    /**
     * Update option.
     *
     * @Route("/users/{userId}/option", methods={"PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="if operation success"
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Access denied",
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="users")
     */
    public function updateOption(int $userId, UserOptionUpdate $userOptionDto, UserOptionRepository $optionRepository): JsonResponse
    {
        $this->assertHasAccess($userId);
        $countUpdated = $optionRepository->update($userOptionDto);

        return $this->serialize(null, Response::HTTP_OK);
    }

    /**
     * @param UserOption[] $options
     */
    private function hydrateOptionsWithDefaultValue($options, User $user): array
    {
        $defaultOptionsWithValue = [
            UserOptionEnum::APP_RATE_VIEW => '0',
        ];
        $availableOptionKeys = [];
        foreach ($options as $option) {
            $availableOptionKeys[] = $option->getUserOption()->getValue();
        }
        $defaultOptionsThatNotExist = array_diff(array_keys($defaultOptionsWithValue), $availableOptionKeys);
        if (!empty($defaultOptionsThatNotExist)) {
            foreach ($defaultOptionsThatNotExist as $option) {
                $options[] = new UserOption($user, new UserOptionEnum($option), $defaultOptionsWithValue[$option]);
            }
        }

        return $options;
    }
}
