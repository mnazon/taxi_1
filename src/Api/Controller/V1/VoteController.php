<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\AddVoteDto;
use App\Api\DTO\PageDto;
use App\Api\Entity\TripVote;
use App\Api\Exception\WrongRequestArgumentException;
use App\Api\Repository\TripVoteRepository;
use App\Api\Service\Vote\VoteManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class VoteController extends AbstractApiController
{
    /**
     * @var VoteManager
     */
    private $voteManager;
    /**
     * @var TripVoteRepository
     */
    private $tripVoteRepository;

    public function __construct(
        ValidatorInterface $validator,
        VoteManager $voteManager,
        TripVoteRepository $tripVoteRepository
    ) {
        parent::__construct($validator);
        $this->voteManager = $voteManager;
        $this->tripVoteRepository = $tripVoteRepository;
    }

    /**
     * Add vote to trip.
     *
     * @Route("/votes", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Vote data",
     *     required=true,
     *     format="application/json",
     *     @Model(type=AddVoteDto::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response=201,
     *     description="Create new vote for trip",
     *     @Model(type=TripVote::class, groups={"api"})
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="votes")
     *
     * @throws \Throwable
     */
    public function add(AddVoteDto $addVoteDto): JsonResponse
    {
        $user = $this->getUser();

        $tripVote = $this->voteManager->addVote($addVoteDto, $user);

        return $this->serialize($tripVote, Response::HTTP_CREATED);
    }

    /**
     * Find votes for taxiProvider.
     *
     * @Route("/votes", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="taxiProviderId",
     *     in="query",
     *     type="integer",
     *     required=true
     * ),
     * @SWG\Parameter(ref="#/parameters/page"),
     * @SWG\Parameter(ref="#/parameters/perPage"),
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns votes collection",
     *     @SWG\Items(ref=@Model(type=TripVote::class, groups={"api"})),
     * ),
     * @Security(name="UserAuth")
     * @SWG\Tag(name="votes")
     *
     * @throws \Throwable
     */
    public function find(Request $request, PageDto $pageDto): JsonResponse
    {
        $taxiProviderId = (int) $request->query->get('taxiProviderId');
        if ($taxiProviderId <= 0) {
            throw new WrongRequestArgumentException('taxiProviderId', 'Must be positive');
        }

        $tripVotes = $this->tripVoteRepository->findByTaxiProvider(
            $taxiProviderId,
            $pageDto
        );

        return $this->serialize($tripVotes, Response::HTTP_OK);
    }
}
