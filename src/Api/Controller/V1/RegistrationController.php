<?php

namespace App\Api\Controller\V1;

use App\Api\DTO\RegistrationRequest;
use App\Api\Entity\UserToken;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\Repository\UserRepository;
use App\Api\Service\SecurityManager;
use App\Api\Utils\PlatformDetector;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractApiController
{
    /**
     * @var SecurityManager
     */
    private $securityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        SecurityManager $securityManager,
        UserRepository $userRepository
    ) {
        $this->securityManager = $securityManager;
        $this->userRepository = $userRepository;
    }

    /**
     * Validate confirmation code, register if not exits and send accessToken to client.
     *
     * @Route("/registerOrLogin", name="api_user_registration_or_login", methods={"POST"})
     * @SWG\Parameter(
     *     name="credentials",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *          type="object",
     *          ref=@Model(type=RegistrationRequest::class, groups={"api"})
     *      ),
     * ),
     *
     * @SWG\Tag(name="users")
     * @SWG\Response(
     *     response=200,
     *     description="If user login",
     *     @Model(type=UserToken::class, groups={"api"})
     * ),
     * @SWG\Response(
     *     response=201,
     *     description="If user registered",
     *     @Model(type=UserToken::class, groups={"api"})
     * ),
     *
     * @throws ConstraintViolationListException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \App\Api\Exception\PhoneConfirmationException
     */
    public function registerOrLogin(RegistrationRequest $registrationRequest, Request $request): JsonResponse
    {
        //todo - удалить когда не будет использоваться более
        if (!$registrationRequest->getPlatform()) {
            $detector = new PlatformDetector();
            $platform = $detector->detectPlatformByRequest($request);
            $registrationRequest->setPlatform($platform);
        }

        $user = $this->userRepository->findByPhone($registrationRequest->getPhone());
        if (null === $user) {
            $statusCode = Response::HTTP_CREATED;
            $userToken = $this->securityManager->register($registrationRequest);
        } else {
            $statusCode = Response::HTTP_OK;
            $userToken = $this->securityManager->login($user, $registrationRequest);
        }

        return $this->serialize($userToken, $statusCode);
    }
}
