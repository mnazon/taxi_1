<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\DeviceData;
use App\Api\Entity\UserDevice;
use App\Api\Repository\UserDeviceRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DeviceController extends AbstractApiController
{
    /**
     * @var UserDeviceRepository
     */
    private $userDeviceRepository;

    public function __construct(
        ValidatorInterface $validator,
        UserDeviceRepository $userDeviceRepository
    ) {
        parent::__construct($validator);
        $this->userDeviceRepository = $userDeviceRepository;
    }

    /**
     * Update or add user device data.
     *
     * @Route("/devices", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="device data",
     *     required=true,
     *     format="application/json",
     *     @Model(type=DeviceData::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="When device data update",
     *     @Model(type=UserDevice::class, groups={"api"})
     * ),
     * @SWG\Response(
     *     response=201,
     *     description="When device created",
     *     @Model(type=UserDevice::class, groups={"api"})
     * ),
     *
     * @Security(name="UserAuth")
     * @SWG\Tag(name="device")
     *
     * @throws \Exception
     */
    public function updateDeviceData(DeviceData $deviceData): JsonResponse
    {
        //todo - в данном случае, если апдейтить девайс данные
        //история нотификаций по поездкам - не будет сохраняться.
        $user = $this->getUser();
        $device = $this->userDeviceRepository->findByDeviceToken($deviceData->getDeviceToken());
        $responseCode = Response::HTTP_OK;
        if (empty($device)) {
            $responseCode = Response::HTTP_CREATED;
            $device = new UserDevice();
            $device->setCreatedAt(new \DateTimeImmutable())
                ->setDeviceToken($deviceData->getDeviceToken())
                ->setUser($user)
            ;
        }
        if ($deviceData->getFirebaseRegistrationToken() !== $device->getFirebaseRegistrationToken()) {
            $device->setFirebaseRegistrationToken($deviceData->getFirebaseRegistrationToken());
        }
        $this->userDeviceRepository->save($device);

        return $this->serialize($device, $responseCode);
    }

    /**
     * Update or add user device data.
     *
     * @Route("/devices/{deviceToken}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="When device data update",
     *     @Model(type=UserDevice::class, groups={"api"})
     * ),
     *
     * @Security(name="UserAuth")
     * @SWG\Tag(name="device")
     */
    public function getDeviceData(string $deviceToken): JsonResponse
    {
        $device = $this->userDeviceRepository->findByDeviceToken($deviceToken);

        return $this->serialize($device);
    }
}
