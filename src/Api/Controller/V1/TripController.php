<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\EstimatedTripItem;
use App\Api\DTO\PageDto;
use App\Api\DTO\Trip\AdditionalCostRequest;
use App\Api\DTO\Trip\EstimateSort;
use App\Api\DTO\Trip\TripCancelRequest;
use App\Api\DTO\Trip\TripCompareRequest;
use App\Api\DTO\Trip\TripCompareResponse;
use App\Api\DTO\Trip\TripCostHintResponse;
use App\Api\Entity\Trip;
use App\Api\Enum\TripStatus;
use App\Api\Event\TripExternalForceStatusChanged;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\HttpDTO\CreateTripRequest;
use App\Api\HttpDTO\EstimateTripRequest;
use App\Api\Repository\TripRepository;
use App\Api\Service\TaxiProvider\CreateTripManager;
use App\Api\Service\TaxiProvider\OrderAggregator;
use App\Api\Service\TaxiProvider\TripCancellation;
use App\Api\Service\Trip\TripCostHintCalculator;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class TripController extends AbstractApiController
{
    /**
     * @var TripRepository
     */
    private $tripRepository;
    /**
     * @var TripCancellation
     */
    private $tripCancellation;
    /**
     * @var CreateTripManager
     */
    private $createTripManager;
    /**
     * @var OrderAggregator
     */
    private $orderAggregator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        CreateTripManager $createTripManager,
        OrderAggregator $orderAggregator,
        TripRepository $tripRepository,
        ValidatorInterface $validator,
        LoggerInterface $logger,
        TripCancellation $tripCancellation
    ) {
        parent::__construct($validator);
        $this->tripRepository = $tripRepository;
        $this->tripCancellation = $tripCancellation;
        $this->createTripManager = $createTripManager;
        $this->orderAggregator = $orderAggregator;
        $this->logger = $logger;
    }

    /**
     * Get current user trips.
     *
     * @Route("/trips/current", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="some desc",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Trip::class, groups={"api"}))
     *     )
     * ),
     * @SWG\Response(
     *     response="403",
     *     description="Access denied",
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function current(): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $statuses = [
            TripStatus::WAITING_FOR_CAR_SEARCH,
            TripStatus::SEARCH_FOR_CAR,
            TripStatus::CAR_FOUND,
            TripStatus::RUNNING,
        ];
        $trips = $this->tripRepository->findByUserAndStatuses($userId, $statuses);

        return $this->serialize($trips, Response::HTTP_OK);
    }

    /**
     * Get trip info.
     *
     * @Route("/trips/{id}", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="get trip by id",
     *     @Model(type=Trip::class, groups={"api"}))
     * ),
     * @SWG\Response(
     *     response="403",
     *     description="Access denied",
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @param $id
     */
    public function find($id): JsonResponse
    {
        $trip = $this->getTrip($id);
        $this->assertHasAccess($trip->getUser()->getId());

        return $this->serialize($trip, Response::HTTP_OK);
    }

    /**
     * Get trip status info.
     *
     * @Route("/trips/{id}/external-sync", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="get trip external sync by id",
     *     @Model(type=Trip::class, groups={"api"}))
     * ),
     * @SWG\Response(
     *     response="403",
     *     description="Access denied",
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @param $id
     */
    public function findExternalSync($id, EventDispatcherInterface $eventDispatcher): JsonResponse
    {
        $trip = $this->getTrip($id);
        $this->assertHasAccess($trip->getUser()->getId());

        $eventDispatcher->dispatch(
            new TripExternalForceStatusChanged($trip)
        );
        $trip = $this->getTrip($trip->getId());

        return $this->serialize($trip, Response::HTTP_OK);
    }

    /**
     * Get user trips.
     *
     * @Route("/trips", methods={"GET"})
     * @SWG\Parameter(
     *     name="userId",
     *     in="query",
     *     required=true,
     *     type="integer",
     *     description="User id",
     *     default="1",
     * ),
     * @SWG\Parameter(ref="#/parameters/page"),
     * @SWG\Parameter(ref="#/parameters/perPage"),
     * @SWG\Response(
     *     response="200",
     *     description="some desc",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Trip::class, groups={"list"}))
     *     )
     * ),
     * @SWG\Response(
     *     response="403",
     *     description="Access denied",
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function findUserTrips(Request $request, PageDto $pageDto): JsonResponse
    {
        $userId = (int) $request->query->get('userId', null);
        $this->assertHasAccess($userId);

        $trips = $this->tripRepository->findByUserId($userId, $pageDto);

        return $this->serialize($trips, Response::HTTP_OK, ['groups' => self::RESPONSE_GROUP_LIST]);
    }

    /**
     * Create trip(start car searching).
     *
     * @Route("/trips", methods={"POST"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=CreateTripRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="201",
     *     description="some desc",
     *     @Model(type=Trip::class, groups={"api"}))
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @throws \Throwable
     */
    public function create(CreateTripRequest $tripRequest): JsonResponse
    {
        $this->setupUserFullName($tripRequest->getTrip());

        $trip = $this->createTripManager->create($tripRequest);

        return $this->serialize($trip, 201);
    }

    /**
     * @Route("/trips/{tripId}/cost/additional/{additionalCost}", methods={"PUT"})
     *
     * @SWG\Response(
     *     response="200",
     *     description="Update trip data",
     *     @Model(type=Trip::class, groups={"api"}))
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @param int   $tripId
     * @param float $additionalCost
     *
     * @throws ApiErrorCodeException
     * @throws \App\Api\Exception\ConstraintViolationListException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAdditionalCost($tripId, $additionalCost, CreateTripManager $createRideManager): JsonResponse
    {
        $additionalCostRequest = new AdditionalCostRequest(
            $tripId,
            $additionalCost
        );
        $this->assertValidData($additionalCostRequest);

        $user = $this->getUser();
        $this->assertHasAccess($user->getId());
        $trip = $createRideManager->updateAdditionalCost($additionalCostRequest, $user);

        return $this->serialize($trip, 200);
    }

    /**
     * @Route("/trips/estimate", methods={"PUT"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=EstimateTripRequest::class, groups={"api"}),
     * ),
     * @SWG\Parameter(ref="#/parameters/sortField"),
     * @SWG\Parameter(ref="#/parameters/sortDirection"),
     *
     * @SWG\Response(
     *     response="200",
     *     description="some desc",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=EstimatedTripItem::class, groups={"api"})),
     *     )
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     */
    public function estimate(EstimateTripRequest $costRequest, EstimateSort $sort): JsonResponse
    {
        //no additional cost need to setup for now! hack
        $costRequest->setAdditionalCost(null);

        $this->setupUserFullName($costRequest);
        $costResponse = $this->orderAggregator->cost($costRequest, $sort);
        $response = $costResponse->getData();

        return $this->serialize($response, Response::HTTP_OK);
    }

    /**
     * @Route("/trips/compare", methods={"PUT"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=TripCompareRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="some desc",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TripCompareResponse::class, groups={"api"})),
     *     )
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     */
    public function compare(TripCompareRequest $request): JsonResponse
    {
        $this->setupUserFullName($request->getTrip());
        $userAdditionalCost = $request->getTrip()->getAdditionalCost();
        $userBaseCost = $request->getBaseCost();
        try {
            $estimate = $this->orderAggregator->getClientEsitmate($request->getTaxiProviderId(), $request->getTrip());

            $userCost = $userBaseCost + $userAdditionalCost;
            $estimateBaseCost = $estimate->getCost() - $request->getTrip()->getAdditionalCost();
            $difference = $userCost - $estimateBaseCost;
            $delta = 0.0;
            $isPriceChanged = $difference < $delta;
            $newAdditionalCost = $difference;
            $response = new TripCompareResponse($isPriceChanged, $estimateBaseCost, $newAdditionalCost);
        } catch (\Throwable $e) {
            $dataToLog = [
                'exception' => $e,
                'taxiProvider' => $request->getTaxiProviderId(),
            ];
            $this->logger->warning('Cannot compare request', $dataToLog);
            $response = new TripCompareResponse(false, $userBaseCost, $userAdditionalCost ?? 0);
        }

        return $this->serialize($response, Response::HTTP_OK);
    }

    /**
     * @Route("/trips/{tripId}/cancel", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=false,
     *     format="application/json",
     *     @Model(type=TripCancelRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="If successfully canceled",
     * ),
     *
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="trip not found",
     * ),
     *
     * @SWG\Tag(name="trips")
     * @Security(name="UserAuth")
     *
     * @throws ApiErrorCodeException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cancel(int $tripId, ?TripCancelRequest $tripCancelRequest): JsonResponse
    {
        $trip = $this->getTrip($tripId);
        $this->assertHasAccess($trip->getUser()->getId());
        $this->tripCancellation->cancel($trip, $tripCancelRequest);

        return $this->serialize([], 200);
    }

    /**
     * @Route("/trips/{tripId}/costHint", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="some desc",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TripCostHintResponse::class, groups={"api"})),
     *     )
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Trip not found"
     * ),
     * @SWG\Tag(name="trips")
     *
     * @Security(name="UserAuth")
     *
     * @throws \Exception
     */
    public function costHint(int $tripId, TripCostHintCalculator $costHintCalculator): JsonResponse
    {
        $trip = $this->getTrip($tripId);
        $baseCost = $trip->getBaseCost();
        $date = new \DateTimeImmutable(date('Y-m-d H:i:s', time()));

        $additionalCost = $costHintCalculator->calculateAdditionalCost($baseCost, $date, $trip->getTaxiProvider());
        $data = $additionalCost ? new TripCostHintResponse($baseCost, $additionalCost) : null;

        return $this->serialize($data, Response::HTTP_OK);
    }

    private function setupUserFullName(EstimateTripRequest $costRequest): void
    {
        if (empty($costRequest->getUserFullName())) {
            $user = $this->getUser();
            $fullName = ($user->getFullName()) ? $user->getFullName() : 'taximer user '.$user->getId();
            $costRequest->setUserFullName($fullName);
        }
    }

    /**
     * @param $id
     */
    private function getTrip($id): ?Trip
    {
        $trip = $this->tripRepository->find($id);
        if (null === $trip) {
            throw $this->createNotFoundException('Trip not found');
        }

        return $trip;
    }
}
