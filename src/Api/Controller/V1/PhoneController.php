<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\phone\SendPhoneConfirmationDTO;
use App\Api\DTO\phone\SendPhoneResponseDto;
use App\Api\Service\Phone\Adapter\DummySmsSender;
use App\Api\Service\Phone\PhoneConfirmationManager;
use App\Api\Service\ViolationFactory;
use Doctrine\ORM\ORMException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PhoneController extends AbstractApiController
{
    /**
     * @var PhoneConfirmationManager
     */
    private $phoneVerificationManager;
    /**
     * @var ViolationFactory
     */
    private $violationFactory;

    public function __construct(
        PhoneConfirmationManager $phoneVerificationManager,
        ViolationFactory $violationFactory,
        ValidatorInterface $validator
    ) {
        parent::__construct($validator);
        $this->phoneVerificationManager = $phoneVerificationManager;
        $this->violationFactory = $violationFactory;
    }

    /**
     * Validate, save and sent confirmation code to phone number.
     *
     * @Route("/phones/send/code", methods={"POST"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="phone number in e164 format",
     *     required=true,
     *     format="application/json",
     *     @Model(type=SendPhoneConfirmationDTO::class, groups={"api"}),
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns confirmation code",
     *     @Model(type=SendPhoneResponseDto::class, groups={"api"}),
     * ),
     * @SWG\Tag(name="phones")
     *
     * @throws ORMException
     * @throws \App\Api\Exception\ApiErrorCodeException
     */
    public function sendConfirmationCode(SendPhoneConfirmationDTO $sendPhoneConfirmationDTO): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $result = $this->phoneVerificationManager->sendConfirmationCode($sendPhoneConfirmationDTO);

        return $this->json($result, Response::HTTP_OK, []);
    }

    /**
     * Validate, save and return confirmation code in response.
     *
     * @Route("/phones/send/code/test", methods={"POST"})
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="phone number in e164 format",
     *     required=true,
     *     format="application/json",
     *     @Model(type=SendPhoneConfirmationDTO::class, groups={"api"}),
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Returns confirmation code",
     *     @Model(type=SendPhoneResponseDto::class, groups={"api"}),
     * )
     * @SWG\Tag(name="phones")
     *
     * @throws ORMException
     * @throws \App\Api\Exception\ApiErrorCodeException
     */
    public function sendTestConfirmationCode(
        Request $request,
        SendPhoneConfirmationDTO $sendPhoneConfirmationDTO,
        DummySmsSender $dummySmsSender
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        if (!$request->server->getBoolean('CAN_USE_TEST_CONFIRMATION', false)) {
            throw $this->createAccessDeniedException();
        }

        $this->phoneVerificationManager->setSmsSenderAdapter($dummySmsSender);
        $result = $this->phoneVerificationManager->sendConfirmationCode($sendPhoneConfirmationDTO);
        $result->setNeedSendConfirmationCodeInResponse(true);

        return $this->json($result, Response::HTTP_OK, []);
    }
}
