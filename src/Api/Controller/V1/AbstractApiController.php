<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\Entity\User;
use App\Api\Exception\ConstraintViolationListException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractApiController.
 *
 * @method User getUser()
 */
abstract class AbstractApiController extends AbstractController
{
    public const RESPONSE_DEFAULT_GROUP = 'api';
    public const RESPONSE_GROUP_LIST = 'list';

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    private $defaultResponseContext = [
        'groups' => [self::RESPONSE_DEFAULT_GROUP],
    ];

    protected function validateFunction(): \Closure
    {
        $validateFunction = function ($object, \Closure $executeIfValid) {
            $violations = $this->validator->validate($object);
            if ($violations->count() > 0) {
                return $this->serialize($violations, Response::HTTP_BAD_REQUEST);
            }

            return $executeIfValid($object);
        };

        return $validateFunction;
    }

    /**
     * @param $request
     *
     * @throws ConstraintViolationListException
     */
    protected function assertValidData(object $request)
    {
        $violations = $this->validator->validate($request);

        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }
    }

    public function serialize($data, $status = 200, array $context = [], array $headers = [], $format = 'json')
    {
        $context = array_merge($this->defaultResponseContext, $context);

        return $this->json($data, $status, $headers, $context);
    }

    /**
     * @throws AccessDeniedException
     */
    protected function assertHasAccess(int $userId): User
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getId() !== $userId) {
            throw $this->createAccessDeniedException('You are not authorized for this action');
        }

        return $user;
    }
}
