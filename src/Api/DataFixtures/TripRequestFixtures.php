<?php

declare(strict_types=1);

namespace App\Api\DataFixtures;

use App\Api\Entity\Trip;
use App\Api\Entity\TripRequest;
use App\DataFixtures\AppFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TripRequestFixtures extends Fixture implements DependentFixtureInterface
{
    public const TRIP_REQUEST = 'tripRequest';
    const INTERNAL_TRIP_REQUEST = 'internalTripRequest';

    public function load(ObjectManager $manager)
    {
        $tripRequest = new TripRequest();
        $tripRequest->setUserPhone('1111111')
            ->setUserFullName('full_name')
            ->setCarType('car_type')
            ->setOptions(['conditioner'])
            ->setReservedAt(new \DateTimeImmutable())
            ->setStartRouteEntranceFrom(1)
            ->setReservedAt(new \DateTimeImmutable());

        $evosTrip = $this->getEvosTrip();
        $evosTrip->setRequest($tripRequest);
        $manager->persist($evosTrip);
        $manager->persist($tripRequest);

        $this->setReference(self::TRIP_REQUEST, $tripRequest);
        //internal

        $internalTripRequest = clone $tripRequest;
        $internalTrip = $this->getInternalTrip();
        $internalTrip->setRequest($internalTripRequest);
        $manager->persist($internalTrip);
        $manager->persist($internalTripRequest);

        $this->setReference(self::INTERNAL_TRIP_REQUEST, $internalTripRequest);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AppFixtures::class,
        ];
    }

    public function getEvosTrip(): Trip
    {
        /* @var Trip $trip */
        return $this->getReference(AppFixtures::TRIP_REF);
    }

    public function getInternalTrip(): Trip
    {
        /* @var Trip $trip */
        return $this->getReference(AppFixtures::INTERNAL_TRIP_REF);
    }
}
