<?php

declare(strict_types=1);

namespace App\Api\DataFixtures;

use App\Api\Entity\TripCostFactor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TripCostFactorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $date = new \DateTimeImmutable('2019-10-08 15:00:00');
        $tripCostFactor = (new TripCostFactor())
            ->setDateOfWeek((int) $date->format('N'))
            ->setTimeFrom($date)
            ->setTimeTo($date->modify('+ 2 hour'))
            ->setFactorMin(30)
            ->setFactorMax(70)
        ;

        $manager->persist($tripCostFactor);
        $manager->flush($tripCostFactor);
    }
}
