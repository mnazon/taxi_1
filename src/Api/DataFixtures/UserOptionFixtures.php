<?php

declare(strict_types=1);

namespace App\Api\DataFixtures;

use App\Api\Entity\UserOption;
use App\Api\Enum\UserOptionEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserOptionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userOption = new UserOption(
            $this->getReference(UserFixtures::USER_REFERENCE),
            UserOptionEnum::APP_RATE_VIEW(),
            '1'
        );

        $manager->persist($userOption);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
