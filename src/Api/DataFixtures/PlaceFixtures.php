<?php

namespace App\Api\DataFixtures;

use App\Api\DTO\Geo\Coordinates;
use App\Api\Entity\Place;
use App\Api\Entity\TripRequest;
use App\Api\Entity\TripRequestToPlace;
use App\Api\Entity\User;
use App\Api\Entity\UserTopPlace;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PlaceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Place[] $places */
        $places = [];
        $places[] = new Place(
            'тест 1',
            'тест sec1',
            'street',
            'locality',
            'district',
            'kiev',
            '123123',
            new Coordinates(50.474613, 50.371123)
        );
        $places[] = new Place(
            'тест 2',
            'тест sec2',
            'street',
            'kiev',
            'district',
            'kiev',
            '123123123',
            new Coordinates(50.474611, 50.371121)
        );

        /** @var User $user */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);
        /** @var TripRequest $tripRequest */
        $tripRequest = $this->getReference(TripRequestFixtures::TRIP_REQUEST);
        /** @var TripRequest $internalTripRequest */
        $internalTripRequest = $this->getReference(TripRequestFixtures::INTERNAL_TRIP_REQUEST);
        foreach ($places as $place) {
            $userTopPlace = new UserTopPlace();
            $userTopPlace->setPlace($place)
                ->setUser($user);

            $tripRequestToPlace = (new TripRequestToPlace($tripRequest, $place));
            $tripRequest->addToRoute($tripRequestToPlace);

            $internalTripRequestToPlace = (new TripRequestToPlace($internalTripRequest, $place));
            $internalTripRequest->addToRoute($internalTripRequestToPlace);

            $manager->persist($place);
            $manager->persist($userTopPlace);
            $manager->persist($tripRequestToPlace);
            $manager->persist($internalTripRequestToPlace);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            TripRequestFixtures::class,
        ];
    }
}
