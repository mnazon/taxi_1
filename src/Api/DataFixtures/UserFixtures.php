<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserFixtures extends Fixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public const USER_PHONE = '+380958747485';
    public const USER_PASS = '123456';

    public const USER_REFERENCE = 'user-ref';
    /** @var User */
    private $user;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setPhone(self::USER_PHONE)
            ->setCreatedAt(new \DateTimeImmutable())
            ->setIsPolicyAccepted(true)
            ->setRoles(['ROLE_APIDOC'])
            ->setFullName('fakeuser');

        $manager->persist($user);
        $manager->flush();

        $this->addReference(self::USER_REFERENCE, $user);
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
