<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\TripVote;
use App\DataFixtures\AppFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TripVoteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tripVote = new TripVote(
            4,
            $this->getReference(UserFixtures::USER_REFERENCE),
            $this->getReference(AppFixtures::TRIP_REF),
            new \DateTimeImmutable(),
            'comment test'
        );

        $manager->persist($tripVote);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            AppFixtures::class,
        ];
    }
}
