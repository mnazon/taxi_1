<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\UserToken;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserTokenFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userToken = new UserToken(
            md5('1'),
            $this->getReference(UserFixtures::USER_REFERENCE),
            new \DateTimeImmutable()
        );

        $manager->persist($userToken);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
