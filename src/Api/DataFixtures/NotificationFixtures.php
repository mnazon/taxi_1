<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\Notification;
use App\Api\Entity\User;
use App\DataFixtures\AppFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NotificationFixtures extends Fixture implements DependentFixtureInterface
{
    /** @var User */
    private $user;

    public function load(ObjectManager $manager)
    {
        $notification = new Notification();
        $notification->setUser($this->getReference(UserFixtures::USER_REFERENCE))
            ->setTrip($this->getReference(AppFixtures::TRIP_REF))
        ;

        $manager->persist($notification);
        $manager->flush();

        $this->user = $this->getReference(UserFixtures::USER_REFERENCE);
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            AppFixtures::class,
        ];
    }
}
