<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\UserDevice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserDeviceFixtures extends Fixture implements DependentFixtureInterface
{
    const DEVICE_TOKEN = 'deviceTokenId1';
    /**
     * @var UserDevice
     */
    private $userDevice;

    public function load(ObjectManager $manager)
    {
        $this->userDevice = new UserDevice();
        $this->userDevice->setUser($this->getReference(UserFixtures::USER_REFERENCE))
            ->setDeviceToken(self::DEVICE_TOKEN)
            ->setCreatedAt(new \DateTimeImmutable())
            ->setFirebaseRegistrationToken('setFirebaseRegistrationToken')
        ;

        $manager->persist($this->userDevice);
        $manager->flush();
    }

    public function getUserDevice(): UserDevice
    {
        return $this->userDevice;
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
