<?php

declare(strict_types=1);

namespace App\Api\DataFixtures;

use App\Api\Entity\Phone;
use App\Api\Entity\PhoneConfirmation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use libphonenumber\PhoneNumber;

class PhoneConfirmationFixture extends Fixture
{
    const PHONE_NUMBER = UserFixtures::USER_PHONE;
    const CONFIRMATION_CODE = '1111';
    /**
     * @var int
     */
    private $countOfTries;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var \DateTimeImmutable
     */
    private $bannedUntill;

    public function __construct($countOfTries = 0, $phone = self::PHONE_NUMBER, \DateTimeImmutable $bannedUntill = null)
    {
        $this->countOfTries = $countOfTries;
        $this->phone = $phone;
        $this->bannedUntill = $bannedUntill;
    }

    public function load(ObjectManager $manager)
    {
        $createdAt = new \DateTimeImmutable();
        $phone = new Phone(
            (new PhoneNumber())->setRawInput($this->phone),
            $createdAt
        );
        if ($this->bannedUntill) {
            $phone->setConfirmationBlockedUntil($this->bannedUntill);
        }
        $phone->setConfirmationAttemptNumber($this->countOfTries);
        $manager->persist($phone);

        $confirmation = new PhoneConfirmation();
        $confirmation->setCode((int) self::CONFIRMATION_CODE);
        $confirmation->setSentAt($createdAt);
        $confirmation->setIp(111111111);
        $phone->addPhoneConfirmation($confirmation);
        $manager->persist($confirmation);

        $manager->flush();
    }
}
