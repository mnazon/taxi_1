<?php

namespace App\Api\DataFixtures;

use App\Api\Entity\TaxiProviderRating;
use App\DataFixtures\AppFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProviderRatingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @noinspection PhpParamsInspection */
        $userToken = new TaxiProviderRating(
            4.2,
            101,
            $this->getReference(AppFixtures::EVOS_TAXI_PROVIDER_REF)
        );

        $manager->persist($userToken);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AppFixtures::class,
        ];
    }
}
