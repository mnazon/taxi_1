<?php

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class TripCancellationReason extends Enum
{
    public const PLANS_CHANGED = 0;
    public const SEARCH_TOO_LONG = 1;
    // Водитель не приехал/не выходит на связь
    public const DRIVER_LOST = 2;
    public const DRIVER_ASKED_TO_CANCEL = 3;
    public const DRIVER_ASKED_UPDATE_PRICE = 4;
    public const DRIVER_AT_WRONG_PLACE = 5;
    public const DRIVER_BEHAVIOR = 6;
    // класс мащины не соответсвует / нет заказанных доп. услуг
    public const WRONG_TRIP_CLASS = 7;
    public const CAR_BAD_CONDITION = 8;
    public const OTHER = 9;
}
