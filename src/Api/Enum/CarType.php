<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class CarType.
 *
 * @method self STANDART()
 * @method self WAGON()
 * @method self MINIBUS()
 * @method self PREMIUM()
 */
class CarType extends Enum
{
    const STANDART = 'standart';
    const WAGON = 'wagon';
    const MINIBUS = 'minibus';
    const PREMIUM = 'premium';
}
