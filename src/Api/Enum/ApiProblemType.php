<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class ApiProblemType extends Enum
{
    public const COMMON = 'https://gitlab.com/arkonchik/taxi/wikis/api-errors';

    public const AUTH_FAILED = 'https://gitlab.com/arkonchik/taxi/wikis/api-errors';
    public const AUTH_REQUIRED = 'auth_required';
    public const PHONE_CONFIRMATION_MISSING = 'phone_confirmation_missing';
}
