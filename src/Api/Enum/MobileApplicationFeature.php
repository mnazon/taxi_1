<?php

declare(strict_types=1);

namespace App\Api\Enum;

use App\Api\DTO\AppVersionDto;
use MyCLabs\Enum\Enum;

/**
 * @method APP_RATE_PUSH_NOTIFICATION()
 * @method APP_RATE_VIEW()
 */
class MobileApplicationFeature extends Enum
{
    public const APP_RATE_PUSH_NOTIFICATION = 1;
    public const APP_RATE_VIEW = 2;

    private static $featureToAppVersions = [
        self::APP_RATE_PUSH_NOTIFICATION => [
            [DevicePlatform::ANDROID, 9],
            [DevicePlatform::IOS, 67],
        ],
        self::APP_RATE_VIEW => [
            [DevicePlatform::IOS, 67],
            [DevicePlatform::ANDROID, 10],
        ],
    ];

    public function getSupportedAppVersions(): array
    {
        $appVersions = [];
        if (isset(self::$featureToAppVersions[$this->value])) {
            foreach (self::$featureToAppVersions[$this->value] as [$devicePlatform, $appVersion]) {
                $appVersions[] = new AppVersionDto(new DevicePlatform($devicePlatform), $appVersion);
            }
        }

        return $appVersions;
    }
}
