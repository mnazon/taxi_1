<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class CurrencySign extends Enum
{
    const UKR = '₴';
}
