<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static CAR_FOUND()
 */
class TripStatus extends Enum
{
    /**
     * @var string
     */
    protected $value;
    /** @var string */
    protected $key;

    //WaitingCarSearch Ожадается поиск машины (для предварительного заказа) https://docs.evos.in.ua/kb/dokumentatsiya-k-webapi-15499959.html#id-%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F%D0%BAWebAPI-%D0%9F%D0%BE%D0%B8%D1%81%D0%BA%D0%B3%D0%B5%D0%BE-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85(%D1%83%D0%BB%D0%B8%D1%86)%D0%BF%D0%BE%D0%BD%D0%B5%D1%81%D0%BA%D0%BE%D0%BB%D1%8C%D0%BA%D0%B8%D0%BC%D0%B1%D1%83%D0%BA%D0%B2%D0%B0%D0%BC.
    public const UNDEFINED = 0;
    public const WAITING_FOR_CAR_SEARCH = 1;
    public const SEARCH_FOR_CAR = 5;
    public const CAR_FOUND = 10;
    public const RUNNING = 15;
    public const CANCELED = 20;
    public const EXECUTED = 25;

    public function isCompleted()
    {
        return \in_array($this->getValue(), [
            self::CANCELED,
            self::EXECUTED,
        ]);
    }

    public function isCarSearching(): bool
    {
        return \in_array($this->getValue(), [
            self::WAITING_FOR_CAR_SEARCH,
            self::SEARCH_FOR_CAR,
        ]);
    }

    /**
     * Is car already founded or next steps after.
     */
    public function isBiggerOrEqualThanCarFound(): bool
    {
        return $this->getValue() >= self::CAR_FOUND;
    }

    public function isCarFound(): bool
    {
        return self::CAR_FOUND === $this->getValue();
    }

    public function isRunning(): bool
    {
        return self::RUNNING === $this->getValue();
    }

    public function isExecuted(): bool
    {
        return self::EXECUTED === $this->getValue();
    }

    public function isCanceled(): bool
    {
        return self::CANCELED === $this->getValue();
    }
}
