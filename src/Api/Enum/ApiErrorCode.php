<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self PHONE_CONFIRMATION_CODE()
 * @method static self PHONE_BANNED()
 * @method static self PHONE_CONFIRMATION_CODE_EXPIRED()
 * @method static self CONFIRMATION_CODE_SENDING_RATE_LIMIT()
 * @method static self ADDITIONAL_COST_CAR_FOUND()
 * @method static self CAR_ALREADY_FOUND()
 * @method static self AUTH_REQUIRED()
 * @method static self AUTH_FAILED()
 * @method static self BAD_REQUEST_DATA()
 * @method static self USER_DUPLICATE_PHONE()
 * @method static self WRONG_PROVIDER_RESPONSE()
 * @method static self LOCATION_NOT_SUPPORTED()
 * @method static self TRIP_CANNOT_CANCEL_TIME_IS_UP()
 */
class ApiErrorCode extends Enum
{
    public const AUTH_FAILED = 'auth_failed';
    public const AUTH_REQUIRED = 'auth_required';
    public const INTERNAL_ERROR = 'internal_error';
    public const BAD_REQUEST_DATA = 'bad_request_data';
    public const LOCATION_NOT_SUPPORTED = 'location_not_supported';
    public const TRIP_CANNOT_CANCEL_TIME_IS_UP = 'cannot_cancel_trip_time_is_up';

    public const CONFIRMATION_CODE_SENDING_RATE_LIMIT = 'confirmation_code_rate_limit';
    public const PHONE_CONFIRMATION_CODE = 'wrong_phone_confirmation_code';
    public const PHONE_BANNED = 'phone_banned';
    public const PHONE_CONFIRMATION_CODE_EXPIRED = 'phone_confirmation_code_expired';

    public const ADDITIONAL_COST_CAR_FOUND = 'car_searching_already_finished';
    public const CAR_ALREADY_FOUND = 'car_already_found';

    public const USER_DUPLICATE_PHONE = 'user_duplicate_phone';

    public const WRONG_PROVIDER_RESPONSE = 'wrong_provider_response';
    /**
     * @var string
     */
    private $title;

    private static $titleToCode = [
        self::PHONE_CONFIRMATION_CODE => 'Wrong confirmation code',
        self::AUTH_FAILED => 'Bad credentials',
        self::AUTH_REQUIRED => 'Auth required',
        self::BAD_REQUEST_DATA => 'Bad request data',
        self::CAR_ALREADY_FOUND => 'Car already found',
    ];

    public function getUrl(): string
    {
        return ApiProblemType::COMMON;
    }

    public function getTitle(): string
    {
        if (!empty($this->title)) {
            return $this->title;
        }

        return self::$titleToCode[$this->getValue()] ?? 'Validation error';
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
