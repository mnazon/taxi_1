<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class TaxiAdditionalService extends Enum
{
    public const BAGGAGE = 'baggage';
    public const ANIMAL = 'animal';
    public const CONDITIONER = 'conditioner';
    public const COURIER_DELIVERY = 'courier';
}
