<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method self ANDROID()
 * @method self IOS()
 */
class DevicePlatform extends Enum
{
    const ANDROID = 1;
    const IOS = 2;

    public static $stringsToInt = [
        'android' => self::ANDROID,
        'ios' => self::IOS,
    ];

    public static function validValues()
    {
        $validValues = array_flip(self::$stringsToInt);

        return $validValues;
    }

    public static function createFromLiteral(string $literal): self
    {
        $value = self::$stringsToInt[$literal];

        return new self($value);
    }

    public function isAndroid(): bool
    {
        return self::ANDROID === $this->getValue();
    }

    public function isIos(): bool
    {
        return self::IOS === $this->getValue();
    }
}
