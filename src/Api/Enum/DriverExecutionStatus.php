<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class DriverExecutionStatus extends Enum
{
    const STANDART = 'standart';
    const WAGON = 'wagon';
    const MINIBUS = 'minibus';
    const PREMIUM = 'premium';
}
