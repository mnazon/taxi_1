<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;
use Swagger\Annotations as SWG;

/**
 * @method self STUDENT_TAXI()
 */
class TaxiProvider extends Enum
{
    /**
     * @var string
     * @SWG\Property(
     *      enum="['1', '2', '3']",
     * )
     */
    protected $value;
    /** @var string */
    protected $key;

    public const STUDENT_TAXI = '1';
}
