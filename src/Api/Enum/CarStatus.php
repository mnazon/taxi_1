<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class CarStatus extends Enum
{
    public const FREE = 1;
    public const BUSY = 2;
    public const CLOSED = 3;

    public static $stringsToInt = [
        'Busy' => self::BUSY,
        'Free' => self::FREE,
        'Closed' => self::CLOSED,
    ];

    public static function createFromLiteral(string $literal): self
    {
        $value = self::$stringsToInt[$literal];

        return new self($value);
    }
}
