<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method APP_RATE_PUSH_NOTIFICATION()
 * @method APP_RATE_VIEW()
 */
class UserOptionEnum extends Enum
{
    public const APP_RATE_PUSH_NOTIFICATION = 1;
    public const APP_RATE_VIEW = 2;

    public static function getAvailableOptions()
    {
        return [
            self::APP_RATE_VIEW,
        ];
    }

    public static function fromKey(string $key): ?self
    {
        if (self::isValidKey($key)) {
            return self::$key();
        }

        return null;
    }
}
