<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class TaxiPlatform extends Enum
{
    public const EVOS = 1;
    public const UBER = 2;
    public const INTERNAL = 3;
}
