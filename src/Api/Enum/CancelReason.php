<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class CancelReason extends Enum
{
    public const SUCCESSFUL_EXECUTED = 0;
    public const RIDER_CANCEL = 1;
    public const RIDER_CANCEL_FROM_APPLICATION = 3;
    public const DRIVER_CANCEL = 5;
    public const DISPATCHER_CANCEL = 10;
    public const NO_CARS_AVAILABLE = 15;
    public const COST_CALCULATION = 20;
    public const DID_NOT_ARRANGE_TARIFF = 25;
    public const DID_NOT_ARRANGE_TIME = 30;
    public const THROW_ON_COZ = 35;

//    -1 => CancelReason::class, //Выполняется
//    0, //Выполнен
//    1, //Отказ клиента
//    2, //Отказ водителя
//    3, //Отмена по вине диспетчера
//    4, //Нет машины
//    5, //Просчет
//    6, //Отказ клиента не устроил тариф
//    7, //Отказ клиента не устроило время
//    8, //Перекинут на СОЗ (выполнен)
//    9, //'Перекинут на СОЗ (выполнен)'
}
