<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class EstimateSortField extends Enum
{
    public const SMART = 'smart';
    public const PRICE = 'price';
    public const PERCENT = 'percent';
    public const RATING = 'rating';
}
