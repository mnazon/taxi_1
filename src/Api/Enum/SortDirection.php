<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class SortDirection extends Enum
{
    public const ASC = 'asc';
    public const DESC = 'desc';
}
