<?php

namespace App\DataFixtures;

use App\Api\DataFixtures\UserFixtures;
use App\Api\DTO\Vehicle;
use App\Api\Entity\InternalProviderTrip;
use App\Api\Entity\TaxiProvider;
use App\Api\Entity\TaxiProviderAdditionalData;
use App\Api\Entity\TaxiProviderAuth;
use App\Api\Entity\Trip;
use App\Api\Entity\TripRequest;
use App\Api\Enum\TaxiPlatform;
use App\Api\Enum\TripStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture implements DependentFixtureInterface
{
    public const EVOS_TAXI_PROVIDER_REF = 'evos-taxi-provider-ref';
    public const INTERNAL_TAXI_PROVIDER_REF = 'internal-taxi-provider-ref';
    public const TAXI_PROVIDER_NAME = 'student_taxi';
    public const TRIP_EXTERNAL_ID = '0e9214ab5f67447cbbe128a01a25d5ec';
    //todo: rename to EVOS_TRIP_REF
    public const TRIP_REF = 'evos-trip-ref';
    public const INTERNAL_TRIP_REF = 'internal-trip-ref';

    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var int
     */
    private $evosTaxiProviderId;
    /**
     * @var Trip
     */
    private $evosTrip;

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        //evos
        $evosTaxiProvider = new TaxiProvider();
        $evosTaxiProvider->setName('Студент такси');
        $evosTaxiProvider->setCreatedAt(new \DateTimeImmutable());
        $evosTaxiProvider->setEnabled(true);
        $evosTaxiProvider->setTaxiPlatform(TaxiPlatform::EVOS);
        $evosTaxiProvider->addAuthorizer($this->createAuthorizer());
        $evosTaxiProvider->setAdditionalData($this->createAdditionalData());
        $evosTaxiProvider->setSlug(self::TAXI_PROVIDER_NAME);
        $manager->persist($evosTaxiProvider);
        $this->setReference(self::EVOS_TAXI_PROVIDER_REF, $evosTaxiProvider);

        $evosTrip = $this->createTrip($evosTaxiProvider);
        $this->setReference(self::TRIP_REF, $evosTrip);

        $this->loadInternalTrip();

        $this->evosTaxiProviderId = $evosTaxiProvider->getId();
    }

    private function loadInternalTrip()
    {
        $tripRequest = new TripRequest();
        $tripRequest->setUserPhone('1111111')
            ->setUserFullName('full_name')
            ->setCarType('car_type')
            ->setOptions(['conditioner'])
            ->setReservedAt(new \DateTimeImmutable())
            ->setStartRouteEntranceFrom(1)
            ->setReservedAt(new \DateTimeImmutable());

        $this->manager->persist($tripRequest);
        //internal
        $internalTaxiProvider = new TaxiProvider();
        $internalTaxiProvider->setName('Внутреннее такси');
        $internalTaxiProvider->setCreatedAt(new \DateTimeImmutable());
        $internalTaxiProvider->setEnabled(true);
        $internalTaxiProvider->setTaxiPlatform(TaxiPlatform::INTERNAL);
        $internalTaxiProvider->addAuthorizer($this->createAuthorizer());
        $internalTaxiProvider->setAdditionalData($this->createAdditionalData());
        $internalTaxiProvider->setSlug(self::TAXI_PROVIDER_NAME);
        $this->manager->persist($internalTaxiProvider);
        $this->setReference(self::INTERNAL_TAXI_PROVIDER_REF, $internalTaxiProvider);

        $internalTrip = $this->createTrip($internalTaxiProvider);
        $internalTrip->setStatus(new TripStatus(TripStatus::WAITING_FOR_CAR_SEARCH));
        $this->setReference(self::INTERNAL_TRIP_REF, $internalTrip);

        $internalProviderTrip = new InternalProviderTrip();
        $internalProviderTrip->setCost(229);
        $internalProviderTrip->setCurrency('R');
        $internalProviderTrip->setStatus(new TripStatus(TripStatus::CAR_FOUND));

        $this->manager->persist($internalProviderTrip);
        $this->manager->flush();

        $internalTrip->setExternalId((string) ($internalProviderTrip->getId()));
        $internalTrip->setRequest($tripRequest);
        $this->manager->persist($internalTrip);
        $this->manager->flush();
    }

    //todo: rename to getEvosTaxiProviderId

    public function getTaxiProviderId(): int
    {
        return $this->evosTaxiProviderId;
    }

    public function getEvosTrip(): Trip
    {
        return $this->evosTrip;
    }

    /**
     * @expectedException
     *
     * @param $taxiProvider
     */
    private function createTrip($taxiProvider): Trip
    {
        $now = new \DateTimeImmutable();
        /** @var \App\Api\Entity\User $user */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);
        $trip = new Trip();
        $trip->setExternalId(self::TRIP_EXTERNAL_ID)
            ->setTaxiProvider($taxiProvider)
            ->setCreatedAt(new \DateTimeImmutable())
            ->setStatus(new TripStatus(TripStatus::SEARCH_FOR_CAR))
            ->setCurrency('')
            ->setVehicle(new Vehicle('1111', 'makeAndModel', 'color'))
            ->setUser($user)
            ->setStartSearchingCarAt($now)
            ->setCost(123);

        $this->manager->persist($trip);
        $this->evosTrip = $trip;

        return $trip;
    }

    private function createAuthorizer(): TaxiProviderAuth
    {
        $authorizer = new TaxiProviderAuth();
        $data = [
            'username' => 'user',
            'password' => 'pass',
            'baseUrl' => 'url',
            'taxiColumnId' => 1,
        ];
        $authorizer->setAuth($data);
        $authorizer->setIsActive(true);

        return $authorizer;
    }

    private function createAdditionalData(): TaxiProviderAdditionalData
    {
        return new TaxiProviderAdditionalData();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
