<?php

namespace App\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\DBAL\Types\Type;

abstract class AbstractSetType extends Type
{
    /**
     * @var string Name of this type
     */
    protected $name = '';

    /**
     * @var array Map of available SET type, key and label
     * @static
     */
    protected static $choices = [];

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!\is_array($value) || \count($value) <= 0) {
            return null;
        }

        $diff = array_diff($value, $this->getValues());
        if (\count($diff) > 0) {
            throw new \InvalidArgumentException(sprintf('Invalid value "%s". It is not defined in "%s::$choices"', implode(',', $diff), \get_class($this)));
        }

        return implode(',', $value);
    }

    /**
     * @param string|null $value
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): array
    {
        if (null === $value || '' === $value) {
            return [];
        }
        if (false === strpos($value, ',')) {
            return [$value];
        }

        return explode(',', $value);
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        $values = implode(', ', array_map(function ($value) {
            return "'{$value}'";
        },
                                  $this->getValues()
                              )
        );

        if (!$platform instanceof MySqlPlatform) {
            return $platform->getClobTypeDeclarationSQL($fieldDeclaration);
        }

        return sprintf('SET(%s)', $values);
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name ?: (new \ReflectionClass(\get_class($this)))->getShortName();
    }

    /**
     * Get choices Label for the Set form field type.
     *
     * @return array Values for the SET field
     */
    public static function getChoices(): array
    {
        return static::$choices;
    }

    /**
     * Get values for the SET field.
     *
     * @return array Values for the SET field
     */
    public static function getValues(): array
    {
        return array_keys(static::getChoices());
    }
}
