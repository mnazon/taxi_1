<?php
declare(strict_types=1);

namespace Deployer;


require_once 'recipe/symfony4.php';
require_once 'recipe/rsync.php';

//https://deployer.org/docs/configuration
//dep deploy --tag="v0.1"
//dep deploy --revision="5daefb59edbaa75"

set('shared_files', ['.env.local', 'config/firebase_service_account.json']);
set('shared_dirs', ['var/log', 'var/sessions', 'public/upload']);
set('writable_dirs', ['var', 'var/cache', 'public/upload']);
set('writable_mode', 'chown');
set('deploy_path', '/home/taximer/www/api.taximer.com.ua');
set('rsync_src', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);
set('rsync_dest','{{release_path}}');

host('production')
    ->hostname(...['165.22.66.157'])
    ->user('taximer')
    ->stage('master')
    ->forwardAgent()
    ->set('http_user', 'taximer')
;

host('staging')
    ->hostname(...['104.248.135.40'])
    ->user('root')
    ->stage('staging')
    ->forwardAgent()
    ->set('http_user', 'www-data')
;

desc('Deploy project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    //'deploy:vendors',
    'rsync',
    'deploy:shared',
    'deploy:writable',
    'database:migrate',
    'deploy:cache:clear',
    'deploy:cache:warmup',
    'deploy:writable',
    'deploy:symlink',
    'reload:php-fpm',
    'reload:supervisorctl',
    'deploy:unlock',
    'cleanup',
]);

desc('Reload PHP-FPM service');
task('reload:php-fpm', function() {
    run('sudo systemctl reload php-fpm');
});

desc('Reload supervisor configuration and processes');
task('reload:supervisorctl', function() {
    //http://www.onurguzel.com/supervisord-restarting-and-reloading/
    //https://github.com/Supervisor/supervisor/issues/720
    run('sudo supervisorctl reload');
});

after('rollback', 'reload:php-fpm');
after('rollback', 'reload:supervisorctl');
///https://medium.com/@nickdenardis/zero-downtime-local-build-laravel-5-deploys-with-deployer-a152f0a1411f
/// rsync to server

//\Deployer\task('config:ssh:agent', function() {
//    if (empty(getenv('SSH_AUTH_SOCK'))) {
//        echo  "111";
//        return;
//    }
//    echo  "222";
//    /** @var ServerInterface */
//    $server = \Deployer\Task\Context::get()->getServer();
//    /** @var Configuration */
//    $config = $server->getConfiguration();
//    $config->setAuthenticationMethod(\Deployer\Configuration\Configuration::AUTH_BY_AGENT);
//});
